
# OPTIONAL. SSL CERTIFICATES EXPIRE IN 3-6 MONTH WITHOUT RENEW

docker run --rm --name certbot \
            -v "/var/www/cyprustourism/cyprus/public:/var/www/html" \
            -v "/var/www/cyprustourism/www/public:/usr/share/nginx/html/public" \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot renew

#docker-compose restart app