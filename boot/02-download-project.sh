#!/bin/bash

cd /var/www

git clone ssh://git@bitbucket.org/gram7gram/cyprustourism --depth=1

cp docker-compose.yml.dist docker-compose.yml

cp cyprus/.env.dist cyprus/.env

cp cyprus/env/Dockerfile.dist cyprus/env/Dockerfile