#!/bin/bash

source cyprus/.env

# Build containers
docker-compose build

# Start containers
docker-compose up -d

# Install dependencies
./php ./composer.phar install

# Create database if not exists
./php bin/console doctrine:database:create --if-not-exists

# Update project
./update

# Create PDF invoice directory
docker-compose exec app mkdir -p /var/www/html/public/invoices

docker-compose exec app chmod 777 -R /var/www/html/public/invoices