
docker run -it --rm --name certbot \
            -v "/var/www/cyprustourism/www/public:/usr/share/nginx/html/public" \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot certonly --webroot --webroot-path /usr/share/nginx/html/public \
            -d vacbids.com \
            -m gram7gram.work@gmail.com

docker run -it --rm --name certbot \
            -v "/var/www/cyprustourism/cyprus/public:/var/www/html" \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot certonly --webroot --webroot-path /var/www/html \
            -d cyprus.vacbids.com \
            -m gram7gram.work@gmail.com

# Add renew script to CRON

cat $PROJECT_DIR/boot/cron/ssl-renew.conf | crontab -u root -
