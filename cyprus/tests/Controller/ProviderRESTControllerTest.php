<?php

namespace App\Tests\Controller;

use App\Entity\InquiryType;
use App\Entity\ProviderStatus;
use App\Service\ProviderService;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\ProviderRESTController
 */
class ProviderRESTControllerTest extends WebTestCase
{

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/providers", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/providers", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $client->request('GET', "/api/v1/providers", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_post()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/providers", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ], json_encode([
            'type' => InquiryType::CAR_RENT,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => md5(uniqid()),
                'password' => '12345',
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertEquals(ProviderStatus::CREATED, $content['status'], 'Invalid status');
        $this->assertTrue(isset($content['user']), 'Missing user');
        $this->assertTrue(isset($content['user']['isActive']), 'Missing user.isActive');
        $this->assertFalse($content['user']['isActive'], 'Invalid user.isActive');
        $this->assertTrue(isset($content['user']['isEmailVerified']), 'Missing user.isEmailVerified');
        $this->assertFalse($content['user']['isEmailVerified'], 'Invalid user.isEmailVerified');
        $this->assertTrue(isset($content['user']['isPhoneVerified']), 'Missing user.isPhoneVerified');
        $this->assertFalse($content['user']['isPhoneVerified'], 'Invalid user.isPhoneVerified');
    }

    /**
     * @small
     */
    public function test_put_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/providers/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('PUT', "/api/v1/providers/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_put_provider_requests_not_himself_forbidden()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(ProviderService::class);

        $provider1 = $service->create([
            'type' => InquiryType::BOAT_RENT,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => md5(uniqid()),
                'password' => '12345',
            ]
        ]);

        $provider2 = $service->create([
            'type' => InquiryType::ACCOMMODATION_RENT,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => md5(uniqid()),
                'password' => '12345',
            ]
        ]);

        $client = $this->createAuthorizedClient($provider1->getUser()->getEmail());

        $client->request('PUT', "/api/v1/providers/" . $provider2->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_put()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(ProviderService::class);

        $provider1 = $service->create([
            'type' => InquiryType::BOAT_RENT,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => rand(10000, 20000) . rand(10000, 20000),
                'password' => '12345',
            ]
        ]);

        $client = $this->createAuthorizedClient($provider1->getUser()->getEmail());

        $newSite = md5(uniqid());
        $newName = md5(uniqid());
        $newPhone = rand(10000, 20000) . rand(10000, 20000);
        $newEmail = md5(uniqid()) . '@mail.com';
        $newCompanyName = md5(uniqid());
        $newCompanyId = md5(uniqid());
        $newAddress = md5(uniqid());

        $client->request('PUT', "/api/v1/providers/" . $provider1->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ], json_encode([
            'website' => $newSite,
            'companyName' => $newCompanyName,
            'companyId' => $newCompanyId,
            'address' => $newAddress,
            'user' => [
                'name' => $newName,
                'phone' => $newPhone,
                'email' => $newEmail,
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertEquals($newSite, $content['website']);
        $this->assertEquals($newCompanyName, $content['companyName']);
        $this->assertEquals($newCompanyId, $content['companyId']);
        $this->assertEquals($newAddress, $content['address']);
        $this->assertEquals($newName, $content['user']['name']);
        $this->assertEquals($newPhone, $content['user']['phone']);
        $this->assertEquals($newEmail, $content['user']['email']);

    }

    /**
     * @small
     */
    public function test_get_me_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_me_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_get_me()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(ProviderService::class);

        $provider1 = $service->create([
            'type' => InquiryType::TRANSFER,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => rand(10000, 20000),
                'password' => '12345',
            ]
        ]);

        $client = $this->createAuthorizedClient($provider1->getUser()->getEmail());


        $client->request('GET', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_me_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_me_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('PUT', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_put_me()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(ProviderService::class);

        $provider1 = $service->create([
            'type' => InquiryType::BOAT_RENT,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => rand(10000, 20000) . rand(10000, 20000),
                'password' => '12345',
            ]
        ]);

        $client = $this->createAuthorizedClient($provider1->getUser()->getEmail());

        $newSite = md5(uniqid());
        $newName = md5(uniqid());
        $newPhone = rand(10000, 20000) . rand(10000, 20000);
        $newEmail = md5(uniqid()) . '@mail.com';
        $newCompanyName = md5(uniqid());
        $newCompanyId = md5(uniqid());
        $newAddress = md5(uniqid());

        $client->request('PUT', "/api/v1/providers/me", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ], json_encode([
            'website' => $newSite,
            'companyName' => $newCompanyName,
            'companyId' => $newCompanyId,
            'address' => $newAddress,
            'user' => [
                'name' => $newName,
                'phone' => $newPhone,
                'email' => $newEmail,
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertEquals($newSite, $content['website']);
        $this->assertEquals($newCompanyName, $content['companyName']);
        $this->assertEquals($newCompanyId, $content['companyId']);
        $this->assertEquals($newAddress, $content['address']);
        $this->assertEquals($newName, $content['user']['name']);
        $this->assertEquals($newPhone, $content['user']['phone']);
        $this->assertEquals($newEmail, $content['user']['email']);

    }
}
