<?php

namespace App\Tests\Controller;

use App\Entity\ReviewStatus;
use App\Service\InquiryService;
use App\Service\QuoteService;
use App\Service\ReviewService;
use App\Service\SelectedQuoteService;
use App\Tests\Classes\InquiryCreator;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\UserCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\ReviewRESTController
 */
class ReviewRESTControllerTest extends WebTestCase
{

    use ProviderCreator;
    use InquiryCreator;
    use UserCreator;

    /**
     * @small
     */
    public function test_gets_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/reviews", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/reviews/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/reviews/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/selected-quotes/1/reviews", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('PUT', "/api/v1/reviews/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('PUT', "/api/v1/reviews/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_post_user()
    {
        $client = $this->createAuthorizedAdmin();

        $user = $this->createUser($client->getContainer());

        $provider = $this->createProvider($client->getContainer());

        //Create inquiry

        $inquiry = $this->createInquiry($client->getContainer(), $user);

        //Create quote by provider

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote = $this->createQuote($client->getContainer(), $inquiry);

        //Select quote by user

        $client = $this->createAuthorizedClient($user->getUsername());

        $quoteService = $client->getContainer()->get(QuoteService::class);
        $inquiryService = $client->getContainer()->get(InquiryService::class);
        $selectedQuoteService = $client->getContainer()->get(SelectedQuoteService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);
        $quote = $quoteService->findOneByFilter(['id' => $quote->getId()]);

        $selectedQuote = $selectedQuoteService->create($inquiry, $quote);

        $client->request('POST', "/api/v1/selected-quotes/" . $selectedQuote->getId() . "/reviews", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'text' => md5(uniqid()),
            'rating' => rand(1, 5),
            'isPublishAllowed' => true
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertEquals(ReviewStatus::CREATED, $content['status'], 'Invalid status');
    }

    /**
     * @large
     */
    public function test_put_review_is_approved_by_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $user = $this->createUser($client->getContainer());

        $provider = $this->createProvider($client->getContainer());


        //Create inquiry

        $client = $this->createUnauthorizedClient();

        $inquiry = $this->createInquiry($client->getContainer(), $user);

        $this->assertEquals($inquiry->getCreator()->getId(), $user->getId(), 'Invalid inquiry creator');


        //Create quote by provider

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote = $this->createQuote($client->getContainer(), $inquiry);


        //Select quote by user

        $client = $this->createAuthorizedClient($user->getUsername());

        $quoteService = $client->getContainer()->get(QuoteService::class);
        $inquiryService = $client->getContainer()->get(InquiryService::class);
        $selectedQuoteService = $client->getContainer()->get(SelectedQuoteService::class);
        $reviewService = $client->getContainer()->get(ReviewService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);
        $quote = $quoteService->findOneByFilter(['id' => $quote->getId()]);

        $selectedQuote = $selectedQuoteService->create($inquiry, $quote);


        //Create review by user

        $review = $reviewService->create($selectedQuote, [
            'text' => md5(uniqid()),
            'rating' => rand(1, 5),
            'isPublishAllowed' => true
        ]);


        //Approve review by admin

        $client = $this->createAuthorizedAdmin();

        $client->request('PUT', "/api/v1/reviews/" . $review->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'status' => ReviewStatus::APPROVED
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertEquals(ReviewStatus::APPROVED, $content['status'], 'Invalid status');
    }

    /**
     * @large
     */
    public function test_put_review_is_rejected_by_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $user = $this->createUser($client->getContainer());

        $provider = $this->createProvider($client->getContainer());


        //Create inquiry

        $client = $this->createUnauthorizedClient();

        $inquiry = $this->createInquiry($client->getContainer(), $user);

        $this->assertEquals($inquiry->getCreator()->getId(), $user->getId(), 'Invalid inquiry creator');


        //Create quote by provider

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote = $this->createQuote($client->getContainer(), $inquiry);


        //Select quote by user

        $client = $this->createAuthorizedClient($user->getUsername());

        $quoteService = $client->getContainer()->get(QuoteService::class);
        $inquiryService = $client->getContainer()->get(InquiryService::class);
        $selectedQuoteService = $client->getContainer()->get(SelectedQuoteService::class);
        $reviewService = $client->getContainer()->get(ReviewService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);
        $quote = $quoteService->findOneByFilter(['id' => $quote->getId()]);

        $selectedQuote = $selectedQuoteService->create($inquiry, $quote);


        //Create review by user

        $review = $reviewService->create($selectedQuote, [
            'text' => md5(uniqid()),
            'rating' => rand(1, 5),
            'isPublishAllowed' => true
        ]);


        //Reject review by admin

        $client = $this->createAuthorizedAdmin();

        $client->request('PUT', "/api/v1/reviews/" . $review->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'status' => ReviewStatus::REJECTED
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['deletedAt']), 'Missing deletedAt');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertEquals(ReviewStatus::REJECTED, $content['status'], 'Invalid status');
    }

    /**
     * @large
     */
    public function test_review_can_be_created_once_per_provider_in_inquiry_by_user()
    {
        $client = $this->createAuthorizedAdmin();

        $user = $this->createUser($client->getContainer());

        $provider1 = $this->createProvider($client->getContainer());

        $provider2 = $this->createProvider($client->getContainer());


        // STEP 1: Create inquiry

        $client = $this->createUnauthorizedClient();

        $inquiry = $this->createInquiry($client->getContainer(), $user);

        $this->assertEquals($inquiry->getCreator()->getId(), $user->getId(), 'Invalid inquiry creator');


        // STEP 2: Create quote by provider1

        $client = $this->createAuthorizedClient($provider1->getUser()->getUsername());

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote1 = $this->createQuote($client->getContainer(), $inquiry);


        // STEP 3: Create quote by provider2

        $client = $this->createAuthorizedClient($provider2->getUser()->getUsername());

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote2 = $this->createQuote($client->getContainer(), $inquiry);


        // STEP 4: Create quote by provider2

        $quote3 = $this->createQuote($client->getContainer(), $inquiry);


        // STEP 5: Select provider1 quote1 by user

        $client = $this->createAuthorizedClient($user->getUsername());

        $quoteService = $client->getContainer()->get(QuoteService::class);
        $inquiryService = $client->getContainer()->get(InquiryService::class);
        $selectedQuoteService = $client->getContainer()->get(SelectedQuoteService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);
        $quote1 = $quoteService->findOneByFilter(['id' => $quote1->getId()]);
        $quote2 = $quoteService->findOneByFilter(['id' => $quote2->getId()]);
        $quote3 = $quoteService->findOneByFilter(['id' => $quote3->getId()]);

        $selectedQuote1 = $selectedQuoteService->create($inquiry, $quote1);

        // STEP 6: Select provider2 quote2 by user

        $selectedQuote2 = $selectedQuoteService->create($inquiry, $quote2);

        // STEP 7: Select provider2 quote3 by user

        $selectedQuote3 = $selectedQuoteService->create($inquiry, $quote3);

        // STEP 7: Open provider1 review by user

        $client->request('GET', "/selected-quotes/" . $selectedQuote1->getId() . "/review");

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), 'User should open review form in browser');


        // STEP 8: Create provider1 review by user

        $client->xmlHttpRequest('POST', "/api/v1/selected-quotes/" . $selectedQuote1->getId() . "/reviews", [], [], [], json_encode([
            'text' => md5(uniqid()),
            'rating' => rand(1, 5),
            'isPublishAllowed' => true
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());


        // STEP 9: Open provider2 review by user

        $client->request('GET', "/selected-quotes/" . $selectedQuote2->getId() . "/review");

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), 'User should open review form in browser');


        // STEP 10: Create provider2 review by user

        $client->xmlHttpRequest('POST', "/api/v1/selected-quotes/" . $selectedQuote2->getId() . "/reviews", [], [], [], json_encode([
            'text' => md5(uniqid()),
            'rating' => rand(1, 5),
            'isPublishAllowed' => true
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());


        // STEP 11: User should not be bale to access review form again for provider1, because he already reviewed provider

        $client->request('GET', "/selected-quotes/" . $selectedQuote1->getId() . "/review");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection(), 'User should not open review form in browser');


        // STEP 12: User should not be bale to access review form again for provider2, because he already reviewed provider

        $client->request('GET', "/selected-quotes/" . $selectedQuote2->getId() . "/review");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection(), 'User should not open review form in browser');


        // STEP 13: User should not be able to access review form again for provider2, because THERE IS ALREADY A REVIEW FOR THIS PROVIDER

        $client->request('GET', "/selected-quotes/" . $selectedQuote3->getId() . "/review");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection(), 'User should not open review form in browser');

    }

}