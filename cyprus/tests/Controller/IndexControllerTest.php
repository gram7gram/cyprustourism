<?php

namespace App\Tests\Controller;

use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\IndexController
 */
class IndexControllerTest extends WebTestCase
{

    public function provider()
    {
        return [
            ['/'],
            ['/car-rent'],
            ['/boat-rent'],
            ['/accommodation-rent'],
            ['/transfer'],
            ['/reset-password'],
            ['/privacy'],
            ['/terms'],
            ['/login'],
            ['/register'],
            ['/register-provider'],
            ['/how-it-works'],
            ['/faq'],
            ['/provider'],
            ['/contact-us'],
        ];
    }

    /**
     * @dataProvider provider
     *
     * @small
     * @param $page
     */
    public function test_page_opens($page)
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

}