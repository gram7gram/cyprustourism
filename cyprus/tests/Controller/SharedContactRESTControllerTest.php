<?php

namespace App\Tests\Controller;

use App\Entity\InquiryType;
use App\Service\ProviderService;
use App\Service\UserService;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\SharedContactRESTController
 */
class SharedContactRESTControllerTest extends WebTestCase
{

    /**
     * @small
     */
    public function test_gets_for_user_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/users/1/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_for_provider_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/providers/1/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_for_user()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $client->request('GET', "/api/v1/users/" . $user1->getId() . "/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_for_provider()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(ProviderService::class);

        $provider = $service->create([
            'type' => InquiryType::TRANSFER,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => md5(uniqid()),
                'password' => '12345',
            ]
        ]);

        $client = $this->createAuthorizedClient($provider->getUser()->getEmail());

        $client->request('GET', "/api/v1/providers/" . $provider->getId() . "/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_me_as_provider()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('GET', "/api/v1/me/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_me_as_user()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/me/shared-contacts", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

}