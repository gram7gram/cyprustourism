<?php

namespace App\Tests\Controller;

use App\Service\InquiryService;
use App\Service\QuoteService;
use App\Service\SelectedQuoteService;
use App\Service\SharedContactService;
use App\Tests\Classes\InquiryCreator;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\UserCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\MembersAreaController
 */
class MembersAreaControllerTest extends WebTestCase
{

    use InquiryCreator;
    use UserCreator;
    use ProviderCreator;

    public function adminProvider()
    {
        return [
            ['/users'],
            ['/providers'],
            ['/categories'],
            ['/categories/new'],
            ['/reviews'],
            ['/inquiries'],
            ['/profile'],
        ];
    }

    public function providerProvider()
    {
        return [
            ['/billing'],
            ['/inquiries'],
            ['/profile'],
        ];
    }

    public function userProvider()
    {
        return [
            ['/inquiries'],
            ['/profile'],
        ];
    }

    /**
     * @dataProvider userProvider
     *
     * @small
     * @param $page
     */
    public function test_page_opens_for_user($page)
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());

        $client = $this->createAuthorizedUser();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider adminProvider
     *
     * @small
     * @param $page
     */
    public function test_page_opens_for_admin($page)
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());

        $client = $this->createAuthorizedAdmin();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider providerProvider
     *
     * @small
     *
     * @param $page
     */
    public function test_page_opens_for_provider($page)
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());

        $client = $this->createAuthorizedProvider();

        $client->request('GET', $page);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_provider_opens_by_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createProvider($client->getContainer());

        $client->request('GET', '/providers/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_provider_does_not_open_if_user()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createProvider($client->getContainer());

        $client = $this->createAuthorizedUser();

        $client->request('GET', '/providers/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_provider_does_not_open_if_provider()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createProvider($client->getContainer());

        $client = $this->createAuthorizedProvider();

        $client->request('GET', '/providers/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_provider_does_not_open_if_unauthorized()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createProvider($client->getContainer());

        $client = $this->createUnauthorizedClient();

        $client->request('GET', '/providers/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_user_opens_by_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createUser($client->getContainer());

        $client->request('GET', '/users/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_user_does_not_open_if_user()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createUser($client->getContainer());

        $client = $this->createAuthorizedUser();

        $client->request('GET', '/users/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_user_does_not_open_if_provider()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createUser($client->getContainer());

        $client = $this->createAuthorizedProvider();

        $client->request('GET', '/users/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_user_does_not_open_if_unauthorized()
    {
        $client = $this->createAuthorizedAdmin();

        $entity = $this->createUser($client->getContainer());

        $client = $this->createUnauthorizedClient();

        $client->request('GET', '/users/' . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_category_opens_by_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $category = $this->createCategory($client->getContainer());

        $client->request('GET', '/categories/' . $category->getId());

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_category_does_not_open_if_user()
    {
        $client = $this->createAuthorizedUser();

        $category = $this->createCategory($client->getContainer());

        $client->request('GET', '/categories/' . $category->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_category_does_not_open_if_provider()
    {
        $client = $this->createAuthorizedProvider();

        $category = $this->createCategory($client->getContainer());

        $client->request('GET', '/categories/' . $category->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function test_category_does_not_open_if_unauthorized()
    {
        $client = $this->createAuthorizedProvider();

        $category = $this->createCategory($client->getContainer());

        $client->request('GET', '/categories/' . $category->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }

}