<?php

namespace App\Tests\Controller;

use App\Entity\CategoryGroup;
use App\Entity\InquiryType;
use App\Service\CategoryService;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\CategoryRESTController
 */
class CategoryRESTControllerTest extends WebTestCase
{

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('GET', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('DELETE', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('DELETE', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('DELETE', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('PUT', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('PUT', "/api/v1/categories/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/categories", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('POST', "/api/v1/categories", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('POST', "/api/v1/categories", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_admin()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/categories", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_admin()
    {
        $client = $this->createAuthorizedAdmin();
        $service = $client->getContainer()->get(CategoryService::class);

        $category = $service->create([
            'name' => md5(uniqid()),
            'group' => CategoryGroup::LOCATION,
            'locale' => 'en',
            'type' => InquiryType::TRANSFER
        ]);

        $client->request('GET', "/api/v1/categories/" . $category->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $client->request('POST', "/api/v1/categories", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'name' => md5(uniqid()),
            'group' => CategoryGroup::LOCATION,
            'ordering' => 1,
            'locale' => 'en',
            'type' => InquiryType::TRANSFER
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_admin()
    {
        $client = $this->createAuthorizedAdmin();
        $service = $client->getContainer()->get(CategoryService::class);

        $category = $service->create([
            'name' => md5(uniqid()),
            'group' => CategoryGroup::LOCATION,
            'locale' => 'en',
            'ordering' => 2,
            'type' => InquiryType::TRANSFER
        ]);

        $client->request('PUT', "/api/v1/categories/" . $category->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'name' => md5(uniqid()),
            'ordering' => 1,
            'group' => CategoryGroup::EXTRAS,
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_admin()
    {
        $client = $this->createAuthorizedAdmin();
        $service = $client->getContainer()->get(CategoryService::class);

        $category = $service->create([
            'name' => md5(uniqid()),
            'group' => CategoryGroup::BOAT_TYPE,
            'locale' => 'en',
            'type' => InquiryType::BOAT_RENT
        ]);

        $client->request('DELETE', "/api/v1/categories/" . $category->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}