<?php

namespace App\Tests\Controller;

use App\Entity\CategoryGroup;
use App\Entity\InquiryStatus;
use App\Entity\InquirySubtype;
use App\Entity\InquiryType;
use App\Entity\PaymentType;
use App\Service\UserService;
use App\Tests\Classes\InquiryCreator;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\UserCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\InquiryRESTController
 */
class InquiryRESTControllerTest extends WebTestCase
{

    use InquiryCreator;
    use UserCreator;
    use ProviderCreator;

    /**
     * @small
     */
    public function test_gets_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/inquiries/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('DELETE', "/api/v1/inquiries/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('DELETE', "/api/v1/inquiries/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_delete_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('DELETE', "/api/v1/inquiries/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/inquiries/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_user()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_provider()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('GET', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $inquiry = $this->createInquiry($client->getContainer());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_user()
    {
        $client = $this->createUnauthorizedClient();

        $user = $this->createUser($client->getContainer());
        $inquiry = $this->createInquiry($client->getContainer(), $user);

        $client = $this->createAuthorizedClient($user->getUsername());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertTrue(isset($content['selectedQuotes']), 'Missing selectedQuotes');
        $this->assertTrue(isset($content['quotes']), 'Missing quotes');
    }

    /**
     * @small
     */
    public function test_get_user_cannot_access_inquiry_he_did_not_create()
    {
        $client = $this->createUnauthorizedClient();

        $user1 = $this->createUser($client->getContainer());
        $user2 = $this->createUser($client->getContainer());

        $inquiry = $this->createInquiry($client->getContainer(), $user2);

        $client = $this->createAuthorizedClient($user1->getUsername());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_provider_can_access_open_inquiry_with_same_type()
    {
        $client = $this->createUnauthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();

        $provider = $this->createProvider($client->getContainer());

        $inquiry = $this->createInquiry($client->getContainer());

        $inquiry->setStatus(InquiryStatus::OPEN);
        $inquiry->setType(InquiryType::CAR_RENT);
        $provider->setType(InquiryType::CAR_RENT);

        $em->persist($inquiry);
        $em->persist($provider);

        $em->flush();

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertTrue(isset($content['selectedQuotes']), 'Missing selectedQuotes');
        $this->assertTrue(isset($content['quotes']), 'Missing quotes');
    }

    /**
     * @small
     */
    public function test_get_provider_can_access_closed_inquiry()
    {
        $client = $this->createUnauthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();

        $provider = $this->createProvider($client->getContainer());

        $inquiry = $this->createInquiry($client->getContainer());

        $inquiry->setStatus(InquiryStatus::CLOSED);
        $inquiry->setType(InquiryType::CAR_RENT);
        $provider->setType(InquiryType::CAR_RENT);

        $em->persist($inquiry);
        $em->persist($provider);

        $em->flush();

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');
        $this->assertTrue(isset($content['selectedQuotes']), 'Missing selectedQuotes');
        $this->assertTrue(isset($content['quotes']), 'Missing quotes');
    }

    /**
     * @small
     */
    public function test_get_provider_can_not_access_pending_inquiry()
    {
        $client = $this->createUnauthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();

        $provider = $this->createProvider($client->getContainer());

        $inquiry = $this->createInquiry($client->getContainer());

        $inquiry->setStatus(InquiryStatus::PENDING);
        $inquiry->setType(InquiryType::CAR_RENT);
        $provider->setType(InquiryType::CAR_RENT);

        $em->persist($inquiry);
        $em->persist($provider);

        $em->flush();

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId(), [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_post_unauthorized()
    {
        $client = $this->createUnauthorizedClient();
        $container = $client->getContainer();

        $category1 = $this->createCategory($container);
        $category2 = $this->createCategory($container);
        $category3 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category4 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        $client->request('POST', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'type' => InquiryType::CAR_RENT,
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category3->getId(),
                'origin' => $category4->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
                'subtypes' => [InquirySubtype::VILLA, InquirySubtype::HOTEL]
            ],
            'categories' => [
                $category1->getId(), $category2->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');

        $this->assertEquals(InquiryStatus::PENDING, $content['status']);

        $this->assertFalse(isset($content['creator']), 'Invalid creator');
        $this->assertTrue(isset($content['selectedQuotes']), 'Missing selectedQuotes');
        $this->assertTrue(isset($content['quotes']), 'Missing quotes');
        $this->assertTrue(isset($content['categories']), 'Missing categories');
    }

    /**
     * @medium
     */
    public function test_post_user()
    {
        $client = $this->createAuthorizedUser();

        $container = $client->getContainer();

        $category1 = $this->createCategory($container);
        $category2 = $this->createCategory($container);
        $category3 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category4 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        $client->request('POST', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'type' => InquiryType::CAR_RENT,
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'roomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category3->getId(),
                'origin' => $category4->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
                'subtypes' => [InquirySubtype::VILLA, InquirySubtype::HOTEL]
            ],
            'categories' => [
                $category1->getId(), $category2->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');

        $this->assertEquals(InquiryStatus::OPEN, $content['status']);

        $this->assertTrue(isset($content['creator']), 'Missing creator');
        $this->assertTrue(isset($content['selectedQuotes']), 'Missing selectedQuotes');
        $this->assertTrue(isset($content['quotes']), 'Missing quotes');
        $this->assertTrue(isset($content['categories']), 'Missing categories');
    }

    /**
     * @medium
     */
    public function test_post_unauthorized_and_update_user_later()
    {
        $client = $this->createUnauthorizedClient();
        $userService = $client->getContainer()->get(UserService::class);

        $container = $client->getContainer();

        $category1 = $this->createCategory($container);
        $category2 = $this->createCategory($container);
        $category3 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category4 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        $client->request('POST', "/api/v1/inquiries", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'type' => InquiryType::CAR_RENT,
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'roomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category3->getId(),
                'origin' => $category4->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
                'subtypes' => [InquirySubtype::VILLA, InquirySubtype::HOTEL]
            ],
            'categories' => [
                $category1->getId(), $category2->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');

        $this->assertEquals(InquiryStatus::PENDING, $content['status']);

        $this->assertFalse(isset($content['creator']), 'Invalid creator');

        $user = $userService->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user->getEmail());

        $client->request('PUT', "/api/v1/inquiries/" . $content['guid'], [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'creator' => $user->getId(),
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['guid']), 'Missing guid');
        $this->assertTrue(isset($content['status']), 'Missing status');

        $this->assertEquals(InquiryStatus::OPEN, $content['status']);

        $this->assertTrue(isset($content['creator']), 'Missing creator');

        $this->assertEquals($user->getId(), $content['creator']['id'], 'Invalid creator');
    }

}