<?php

namespace App\Tests\Controller;

use App\Entity\CategoryGroup;
use App\Entity\InquiryType;
use App\Entity\PaymentType;
use App\Service\InquiryService;
use App\Service\UserService;
use App\Tests\Classes\InquiryCreator;
use App\Tests\Classes\MediaCreator;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\QuoteRESTController
 */
class QuoteRESTControllerTest extends WebTestCase
{

    use ProviderCreator;
    use InquiryCreator;
    use MediaCreator;

    /**
     * @small
     */
    public function test_gets_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/inquiries/1/quotes", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/inquiries/1/quotes/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/inquiries/sss/quotes", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post_user()
    {
        $client = $this->createAuthorizedUser();

        $client->request('POST', "/api/v1/inquiries/sss/quotes", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_user()
    {
        $client = $this->createAuthorizedUser();

        $inquiry = $this->createInquiry($client->getContainer());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId() . '/quotes', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_provider()
    {
        $client = $this->createAuthorizedProvider();

        $inquiry = $this->createInquiry($client->getContainer());

        $client->request('GET', "/api/v1/inquiries/" . $inquiry->getId() . '/quotes', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @medium
     */
    public function test_post_provider()
    {
        $client = $this->createAuthorizedUser();
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container, null, InquiryType::BOAT_RENT);

        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $provider = $this->createProvider($container, InquiryType::BOAT_RENT);

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $client->request('POST', "/api/v1/inquiries/" . $inquiry->getGuid() . '/quotes', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'type' => InquiryType::BOAT_RENT,
            'text' => md5(uniqid()),
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
    }

    /**
     * @medium
     *
     * @group ignore
     */
    public function test_post_provider_with_media()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $media1 = $this->createMediaImage($container, 'apple-touch-icon-57x57.png');
        $media2 = $this->createMediaImage($container, 'apple-touch-icon-152x152.png');

        $category1 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category2 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        $client = $this->createAuthorizedUser();
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container, null, InquiryType::TRANSFER);

        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $provider = $this->createProvider($container, InquiryType::TRANSFER);

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());

        $client->request('POST', "/api/v1/inquiries/" . $inquiry->getGuid() . '/quotes', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'type' => InquiryType::TRANSFER,
            'text' => md5(uniqid()),
            'media' => [$media1->getId(), $media2->getId()],
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category1->getId(),
                'origin' => $category2->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['media']), 'Missing media');
        $this->assertEquals(2, count($content['media']), 'Invalid media');

    }

    /**
     * @medium
     */
    public function test_post_make_quote_selected_by_user()
    {
        $client = $this->createAuthorizedAdmin();
        $userService = $client->getContainer()->get(UserService::class);

        $user = $userService->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user->getUsername());

        $user = $userService->findOneByFilter(['id' => $user->getId()]);

        $inquiry = $this->createInquiry($client->getContainer(), $user);

        $client = $this->createAuthorizedProvider();

        $inquiryService = $client->getContainer()->get(InquiryService::class);

        $inquiry = $inquiryService->findOneByFilter(['id' => $inquiry->getId()]);

        $quote = $this->createQuote($client->getContainer(), $inquiry);

        $client = $this->createAuthorizedClient($user->getUsername());

        $client->request('POST', "/api/v1/inquiries/" . $inquiry->getGuid() . '/quotes/' . $quote->getId() . '/select', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');

    }
}