<?php

namespace App\Tests\Controller;

use App\Service\UserService;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\UserRESTController
 */
class UserRESTControllerTest extends WebTestCase
{

    /**
     * @small
     */
    public function test_gets_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/users", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_user_forbidden()
    {
        $client = $this->createAuthorizedUser();

        $client->request('GET', "/api/v1/users", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('GET', "/api/v1/users", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_gets_admin()
    {
        $client = $this->createAuthorizedAdmin();

        $client->request('GET', "/api/v1/users", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/api/v1/users/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('GET', "/api/v1/users/1", [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_Content-Type' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get_user_requests_different_user()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $user2 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $client->request('GET', "/api/v1/users/" . $user2->getId(), [], []);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_get()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $client->request('GET', "/api/v1/users/" . $user1->getId(), [], []);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_post()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/users", [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]));

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['id']), 'Missing id');
        $this->assertTrue(isset($content['isActive']), 'Missing isActive');
        $this->assertFalse($content['isActive'], 'Invalid isActive');
    }

    /**
     * @small
     */
    public function test_put_unauthorized()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('PUT', "/api/v1/users/1", [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_provider_forbidden()
    {
        $client = $this->createAuthorizedProvider();

        $client->request('PUT', "/api/v1/users/1", [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put_user_requests_different_user()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $user2 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $client->request('PUT', "/api/v1/users/" . $user2->getId(), [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @small
     */
    public function test_put()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $newName = md5(uniqid());
        $newPhone = rand(10000, 20000) . rand(10000, 20000);
        $newEmail = md5(uniqid()) . '@mail.com';

        $client->request('PUT', "/api/v1/users/" . $user1->getId(), [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'name' => $newName,
            'phone' => $newPhone,
            'email' => $newEmail,
        ]));

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertEquals($newName, $content['name']);
        $this->assertEquals($newPhone, $content['phone']);
        $this->assertEquals($newEmail, $content['email']);
    }

    /**
     * @small
     */
    public function test_put_me()
    {
        $client = $this->createUnauthorizedClient();
        $service = $client->getContainer()->get(UserService::class);

        $user1 = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        $client = $this->createAuthorizedClient($user1->getEmail());

        $newName = md5(uniqid());
        $newPhone = rand(10000, 20000) . rand(10000, 20000);
        $newEmail = md5(uniqid()) . '@mail.com';

        $client->request('PUT', "/api/v1/users/me", [], [], [
            'HTTP_Content-Type' => 'application/json',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ], json_encode([
            'name' => $newName,
            'phone' => $newPhone,
            'email' => $newEmail,
        ]));

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertEquals($newName, $content['name']);
        $this->assertEquals($newPhone, $content['phone']);
        $this->assertEquals($newEmail, $content['email']);
    }
}
