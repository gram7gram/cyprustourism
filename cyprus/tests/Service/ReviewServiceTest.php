<?php

namespace App\Tests\Service;

use App\Entity\Review;
use App\Entity\ReviewStatus;
use App\Service\ProviderService;
use App\Service\ReviewService;
use App\Service\UserService;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\UserCreator;
use App\Tests\Classes\WebTestCase;

class ReviewServiceTest extends WebTestCase
{
    use ProviderCreator;
    use UserCreator;


    /**
     * @medium
     */
    public function test_provider_rating_should_change_after_approved_review()
    {
        $client = $this->createAuthorizedAdmin();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $provider = $this->createProvider($client->getContainer());

        $this->assertEquals(0, $provider->getRating(), 'Invalid rating');

        $values = [2, 4, 4, 5];

        foreach ($values as $value) {
            $client = $this->createAuthorizedAdmin();

            $user = $this->createUser($client->getContainer());

            $client = $this->createAuthorizedClient($user);
            $service = $client->getContainer()->get(ReviewService::class);
            $providerService = $client->getContainer()->get(ProviderService::class);
            $userService = $client->getContainer()->get(UserService::class);

            $provider = $providerService->findOneByFilter([
                'id' => $provider->getId()
            ]);

            $user = $userService->findOneByFilter([
                'id' => $user->getId()
            ]);

            $review = new Review();
            $review->setProvider($provider);
            $review->setUser($user);

            $service->update($review, [
                'rating' => $value
            ]);

            $this->assertEquals(ReviewStatus::CREATED, $review->getStatus(), 'Invalid status');
            $this->assertEquals(0, $provider->getRating(), 'Invalid rating');
        }

        $em->clear();

        $client = $this->createAuthorizedAdmin();
        $service = $client->getContainer()->get(ReviewService::class);
        $providerService = $client->getContainer()->get(ProviderService::class);

        $reviews = $service->findByFilter([
            'status' => ReviewStatus::CREATED,
            'provider' => $provider->getId()
        ]);

        $this->assertEquals(4, count($reviews), 'Invalid count');

        foreach ($reviews as $rating) {
            $service->update($rating, [
                'status' => ReviewStatus::APPROVED
            ]);
        }

        $provider = $providerService->findOneByFilter([
            'id' => $provider->getId()
        ]);

        $this->assertEquals(3.75, $provider->getRating(), 'Invalid rating', 0.01);
    }
}