<?php

namespace App\Tests\Service;

use App\Entity\Inquiry;
use App\Entity\Quote;
use App\Service\EmailService;
use App\Service\SelectedQuoteService;
use App\Service\SharedContactService;
use App\Tests\Classes\InquiryCreator;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\UserCreator;
use App\Tests\Classes\WebTestCase;

class EmailServiceTest extends WebTestCase
{
    use InquiryCreator;
    use ProviderCreator;
    use UserCreator;

    /**
     * @small
     */
    public function test_onInvoicePayPalPaid()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);

        $container->get(EmailService::class)->onInvoicePayPalPaid($user);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_onInvoiceBankPaid()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);

        $container->get(EmailService::class)->onInvoiceBankPaid($user);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_sendVerificationEmail()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);

        $user->refreshEmailCode();

        $container->get(EmailService::class)->sendVerificationEmail($user);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_sendResetPassword()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);

        $user->refreshPasswordToken();

        $container->get(EmailService::class)->sendResetPassword($user);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_onInquiryCreated()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);
        $provider = $this->createProvider($container);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container);

        $container->get(EmailService::class)->onInquiryCreated($provider, $inquiry);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @medium
     */
    public function test_onReviewRequest()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);
        $provider = $this->createProvider($container);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container);

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();

        $inquiry = $em->getRepository(Inquiry::class)->find($inquiry->getId());

        $quote1 = $this->createQuote($container, $inquiry);
        $quote2 = $this->createQuote($container, $inquiry);
        $quote3 = $this->createQuote($container, $inquiry);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();

        $selectedService = $container->get(SelectedQuoteService::class);
        $em = $container->get('doctrine')->getManager();

        $inquiry = $em->getRepository(Inquiry::class)->find($inquiry->getId());
        $quote1 = $em->getRepository(Quote::class)->find($quote1->getId());
        $quote2 = $em->getRepository(Quote::class)->find($quote2->getId());
        $quote3 = $em->getRepository(Quote::class)->find($quote3->getId());

        $selectedService->create($inquiry, $quote1);
        $selectedService->create($inquiry, $quote2);
        $selectedService->create($inquiry, $quote3);

        $container->get(EmailService::class)->onReviewRequest($user, $inquiry->getSelectedQuotes()->toArray());

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @medium
     */
    public function test_onQuoteCreated()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);
        $provider = $this->createProvider($container);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container);

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();

        $inquiry = $em->getRepository(Inquiry::class)->find($inquiry->getId());

        $quote1 = $this->createQuote($container, $inquiry);

        $container->get(EmailService::class)->onQuoteCreated($user, $quote1);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_onProviderRegistered()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $provider = $this->createProvider($container);

        $container->get(EmailService::class)->onProviderRegistered($provider);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_onContactPending()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $provider = $this->createProvider($container);

        $container->get(EmailService::class)->onContactPending($provider);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @medium
     */
    public function test_onContactCreated()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);
        $provider = $this->createProvider($container);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();

        $inquiry = $this->createInquiry($container);

        $client = $this->createAuthorizedClient($provider->getUser()->getUsername());
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();

        $inquiry = $em->getRepository(Inquiry::class)->find($inquiry->getId());

        $quote1 = $this->createQuote($container, $inquiry);

        $client = $this->createAuthorizedClient($user->getUsername());
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();
        $quoteService = $container->get(SelectedQuoteService::class);
        $contactService = $container->get(SharedContactService::class);

        $inquiry = $em->getRepository(Inquiry::class)->find($inquiry->getId());
        $quote1 = $em->getRepository(Quote::class)->find($quote1->getId());

        $selectedQuote = $quoteService->create($inquiry, $quote1);

        $entity = $contactService->create($selectedQuote, 5);

        $container->get(EmailService::class)->onContactCreated($entity);

        $this->assertTrue(true); //dummy condition
    }

    /**
     * @small
     */
    public function test_onUserRegistered()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $user = $this->createUser($container);

        $container->get(EmailService::class)->onUserRegistered($user);

        $this->assertTrue(true); //dummy condition
    }
}