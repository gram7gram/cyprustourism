<?php

namespace App\Tests\Service;

use App\Entity\ProviderInvoiceStatus;
use App\Entity\ProviderInvoiceType;
use App\Service\BillingPlanService;
use App\Service\PdfService;
use App\Tests\Classes\ProviderCreator;
use App\Tests\Classes\WebTestCase;

class PdfServiceTest extends WebTestCase
{
    use ProviderCreator;

    /**
     * @small
     */
    public function test_generateInvoice()
    {
        $client = $this->createAuthorizedAdmin();
        $container = $client->getContainer();

        $service = $container->get(BillingPlanService::class);
        $pdf = $container->get(PdfService::class);

        $provider = $this->createProvider($container);
        $plans = $service->findByFilter([], 1, 1);

        $plan = $plans[0];

        $invoice = $pdf->generateInvoice($provider, $plan,
            ProviderInvoiceType::BANK,
            ProviderInvoiceStatus::UNPAID);

        $this->assertTrue($invoice->getId() > 0, 'Invalid id');
    }
}