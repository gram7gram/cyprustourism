<?php

namespace App\Tests\Classes;

use App\Entity\InquiryType;
use App\Service\ProviderService;
use App\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait UserCreator
{

    /**
     * @param ContainerInterface $container
     * @return \App\Entity\User
     * @throws \Exception
     */
    public function createUser(ContainerInterface $container)
    {
        $service = $container->get(UserService::class);

        $entity = $service->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'phone' => md5(uniqid()),
            'password' => '12345',
        ]);

        return $entity;
    }
}