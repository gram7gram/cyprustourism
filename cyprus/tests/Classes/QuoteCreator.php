<?php

namespace App\Tests\Classes;

use App\Entity\CategoryGroup;
use App\Entity\Inquiry;
use App\Entity\InquiryType;
use App\Entity\PaymentType;
use App\Service\QuoteService;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait QuoteCreator
{
    use CategoryCreator;

    /**
     * @param ContainerInterface $container
     * @param Inquiry $inquiry
     * @return \App\Entity\Quote
     * @throws \Exception
     */
    public function createQuote(ContainerInterface $container, Inquiry $inquiry)
    {
        $service = $container->get(QuoteService::class);

        $category1 = $this->createCategory($container);
        $category2 = $this->createCategory($container);
        $category3 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category4 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        $entity = $service->create($inquiry, [
            'text' => md5(uniqid()),
            'categories' => [$category1->getId(), $category2->getId()],
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category3->getId(),
                'origin' => $category4->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
            ],
        ]);

        return $entity;
    }
}