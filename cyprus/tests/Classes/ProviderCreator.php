<?php

namespace App\Tests\Classes;

use App\Entity\InquiryType;
use App\Service\ProviderService;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait ProviderCreator
{

    /**
     * @param ContainerInterface $container
     * @param null $type
     * @return \App\Entity\Provider
     * @throws \Exception
     */
    public function createProvider(ContainerInterface $container, $type = null)
    {
        $service = $container->get(ProviderService::class);

        if (!$type) {
            $types = [
                InquiryType::BOAT_RENT,
                InquiryType::TRANSFER,
                InquiryType::CAR_RENT,
                InquiryType::ACCOMMODATION_RENT,
            ];

            $type = $types[array_rand($types)];
        }

        $entity = $service->create([
            'credits' => 1000,
            'type' => $type,
            'companyName' => md5(uniqid()),
            'companyId' => md5(uniqid()),
            'address' => md5(uniqid()),
            'website' => md5(uniqid()),
            'user' => [
                'name' => md5(uniqid()),
                'email' => md5(uniqid()) . '@mail.com',
                'phone' => md5(uniqid()),
                'country' => md5(uniqid()),
                'password' => '12345',
            ]
        ]);

        return $entity;
    }
}