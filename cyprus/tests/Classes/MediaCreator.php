<?php

namespace App\Tests\Classes;

use App\Service\MediaService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait MediaCreator
{

    public function createMediaImage(ContainerInterface $container, $name = 'apple-touch-icon-114x114.png')
    {
        $mediaService = $container->get(MediaService::class);
        $root = $container->getParameter('kernel.root_dir') . '/../public';

        $path1 = $root . '/img/favicon/' . $name;

        copy($path1, '/tmp/test-' . $name);

        $file = new UploadedFile('/tmp/test-' . $name, $name, 'image/png', UPLOAD_ERR_OK, true);

        $media = $mediaService->create($file);

        return $media;
    }
}