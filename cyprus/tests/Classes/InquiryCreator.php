<?php

namespace App\Tests\Classes;

use App\Entity\CategoryGroup;
use App\Entity\InquirySubtype;
use App\Entity\InquiryType;
use App\Entity\PaymentType;
use App\Entity\User;
use App\Service\InquiryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait InquiryCreator
{

    use QuoteCreator;

    /**
     * @param ContainerInterface $container
     * @param User|null $creator
     * @param null $type
     * @return \App\Entity\Inquiry
     * @throws \Exception
     */
    public function createInquiry(ContainerInterface $container, User $creator = null, $type = null)
    {
        $inquiryService = $container->get(InquiryService::class);

        $category1 = $this->createCategory($container);
        $category2 = $this->createCategory($container);
        $category3 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);
        $category4 = $this->createCategory($container, InquiryType::TRANSFER, CategoryGroup::DESTINATION);

        if (!$type) {
            $types = [
                InquiryType::BOAT_RENT,
                InquiryType::TRANSFER,
                InquiryType::CAR_RENT,
                InquiryType::ACCOMMODATION_RENT,
            ];

            $type = $types[array_rand($types)];
        }

        $inquiry = $inquiryService->create([
            'creator' => $creator ? $creator->getId() : null,
            'text' => md5(uniqid()),
            'type' => $type,
            'parameters' => [
                'paymentType' => PaymentType::CASH,
                'bedroomCount' => rand(1, 5),
                'adultsCount' => rand(1, 5),
                'childrenCount' => rand(1, 5),
                'passengerCount' => rand(1, 5),
                'driverAgeFrom' => rand(18, 100),
                'destination' => $category3->getId(),
                'origin' => $category4->getId(),
                'isFixedDate' => true,
                'rentalStart' => date('Y-m-02'),
                'rentalEnd' => date('Y-m-12'),
                'dayCount' => 10,
                'subtypes' => [InquirySubtype::VILLA, InquirySubtype::HOTEL]
            ],
            'categories' => [
                $category1->getId(), $category2->getId()
            ]
        ]);

        return $inquiry;
    }
}