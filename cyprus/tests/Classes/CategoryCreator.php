<?php

namespace App\Tests\Classes;

use App\Entity\CategoryGroup;
use App\Entity\InquiryType;
use App\Service\CategoryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait CategoryCreator
{

    /**
     * @param ContainerInterface $container
     * @param null $type
     * @param null $group
     * @return \App\Entity\Category
     * @throws \Exception
     */
    public function createCategory(ContainerInterface $container, $type = null, $group = null)
    {
        $categoryService = $container->get(CategoryService::class);

        if (!$type) {
            $types = [
                InquiryType::BOAT_RENT,
                InquiryType::TRANSFER,
                InquiryType::CAR_RENT,
                InquiryType::ACCOMMODATION_RENT,
            ];

            $type = $types[array_rand($types)];
        }

        if (!$group) {
            $groups = [
                CategoryGroup::DESTINATION,
                CategoryGroup::CAR_TYPE,
                CategoryGroup::BOAT_TYPE,
                CategoryGroup::LOCATION,
                CategoryGroup::EXTRAS,
            ];

            $group = $groups[array_rand($groups)];

        }

        $extra = null;
        if ($group === CategoryGroup::DESTINATION) {
            $extra = json_encode([
                'lat' => rand(-180, 180),
                'lng' => rand(-180, 180)
            ]);
        }

        $entity = $categoryService->create([
            'name' => md5(uniqid()),
            'group' => $group,
            'locale' => 'en',
            'ordering' => 25,
            'type' => $type,
            'extra' => $extra
        ]);

        return $entity;
    }
}