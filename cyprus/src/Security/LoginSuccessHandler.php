<?php

namespace App\Security;

use App\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /** @var ContainerInterface */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $userService = $this->container->get(UserService::class);
        $router = $this->container->get('router');
        $host = $this->container->getParameter('project_host');

        $user = $userService->getUser();

        $content = $userService->serialize($user);

        $next = $router->generate('my_inquiry_index');

//        $referer = $request ? $request->headers->get('referer') : null;
//
//        if ($referer) {
//            if (strpos($referer, $host) === 0) {
//                $next = $referer;
//            }
//        }

        return new JsonResponse([
            'next' => $next,
            'user' => $content
        ]);
    }
}
