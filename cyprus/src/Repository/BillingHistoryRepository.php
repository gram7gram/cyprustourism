<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class BillingHistoryRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('history.createdAt', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('history');
        $e = $qb->expr();

        $qb
            ->addSelect('provider')
            ->addSelect('inquiry');

        $qb
            ->join('history.provider', 'provider')
            ->leftJoin('history.inquiry', 'inquiry');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('history.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('history.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}