<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class CategoryRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb
            ->orderBy('category.ordering', 'ASC')
            ->addOrderBy('category.group', 'ASC')
            ->addOrderBy('category.name', 'ASC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('category');
        $e = $qb->expr();

        $qb->addSelect('icon');

        $qb->leftJoin('category.icon', 'icon');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('category.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'ids':
                    $qb->andWhere($e->in('category.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'locale':
                    $qb->andWhere($e->eq('category.locale', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'type':
                    $qb->andWhere($e->eq('category.type', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'types':
                    $qb->andWhere($e->in('category.type', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'group':
                    $qb->andWhere($e->eq('category.group', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'groups':
                    $qb->andWhere($e->in('category.group', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'name':
                    $qb->andWhere($e->eq('category.name', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'search':
                    $qb->andWhere($e->orX()
                        ->add($e->like($e->lower('category.name'), ":$key"))
                        ->add($e->like($e->lower('category.locale'), ":$key"))
                        ->add($e->like($e->lower('category.type'), ":$key"))
                        ->add($e->like($e->lower('category.group'), ":$key"))
                    )->setParameter($key, '%' . mb_strtolower($value, 'utf8') . '%');
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('category.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}