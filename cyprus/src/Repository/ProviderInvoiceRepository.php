<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ProviderInvoiceRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('invoice.createdAt', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('invoice');
        $e = $qb->expr();

        $qb
            ->addSelect('media')
            ->addSelect('billingPlan')
            ->addSelect('provider');

        $qb
            ->join('invoice.billingPlan', 'billingPlan')
            ->join('invoice.media', 'media')
            ->join('invoice.provider', 'provider');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('invoice.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('invoice.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'statuses':
                    $qb->andWhere($e->in('invoice.status', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'type':
                    $qb->andWhere($e->eq('invoice.type', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'types':
                    $qb->andWhere($e->in('invoice.type', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'search':
                    $qb->andWhere($e->orX()
                        ->add($e->like($e->lower('invoice.number'), ":$key"))
                        ->add($e->like($e->lower('billingPlan.name'), ":$key"))
                    )->setParameter($key, '%' . mb_strtolower($value, 'utf8') . '%');
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('invoice.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}