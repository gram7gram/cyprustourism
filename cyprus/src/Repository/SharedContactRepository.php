<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class SharedContactRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('contact.id', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('contact');
        $e = $qb->expr();

        $qb
            ->addSelect('user')
            ->addSelect('provider')
            ->addSelect('providerUser')
            ->addSelect('selectedQuote')
            ->addSelect('quote')
            ->addSelect('inquiry');

        $qb
            ->join('contact.user', 'user')
            ->join('contact.provider', 'provider')
            ->join('provider.user', 'providerUser')
            ->join('contact.selectedQuote', 'selectedQuote')
            ->join('selectedQuote.quote', 'quote')
            ->join('selectedQuote.inquiry', 'inquiry');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('contact.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'ids':
                    $qb->andWhere($e->in('contact.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('contact.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'user':
                    $qb->andWhere($e->eq('user.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'selectedQuote':
                    $qb->andWhere($e->eq('selectedQuote.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('contact.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}