<?php

namespace App\Repository;

use App\Entity\SharedContactStatus;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class InquirySelectedQuoteRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('selected.createdAt', 'ASC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('selected');
        $e = $qb->expr();

        $qb
            ->addSelect('review')
            ->addSelect('reviewProvider')
            ->addSelect('inquiry')
            ->addSelect('inquiryCreator')
            ->addSelect('contact')
            ->addSelect('quote')
            ->addSelect('parameters')
            ->addSelect('subtype')
            ->addSelect('daytimeRange')
            ->addSelect('hourlyRange')
            ->addSelect('parametersOrigin')
            ->addSelect('parametersDestination')
            ->addSelect('provider')
            ->addSelect('providerUser')
            ->addSelect('quoteCategory')
            ->addSelect('category')
            ->addSelect('categoryIcon')
            ->addSelect('quoteMedia')
            ->addSelect('media')
        ;

        $qb
            ->join('selected.inquiry', 'inquiry')
            ->join('selected.quote', 'quote')
            ->join('quote.parameters', 'parameters')
            ->join('quote.provider', 'provider')
            ->join('provider.user', 'providerUser')
            ->leftJoin('quote.categories', 'quoteCategory')
            ->leftJoin('quoteCategory.category', 'category')
            ->leftJoin('parameters.subtypes', 'subtype')
            ->leftJoin('parameters.daytimeRanges', 'daytimeRange')
            ->leftJoin('parameters.hourlyRanges', 'hourlyRange')
            ->leftJoin('parameters.origin', 'parametersOrigin')
            ->leftJoin('parameters.destination', 'parametersDestination')
            ->leftJoin('category.icon', 'categoryIcon')
            ->leftJoin('selected.review', 'review')
            ->leftJoin('review.provider', 'reviewProvider')
            ->leftJoin('selected.contact', 'contact', 'WITH', 'contact.status = :joinContactStatus')
            ->leftJoin('inquiry.creator', 'inquiryCreator')
            ->leftJoin('quote.media', 'quoteMedia')
            ->leftJoin('quoteMedia.media', 'media');

        $qb->setParameter('joinContactStatus', SharedContactStatus::CREATED);

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('selected.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiry':
                    $qb->andWhere($e->eq('inquiry.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'quote':
                    $qb->andWhere($e->eq('quote.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiryCreator':
                    $qb->andWhere($e->eq('inquiryCreator.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiryGuid':
                    $qb->andWhere($e->eq('inquiry.guid', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('selected.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}