<?php

namespace App\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class InquiryRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findIdsByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->select('inquiry.id')->distinct(true)
            ->addSelect('inquiry.createdAt');

        $qb->orderBy('inquiry.createdAt', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $result = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getArrayResult();

        return array_map(function ($item) {
            return $item['id'];
        }, $result);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $ids = $this->findIdsByFilter($filter, $page, $limit);

        if (count($ids) === 0) return [];

        $qb = $this->createFilterQuery([
            'ids' => $ids
        ]);

        $qb->orderBy('inquiry.createdAt', 'DESC');

        return $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('inquiry');
        $e = $qb->expr();

        $qb
            ->addSelect('parameters')
            ->addSelect('subtype')
            ->addSelect('hourlyRange')
            ->addSelect('daytimeRange')
            ->addSelect('parametersOrigin')
            ->addSelect('parametersDestination')
            ->addSelect('inquireCategory')
            ->addSelect('category')
            ->addSelect('categoryIcon')
            ->addSelect('review')
            ->addSelect('reviewUser')
            ->addSelect('creator');

        $qb
            ->join('inquiry.parameters', 'parameters')
            ->join('inquiry.categories', 'inquireCategory')
            ->join('inquireCategory.category', 'category')
            ->leftJoin('parameters.hourlyRanges', 'hourlyRange')
            ->leftJoin('parameters.daytimeRanges', 'daytimeRange')
            ->leftJoin('parameters.subtypes', 'subtype')
            ->leftJoin('parameters.origin', 'parametersOrigin')
            ->leftJoin('parameters.destination', 'parametersDestination')
            ->leftJoin('category.icon', 'categoryIcon')
            ->leftJoin('inquiry.creator', 'creator')
            ->leftJoin('inquiry.review', 'review')
            ->leftJoin('review.user', 'reviewUser');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('inquiry.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'guid':
                    $qb->andWhere($e->eq('inquiry.guid', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'ids':
                    $qb->andWhere($e->in('inquiry.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('inquiry.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'statuses':
                    $qb->andWhere($e->in('inquiry.status', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'types':
                    $qb->andWhere($e->in('inquiry.type', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'type':
                    $qb->andWhere($e->eq('inquiry.type', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'creator':
                    $qb->andWhere($e->eq('creator.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'areProvidersNotified':
                    $qb->andWhere($e->eq('inquiry.areProvidersNotified', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'isVerified':
                    $qb->andWhere($e->eq('creator.isPhoneVerified', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'hasReview':

                    if (!$value) {
                        $qb->andWhere($e->isNull('review.id'));
                    } else {
                        $qb->andWhere($e->isNotNull('review.id'));
                    }

                    break;
                case 'providerQuotedOnly':
                    $value = intval($value) === 1;
                    if ($value && isset($filter['provider'])) {
                        $qb->andWhere("(
                            SELECT COUNT(iq.id) FROM App\Entity\Quote iq
                            WHERE iq.inquiry = inquiry.id AND iq.provider = :provider
                        ) > 0")->setParameter('provider', $filter['provider']);
                    }
                    break;
                case 'providerSelectedOnly':
                    $value = intval($value) === 1;
                    if ($value && isset($filter['provider'])) {
                        $qb->andWhere("(
                            SELECT COUNT(isq.id) FROM App\Entity\InquirySelectedQuote isq
                            JOIN App\Entity\Quote isqQuote WITH isqQuote.provider = :provider AND isq.quote = isqQuote
                            WHERE isq.inquiry = inquiry.id
                        ) > 0")->setParameter('provider', $filter['provider']);
                    }
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('inquiry.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }

    public function findIdsWithoutReviews()
    {
        $qb = $this->createFilterQuery([
            'hasReview' => false
        ]);
        $e = $qb->expr();

        $previousWeek = new \DateTime();
        $previousWeek->modify('-7 days');
        $previousWeek->setTime(23, 59, 59);

        $qb->select('inquiry.id')->distinct(true);

        $qb
            ->join('inquiry.selectedQuotes', 'selectedQuote');

        $qb->andWhere($e->lte('selectedQuote.createdAt', ':previousWeek'))
            ->setParameter('previousWeek', $previousWeek);

        $result = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getArrayResult();

        return array_map(function ($item) {
            return $item['id'];
        }, $result);

    }

    public function findMetadata($ids, $providerId = null)
    {
        $qb = $this->createQueryBuilder('inquiry');
        $e = $qb->expr();

        $qb->select('inquiry.id');

        if ($providerId) {

            $qb
                ->addSelect('(
                    SELECT COUNT(iq.id) FROM App\Entity\Quote iq
                    WHERE iq.inquiry = inquiry.id AND iq.provider = :provider
                ) AS quoteCount')
                ->addSelect('(
                    SELECT COUNT(isq.id) FROM App\Entity\InquirySelectedQuote isq
                    JOIN App\Entity\Quote isqQuote WITH isqQuote.provider = :provider AND isq.quote = isqQuote
                    WHERE isq.inquiry = inquiry.id
                ) AS selectedQuoteCount');

            $qb->setParameter('provider', $providerId);
        } else {

            $qb
                ->addSelect('(
                    SELECT COUNT(iq.id) FROM App\Entity\Quote iq
                    WHERE iq.inquiry = inquiry
                ) AS quoteCount')
                ->addSelect('(
                    SELECT COUNT(isq.id) FROM App\Entity\InquirySelectedQuote isq
                    WHERE isq.inquiry = inquiry
                ) AS selectedQuoteCount');
        }

        $qb->andWhere($e->in('inquiry.id', ":ids"))
            ->setParameter('ids', $ids);

        $result = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getArrayResult();

        return array_map(function ($item) {
            return [
                'id' => intval($item['id']),
                'quoteCount' => intval($item['quoteCount']),
                'selectedQuoteCount' => intval($item['selectedQuoteCount']),
            ];
        }, $result);

    }

}