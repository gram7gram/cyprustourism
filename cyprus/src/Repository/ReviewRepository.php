<?php

namespace App\Repository;

use App\Entity\ReviewStatus;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ReviewRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('review.createdAt', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('review');
        $e = $qb->expr();

        $qb
            ->addSelect('creator')
            ->addSelect('provider')
            ->addSelect('providerUser');

        $qb
            ->join('review.user', 'creator')
            ->join('review.provider', 'provider')
            ->join('provider.user', 'providerUser');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('review.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'rating':
                    $qb->andWhere($e->eq('review.rating', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('review.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'isPublished':
                    $qb->andWhere($e->eq('review.isPublished', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'statuses':
                    $qb->andWhere($e->in('review.status', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'search':
                    $qb->andWhere($e->orX()
                        ->add($e->like($e->lower('review.text'), ":$key"))
                        ->add($e->like($e->lower('providerUser.name'), ":$key"))
                        ->add($e->like($e->lower('providerUser.surname'), ":$key"))
                        ->add($e->like($e->lower('creator.name'), ":$key"))
                        ->add($e->like($e->lower('creator.surname'), ":$key"))
                    )->setParameter($key, '%' . mb_strtolower($value, 'utf8') . '%');
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('review.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }

    /**
     * @param $provider
     * @return array
     */
    public function findRatingForProvider($provider)
    {
        $qb = $this->createQueryBuilder('review');
        $e = $qb->expr();

        $qb->select('review.id')
            ->addSelect('review.rating');

        $qb->join('review.provider', 'provider');

        $qb->andWhere($e->eq('provider.id', ":id"))
            ->setParameter('id', $provider);

        $qb->andWhere($e->eq('review.status', ":status"))
            ->setParameter('status', ReviewStatus::APPROVED);

        return $qb->getQuery()
            ->useQueryCache(true)
            ->getArrayResult();
    }
}