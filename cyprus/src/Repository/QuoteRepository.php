<?php

namespace App\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class QuoteRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->select('quote.id')->distinct(true)
            ->addSelect('quote.createdAt');

        $qb->orderBy('quote.createdAt', 'ASC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $result = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getArrayResult();

        if (count($result) === 0) return [];

        $ids = array_map(function ($item) {
            return $item['id'];
        }, $result);

        $qb = $this->createFilterQuery([
            'ids' => $ids
        ]);

        $qb->orderBy('quote.createdAt', 'DESC');

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('quote');
        $e = $qb->expr();

        $qb
            ->addSelect('price')
            ->addSelect('inquiry')
            ->addSelect('inquiryCreator')
            ->addSelect('parameters')
            ->addSelect('subtype')
            ->addSelect('hourlyRange')
            ->addSelect('daytimeRange')
            ->addSelect('parametersOrigin')
            ->addSelect('parametersDestination')
            ->addSelect('quoteCategory')
            ->addSelect('category')
            ->addSelect('provider')
            ->addSelect('user')
            ->addSelect('quoteMedia')
            ->addSelect('media');

        $qb
            ->leftJoin('quote.categories', 'quoteCategory')
            ->leftJoin('quoteCategory.category', 'category')
            ->join('quote.parameters', 'parameters')
            ->join('quote.provider', 'provider')
            ->join('quote.inquiry', 'inquiry')
            ->join('provider.user', 'user')
            ->leftJoin('inquiry.creator', 'creator')
            ->leftJoin('parameters.daytimeRanges', 'daytimeRange')
            ->leftJoin('parameters.hourlyRanges', 'hourlyRange')
            ->leftJoin('parameters.subtypes', 'subtype')
            ->leftJoin('parameters.origin', 'parametersOrigin')
            ->leftJoin('parameters.destination', 'parametersDestination')
            ->leftJoin('quote.price', 'price')
            ->leftJoin('inquiry.creator', 'inquiryCreator')
            ->leftJoin('quote.media', 'quoteMedia')
            ->leftJoin('quoteMedia.media', 'media');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('quote.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'ids':
                    $qb->andWhere($e->in('quote.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiry':
                    $qb->andWhere($e->eq('inquiry.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'creator':
                    $qb->andWhere($e->eq('creator.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiryType':
                    $qb->andWhere($e->eq('inquiry.type', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'inquiryStatuses':
                    $qb->andWhere($e->in('inquiry.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'provider':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'type':
                    $qb->andWhere($e->eq('quote.type', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('inquiry.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'receiveEmailOnNewQuote':
                    $qb->andWhere($e->eq('inquiry.receiveEmailOnNewQuote', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'isCreatorNotified':
                    $qb->andWhere($e->eq('quote.isCreatorNotified', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('quote.id'));

        return (int)$qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}