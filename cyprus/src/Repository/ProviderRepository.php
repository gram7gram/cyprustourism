<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ProviderRepository extends EntityRepository
{

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('provider.id', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $items = $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $items;
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('provider');
        $e = $qb->expr();

        $qb->addSelect('user');

        $qb->join('provider.user', 'user');

        foreach ($filter as $key => $value) {

            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('provider.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('provider.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'statuses':
                    $qb->andWhere($e->in('provider.status', ":$key"))
                        ->setParameter($key, explode(',', $value));
                    break;
                case 'user':
                    $qb->andWhere($e->eq('user.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'search':
                    $qb->andWhere($e->orX()
                        ->add($e->like($e->lower('user.name'), ":$key"))
                        ->add($e->like($e->lower('user.surname'), ":$key"))
                        ->add($e->like($e->lower('user.email'), ":$key"))
                        ->add($e->like($e->lower('provider.companyName'), ":$key"))
                        ->add($e->like($e->lower('provider.companyId'), ":$key"))
                    )->setParameter($key, '%' . mb_strtolower($value, 'utf8') . '%');
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('provider.id'));

        return (int) $qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}