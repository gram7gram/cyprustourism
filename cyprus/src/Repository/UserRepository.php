<?php

namespace App\Repository;

use App\Classes\InactiveProviderException;
use App\Entity\ProviderStatus;
use App\Entity\User;
use App\Classes\InactiveUserException;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    /**
     * @param string $username
     *
     * @return null|User
     */
    public function loadUserByUsername($username)
    {
        if (!$username) return null;

        $items = $this->findByFilter([
            'login' => $username
        ], 1, 1);

        if (count($items) !== 1) return null;

        /** @var User $user */
        $user = $items[0];

        if (!$user->isActive()) {
            throw new InactiveUserException();
        }

        $provider = $user->getProvider();

        if ($provider) {
            if ($provider->getStatus() !== ProviderStatus::APPROVED) {
                throw new InactiveProviderException();
            }
        }

        return $user;
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('user.id', 'DESC');

        $qb->select('user.id')->distinct(true);

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        $result = $qb->getQuery()
            ->useQueryCache(true)
            ->getArrayResult();

        if (count($result) === 0) return [];

        $ids = array_map(function ($item) {
            return $item['id'];
        }, $result);

        $qb = $this->createFilterQuery([
            'ids' => $ids
        ]);

        return $qb->getQuery()
            ->useQueryCache(true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('user');
        $e = $qb->expr();

        $qb->addSelect('provider');

        $qb->leftJoin('user.provider', 'provider');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'id':
                    $qb->andWhere($e->eq('user.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'ids':
                    $qb->andWhere($e->in('user.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'email':
                    $qb->andWhere($e->eq('user.email', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'phone':
                    $qb->andWhere($e->eq('user.phone', ":$key"))
                        ->setParameter($key, preg_replace('/[^0-9]/', '', $value));
                    break;
                case 'login':
                    $qb->andWhere($e->orX()
                        ->add($e->eq('user.email', ":$key"))
                        ->add($e->eq('user.phone', ":$key"))
                    )->setParameter($key, $value);
                    break;
                case 'isActive':
                    $qb->andWhere($e->eq('user.isActive', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'emailToken':
                    $qb->andWhere($e->eq('user.emailToken', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'passwordToken':
                    $qb->andWhere($e->eq('user.passwordToken', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'smsCode':
                    $qb->andWhere($e->eq('user.smsCode', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('user.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'isAdmin':
                    $qb->andWhere($e->eq('user.isAdmin', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'isEmailSubscribed':
                    $qb->andWhere($e->eq('user.isEmailSubscribed', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'search':
                    $qb->andWhere($e->orX()
                        ->add($e->like($e->lower('user.email'), ":$key"))
                        ->add($e->like($e->lower('user.phone'), ":$key"))
                        ->add($e->like($e->lower('user.country'), ":$key"))
                        ->add($e->like($e->lower('user.name'), ":$key"))
                        ->add($e->like($e->lower('user.surname'), ":$key"))
                    )->setParameter($key, '%' . mb_strtolower($value, 'utf8') . '%');
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('user.id'));

        return (int) $qb->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }
}
