<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserStatus;
use App\Service\EmailService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserEmailRESTController extends Controller
{
    public function postMeSend()
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $service = $this->get(UserService::class);
        $emailService = $this->get(EmailService::class);
        $em = $this->get('doctrine')->getManager();

        $user = $service->getUser();

        $user = $service->findOneByFilter([
            'id' => $user->getId(),
        ]);

        try {

            $user->refreshEmailCode();

            $em->persist($user);
            $em->flush();

            $emailService->sendVerificationEmail($user);

            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postSend(Request $request)
    {
        $trans = $this->get('translator');

        $content = json_decode($request->getContent(), true);
        if (!isset($content['email'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $email = mb_strtolower(trim($content['email']), 'utf8');

        $service = $this->get(UserService::class);
        $emailService = $this->get(EmailService::class);
        $em = $this->get('doctrine')->getManager();

        $user = $service->findOneByFilter([
            'email' => $email,
            'status' => UserStatus::CREATED
        ]);
        if ($user) {
            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_OK);
        }

        $user = $service->findOneByFilter([
            'email' => $email,
            'status' => UserStatus::REGISTRATION
        ]);
        if (!$user) {
            $user = new User();
            $user->setEmail($email);
            $user->setStatus(UserStatus::REGISTRATION);
        }

        try {

            $user->refreshEmailCode();

            $em->persist($user);
            $em->flush();

            $emailService->sendVerificationEmail($user);

            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postProviderSend(Request $request)
    {
        $trans = $this->get('translator');

        $content = json_decode($request->getContent(), true);
        if (!isset($content['email'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $email = mb_strtolower(trim($content['email']), 'utf8');

        $service = $this->get(UserService::class);
        $emailService = $this->get(EmailService::class);
        $em = $this->get('doctrine')->getManager();

        $user = $service->findOneByFilter([
            'email' => $email,
            'status' => UserStatus::CREATED
        ]);
        if ($user) {
            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_OK);
        }

        $user = $service->findOneByFilter([
            'email' => $email,
            'status' => UserStatus::REGISTRATION
        ]);
        if (!$user) {
            $user = new User();
            $user->setEmail($email);
            $user->setStatus(UserStatus::REGISTRATION);
        }

        try {

            $user->refreshEmailCode();

            $em->persist($user);
            $em->flush();

            $emailService->sendVerificationEmailForProvider($user);

            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postVerify(Request $request, $id)
    {
        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();
        $service = $this->get(UserService::class);

        $content = json_decode($request->getContent(), true);
        if (!isset($content['code'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $code = $content['code'];

        $user = $service->findOneByFilter([
            'id' => $id,
        ]);
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.not_found')
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        try {

            $isVerified = $user->getEmailCode() === $code;
            if (!$isVerified) {
                throw new \Exception($trans->trans('validation.sms_code_mismatch'), 400);
            }

            $user->setEmailCode(null);
            $user->setEmailVerified(true);

            if ($user->isPhoneVerified() || $user->isEmailVerified()) {
                $user->setIsActive(true);
            }

            $em->persist($user);
            $em->flush();

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAuthorized()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }
}
