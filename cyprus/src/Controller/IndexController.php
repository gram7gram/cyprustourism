<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function landing()
    {
        return $this->render('landing.html.twig');
    }

    public function provider()
    {
        return $this->render('provider.html.twig');
    }

    public function newInquiry()
    {
        return $this->render('inquiry-create.html.twig');
    }

    public function carRent()
    {
        return $this->render('car-rent.html.twig');
    }

    public function boatRent()
    {
        return $this->render('boat-rent.html.twig');
    }

    public function accommodationRent()
    {
        return $this->render('accommodation-rent.html.twig');
    }

    public function transfer()
    {
        return $this->render('transfer.html.twig');
    }

    public function login()
    {
        return $this->render('login.html.twig');
    }

    public function registerUser()
    {
        return $this->render('user/register.html.twig');
    }

    public function registerProvider()
    {
        return $this->render('provider/register.html.twig');
    }

    public function howItWorks()
    {
        return $this->render('how-it-works.html.twig');
    }

    public function faq()
    {
        return $this->render('faq.html.twig');
    }

    public function privacy()
    {
        return $this->render('privacy.html.twig');
    }

    public function terms()
    {
        return $this->render('terms.html.twig');
    }

    public function contactUs()
    {
        return $this->render('contact-us.html.twig');
    }

    public function logout()
    {
        return $this->redirect($this->generateUrl('index'));
    }

    public function redirectToIndex()
    {
        return $this->redirect($this->generateUrl('index'));
    }
}
