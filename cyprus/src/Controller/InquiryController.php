<?php

namespace App\Controller;

use App\Entity\InquiryType;
use App\Service\InquiryService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InquiryController extends Controller
{

    public function index()
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $userService = $this->get(UserService::class);

        if ($userService->getAdmin()) {
            return $this->render('admin/inquiries.html.twig');
        }

        if ($userService->getProvider()) {
            return $this->render('provider/my-inquiries.html.twig');
        }

        return $this->render('user/my-inquiries.html.twig');
    }

    public function show($id)
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $service = $this->get(InquiryService::class);
        $userService = $this->get(UserService::class);
        $provider = $userService->getProvider();

        $accessFilter = $service->getAccessFilter([], $id);

        $entity = $service->findOneByFilter($accessFilter);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        if ($provider) {
            return $this->render('provider/inquiry.html.twig', [
                'inquiry' => $entity
            ]);
        }

        return $this->render('user/inquiry.html.twig', [
            'inquiry' => $entity
        ]);
    }

    public function showByGuid($guid)
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $service = $this->get(InquiryService::class);
        $userService = $this->get(UserService::class);
        $provider = $userService->getProvider();

        $accessFilter = $service->getAccessFilter();
        $accessFilter['guid'] = $guid;

        $entity = $service->findOneByFilter($accessFilter);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        if ($provider) {
            return $this->render('provider/inquiry.html.twig', [
                'inquiry' => $entity
            ]);
        }

        return $this->render('user/inquiry.html.twig', [
            'inquiry' => $entity
        ]);
    }

    public function quoteCreate($guid)
    {
        $response = $this->redirectUnlessProvider();
        if ($response) return $response;

        $service = $this->get(InquiryService::class);

        $accessFilter = $service->getAccessFilter();
        $accessFilter['guid'] = $guid;

        $entity = $service->findOneByFilter($accessFilter);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        switch ($entity->getType()) {
            case InquiryType::TRANSFER:
                return $this->render('provider/transfer.html.twig', [
                    'inquiry' => $entity
                ]);
            case InquiryType::CAR_RENT:
                return $this->render('provider/car-rent.html.twig', [
                    'inquiry' => $entity
                ]);
            case InquiryType::BOAT_RENT:
                return $this->render('provider/boat-rent.html.twig', [
                    'inquiry' => $entity
                ]);
            case InquiryType::ACCOMMODATION_RENT:
                return $this->render('provider/accommodation-rent.html.twig', [
                    'inquiry' => $entity
                ]);
        }

        return $this->redirect($this->generateUrl('index'));
    }

    private function redirectUnlessAuthorized()
    {
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return $this->redirect($this->generateUrl('login'));
        }

        return null;
    }

    private function redirectUnlessProvider()
    {
        $userService = $this->get(UserService::class);
        if (!$userService->getProvider()) {
            return $this->redirect($this->generateUrl('login'));
        }

        return null;
    }
}
