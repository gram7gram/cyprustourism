<?php

namespace App\Controller;

use App\Service\BillingPlanService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BillingPlanRESTController extends Controller
{

    public function gets(Request $request)
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $filter = $request->get('filter', []);

        $service = $this->get(BillingPlanService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAuthorized()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }
}
