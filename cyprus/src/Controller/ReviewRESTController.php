<?php

namespace App\Controller;

use App\Entity\Review;
use App\Service\ReviewService;
use App\Service\SelectedQuoteService;
use App\Service\UserService;
use Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReviewRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $service = $this->get(ReviewService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serializeAdmin($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request, $id)
    {
        $id = intval($id);

        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $content = json_decode($request->getContent(), true);

        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();
        $service = $this->get(ReviewService::class);
        $quoteService = $this->get(SelectedQuoteService::class);

        $em->beginTransaction();
        try {

            $quote = $quoteService->findOneByFilter([
                'id' => $id,
            ]);
            if (!$quote) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            if ($quote->getInquiry()->getReview()) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $entity = $service->create($quote, $content);

            $em->commit();

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            $em->rollback();

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $id = intval($id);

        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $service = $this->get(ReviewService::class);
        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();

        /** @var SoftDeleteableFilter $soft */
        $soft = $em->getFilters()->getFilter('softdeleteable');

        $soft->disableForEntity(Review::class);

        $content = json_decode($request->getContent(), true);

        try {
            $entity = $service->findOneByFilter([
                'id' => $id,
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $soft->enableForEntity(Review::class);

            $service->update($entity, $content);

            $item = $service->serializeAdmin($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAuthorized()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }

    private function denyAccessUnlessAdmin()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getAdmin()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
