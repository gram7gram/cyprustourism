<?php

namespace App\Controller;

use App\Entity\UserStatus;
use App\Service\EmailService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $service = $this->get(UserService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $id = intval($id);

        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $user = $service->getUser();
        $admin = $service->getAdmin();
        if (!$admin) {
            if (!$user) {
                return new JsonResponse([
                    'message' => $trans->trans('validation.unauthorized')
                ], JsonResponse::HTTP_UNAUTHORIZED);
            }

            if ($user->getId() !== $id) {
                return new JsonResponse([
                    'message' => $trans->trans('validation.forbidden')
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        try {

            $user = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$user) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getMeAction()
    {
        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $user = $service->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        $user = $service->getUser();

        try {

            $user = $service->findOneByFilter([
                'id' => $user->getId()
            ]);
            if (!$user) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $service = $this->get(UserService::class);
        $emailService = $this->get(EmailService::class);

        try {

            $entity = $service->create($content);

            $item = $service->serialize($entity);

            $emailService->onUserRegistered($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $id = intval($id);

        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $user = $service->getUser();
        $admin = $service->getAdmin();

        try {
            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            switch ($entity->getStatus()) {
                case UserStatus::CREATED:

                    if (!$admin) {
                        if (!$user) {
                            return new JsonResponse([
                                'message' => $trans->trans('validation.unauthorized')
                            ], JsonResponse::HTTP_UNAUTHORIZED);
                        }

                        if ($user->getId() !== $id) {
                            return new JsonResponse([
                                'message' => $trans->trans('validation.forbidden')
                            ], JsonResponse::HTTP_FORBIDDEN);
                        }
                    }
            }

            $content = json_decode($request->getContent(), true);

            $service->update($entity, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putMeAction(Request $request)
    {
        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $user = $service->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        $content = json_decode($request->getContent(), true);

        $user = $service->getUser();
        try {

            $user = $service->findOneByFilter([
                'id' => $user->getId()
            ]);
            if (!$user) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $service->update($user, $content);

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAdmin()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        $admin = $userService->getAdmin();
        if (!$admin) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
