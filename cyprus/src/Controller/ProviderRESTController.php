<?php

namespace App\Controller;

use App\Service\EmailService;
use App\Service\ProviderService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProviderRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $service = $this->get(ProviderService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $id = intval($id);

        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $trans = $this->get('translator');
        $service = $this->get(ProviderService::class);

        try {

            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getMeAction()
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $trans = $this->get('translator');
        $service = $this->get(ProviderService::class);
        $provider = $this->get(UserService::class)->getProvider();

        try {

            $entity = $service->findOneByFilter([
                'id' => $provider->getId()
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $service = $this->get(ProviderService::class);
        $email = $this->get(EmailService::class);

        try {

            $entity = $service->create($content);

            $item = $service->serialize($entity);

            $email->onProviderRegistered($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $id = intval($id);

        $response = $this->denyAccessUnlessAdminOrProvider();
        if ($response) return $response;

        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);

        if (!$userService->getAdmin()) {
            if ($userService->getProvider()->getId() !== $id) {
                return new JsonResponse([
                    'message' => $trans->trans('validation.forbidden')
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $service = $this->get(ProviderService::class);
        $content = json_decode($request->getContent(), true);

        try {
            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $service->update($entity, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putMeAction(Request $request)
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $trans = $this->get('translator');
        $provider = $this->get(UserService::class)->getProvider();

        $service = $this->get(ProviderService::class);
        $content = json_decode($request->getContent(), true);

        try {
            $entity = $service->findOneByFilter([
                'id' => $provider->getId()
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $service->update($entity, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAdmin()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        $admin = $userService->getAdmin();
        if (!$admin) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }

    private function denyAccessUnlessProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getProvider()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }

    private function denyAccessUnlessAdminOrProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!($user->isAdmin() || $userService->getProvider())) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
