<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SitemapController extends Controller
{
    public function index()
    {
        $router = $this->get('router');

        $urls = [
            $router->generate('index'),
            $router->generate('faq_index'),
            $router->generate('contact_index'),
            $router->generate('how_index'),
            $router->generate('terms_index'),
            $router->generate('privacy_index'),
            $router->generate('inquiry_create_unauthorized_index'),
            $router->generate('car_rent_index'),
            $router->generate('boat_rent_index'),
            $router->generate('accommodation_rent_index'),
            $router->generate('transfer_index'),
            $router->generate('register_provider_index'),
            $router->generate('register_user_index'),
            $router->generate('login'),
        ];

        $updatedAt = date('Y-m-d');

        return $this->render('sitemap.xml.twig', [
            'urls' => $urls,
            'updatedAt' => $updatedAt
        ]);
    }
}
