<?php

namespace App\Controller;

use App\Entity\Category;
use App\Service\CategoryService;
use App\Service\ProviderService;
use App\Service\SelectedQuoteService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MembersAreaController extends Controller
{

    public function inquiryCosts()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/inquiry-costs.html.twig');
    }

    public function categories()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/categories.html.twig');
    }

    public function categoryNew()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        $entity = new Category();

        return $this->render('admin/category-show.html.twig', [
            'category' => $entity
        ]);
    }

    public function categoryShow($id)
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        $service = $this->get(CategoryService::class);

        $entity = $service->findOneByFilter([
            'id' => $id
        ]);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('admin/category-show.html.twig', [
            'category' => $entity
        ]);
    }

    public function providers()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/providers.html.twig');
    }

    public function providerShow($id)
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        $service = $this->get(ProviderService::class);

        $entity = $service->findOneByFilter([
            'id' => $id
        ]);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('admin/profile-provider.html.twig', [
            'provider' => $entity
        ]);
    }

    public function users()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/users.html.twig');
    }

    public function userShow($id)
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        $userService = $this->get(UserService::class);

        $user = $userService->findOneByFilter([
            'id' => $id
        ]);
        if (!$user) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('admin/profile-user.html.twig', [
            'user' => $user
        ]);
    }

    public function reviews()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/reviews.html.twig');
    }

    public function invoices()
    {
        $response = $this->redirectUnlessAdmin();
        if ($response) return $response;

        return $this->render('admin/invoices.html.twig');
    }

    public function billing()
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $provider = $this->get(UserService::class)->getProvider();
        if (!$provider) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('provider/billing.html.twig');
    }

    public function profile()
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $provider = $this->get(UserService::class)->getProvider();
        if ($provider) {
            return $this->render('provider/my-profile.html.twig');
        }

        return $this->render('user/my-profile.html.twig');
    }

    public function reviewCreate($id)
    {
        $response = $this->redirectUnlessAuthorized();
        if ($response) return $response;

        $selectedQuoteService = $this->get(SelectedQuoteService::class);
        $userService = $this->get(UserService::class);

        $user = $userService->getUser();

        if ($userService->getProvider()) {
            return $this->redirect($this->generateUrl('index'));
        }

        $selectedQuote = $selectedQuoteService->findOneByFilter([
            'inquiryCreator' => $user->getId(),
            'id' => $id,
        ]);
        if (!$selectedQuote || $selectedQuote->getReview()) {
            return $this->redirect($this->generateUrl('index'));
        }

        $reviewedProvider = $selectedQuote->getQuote()->getProvider();

        return $this->render('user/review.html.twig', [
            'provider' => $reviewedProvider
        ]);
    }

    private function redirectUnlessAuthorized()
    {
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return $this->redirect($this->generateUrl('login'));
        }

        return null;
    }

    private function redirectUnlessAdmin()
    {
        $userService = $this->get(UserService::class);
        $admin = $userService->getAdmin();
        if (!$admin) {
            return $this->redirect($this->generateUrl('login'));
        }

        return null;
    }
}
