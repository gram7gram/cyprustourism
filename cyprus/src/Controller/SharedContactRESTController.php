<?php

namespace App\Controller;

use App\Entity\SharedContactStatus;
use App\Service\SharedContactService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SharedContactRESTController extends Controller
{

    public function getsForProviderAction(Request $request, $id)
    {
        $response = $this->denyAccessUnlessProviderOrUser();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $filter['provider'] = $id;
        $filter['status'] = SharedContactStatus::CREATED;

        $service = $this->get(SharedContactService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getsForUserAction(Request $request, $id)
    {
        $response = $this->denyAccessUnlessProviderOrUser();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $filter['user'] = $id;
        $filter['status'] = SharedContactStatus::CREATED;

        $service = $this->get(SharedContactService::class);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getsForMeAction(Request $request)
    {
        $response = $this->denyAccessUnlessProviderOrUser();
        if ($response) return $response;

        $service = $this->get(SharedContactService::class);

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $accessFilter = $service->getAccessFilter();

        $filter = array_merge($filter, $accessFilter);

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getForMeAction($id)
    {

        $response = $this->denyAccessUnlessProviderOrUser();
        if ($response) return $response;

        $id = intval($id);

        $trans = $this->get('translator');
        $service = $this->get(SharedContactService::class);

        $accessFilter = $service->getAccessFilter($id);

        try {

            $entity = $service->findOneByFilter($accessFilter);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessProviderOrUser()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }
}
