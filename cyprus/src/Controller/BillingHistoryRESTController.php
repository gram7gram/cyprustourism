<?php

namespace App\Controller;

use App\Service\BillingHistoryService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BillingHistoryRESTController extends Controller
{

    public function getsMe(Request $request)
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $userService = $this->get(UserService::class);
        $service = $this->get(BillingHistoryService::class);

        $filter['provider'] = $userService->getProvider()->getId();

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'page' => $page,
                'limit' => $limit,
                'total' => $total,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getProvider()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
