<?php

namespace App\Controller;

use App\Entity\BillingHistory;
use App\Entity\ProviderInvoiceStatus;
use App\Entity\ProviderInvoiceType;
use App\Service\BillingHistoryService;
use App\Service\BillingPlanService;
use App\Service\EmailService;
use App\Service\PaypalService;
use App\Service\PdfService;
use App\Service\ProviderService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BillingController extends Controller
{

    public function purchasePayPal($id)
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $trans = $this->get('translator');
        $service = $this->get(BillingPlanService::class);
        $provider = $this->get(UserService::class)->getProvider();
        $paypal = $this->get(PaypalService::class);

        try {
            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $payment = $paypal->contactPayPal($entity, $provider);

            return new JsonResponse([
                'next' => $payment->getApprovalLink()
            ]);
        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function purchaseBank($id)
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $trans = $this->get('translator');
        $provider = $this->get(UserService::class)->getProvider();
        $service = $this->get(BillingPlanService::class);
        $pdf = $this->container->get(PdfService::class);
        $email = $this->container->get(EmailService::class);

        try {
            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $invoice = $pdf->generateInvoice($provider, $entity,
                ProviderInvoiceType::BANK,
                ProviderInvoiceStatus::UNPAID);

            $email->onInvoiceBankPaid($provider->getUser(), $invoice);

            $item = $service->serialize($invoice);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);
        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function beforeSuccess($id, $providerId)
    {
        $service = $this->get(BillingPlanService::class);
        $historyService = $this->get(BillingHistoryService::class);
        $providerService = $this->get(ProviderService::class);
        $pdf = $this->container->get(PdfService::class);
        $email = $this->container->get(EmailService::class);
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');

        try {
            $provider = $providerService->findOneByFilter([
                'id' => $providerId
            ]);
            if (!$provider) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $entity = $service->findOneByFilter([
                'id' => $id
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $provider->setCredits($provider->getCredits() + $entity->getCredits());

            $em->persist($provider);

            $historyService->create($provider, $entity->getCredits(), BillingHistory::PLUS, null, false);

            $providerService->onCreditsAvailable($provider, false);

            $em->flush();

            $invoice = $pdf->generateInvoice($provider, $entity,
                ProviderInvoiceType::PAYPAL,
                ProviderInvoiceStatus::PAID);

            $email->onInvoicePayPalPaid($provider->getUser(), $invoice);

            return $this->redirect($this->generateUrl('billing_plan_paypal_purchase_success', [
                'id' => $id
            ]));
        } catch (\Exception $e) {
            return $this->redirect($this->generateUrl('billing_plan_paypal_purchase_failure', [
                'id' => $id,
                'providerId' => $providerId
            ]));
        }
    }

    public function success($id)
    {
        $service = $this->get(BillingPlanService::class);

        $entity = $service->findOneByFilter([
            'id' => $id
        ]);
        if (!$entity) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('provider/billing-success.html.twig', [
            'plan' => $entity
        ]);
    }

    public function failure()
    {
        return $this->render('provider/billing-failure.html.twig');
    }

    private function denyAccessUnlessProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        if (!$userService->getUser()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getProvider()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
