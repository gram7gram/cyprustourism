<?php

namespace App\Controller;

use App\Service\InquiryService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class InquiryRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $provider = $this->get(UserService::class)->getProvider();
        $service = $this->get(InquiryService::class);

        try {

            $accessFilter = $service->getAccessFilter($filter);

            $total = $service->countByFilter($accessFilter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($accessFilter, $page, $limit);

                $items = $service->serialize($entities);

                $service->findMetadata($items, $provider);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $id = intval($id);

        $trans = $this->get('translator');
        $service = $this->get(InquiryService::class);

        try {

            $accessFilter = $service->getAccessFilter([], $id);

            $entity = $service->findOneByFilter($accessFilter);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serializeOneWithCollections($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getByGuid($guid)
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $trans = $this->get('translator');
        $service = $this->get(InquiryService::class);

        try {

            $accessFilter = $service->getAccessFilter();
            $accessFilter['guid'] = $guid;

            $entity = $service->findOneByFilter($accessFilter);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $item = $service->serializeOneWithCollections($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {
        $response = $this->denyAccessIfProvider();
        if ($response) return $response;

        $content = json_decode($request->getContent(), true);

        $service = $this->get(InquiryService::class);

        try {

            $entity = $service->create($content);

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $id = intval($id);

        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $userService = $this->get(UserService::class);
        $service = $this->get(InquiryService::class);
        $trans = $this->get('translator');

        $currentUser = $userService->getUser();

        $content = json_decode($request->getContent(), true);

        try {

            $accessFilter = $service->getAccessFilter([], $id);

            //creator can be empty
            $entity = $service->findOneByFilter($accessFilter);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            if ($currentUser && !$userService->getAdmin()) {
                $creator = $entity->getCreator();
                if ($creator && $creator->getId() !== $currentUser->getId()) {
                    throw new \Exception($trans->trans('validation.forbidden'), 403);
                }
            }

            $service->update($entity, $content);

            $item = $service->serializeOneWithCollections($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putGuidAction(Request $request, $guid)
    {
        $userService = $this->get(UserService::class);
        $service = $this->get(InquiryService::class);
        $trans = $this->get('translator');

        $currentUser = $userService->getUser();

        $content = json_decode($request->getContent(), true);

        try {
            //creator can be empty
            $entity = $service->findOneByFilter([
                'guid' => $guid
            ]);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            if ($currentUser && !$userService->getAdmin()) {
                $creator = $entity->getCreator();
                if ($creator && $creator->getId() !== $currentUser->getId()) {
                    throw new \Exception($trans->trans('validation.forbidden'), 403);
                }
            }

            $service->update($entity, $content);

            if (!$currentUser) {
                $item = $service->serialize($entity);
            } else {
                $item = $service->serializeOneWithCollections($entity);
            }

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteAction($id)
    {

        $response = $this->denyAccessUnlessAdmin();
        if ($response) return $response;

        $id = intval($id);

        $trans = $this->get('translator');
        $service = $this->get(InquiryService::class);
        $userService = $this->get(UserService::class);

        try {

            $accessFilter = [
                'id' => $id
            ];

            if ($userService->getAdmin()) {
                $accessFilter['creator'] = $userService->getUser()->getId();
            }

            $entity = $service->findOneByFilter($accessFilter);
            if (!$entity) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $service->remove($entity);

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAdmin()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        $admin = $userService->getAdmin();
        if (!$admin) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }

    private function denyAccessUnlessAuthorized()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }

    private function denyAccessIfProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $provider = $userService->getProvider();
        if ($provider) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
