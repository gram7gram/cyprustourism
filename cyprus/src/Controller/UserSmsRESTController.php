<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserStatus;
use App\Service\SmsService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserSmsRESTController extends Controller
{
    public function postSend(Request $request)
    {
        $trans = $this->get('translator');

        $content = json_decode($request->getContent(), true);
        if (!isset($content['phone'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $phone = preg_replace('/[^0-9]/', '', $content['phone']);
        $dialCode = preg_replace('/[^0-9]/', '', $content['dialCode']);

        $service = $this->get(UserService::class);
        $sms = $this->get(SmsService::class);
        $em = $this->get('doctrine')->getManager();

        $user = $service->findOneByFilter([
            'phone' => $phone,
            'status' => UserStatus::CREATED
        ]);
        if ($user) {

            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_OK);
        }

        $user = $service->findOneByFilter([
            'phone' => $phone,
            'status' => UserStatus::REGISTRATION
        ]);
        if (!$user) {
            $user = new User();
            $user->setStatus(UserStatus::REGISTRATION);
        }

        try {

            $user->setPhone($phone);
            $user->setDialCode($dialCode);

            $em->persist($user);
            $em->flush();

            $sms->sendVerificationCode($user);

            $item = $service->serialize($user);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postMeSend()
    {
        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $sms = $this->get(SmsService::class);

        $user = $service->findOneByFilter([
            'id' => $service->getUser()->getId()
        ]);
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.not_found')
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        try {

            $sms->sendVerificationCode($user);

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postVerify(Request $request, $id)
    {
        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();
        $service = $this->get(UserService::class);
        $smsService = $this->get(SmsService::class);

        $content = json_decode($request->getContent(), true);
        if (!isset($content['code'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $code = $content['code'];

        $user = $service->findOneByFilter([
            'id' => $id,
        ]);
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.not_found')
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        try {

            $isVerified = $smsService->verifyCode($user, $code);

            if (!$isVerified) {
                throw new \Exception($trans->trans('validation.sms_code_mismatch'), 400);
            }

            $user->setPhoneVerified(true);

            if ($user->isPhoneVerified() || $user->isEmailVerified()) {
                $user->setIsActive(true);
            }

            $em->persist($user);
            $em->flush();

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
