<?php

namespace App\Controller;

use App\Service\ProviderInvoiceService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProviderInvoiceRESTController extends Controller
{

    public function getsMe()
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $service = $this->get(ProviderInvoiceService::class);
        $provider = $this->get(UserService::class)->getProvider();

        $filter = [
            'provider' => $provider->getId()
        ];

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);

        if (!$userService->getUser()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getProvider()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
