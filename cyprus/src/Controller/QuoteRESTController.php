<?php

namespace App\Controller;

use App\Service\InquiryCostService;
use App\Service\InquiryService;
use App\Service\QuoteService;
use App\Service\SelectedQuoteService;
use App\Service\SharedContactService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QuoteRESTController extends Controller
{

    public function getsAction(Request $request, $inquiryId)
    {
        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $inquiryId = intval($inquiryId);

        $filter = $request->get('filter', []);
        $limit = intval($request->get('limit', 10));
        $page = intval($request->get('page', 1));

        $page = $page >= 0 ? $page : 1;
        $limit = $limit >= 0 ? $limit : 10;

        $service = $this->get(QuoteService::class);

        $accessFilter = $service->getAccessFilter();

        $filter = array_merge($filter, $accessFilter);
        $filter['inquiry'] = $inquiryId;

        try {

            $total = $service->countByFilter($filter);
            $items = [];
            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postByGuid(Request $request, $inquiryGuid)
    {
        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $content = json_decode($request->getContent(), true);

        $trans = $this->get('translator');
        $service = $this->get(QuoteService::class);
        $inquiryService = $this->get(InquiryService::class);

        try {

            $accessFilter = $inquiryService->getAccessFilter();
            $accessFilter['guid'] = $inquiryGuid;

            $inquiry = $inquiryService->findOneByFilter($accessFilter);
            if (!$inquiry) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $entity = $service->create($inquiry, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteAction($inquiryId, $quoteId)
    {
        $quoteId = intval($quoteId);
        $inquiryId = intval($inquiryId);

        $response = $this->denyAccessUnlessProvider();
        if ($response) return $response;

        $userService = $this->get(UserService::class);
        $quoteService = $this->get(QuoteService::class);
        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();

        $provider = $userService->getProvider();

        $em->beginTransaction();
        try {

            $quote = $quoteService->findOneByFilter([
                'id' => $quoteId,
                'provider' => $provider->getId(),
                'inquiry' => $inquiryId
            ]);
            if (!$quote) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $quoteService->remove($quote);

            $em->commit();

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);

        } catch (\Exception $e) {

            $em->rollback();

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postSelectByGuid($inquiryGuid, $quoteId)
    {
        $quoteId = intval($quoteId);

        $response = $this->denyAccessUnlessAuthorized();
        if ($response) return $response;

        $userService = $this->get(UserService::class);
        $inquiryService = $this->get(InquiryService::class);
        $quoteService = $this->get(QuoteService::class);
        $service = $this->get(SelectedQuoteService::class);
        $contactService = $this->get(SharedContactService::class);
        $costService = $this->get(InquiryCostService::class);
        $trans = $this->get('translator');
        $em = $this->get('doctrine')->getManager();

        $user = $userService->getUser();

        $em->beginTransaction();
        try {
            $inquiry = $inquiryService->findOneByFilter([
                'guid' => $inquiryGuid,
                'creator' => $user->getId()
            ]);
            if (!$inquiry) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $quote = $quoteService->findOneByFilter([
                'id' => $quoteId,
                'inquiry' => $inquiry->getId()
            ]);
            if (!$quote) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $selectedCount = $service->countByFilter([
                'inquiry' => $inquiry->getId()
            ]);
            if ($selectedCount === 3) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $contactCost = $costService->findOneByFilter([
                'type' => $inquiry->getType()
            ]);
            if (!$contactCost) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $selectedByProviderCount = $service->countByFilter([
                'inquiry' => $inquiry->getId(),
                'provider' => $quote->getProvider()->getId()
            ]);

            $canChargeProvider = $selectedByProviderCount === 0;

            $entity = $service->create($inquiry, $quote);

            $contact = $contactService->create($entity, $contactCost->getCredits(), $canChargeProvider);

            $entity->setContact($contact);

            $em->persist($entity);
            $em->flush();

            $em->commit();

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            $em->rollback();

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function denyAccessUnlessAuthorized()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return null;
    }

    private function denyAccessUnlessProvider()
    {
        $trans = $this->get('translator');
        $userService = $this->get(UserService::class);
        $user = $userService->getUser();
        if (!$user) {
            return new JsonResponse([
                'message' => $trans->trans('validation.unauthorized')
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$userService->getProvider()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.forbidden')
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        return null;
    }
}
