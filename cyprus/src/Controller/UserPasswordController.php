<?php

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserPasswordController extends Controller
{

    public function setPassword($token)
    {
        $service = $this->get(UserService::class);

        $now = new \DateTime();

        $user = $service->findOneByFilter([
            'passwordToken' => $token,
        ]);
        if (!$user || $user->getPasswordTokenExpiresAt() <= $now) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('password-set.html.twig');
    }

    public function resetPassword()
    {
        return $this->render('password-reset.html.twig');
    }
}
