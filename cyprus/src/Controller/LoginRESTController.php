<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginRESTController extends Controller
{
    public function loginV1()
    {
        return $this->redirect($this->generateUrl('index'));
    }
}
