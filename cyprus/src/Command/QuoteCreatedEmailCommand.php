<?php

namespace App\Command;

use App\Entity\InquiryStatus;
use App\Entity\Quote;
use App\Service\EmailService;
use App\Service\QuoteService;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QuoteCreatedEmailCommand extends ContainerAwareCommand
{

    const NAME = 'emails:quote-created';

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $quoteService = $this->getContainer()->get(QuoteService::class);
        $emailService = $this->getContainer()->get(EmailService::class);

        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var Connection $con */
        $con = $em->getConnection();

        $quotes = $quoteService->findByFilter([
            'status' => InquiryStatus::OPEN,
            'isCreatorNotified' => false,
            'receiveEmailOnNewQuote' => true
        ], 1, 1);

        if (count($quotes) === 0) {
            $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' No quotes to process');
            exit(0);
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found quotes: ' . count($quotes));

        $ids = [];

        /** @var Quote $quote */
        foreach ($quotes as $quote) {

            $ids[] = $quote->getId();

            $inquiry = $quote->getInquiry();

            $user = $inquiry->getCreator();

            if ($user && $inquiry->isReceiveEmailOnNewQuote()) {
                $emailService->onQuoteCreated($user, $quote);
            }
        }

        $con->prepare(sprintf(
            'UPDATE quotes SET is_creator_notified = 1 WHERE id IN (%s)',
            implode(',', $ids)
        ))->execute();

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Completed');
    }
}