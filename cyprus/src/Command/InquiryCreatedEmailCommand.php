<?php

namespace App\Command;

use App\Entity\Inquiry;
use App\Entity\InquiryType;
use App\Entity\Provider;
use App\Entity\ProviderStatus;
use App\Service\EmailService;
use App\Service\InquiryService;
use App\Service\ProviderService;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InquiryCreatedEmailCommand extends ContainerAwareCommand
{

    const NAME = 'emails:inquiry-created';

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inquiryService = $this->getContainer()->get(InquiryService::class);
        $providerService = $this->getContainer()->get(ProviderService::class);
        $emailService = $this->getContainer()->get(EmailService::class);

        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var Connection $con */
        $con = $em->getConnection();

        $inquiries = $inquiryService->findByFilter([
            'areProvidersNotified' => false
        ]);

        if (count($inquiries) === 0) {
            $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' No inquiries to process');
            exit(0);
        }

        $carRentProviders = $providerService->findByFilter([
            'status' => ProviderStatus::APPROVED,
            'type' => InquiryType::CAR_RENT
        ]);

        $boatRentProviders = $providerService->findByFilter([
            'status' => ProviderStatus::APPROVED,
            'type' => InquiryType::BOAT_RENT
        ]);

        $accommodationRentProviders = $providerService->findByFilter([
            'status' => ProviderStatus::APPROVED,
            'type' => InquiryType::ACCOMMODATION_RENT
        ]);

        $transferProviders = $providerService->findByFilter([
            'status' => ProviderStatus::APPROVED,
            'type' => InquiryType::TRANSFER
        ]);

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found inquiries: ' . count($inquiries));

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found providers: ' . InquiryType::CAR_RENT
            . ' ' . count($carRentProviders));

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found providers: ' . InquiryType::BOAT_RENT
            . ' ' . count($boatRentProviders));

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found providers: ' . InquiryType::ACCOMMODATION_RENT
            . ' ' . count($accommodationRentProviders));

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Found providers: ' . InquiryType::TRANSFER
            . ' ' . count($transferProviders));

        $ids = [];

        /** @var Inquiry $inquiry */
        foreach ($inquiries as $inquiry) {

            $ids[] = $inquiry->getId();

            switch ($inquiry->getType()) {
                case InquiryType::TRANSFER:

                    /** @var Provider $provider */
                    foreach ($transferProviders as $provider) {
                        $emailService->onInquiryCreated($provider, $inquiry);
                    }
                    break;
                case InquiryType::CAR_RENT:

                    /** @var Provider $provider */
                    foreach ($carRentProviders as $provider) {
                        $emailService->onInquiryCreated($provider, $inquiry);
                    }
                    break;
                case InquiryType::BOAT_RENT:

                    /** @var Provider $provider */
                    foreach ($boatRentProviders as $provider) {
                        $emailService->onInquiryCreated($provider, $inquiry);
                    }
                    break;
                case InquiryType::ACCOMMODATION_RENT:

                    /** @var Provider $provider */
                    foreach ($accommodationRentProviders as $provider) {
                        $emailService->onInquiryCreated($provider, $inquiry);
                    }
                    break;
            }
        }

        $con->prepare(sprintf(
            'UPDATE inquiries SET are_providers_notified = 1 WHERE id IN (%s)',
            implode(',', $ids)
        ))->execute();

        $output->writeln('[+] ' . date('Y-m-d H:i:s')
            . ' Completed');
    }
}