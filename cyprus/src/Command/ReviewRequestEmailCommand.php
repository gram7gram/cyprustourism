<?php

namespace App\Command;

use App\Entity\Inquiry;
use App\Service\EmailService;
use App\Service\InquiryService;
use App\Service\SelectedQuoteService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReviewRequestEmailCommand extends ContainerAwareCommand
{

    const NAME = 'emails:review-request';

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emailService = $this->getContainer()->get(EmailService::class);
        $quoteService = $this->getContainer()->get(SelectedQuoteService::class);
        $inquiryService = $this->getContainer()->get(InquiryService::class);

        $em = $this->getContainer()->get('doctrine')->getManager();

        $repo = $em->getRepository(Inquiry::class);

        $inquiriesWithoutReviews = $repo->findIdsWithoutReviews();

        $output->writeln('[+] Found '. count($inquiriesWithoutReviews) . ' inquiries without reviews');

        foreach ($inquiriesWithoutReviews as $inquiryId) {

            $quotes = $quoteService->findByFilter([
                'inquiry' => $inquiryId
            ]);

            $output->writeln('[+] Found '. count($quotes) . ' quotes for inquiry ' . $inquiryId);

            if (!$quotes) continue;

            $inquiry = $inquiryService->findOneByFilter([
                'id' => $inquiryId
            ]);

            $user = $inquiry->getCreator();

            $emailService->onReviewRequest($user, $quotes);

            $em->clear();
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Completed');
    }
}