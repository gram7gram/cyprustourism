<?php

namespace App\Service;

use App\Entity\BillingHistory;
use App\Entity\InquirySelectedQuote;
use App\Entity\SharedContact;
use App\Entity\SharedContactStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SharedContactService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $id
     * @return array
     */
    public function getAccessFilter($id = null)
    {
        $userService = $this->container->get(UserService::class);

        $filter = [];

        if ($id) {
            $filter['id'] = $id;
        }

        $provider = $userService->getProvider();
        $user = $userService->getUser();
        if (!$userService->getAdmin()) {
            if ($provider) {
                $filter['provider'] = $provider->getId();
            } else {
                $filter['user'] = $user->getId();
            }
        }

        $filter['status'] = SharedContactStatus::CREATED;

        return $filter;
    }

    /**
     * @param InquirySelectedQuote $quote
     * @param int $cost
     * @param bool $canChargeProvider
     * @return SharedContact
     */
    public function create(InquirySelectedQuote $quote, int $cost, $canChargeProvider = true)
    {
        $provider = $quote->getQuote()->getProvider();
        $user = $quote->getInquiry()->getCreator();

        $entity = $this->findOneByFilter([
            'selectedQuote' => $quote->getId(),
            'provider' => $provider->getId(),
            'user' => $user->getId()
        ]);
        if ($entity) return $entity;

        $em = $this->container->get('doctrine')->getManager();
        $emailService = $this->container->get(EmailService::class);
        $historyService = $this->container->get(BillingHistoryService::class);

        $entity = new SharedContact();
        $entity->setProvider($provider);
        $entity->setUser($user);
        $entity->setCost($cost);
        $entity->setSelectedQuote($quote);

        if ($provider->hasCredits($cost)) {

            if ($canChargeProvider) {
                $provider->setCredits($provider->getCredits() - $cost);

                $historyService->create($provider, abs($cost), BillingHistory::MINUS, $quote->getInquiry(), false);
            }

            $entity->setStatus(SharedContactStatus::CREATED);
        } else {
            $entity->setStatus(SharedContactStatus::PENDING);
        }

        $em->persist($provider);
        $em->persist($entity);

        $em->flush();

        switch ($entity->getStatus()) {
            case SharedContactStatus::PENDING:
                $emailService->onContactPending($provider);
                break;
            case SharedContactStatus::CREATED:
                $emailService->onContactCreated($entity);
                break;
        }

        return $entity;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(SharedContact::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(SharedContact::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|SharedContact
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1', 'api_v1_shared_contact'])), true);
    }


}