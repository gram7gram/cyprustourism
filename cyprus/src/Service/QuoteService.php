<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Inquiry;
use App\Entity\InquiryParameters;
use App\Entity\InquiryStatus;
use App\Entity\Media;
use App\Entity\Price;
use App\Entity\Quote;
use App\Entity\QuoteCategory;
use App\Entity\QuoteMedia;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QuoteService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getAccessFilter($id = null)
    {
        $userService = $this->container->get(UserService::class);

        $provider = $userService->getProvider();
        $admin = $userService->getAdmin();
        $user = $userService->getUser();

        $accessFilter = [];

        if ($id) {
            $accessFilter['id'] = $id;
        }

        if (!$admin) {
            if ($provider) {
                $accessFilter['inquiryStatuses'] = implode(',', [InquiryStatus::OPEN, InquiryStatus::CLOSED]);
//                $accessFilter['inquiryType'] = $provider->getType();
                $accessFilter['provider'] = $provider->getId();
            } else {
                $accessFilter['creator'] = $user->getId();
            }
        }

        return $accessFilter;
    }

    /**
     * @param Inquiry $inquiry
     * @param $content
     *
     * @param bool $flush
     *
     * @return Quote
     * @throws \Exception
     */
    public function create(Inquiry $inquiry, $content, $flush = true)
    {
        $userService = $this->container->get(UserService::class);
        $provider = $userService->getProvider();

        $entity = new Quote();
        $entity->setInquiry($inquiry);
        $entity->setProvider($provider);
        $entity->setType($inquiry->getType());

        if (isset($content['parameters'])) {
            $this->handleParameters($entity, $content['parameters']);
        }

        if (isset($content['media']) && count($content['media']) > 0) {
            $this->handleMedia($entity, $content['media']);
        }

        if (isset($content['categories'])) {
            $this->handleCategories($entity, $content['categories']);
        }

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param Quote $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(Quote $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');

        $inquiry = $entity->getInquiry();

        if (isset($content['text'])) {
            $entity->setText($content['text']);
        }

        if (isset($content['price'])) {
            $this->handlePrice($entity, $content['price']);
        }

        $inquiry->setUpdatedAt(new \DateTime());

        if (!$entity->getParameters()) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        $em->persist($inquiry);
        $em->persist($entity);

        $flush && $em->flush();
    }

    private function handlePrice(Quote $entity, $content)
    {
        $em = $this->container->get('doctrine')->getManager();

        $price = $entity->getPrice();
        if (!$price) {
            $price = new Price();
        }
        $price->setPrice($content);

        $entity->setPrice($price);

        $em->persist($price);
    }

    private function handleCategories(Quote $entity, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $categoryService = $this->container->get(CategoryService::class);

        $categories = $categoryService->findByFilter([
            'ids' => $content
        ]);
        if (count($categories) !== count($content)) {
            throw new \Exception($trans->trans('validation.not_found'), 404);
        }

        /** @var Category $category */
        foreach ($categories as $category) {

            $quoteCategory = new QuoteCategory();
            $quoteCategory->setCategory($category);
            $quoteCategory->setQuote($entity);

            $em->persist($quoteCategory);

            $entity->getCategories()->add($quoteCategory);
        }
    }

    private function handleMedia(Quote $quote, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $mediaService = $this->container->get(MediaService::class);

        if (count($content) > 10) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        $media = $mediaService->findByFilter(['ids' => $content]);
        if (count($media) !== count($content)) {
            throw new \Exception($trans->trans('validation.not_found'), 404);
        }

        /** @var Media $item */
        foreach ($media as $item) {
            $quoteMedia = new QuoteMedia();
            $quoteMedia->setQuote($quote);
            $quoteMedia->setMedia($item);

            $em->persist($quoteMedia);

            $quote->getMedia()->add($quoteMedia);
        }
    }

    private function handleParameters(Quote $quote, $content)
    {
        $parametersService = $this->container->get(InquireParametersService::class);

        $parameters = $quote->getParameters();
        if (!$parameters) {
            $parameters = new InquiryParameters();
        }

        $parametersService->update($parameters, $content, false);

        $quote->setParameters($parameters);
    }

    public function remove(Quote $quote)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $selectedQuoteService = $this->container->get(SelectedQuoteService::class);

        $matches = $selectedQuoteService->countByFilter([
            'quote' => $quote->getId()
        ]);
        if ($matches > 0) {
            throw new \Exception($trans->trans('validation.quote_is_selected'), 400);
        }

        $quote->setDeletedAt(new \DateTime());

        $em->persist($quote);
        $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Quote::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Quote::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|Quote
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);

        if (is_array($content)) {
            foreach ($result as &$item) {
                $this->onPostSerialize($item);
            }
        } else {
            $this->onPostSerialize($result);
        }

        return $result;
    }

    public function onPostSerialize(&$content)
    {
        if (isset($content['media'])) {
            $content['media'] = array_map(function ($item) {

                $media = $item['media'];

                $media['id'] = $item['id'];

                return $media;
            }, $content['media']);
        }

        if (isset($content['categories'])) {
            $content['categories'] = array_map(function ($item) {

                $category = $item['category'];

                $category['id'] = $item['id'];

                return $category;
            }, $content['categories']);
        }
    }


}