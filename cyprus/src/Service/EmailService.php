<?php

namespace App\Service;

use App\Entity\Inquiry;
use App\Entity\Provider;
use App\Entity\ProviderInvoice;
use App\Entity\Quote;
use App\Entity\SharedContact;
use App\Entity\User;
use Mailgun\Mailgun;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onUserRegistered(User $user)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.user-registered.title');

        $body = $twig->render('emails/user-registered.html.twig');

        $this->send($user->getEmail(), $subject, $body);
    }

    public function onProviderRegistered(Provider $provider)
    {
        $userService = $this->container->get(UserService::class);
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $admins = $userService->findByFilter([
            'isAdmin' => true,
            'isEmailSubscribed' => true,
        ]);

        $subject = $trans->trans('email.provider-registered-admin.title');

        $body = $twig->render('emails/provider-registered-admin.html.twig', [
            'provider' => $provider
        ]);

        /** @var User $admin */
        foreach ($admins as $admin) {

            $this->send($admin->getEmail(), $subject, $body);

        }

        $subject = $trans->trans('email.provider-registered.title');

        $body = $twig->render('emails/provider-registered.html.twig', [
            'provider' => $provider
        ]);

        $this->send($provider->getUser()->getEmail(), $subject, $body);
    }

    public function onContactCreated(SharedContact $contact)
    {
        $userProvider = $contact->getProvider()->getUser();
        $user = $contact->getUser();

        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        if ($userProvider->isEmailSubscribed()) {

            $subject = $trans->trans('email.contact-created-provider.title');

            $body = $twig->render('emails/contact-created-provider.html.twig', [
                'inquiry' => $contact->getSelectedQuote()->getInquiry(),
                'user' => $contact->getUser()
            ]);

            $this->send($userProvider->getEmail(), $subject, $body);
        }

        if ($user->isEmailSubscribed()) {

            $subject = $trans->trans('email.contact-created-user.title');

            $body = $twig->render('emails/contact-created-user.html.twig', [
                'inquiry' => $contact->getSelectedQuote()->getInquiry(),
                'provider' => $contact->getProvider(),
            ]);

            $this->send($user->getEmail(), $subject, $body);
        }
    }

    public function onContactPending(Provider $provider)
    {
        $user = $provider->getUser();

        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.contact-pending.title');

        $body = $twig->render('emails/contact-pending.html.twig');

        if ($user->isEmailSubscribed()) {
            $this->send($user->getEmail(), $subject, $body);
        }
    }

    public function onReviewRequest(User $user, array $selectedQuotes)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.review-request.title');

        $body = $twig->render('emails/review-request.html.twig', [
            'selectedQuotes' => $selectedQuotes
        ]);

        if ($user->isEmailSubscribed()) {
            $this->send($user->getEmail(), $subject, $body);
        }
    }

    public function onInquiryCreated(Provider $provider, Inquiry $inquiry)
    {
        $user = $provider->getUser();
        $creator = $inquiry->getCreator();

        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        if ($user->isEmailSubscribed()) {

            $subject = $trans->trans('email.inquiry-created.title');

            $body = $twig->render('emails/inquiry-created.html.twig', [
                'provider' => $provider,
                'inquiry' => $inquiry
            ]);

            $this->send($user->getEmail(), $subject, $body);
        }

        if ($creator->isEmailSubscribed()) {

            $subject = $trans->trans('email.inquiry-created-user.title');

            $body = $twig->render('emails/inquiry-created-user.html.twig', [
                'inquiry' => $inquiry
            ]);

            $this->send($creator->getEmail(), $subject, $body);
        }
    }

    public function onQuoteCreated(User $user, Quote $quote)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.quote-created.title');

        $body = $twig->render('emails/quote-created.html.twig', [
            'user' => $user,
            'quote' => $quote
        ]);

        if ($user->isEmailSubscribed()) {
            $this->send($user->getEmail(), $subject, $body);
        }
    }

    public function sendVerificationEmail(User $user)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $provider = $user->getProvider();
        if ($provider) {

            $this->sendVerificationEmailForProvider($user);

            return;
        }

        $subject = $trans->trans('email.verify-email.title');

        $body = $twig->render('emails/verify-email.html.twig', [
            'user' => $user
        ]);

        $this->send($user->getEmail(), $subject, $body);
    }

    public function sendVerificationEmailForProvider(User $user)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.verify-email-provider.title');

        $body = $twig->render('emails/verify-email-provider.html.twig', [
            'user' => $user
        ]);

        $this->send($user->getEmail(), $subject, $body);
    }

    public function sendResetPassword(User $user)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.reset_password.title');

        $body = $twig->render('emails/reset-password.html.twig', [
            'user' => $user
        ]);

        $this->send($user->getEmail(), $subject, $body);
    }

    public function onInvoiceBankPaid(User $user, ProviderInvoice $invoice)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.invoice-bank-paid.title');

        $body = $twig->render('emails/invoice-bank-paid.html.twig', [
            'invoice' => $invoice
        ]);

        $this->send($user->getEmail(), $subject, $body);
    }

    public function onInvoicePayPalPaid(User $user, ProviderInvoice $invoice)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $subject = $trans->trans('email.invoice-paypal-paid.title');

        $body = $twig->render('emails/invoice-paypal-paid.html.twig', [
            'invoice' => $invoice
        ]);

        $this->send($user->getEmail(), $subject, $body);
    }

    private function send($email, $subject, $body)
    {

        $client = $this->createClient();

//        file_put_contents("/var/www/html/var/$subject." . md5(uniqid()) . '.html', $body);

        try {
            $domain = $this->container->getParameter('mailgun_domain');
            $sender = $this->container->getParameter('mailgun_sender_name');

            if ($domain && $sender) {
                $client->messages()->send($domain, [
                    'from' => $sender,
                    'to' => $email,
                    'subject' => $subject,
                    'html' => $body
                ]);
            }
        } catch (\Exception $e) {
//            throw new \Exception($trans->trans('email.send_error', [
//                '_MSG_' => $e->getMessage()
//            ]));
        }
    }

    private function createClient()
    {
        return $this->container->get(Mailgun::class);
    }

}