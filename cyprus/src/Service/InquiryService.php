<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Inquiry;
use App\Entity\InquiryCategory;
use App\Entity\InquiryParameters;
use App\Entity\InquiryStatus;
use App\Entity\InquiryType;
use App\Entity\Provider;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InquiryService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getAccessFilter($filter = [], $id = null)
    {
        $userService = $this->container->get(UserService::class);

        $provider = $userService->getProvider();
        $admin = $userService->getAdmin();
        $user = $userService->getUser();

        $accessFilter = array_merge([], $filter);

        if ($id) {
            $accessFilter['id'] = $id;
        }

        if (!$admin) {
            if ($provider) {

                $allowedStatuses = [InquiryStatus::OPEN, InquiryStatus::CLOSED];

                if (isset($filter['statuses'])) {

                    switch ($filter['statuses']) {
                        case InquiryStatus::CLOSED:
                        case InquiryStatus::OPEN:
                            $accessFilter['statuses'] = $filter['statuses'];
                            break;
                        default:
                            $accessFilter['statuses'] = implode(',', $allowedStatuses);
                    }
                } else {
                    $accessFilter['statuses'] = implode(',', $allowedStatuses);
                }

//                $accessFilter['type'] = $provider->getType();
                $accessFilter['provider'] = $provider->getId();
            } else {
                $accessFilter['creator'] = $user->getId();
            }
        }

        return $accessFilter;
    }

    /**
     * @param $content
     *
     * @param bool $flush
     *
     * @return Inquiry
     * @throws \Exception
     */
    public function create($content, $flush = true)
    {
        $trans = $this->container->get('translator');

        $expires = new \DateTime();
        $expires->modify('+1 month');

        $entity = new Inquiry();
        $entity->setExpiresAt($expires);

        if (isset($content['type'])) {

            switch ($content['type']) {
                case InquiryType::ACCOMMODATION_RENT:
                case InquiryType::BOAT_RENT:
                case InquiryType::CAR_RENT:
                case InquiryType::TRANSFER:
                    $entity->setType($content['type']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        if (isset($content['parameters'])) {
            $this->handleParameters($entity, $content['parameters']);
        }

        if (isset($content['categories'])) {
            $this->handleCategories($entity, $content['categories']);
        }

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param Inquiry $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(Inquiry $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $userService = $this->container->get(UserService::class);

        $currentUser = $userService->getUser();
        $isAdmin = $userService->getAdmin();

        $now = new \DateTime();

        $entity->setUpdatedAt($now);

        if (!$currentUser && isset($content['creator'])) {
            $user = $userService->findOneByFilter([
                'id' => $content['creator']
            ]);
            if (!$user) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $entity->setCreator($user);
        } else {
            $entity->setCreator($currentUser);
        }

        if (isset($content['status'])) {
            switch ($content['status']) {
                case InquiryStatus::OPEN:
                case InquiryStatus::PENDING:
                    $entity->setStatus($content['status']);
                    break;
                case InquiryStatus::CLOSED:

                    if (!$isAdmin && $entity->getCreator()) {
                        if ($currentUser !== $entity->getCreator()) {
                            throw new \Exception($trans->trans('validation.bad_request'), 400);
                        }
                    }

                    $entity->setStatus($content['status']);

                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        } else {
            if ($entity->getCreator()) {
                $entity->setStatus(InquiryStatus::OPEN);
            } else {
                $entity->setStatus(InquiryStatus::PENDING);
            }
        }

        if (isset($content['receiveEmailOnNewQuote'])) {
            $entity->setReceiveEmailOnNewQuote($content['receiveEmailOnNewQuote'] === true);
        }

        if (isset($content['text'])) {
            $entity->setText($content['text']);
        }

        if (isset($content['expiresAt'])) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $content['expiresAt']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $entity->setExpiresAt($date);
            $entity->setExtendCount($entity->getExtendCount() + 1);

            if ($entity->getExtendCount() > 1) {
                throw new \Exception($trans->trans('validation.inquiry_extend_limit_reached'), 400);
            }
        }

        if (!$entity->getParameters()) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        $em->persist($entity);

        $flush && $em->flush();
    }

    private function handleCategories(Inquiry $entity, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $categoryService = $this->container->get(CategoryService::class);

        $categories = $categoryService->findByFilter([
            'ids' => $content
        ]);
        if (count($categories) !== count($content)) {
            throw new \Exception($trans->trans('validation.not_found'), 404);
        }

        /** @var Category $category */
        foreach ($categories as $category) {

            $inquiryCategory = new InquiryCategory();
            $inquiryCategory->setCategory($category);
            $inquiryCategory->setInquiry($entity);

            $em->persist($inquiryCategory);

            $entity->getCategories()->add($inquiryCategory);
        }
    }

    private function handleParameters(Inquiry $entity, $content)
    {
        $parametersService = $this->container->get(InquireParametersService::class);

        $parameters = $entity->getParameters();
        if (!$parameters) {
            $parameters = new InquiryParameters();
        }

        $parametersService->update($parameters, $content, false);

        $entity->setParameters($parameters);
    }

    public function remove(Inquiry $entity, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        $entity->setDeletedAt(new \DateTime());

        $em->persist($entity);

        $flush && $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Inquiry::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Inquiry::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|Inquiry
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    /**
     * @param array $inquiries
     * @param Provider|null $provider
     */
    public function findMetadata(array &$inquiries, Provider $provider = null)
    {
        $ids = array_map(function ($item) {
            return $item['id'];
        }, $inquiries);

        $em = $this->container->get('doctrine')->getManager();

        $providerId = null;
        if ($provider) {
            $providerId = $provider->getId();
        }

        $metadata = $em->getRepository(Inquiry::class)->findMetadata($ids, $providerId);

        $metadataRegistry = [];
        foreach ($metadata as $meta) {
            $metadataRegistry[$meta['id']] = $meta;
        }

        foreach ($inquiries as &$inquiry) {
            $meta = $metadataRegistry[$inquiry['id']] ?? null;

            if ($meta) {
                $inquiry['quoteCount'] = $meta['quoteCount'];
                $inquiry['selectedQuoteCount'] = $meta['selectedQuoteCount'];
            }
        }
    }

    public function serializeOneWithCollections(Inquiry $entity)
    {
        $userService = $this->container->get(UserService::class);
        $quoteService = $this->container->get(QuoteService::class);
        $selectedQuoteService = $this->container->get(SelectedQuoteService::class);

        $provider = $userService->getProvider();

        $item = $this->serialize($entity);

        $quoteAccessFilter = [
            'inquiry' => $entity->getId()
        ];

        if ($provider) {
            $quoteAccessFilter['provider'] = $provider->getId();
        }

        //Get all quotes

        $quotes = $quoteService->findByFilter($quoteAccessFilter);

        $quotes = $quoteService->serialize($quotes);

        $item['quotes'] = $quotes;

        //Get selected quotes

        $selectedQuotes = $selectedQuoteService->findByFilter($quoteAccessFilter);

        $selectedQuotes = $selectedQuoteService->serialize($selectedQuotes);

        $item['selectedQuotes'] = $selectedQuotes;

        return $item;
    }

    public function serialize($content)
    {
        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);

        if (is_array($content)) {
            foreach ($result as &$item) {
                $this->onPostSerialize($item);
            }
        } else {
            $this->onPostSerialize($result);
        }

        return $result;
    }

    public function onPostSerialize(&$content)
    {
        $provider = $this->container->get(UserService::class)->getProvider();

        if ($provider) {
            unset($content['creator']);
            unset($content['receiveEmailOnNewQuote']);
        }

        if (isset($content['categories'])) {
            $content['categories'] = array_map(function ($item) {

                $category = $item['category'];

                $category['categoryId'] = $category['id'];

                $category['id'] = $item['id'];

                return $category;
            }, $content['categories']);
        }
    }

}