<?php

namespace App\Service;

use App\Entity\BillingHistory;
use App\Entity\Inquiry;
use App\Entity\Provider;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BillingHistoryService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Inquiry $inquiry
     * @param Provider $provider
     * @param string $value
     * @param string $type
     * @param bool $flush
     * @return BillingHistory
     */
    public function create(Provider $provider, string $value, string $type, Inquiry $inquiry = null, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        $history = new BillingHistory();
        $history->setProvider($provider);
        $history->setType($type);
        $history->setValue($value);
        $history->setInquiry($inquiry);

        $em->persist($history);

        $flush && $em->flush();

        return $history;
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(BillingHistory::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(BillingHistory::class)->countByFilter($filter);
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}