<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SmsService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function sendVerificationCode(User $user)
    {
        $isEnabled = $this->container->getParameter('twilio_enabled');
        if (!$isEnabled) return;

        if (!($user->getDialCode() && $user->getPhone())) return;

        $phone = substr($user->getPhone(), strlen($user->getDialCode()));

        $client = $this->createClient();

        $trans = $this->container->get('translator');

        $res = null;
        try {

            $res = $client->phoneVerificationStart($phone, $user->getDialCode(), 'sms');

        } catch (\Exception $e) {
            throw new \Exception($trans->trans('sms.send_error', [
                '_MSG_' => $e->getMessage()
            ]));
        }

        if (!$res->ok()) {
            throw new \Exception($trans->trans('sms.send_error', [
                '_MSG_' => $res->message()
            ]));
        }
    }

    /**
     * @param User $user
     * @param $code
     * @return bool
     * @throws \Exception
     */
    public function verifyCode(User $user, string $code)
    {
        $isEnabled = $this->container->getParameter('twilio_enabled');
        if (!$isEnabled) return true;

        if (!($user->getDialCode() && $user->getPhone())) return false;

        $phone = substr($user->getPhone(), strlen($user->getDialCode()));

        $client = $this->createClient();

        $trans = $this->container->get('translator');

        $res = null;
        try {

            $res = $client->phoneVerificationCheck($phone, $user->getDialCode(), $code);

        } catch (\Exception $e) {
            throw new \Exception($trans->trans('sms.send_error', [
                '_MSG_' => $e->getMessage()
            ]));
        }

        return $res->ok();
    }

    private function createClient()
    {
        $apiKey = $this->container->getParameter('twilio_verify_api_key');

        return new \Authy\AuthyApi($apiKey);
    }

}