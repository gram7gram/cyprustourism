<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\UserStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $content
     *
     * @param bool $flush
     *
     * @return User
     * @throws \Exception
     */
    public function create($content, $flush = true)
    {
        $entity = new User();
        $entity->setIsActive(false);

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param User $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(User $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $encoder = $this->container->get('security.password_encoder');
        $trans = $this->container->get('translator');
        $userService = $this->container->get(UserService::class);
        $currentUser = $userService->getUser();

        $isAdmin = $currentUser && $currentUser->isAdmin();

        if (isset($content['email'])) {
            $email = mb_strtolower(trim($content['email']), 'utf8');

            $isEmailChanged = $email !== $entity->getEmail();

            $entity->setEmail($email);

            if ($isEmailChanged) {
                $entity->setEmailVerified(false);

                $entity->refreshEmailCode();
            }
        }

        if (isset($content['phone'])) {
            $phone = preg_replace('/[^0-9]/', '', trim($content['phone']));

            $isPhoneChanged = $phone !== $entity->getPhone();

            $entity->setPhone($phone);

            if ($isPhoneChanged) {
                $entity->setPhoneVerified(false);
            }
        }

        if (isset($content['name'])) {
            $entity->setName(trim($content['name']));
        }

        if (isset($content['surname'])) {
            $entity->setSurname(trim($content['surname']));
        }

        if (isset($content['dialCode'])) {
            $entity->setDialCode(preg_replace('/[^0-9]/', '', trim($content['dialCode'])));
        }

        if (isset($content['country'])) {
            $entity->setCountry(trim($content['country']));
        }

        if (isset($content['isEmailSubscribed'])) {
            $entity->setIsEmailSubscribed($content['isEmailSubscribed'] === true);
        }

        if ($isAdmin && isset($content['isActive'])) {
            $isActive = $content['isActive'] === true;

            $entity->setIsActive($isActive);
        }

        if (isset($content['password'])) {

            if ($currentUser && !$isAdmin) {

                if (!isset($content['currentPassword'])) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }

                $isValid = $encoder->isPasswordValid($entity, $content['currentPassword']);
                if (!$isValid) {
                    throw new \Exception($trans->trans('validation.current_password_mismatch'), 400);
                }
            }

            $password = $encoder->encodePassword($entity, $content['password']);
            $entity->setPassword($password);
        }

        if ($entity->getEmail() === 'admin') {
            $entity->setIsActive(true);
        }

        $entity->setStatus(UserStatus::CREATED);

        $this->validate($entity);

        $em->persist($entity);

        $flush && $em->flush();
    }

    /**
     * @param User $entity
     *
     * @throws \Exception
     */
    private function validate(User $entity)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');

        if ($entity->getEmail()) {
            $match = $em->getRepository(User::class)->findOneBy([
                'email' => mb_strtolower($entity->getEmail(), 'utf8'),
                'status' => UserStatus::CREATED
            ]);
            if ($match && $match !== $entity) {
                throw new \Exception($trans->trans('validation.email_reserved'), 400);
            }
        }

        if ($entity->getPhone()) {
            $match = $em->getRepository(User::class)->findOneBy([
                'phone' => $entity->getPhone(),
                'status' => UserStatus::CREATED
            ]);
            if ($match && $match !== $entity) {
                throw new \Exception($trans->trans('validation.phone_reserved'), 400);
            }
        }
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(User::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(User::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|User
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    /**
     * @return \App\Entity\Provider|null
     */
    public function getProvider()
    {
        $user = $this->getUser();
        return $user ? $user->getProvider() : null;
    }

    /**
     * @return \App\Entity\User|null
     */
    public function getAdmin()
    {
        $user = $this->getUser();
        return $user && $user->isAdmin() ? $user : null;
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1', 'api_v1_user'])), true);
    }


}