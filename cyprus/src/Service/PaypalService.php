<?php

namespace App\Service;

use App\Entity\BillingPlan;
use App\Entity\Provider;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PaypalService
{

    const LOGIN_VIEW = 'Login';

    const PAYPAL_PM = 'paypal';

    /** @var ContainerInterface */
    private $container;

    /** @var ApiContext $apiContext */
    private $apiContext;

    /**
     * @param ContainerInterface $container
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct(ContainerInterface $container, string $clientId, string $clientSecret)
    {
        $this->container = $container;
        $this->setApiContext($clientId, $clientSecret);
    }

    public function contactPayPal(BillingPlan $plan, Provider $provider)
    {
        $router = $this->container->get('router');
        $host = $this->container->getParameter('project_host');
        $trans = $this->container->get('translator');
//        $locale = $this->container->get('request_stack')->getCurrentRequest()->getLocale();

        $currency = 'EUR';
        $price = $plan->getPrice();

        $forwardSuccess = $host . $router->generate('billing_plan_paypal_purchase_before_success', [
                'providerId' => $provider->getId(),
                'id' => $plan->getId()
            ]);

        $forwardFailure = $host . $router->generate('billing_plan_paypal_purchase_failure', [
                'providerId' => $provider->getId(),
                'id' => $plan->getId()
            ]);

//        try {
//            $profile = $this->createWebProfile($plan, $locale, self::LOGIN_VIEW);
//        } catch (\Exception $e) {
//
//            throw new \Exception($trans->trans('validation.paypal_payment_error', [
//                '_MSG_' => $e->getMessage()
//            ]), 500);
//        }

        $payer = new Payer();
        $payer->setPaymentMethod(self::PAYPAL_PM);

        $item = new Item();
        $item->setQuantity(1)
            ->setCurrency($currency)
            ->setPrice($price)
            ->setName($plan->getName())
            ->setSku($plan->getId());

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($price);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList);

        $redirectUrls = new RedirectUrls();
        $redirectUrls
            ->setReturnUrl($forwardSuccess)
            ->setCancelUrl($forwardFailure);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions([$transaction])
            ->setRedirectUrls($redirectUrls)
//            ->setExperienceProfileId($profile->getId())
        ;

        try {
            $payment->create($this->apiContext);
        } catch (\Exception $e) {

            throw new \Exception($trans->trans('validation.paypal_payment_error', [
                '_MSG_' => $e->getMessage()
            ]), 500);
        }

        return $payment;
    }

    /**
     * @param BillingPlan $plan
     * @param string $locale
     * @param string $view
     *
     * @return \PayPal\Api\CreateProfileResponse
     */
    private function createWebProfile(BillingPlan $plan, $locale, $view = self::LOGIN_VIEW)
    {
        $host = $this->container->getParameter('project_host');
        $trans = $this->container->get('translator');

        $title = $trans->trans('paypal.webview.title', [
            '_NAME_' => $plan->getName()
        ], null, $locale);

        $logo = $host . $trans->trans('paypal.webview.logo', [], null, $locale);

        $flowConfig = new \PayPal\Api\FlowConfig();
        $flowConfig->setLandingPageType($view);
        $flowConfig->setBankTxnPendingUrl($host);

        $presentation = new \PayPal\Api\Presentation();
        $presentation
            ->setLogoImage($logo)
            ->setBrandName($title)
            ->setLocaleCode($locale);

        $inputFields = new \PayPal\Api\InputFields();
        $inputFields->setAllowNote(false)
            ->setNoShipping(1)
            ->setAddressOverride(0);

        $webProfile = new \PayPal\Api\WebProfile();
        $webProfile->setFlowConfig($flowConfig)
            ->setPresentation($presentation)
            ->setInputFields($inputFields)
            ->setTemporary(true)
            ->setName(md5(json_encode([
                'title' => $title,
                'id' => $plan->getId(),
                'requestedAt' => date('Y-m-d H:i:s'),
            ])));

        return $webProfile->create($this->apiContext);
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    private function setApiContext(string $clientId, string $clientSecret)
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $this->apiContext->setConfig([
            'http.CURLOPT_CONNECTTIMEOUT' => 30,
        ]);

        if ($this->container->getParameter('payment_environment') === 'prod') {
            $this->apiContext->setConfig([
                'mode' => 'live',
            ]);
        }
    }
}