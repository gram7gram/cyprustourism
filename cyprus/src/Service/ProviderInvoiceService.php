<?php

namespace App\Service;

use App\Entity\BillingHistory;
use App\Entity\BillingPlan;
use App\Entity\Media;
use App\Entity\Provider;
use App\Entity\ProviderInvoice;
use App\Entity\ProviderInvoiceStatus;
use App\Entity\ProviderInvoiceType;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProviderInvoiceService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Provider $provider
     * @param BillingPlan $plan
     * @param Media $media
     * @param null $content
     * @param bool $flush
     * @return ProviderInvoice
     * @throws \Exception
     */
    public function create(Provider $provider, BillingPlan $plan, Media $media, $content = null, $flush = true)
    {
        $invoice = new ProviderInvoice();
        $invoice->setBillingPlan($plan);
        $invoice->setProvider($provider);
        $invoice->setMedia($media);

        $this->update($invoice, $content, $flush);

        return $invoice;
    }

    /**
     * @param ProviderInvoice $invoice
     * @param null $content
     * @param bool $flush
     * @throws \Exception
     */
    public function update(ProviderInvoice $invoice, $content = null, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $historyService = $this->container->get(BillingHistoryService::class);
        $admin = $this->container->get(UserService::class)->getAdmin();

        $isPaid = false;

        if (isset($content['number'])) {
            $invoice->setNumber($content['number']);
        }

        if (isset($content['type'])) {

            switch ($content['type']) {
                case ProviderInvoiceType::PAYPAL:
                case ProviderInvoiceType::BANK:
                    $invoice->setType($content['type']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        if (isset($content['status'])) {

            $isPaid = $content['status'] === ProviderInvoiceStatus::PAID
                && $invoice->getStatus() !== ProviderInvoiceStatus::PAID;

            switch ($content['status']) {
                case ProviderInvoiceStatus::UNPAID:
                case ProviderInvoiceStatus::PAID:
                case ProviderInvoiceStatus::CANCELED:
                    $invoice->setStatus($content['status']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        if ($admin) {
            if ($isPaid) {
                $credits = $invoice->getBillingPlan()->getCredits();
                $provider = $invoice->getProvider();

                $provider->setCredits($provider->getCredits() + $credits);

                $em->persist($provider);

                $historyService->create($provider, $credits, BillingHistory::PLUS, null, false);
            }
        }

        $em->persist($invoice);
        $flush && $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(ProviderInvoice::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(ProviderInvoice::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|ProviderInvoice
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}