<?php

namespace App\Service;

use App\Entity\BillingHistory;
use App\Entity\InquiryType;
use App\Entity\Provider;
use App\Entity\ProviderStatus;
use App\Entity\SharedContact;
use App\Entity\SharedContactStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProviderService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $content
     *
     * @param bool $flush
     *
     * @return Provider
     * @throws \Exception
     */
    public function create($content, $flush = true)
    {
        $trans = $this->container->get('translator');

        $entity = new Provider();

        if (isset($content['type'])) {

            switch ($content['type']) {
                case InquiryType::ACCOMMODATION_RENT:
                case InquiryType::BOAT_RENT:
                case InquiryType::CAR_RENT:
                case InquiryType::TRANSFER:
                    $entity->setType($content['type']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param Provider $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(Provider $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $userService = $this->container->get(UserService::class);
        $historyService = $this->container->get(BillingHistoryService::class);

        $isAdmin = $userService->getUser() && $userService->getUser()->isAdmin();

        if ($isAdmin) {
            if (isset($content['type'])) {

                switch ($content['type']) {
                    case InquiryType::ACCOMMODATION_RENT:
                    case InquiryType::BOAT_RENT:
                    case InquiryType::CAR_RENT:
                    case InquiryType::TRANSFER:
                        $entity->setType($content['type']);
                        break;
                    default:
                        throw new \Exception($trans->trans('validation.bad_request'), 400);
                }
            }

            if (isset($content['status'])) {

                switch ($content['status']) {
                    case ProviderStatus::CREATED:
                    case ProviderStatus::APPROVED:
                    case ProviderStatus::REJECTED:
                        $entity->setStatus($content['status']);
                        break;
                    default:
                        throw new \Exception($trans->trans('validation.bad_request'), 400);
                }
            }

            if (isset($content['credits'])) {
                if ($content['credits'] < 0) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }

                $areCreditsAdded = !$entity->hasCredits() && $content['credits'] > 0;

                if ($entity->hasCredits()) {
                    $delta = $entity->getCredits() - $content['credits'];

                    if ($delta < 0) {
                        $historyService->create($entity, abs($delta), BillingHistory::PLUS, null, false);
                    } elseif ($delta > 0) {
                        $historyService->create($entity, $delta, BillingHistory::MINUS, null, false);
                    }
                }

                $entity->setCredits($content['credits']);

                if ($areCreditsAdded) {
                    $this->onCreditsAvailable($entity, false);
                }
            }
        }

        if (isset($content['address'])) {
            $entity->setAddress(trim($content['address']));
        }

        if (isset($content['website'])) {
            $entity->setWebsite(trim($content['website']));
        }

        if (isset($content['companyName'])) {
            $entity->setCompanyName(trim($content['companyName']));
        }

        if (isset($content['companyId'])) {
            $entity->setCompanyId(trim($content['companyId']));
        }

        if (isset($content['paypalToken'])) {
            $entity->setPaypalToken($content['paypalToken']);
        }

        if (isset($content['user'])) {

            if (isset($content['user']['id'])) {
                $user = $userService->findOneByFilter([
                    'id' => $content['user']['id'],
                ]);
                if (!$user) {
                    throw new \Exception($trans->trans('validation.not_found'), 404);
                }

                $match = $this->findOneByFilter([
                    'user' => $user->getId()
                ]);
                if ($match && $match !== $entity) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }

                $entity->setUser($user);
            }

            $userService->update($entity->getUser(), $content['user']);
        }

        $em->persist($entity);

        $flush && $em->flush();
    }

    public function onCreditsAvailable(Provider $provider, $flush = true)
    {
        $contactService = $this->container->get(SharedContactService::class);
        $historyService = $this->container->get(BillingHistoryService::class);
        $em = $this->container->get('doctrine')->getManager();

        if (!$provider->getId()) return;

        $contacts = $contactService->findByFilter([
            'status' => SharedContactStatus::PENDING,
            'provider' => $provider->getId()
        ]);

        /** @var SharedContact $contact */
        foreach ($contacts as $contact) {

            $cost = $contact->getCost();

            $inquiry = $contact->getSelectedQuote()->getInquiry();

            if ($provider->hasCredits($cost)) {

                $provider->setCredits($provider->getCredits() - $cost);

                $historyService->create($provider, abs($cost), BillingHistory::MINUS, $inquiry, false);

                $contact->setStatus(SharedContactStatus::CREATED);

                $em->persist($provider);
                $em->persist($contact);
            }
        }

        $flush && $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Provider::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Provider::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|Provider
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1', 'api_v1_provider'])), true);
    }


}