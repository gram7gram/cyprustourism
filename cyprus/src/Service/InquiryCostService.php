<?php

namespace App\Service;

use App\Entity\InquiryCost;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InquiryCostService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function update(InquiryCost $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        $entity->setUpdatedAt(new \DateTime());

        if (isset($content['credits'])) {
            $entity->setCredits($content['credits']);
        }

        $em->persist($entity);

        $flush && $em->flush();
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(InquiryCost::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(InquiryCost::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     *
     * @return null|InquiryCost
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}