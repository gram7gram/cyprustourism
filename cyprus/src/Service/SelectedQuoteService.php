<?php

namespace App\Service;

use App\Entity\Inquiry;
use App\Entity\InquirySelectedQuote;
use App\Entity\InquiryStatus;
use App\Entity\Quote;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SelectedQuoteService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(Inquiry $inquiry, Quote $quote, $content = null)
    {
        $trans = $this->container->get('translator');

        $entity = $this->findOneByFilter([
            'inquiry' => $inquiry->getId(),
            'quote' => $quote->getId(),
        ]);
        if ($entity) return $entity;

        $quoteLimit = 3;

        $selected = $this->countByFilter([
            'inquiry' => $inquiry->getId()
        ]);
        if ($selected >= $quoteLimit) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        ++$selected;

        if ($selected === $quoteLimit) {
            $inquiry->setStatus(InquiryStatus::CLOSED);
        }

        $entity = new InquirySelectedQuote();
        $entity->setInquiry($inquiry);
        $entity->setQuote($quote);

        $inquiry->getSelectedQuotes()->add($entity);

        $this->update($entity, $content);

        return $entity;
    }

    /**
     * @param InquirySelectedQuote $entity
     * @param null $content
     * @param bool $flush
     */
    public function update(InquirySelectedQuote $entity, $content = null, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        $inquiry = $entity->getInquiry();

        $inquiry->setUpdatedAt(new \DateTime());

        $em->persist($inquiry);
        $em->persist($entity);

        $flush && $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(InquirySelectedQuote::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(InquirySelectedQuote::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|InquirySelectedQuote
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);

        if (is_array($content)) {
            foreach ($result as &$item) {
                $this->onPostSerialize($item);
            }
        } else {
            $this->onPostSerialize($result);
        }

        return $result;
    }

    public function onPostSerialize(&$content)
    {
        if (isset($content['quote']['media'])) {
            $content['quote']['media'] = array_map(function ($item) {

                $media = $item['media'];

                $media['id'] = $item['id'];

                return $media;
            }, $content['quote']['media']);
        }

        if (isset($content['quote']['categories'])) {
            $content['quote']['categories'] = array_map(function ($item) {

                $category = $item['category'];

                $category['id'] = $item['id'];

                return $category;
            }, $content['quote']['categories']);
        }
    }
}