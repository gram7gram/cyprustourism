<?php

namespace App\Service;

use App\Entity\DaytimeRange;
use App\Entity\HourlyRange;
use App\Entity\InquiryParameters;
use App\Entity\InquiryParametersSubtype;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InquireParametersService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $content
     *
     * @param bool $flush
     *
     * @return InquiryParameters
     * @throws \Exception
     */
    public function create($content, $flush = true)
    {
        $entity = new InquiryParameters();

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param InquiryParameters $parameters
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(InquiryParameters $parameters, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $categoryService = $this->container->get(CategoryService::class);

        if (isset($content['paymentType'])) {
            $parameters->setPaymentType($content['paymentType']);
        }

        if (isset($content['bedroomCount'])) {
            $parameters->setBedroomCount($content['bedroomCount']);
        }

        if (isset($content['dayCount'])) {
            $parameters->setDayCount($content['dayCount']);
        }

        if (isset($content['roomCount'])) {
            $parameters->setRoomCount($content['roomCount']);
        }

        if (isset($content['adultsCount'])) {
            $parameters->setAdultsCount($content['adultsCount']);
        }

        if (isset($content['childrenCount'])) {
            $parameters->setChildrenCount($content['childrenCount']);
        }

        if (isset($content['passengerCount'])) {
            $parameters->setPassengerCount($content['passengerCount']);
        }

        if (isset($content['driverAgeFrom'])) {
            $parameters->setDriverAgeFrom($content['driverAgeFrom']);
        }

        if (isset($content['distance'])) {
            $parameters->setDistance($content['distance']);
        }

        if (isset($content['duration'])) {
            $parameters->setDuration($content['duration']);
        }

        if (isset($content['origin'])) {
            $origin = $categoryService->findOneByFilter(['id' => $content['origin']]);
            if (!$origin) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $parameters->setOrigin($origin);
        }

        if (isset($content['destination'])) {
            $destination = $categoryService->findOneByFilter(['id' => $content['destination']]);
            if (!$destination) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $parameters->setDestination($destination);
        }

        if (isset($content['isFixedDate'])) {
            $parameters->setIsFixedDate($content['isFixedDate'] === true);
        }

        if (isset($content['rentalStart'])) {
            $date = \DateTime::createFromFormat('Y-m-d', $content['rentalStart']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $parameters->setRentalStart($date);
        }

        if (isset($content['rentalEnd'])) {
            $date = \DateTime::createFromFormat('Y-m-d', $content['rentalEnd']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $parameters->setRentalEnd($date);
        }

        if (isset($content['isReturn'])) {
            $parameters->setIsReturn($content['isReturn'] === true);
        }

        if (isset($content['returnAt'])) {
            $date = \DateTime::createFromFormat('Y-m-d', $content['returnAt']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $parameters->setReturnAt($date);
        }

        if (isset($content['departAt'])) {
            $date = \DateTime::createFromFormat('Y-m-d', $content['departAt']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $parameters->setDepartAt($date);
        }

        if (isset($content['isHourly'])) {
            $parameters->setIsHourly($content['isHourly'] === true);
        }

        if (isset($content['subtypes'])) {

            $subtypeRegistry = [];

            if ($parameters->getId()) {
                $subtypes = $em->getRepository(InquiryParametersSubtype::class)->findBy([
                    'parameters' => $parameters->getId()
                ]);

                /** @var InquiryParametersSubtype $subtype */
                foreach ($subtypes as $subtype) {
                    $subtypeRegistry[$subtype->getName()] = $subtype;
                }
            }

            foreach ($content['subtypes'] as $subtypeName) {

                if (!isset($subtypeRegistry[$subtypeName])) {

                    $subtype = new InquiryParametersSubtype();
                    $subtype->setParameters($parameters);
                    $subtype->setName($subtypeName);

                    $em->persist($subtype);

                    $parameters->addSubtype($subtype);
                } else {
                    unset($subtypeRegistry[$subtypeName]);
                }
            }

            foreach ($subtypeRegistry as $item) {
                $em->remove($item);
            }
        }

        if (isset($content['hourlyRanges'])) {

            $subtypeRegistry = [];

            if ($parameters->getId()) {
                $subtypes = $em->getRepository(HourlyRange::class)->findBy([
                    'parameters' => $parameters->getId()
                ]);

                /** @var HourlyRange $subtype */
                foreach ($subtypes as $subtype) {
                    $subtypeRegistry[$subtype->getName()] = $subtype;
                }
            }

            foreach ($content['hourlyRanges'] as $subtypeName) {

                if (!isset($subtypeRegistry[$subtypeName])) {

                    $subtype = new HourlyRange();
                    $subtype->setParameters($parameters);
                    $subtype->setName($subtypeName);

                    $em->persist($subtype);

                    $parameters->addHourlyRange($subtype);
                } else {
                    unset($subtypeRegistry[$subtypeName]);
                }
            }

            foreach ($subtypeRegistry as $item) {
                $em->remove($item);
            }
        }

        if (isset($content['daytimeRanges'])) {

            $subtypeRegistry = [];

            if ($parameters->getId()) {
                $subtypes = $em->getRepository(DaytimeRange::class)->findBy([
                    'parameters' => $parameters->getId()
                ]);

                /** @var DaytimeRange $subtype */
                foreach ($subtypes as $subtype) {
                    $subtypeRegistry[$subtype->getName()] = $subtype;
                }
            }

            foreach ($content['daytimeRanges'] as $subtypeName) {

                if (!isset($subtypeRegistry[$subtypeName])) {

                    $subtype = new DaytimeRange();
                    $subtype->setParameters($parameters);
                    $subtype->setName($subtypeName);

                    $em->persist($subtype);

                    $parameters->addDaytimeRange($subtype);
                } else {
                    unset($subtypeRegistry[$subtypeName]);
                }
            }

            foreach ($subtypeRegistry as $item) {
                $em->remove($item);
            }
        }

        $em->persist($parameters);

        $flush && $em->flush();
    }


}