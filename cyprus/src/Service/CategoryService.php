<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\CategoryGroup;
use App\Entity\InquiryType;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CategoryService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $content
     *
     * @param bool $flush
     *
     * @return Category
     * @throws \Exception
     */
    public function create($content, $flush = true)
    {
        $trans = $this->container->get('translator');

        $entity = new Category();

        if (isset($content['locale'])) {
            $entity->setLocale($content['locale']);
        } else {
            $entity->setLocale('en');
        }

        if (isset($content['type'])) {

            switch ($content['type']) {
                case InquiryType::ACCOMMODATION_RENT:
                case InquiryType::BOAT_RENT:
                case InquiryType::CAR_RENT:
                case InquiryType::TRANSFER:
                    $entity->setType($content['type']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        $this->update($entity, $content, $flush);

        return $entity;
    }

    /**
     * @param Category $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(Category $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $mediaService = $this->container->get(MediaService::class);

        if (isset($content['name'])) {
            $entity->setName($content['name']);
        }

        if (isset($content['extra'])) {
            $entity->setExtra($content['extra']);
        }

        if (isset($content['icon'])) {
            $media = $mediaService->findOneByFilter(['id' => $content['icon']]);
            if (!$media) {
                throw new \Exception($trans->trans('validation.not_found'), 404);
            }
            $entity->setIcon($media);
        }

        if (isset($content['group'])) {
            switch ($content['group']) {
                case CategoryGroup::EXTRAS:
                case CategoryGroup::LOCATION:
                case CategoryGroup::BOAT_TYPE:
                case CategoryGroup::CAR_TYPE:
                case CategoryGroup::DESTINATION:
                    $entity->setGroup($content['group']);
                    break;
                default:
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        if (isset($content['ordering'])) {
            $entity->setOrdering(intval($content['ordering']));
        }

        $match = $this->findOneByFilter([
            'locale' => $entity->getLocale(),
            'type' => $entity->getType(),
            'group' => $entity->getGroup(),
            'name' => $entity->getName(),
        ]);
        if ($match && $match !== $entity) {
            throw new \Exception($trans->trans('validation.non_unique_category'), 400);
        }

        $em->persist($entity);

        $flush && $em->flush();
    }

    public function remove(Category $category, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        $category->setDeletedAt(new \DateTime());

        $em->persist($category);

        $flush && $em->flush();
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Category::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Category::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param array $filter
     *
     * @return null|Category
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}