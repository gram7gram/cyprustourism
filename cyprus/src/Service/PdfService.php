<?php

namespace App\Service;

use App\Entity\BillingPlan;
use App\Entity\Media;
use App\Entity\Provider;
use App\Entity\ProviderInvoice;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PdfService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Provider $provider
     * @param BillingPlan $plan
     *
     * @param string $type
     * @param string $status
     * @return ProviderInvoice
     *
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function generateInvoice(Provider $provider, BillingPlan $plan, $type, $status)
    {
        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');
        $root = $this->container->getParameter('kernel.root_dir') . '/../public';
        $host = $this->container->getParameter('project_host');
        $em = $this->container->get('doctrine')->getManager();
        $invoiceService = $this->container->get(ProviderInvoiceService::class);

        $dir = '/invoices';
        $name = $provider->getId() . '-' . md5(uniqid() . $provider->getId()) . '.pdf';

        $link = $dir . '/' . $name;

        $number = 'PR' . $provider->getId() . '-' . 'PL' . $plan->getId() . '-' . date('Ymd');

        $logo = 'data:image/png;base64,' . base64_encode(file_get_contents($root . '/img/vacbids-logo-final-web@256.png'));
        $background = 'data:text/svg;base64,' . base64_encode(file_get_contents($root . '/img/bg-elements-6.svg'));

        $content = $twig->render('pdf/invoice.html.twig', [
            'logo' => $logo,
            'background' => $background,
            'provider' => $provider,
            'plan' => $plan,
            'number' => $number
        ]);

        try {
            $client = $this->createClient();

//            file_put_contents('/var/www/html/var/pdf.html', $content);

            $client->SetDefaultFont('DejaVuSans');
            $client->SetTitle($number);
            $client->SetAuthor($trans->trans('project_name'));
            $client->WriteHTML($content);

            $client->Output($root . $link, 'F');

        } catch (\Exception $e) {
            throw new \Exception($trans->trans('validation.invoice_generation_failed'), 500);
        }

        $hash = hash_file('md5', $root . $link);
        $size = filesize($root . $link);

        $entity = new Media();
        $entity->setSize($size);
        $entity->setMimeType('application/pdf');
        $entity->setName($number . '.pdf');
        $entity->setUrl($host . $link);
        $entity->setHash($hash);

        $em->persist($entity);

        $invoice = $invoiceService->create($provider, $plan, $entity, [
            'number' => $number,
            'type' => $type,
            'status' => $status,
        ], false);

        $em->flush();

        return $invoice;
    }

    private function createClient()
    {
        $root = $this->container->getParameter('kernel.root_dir') . '/../var';

        $mpdf = new \Mpdf\Mpdf([
            'tempDir' => $root,
            'orientation' => 'P',
            'margin_left' => 15,
            'margin_right' => 15,
            'margin_top' => 15,
            'margin_bottom' => 15,
        ]);

        $mpdf->SetDisplayMode('fullpage');

        return $mpdf;
    }

}