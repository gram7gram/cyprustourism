<?php

namespace App\Service;

use App\Entity\InquirySelectedQuote;
use App\Entity\Provider;
use App\Entity\Review;
use App\Entity\ReviewStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReviewService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param InquirySelectedQuote $quote
     * @param $content
     *
     * @return Review
     * @throws \Exception
     */
    public function create(InquirySelectedQuote $quote, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $userService = $this->container->get(UserService::class);

        $user = $userService->getUser();

        $provider = $quote->getQuote()->getProvider();

        $entity = new Review();
        $entity->setProvider($provider);
        $entity->setUser($user);

        $this->update($entity, $content, false);

        $inquiry = $quote->getInquiry();

        $inquiry->setReview($entity);

        $em->persist($inquiry);

        $quote->setReview($entity);

        $em->persist($quote);
        $em->flush();

        return $entity;
    }

    /**
     * @param Review $entity
     * @param $content
     *
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function update(Review $entity, $content, $flush = true)
    {
        $em = $this->container->get('doctrine')->getManager();
        $trans = $this->container->get('translator');
        $userService = $this->container->get(UserService::class);

        $canUpdatedRating = false;
        $isAdmin = $userService->getAdmin();
        $provider = $entity->getProvider();

        if (isset($content['isPublishAllowed'])) {
            $entity->setIsPublishAllowed($content['isPublishAllowed'] === true);
        }

        if (isset($content['text'])) {
            $entity->setText($content['text']);
        }

        if ($isAdmin) {

            if (isset($content['status'])) {
                $isApproved = $entity->getStatus() !== ReviewStatus::APPROVED
                    && $content['status'] === ReviewStatus::APPROVED;

                $isRejected = $entity->getStatus() !== ReviewStatus::REJECTED
                    && $content['status'] === ReviewStatus::REJECTED;

                $canUpdatedRating = $isApproved || $isRejected;

                switch ($content['status']) {
                    case ReviewStatus::CREATED:
                    case ReviewStatus::APPROVED:
                        $entity->setStatus($content['status']);
                        break;
                    case ReviewStatus::REJECTED:
                        $entity->setStatus($content['status']);
                        $entity->setDeletedAt(new \DateTime());
                        break;
                    default:
                        throw new \Exception($trans->trans('validation.bad_request'), 400);
                }
            }

            if (isset($content['isPublished'])) {
                $entity->setIsPublished($content['isPublished'] === true);
            }
        }

        if (isset($content['rating'])) {
            $value = intval($content['rating']);
            if (1 <= $value && $value <= 5) {
                $entity->setRating($value);
            } else {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }
        }

        if ($canUpdatedRating) {
            $this->updateRating($provider, $entity);
        }

        if ($entity->getStatus() !== ReviewStatus::APPROVED) {
            $entity->setIsPublished(false);
        }

        $em->persist($entity);

        $flush && $em->flush();
    }

    private function updateRating(Provider $provider, Review $newReview)
    {
        $em = $this->container->get('doctrine')->getManager();

        $items = $em->getRepository(Review::class)->findRatingForProvider($provider->getId());

        $count = count($items) + 1;
        $total = $newReview->getRating();
        foreach ($items as $item) {
            $total += $item['rating'];
        }

        $avg = round($total / $count, 2);

        $provider->setRating($avg);

        $em->persist($provider);
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Review::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Review::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @return array
     */
    public function getRandomPublic()
    {
        $items = $this->findByFilter([
            'status' => ReviewStatus::APPROVED,
            'isPublished' => true
        ], 1, 3);

        return $items;
    }

    /**
     * @param array $filter
     *
     * @return null|Review
     */
    public function findOneByFilter(array $filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    public function serialize($content)
    {
        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);

        return $result;
    }

    public function serializeAdmin($content)
    {
        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1', 'api_v1_provider'])), true);

        return $result;
    }

}