<?php

namespace App\Entity;

class ReviewStatus
{
    const CREATED = 'created';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';

}