<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="inquiry_parameters")
 * @ORM\Entity()
 */
class InquiryParameters
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $dayCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $roomCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $bedroomCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $adultsCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $childrenCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $passengerCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $driverAgeFrom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $paymentType;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $isHourly;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $isFixedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $rentalStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $rentalEnd;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $isReturn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $departAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $returnAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $distance;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $duration;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     *
     * @JMS\Groups("api_v1")
     */
    private $origin;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     *
     * @JMS\Groups("api_v1")
     */
    private $destination;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\InquiryParametersSubtype", mappedBy="parameters")
     */
    private $subtypes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\DaytimeRange", mappedBy="parameters")
     */
    private $daytimeRanges;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\HourlyRange", mappedBy="parameters")
     */
    private $hourlyRanges;

    public function __construct()
    {
        $this->subtypes = new ArrayCollection();
        $this->hourlyRanges = new ArrayCollection();
        $this->daytimeRanges = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getHourlyRanges()
    {
        if (is_null($this->hourlyRanges)) {
            $this->hourlyRanges = new ArrayCollection();
        }

        return $this->hourlyRanges;
    }

    /**
     * @return ArrayCollection
     */
    public function getDaytimeRanges()
    {
        if (is_null($this->daytimeRanges)) {
            $this->daytimeRanges = new ArrayCollection();
        }

        return $this->daytimeRanges;
    }

    /**
     * @return ArrayCollection
     */
    public function getSubtypes()
    {
        if (is_null($this->subtypes)) {
            $this->subtypes = new ArrayCollection();
        }

        return $this->subtypes;
    }

    /**
     * @param InquiryParametersSubtype $entity
     */
    public function addSubtype(InquiryParametersSubtype $entity)
    {
        $this->getSubtypes()->add($entity);
    }

    /**
     * @param HourlyRange $entity
     */
    public function addHourlyRange(HourlyRange $entity)
    {
        $this->getHourlyRanges()->add($entity);
    }

    /**
     * @param DaytimeRange $entity
     */
    public function addDaytimeRange(DaytimeRange $entity)
    {
        $this->getDaytimeRanges()->add($entity);
    }

    /**
     * @return bool
     */
    public function isHourly(): ?bool
    {
        return $this->isHourly;
    }

    /**
     * @param bool $isHourly
     */
    public function setIsHourly(?bool $isHourly): void
    {
        $this->isHourly = $isHourly;
    }

    /**
     * @return int
     */
    public function getBedroomCount(): ?int
    {
        return $this->bedroomCount;
    }

    /**
     * @param int $bedroomCount
     */
    public function setBedroomCount(?int $bedroomCount): void
    {
        $this->bedroomCount = $bedroomCount;
    }

    /**
     * @return int
     */
    public function getAdultsCount(): ?int
    {
        return $this->adultsCount;
    }

    /**
     * @param int $adultsCount
     */
    public function setAdultsCount(?int $adultsCount): void
    {
        $this->adultsCount = $adultsCount;
    }

    /**
     * @return int
     */
    public function getChildrenCount(): ?int
    {
        return $this->childrenCount;
    }

    /**
     * @param int $childrenCount
     */
    public function setChildrenCount(?int $childrenCount): void
    {
        $this->childrenCount = $childrenCount;
    }

    /**
     * @return int
     */
    public function getPassengerCount(): ?int
    {
        return $this->passengerCount;
    }

    /**
     * @param int $passengerCount
     */
    public function setPassengerCount(?int $passengerCount): void
    {
        $this->passengerCount = $passengerCount;
    }

    /**
     * @return int
     */
    public function getDriverAgeFrom(): ?int
    {
        return $this->driverAgeFrom;
    }

    /**
     * @param int $driverAgeFrom
     */
    public function setDriverAgeFrom(?int $driverAgeFrom): void
    {
        $this->driverAgeFrom = $driverAgeFrom;
    }

    /**
     * @return string
     */
    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     */
    public function setPaymentType(?string $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return bool
     */
    public function isFixedDate(): ?bool
    {
        return $this->isFixedDate;
    }

    /**
     * @param bool $isFixedDate
     */
    public function setIsFixedDate(?bool $isFixedDate): void
    {
        $this->isFixedDate = $isFixedDate;
    }

    /**
     * @return \DateTime
     */
    public function getRentalStart(): ?\DateTime
    {
        return $this->rentalStart;
    }

    /**
     * @param \DateTime $rentalStart
     */
    public function setRentalStart(?\DateTime $rentalStart): void
    {
        $this->rentalStart = $rentalStart;
    }

    /**
     * @return \DateTime
     */
    public function getRentalEnd(): ?\DateTime
    {
        return $this->rentalEnd;
    }

    /**
     * @param \DateTime $rentalEnd
     */
    public function setRentalEnd(?\DateTime $rentalEnd): void
    {
        $this->rentalEnd = $rentalEnd;
    }

    /**
     * @return int
     */
    public function getRoomCount(): ?int
    {
        return $this->roomCount;
    }

    /**
     * @param int $roomCount
     */
    public function setRoomCount(?int $roomCount): void
    {
        $this->roomCount = $roomCount;
    }

    /**
     * @return int
     */
    public function getDayCount(): ?int
    {
        return $this->dayCount;
    }

    /**
     * @param int $dayCount
     */
    public function setDayCount(?int $dayCount): void
    {
        $this->dayCount = $dayCount;
    }

    /**
     * @return bool
     */
    public function isReturn(): ?bool
    {
        return $this->isReturn;
    }

    /**
     * @return \DateTime
     */
    public function getReturnAt(): ?\DateTime
    {
        return $this->returnAt;
    }

    /**
     * @param bool $isReturn
     */
    public function setIsReturn(?bool $isReturn): void
    {
        $this->isReturn = $isReturn;
    }

    /**
     * @param \DateTime $returnAt
     */
    public function setReturnAt(?\DateTime $returnAt): void
    {
        $this->returnAt = $returnAt;
    }

    /**
     * @return string
     */
    public function getDistance(): ?string
    {
        return $this->distance;
    }

    /**
     * @param string $distance
     */
    public function setDistance(?string $distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @return string
     */
    public function getDuration(): ?string
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration(?string $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return Category
     */
    public function getOrigin(): ?Category
    {
        return $this->origin;
    }

    /**
     * @param Category $origin
     */
    public function setOrigin(?Category $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return Category
     */
    public function getDestination(): ?Category
    {
        return $this->destination;
    }

    /**
     * @param Category $destination
     */
    public function setDestination(?Category $destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @return \DateTime
     */
    public function getDepartAt(): ?\DateTime
    {
        return $this->departAt;
    }

    /**
     * @param \DateTime $departAt
     */
    public function setDepartAt(?\DateTime $departAt): void
    {
        $this->departAt = $departAt;
    }


    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("subtypes")
     * @JMS\Groups("api_v1")
     *
     * @return array
     */
    public function serializeSubtypes()
    {
        return $this->getSubtypes()->map(function (InquiryParametersSubtype $subtype) {
            return $subtype->getName();
        })->toArray();
    }


    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("daytimeRanges")
     * @JMS\Groups("api_v1")
     *
     * @return array
     */
    public function serializeDaytimeRanges()
    {
        return $this->getDaytimeRanges()->map(function (DaytimeRange $subtype) {
            return $subtype->getName();
        })->toArray();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("hourlyRanges")
     * @JMS\Groups("api_v1")
     *
     * @return array
     */
    public function serializeHourlyRanges()
    {
        return $this->getHourlyRanges()->map(function (HourlyRange $subtype) {
            return $subtype->getName();
        })->toArray();
    }
}
