<?php

namespace App\Entity;

class InquiryStatus
{
    const PENDING = 'pending';
    const OPEN = 'open';
    const CLOSED = 'closed';
}