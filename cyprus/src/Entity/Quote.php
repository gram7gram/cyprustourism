<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="quotes", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unq_quotes", columns={"inquiry_id", "provider_id", "parameters_id", "deleted_at"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\QuoteRepository")
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Quote
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $deletedAt;

    /**
     * @var Price
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Price")
     * @ORM\JoinColumn(nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $text;

    /**
     * @var Inquiry
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Inquiry", inversedBy="quotes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inquiry;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $provider;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\QuoteCategory", mappedBy="quote", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups("api_v1")
     */
    private $categories;

    /**
     * @var InquiryParameters
     *
     * @ORM\OneToOne(targetEntity="App\Entity\InquiryParameters")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $parameters;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\QuoteMedia", mappedBy="quote", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups("api_v1")
     */
    private $media;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCreatorNotified;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->media = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->isCreatorNotified = false;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return Provider
     */
    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider(?Provider $provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return InquiryParameters
     */
    public function getParameters(): ?InquiryParameters
    {
        return $this->parameters;
    }

    /**
     * @param InquiryParameters $parameters
     */
    public function setParameters(?InquiryParameters $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @return Inquiry
     */
    public function getInquiry(): ?Inquiry
    {
        return $this->inquiry;
    }

    /**
     * @param Inquiry $inquiry
     */
    public function setInquiry(?Inquiry $inquiry): void
    {
        $this->inquiry = $inquiry;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return ArrayCollection
     */
    public function getMedia()
    {
        if (is_null($this->media)) {
            $this->media = new ArrayCollection();
        }
        return $this->media;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories()
    {
        if (is_null($this->categories)) {
            $this->categories = new ArrayCollection();
        }
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isCreatorNotified(): ?bool
    {
        return $this->isCreatorNotified;
    }

    /**
     * @param bool $isCreatorNotified
     */
    public function setIsCreatorNotified(?bool $isCreatorNotified): void
    {
        $this->isCreatorNotified = $isCreatorNotified;
    }

    /**
     * @return Price
     */
    public function getPrice(): ?Price
    {
        return $this->price;
    }

    /**
     * @param Price $price
     */
    public function setPrice(?Price $price): void
    {
        $this->price = $price;
    }
}
