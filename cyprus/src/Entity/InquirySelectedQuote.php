<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="inquiry_selected_quotes", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unq_inquiry_selected_quotes", columns={"inquiry_id", "quote_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\InquirySelectedQuoteRepository")
 */
class InquirySelectedQuote
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var Review
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Review")
     * @ORM\JoinColumn(nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $review;

    /**
     * @var SharedContact
     *
     * @ORM\OneToOne(targetEntity="App\Entity\SharedContact", mappedBy="selectedQuote")
     *
     * @JMS\Groups("api_v1")
     */
    private $contact;

    /**
     * @var Inquiry
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Inquiry", inversedBy="selectedQuotes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inquiry;

    /**
     * @var Quote
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Quote")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $quote;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return Inquiry
     */
    public function getInquiry(): ?Inquiry
    {
        return $this->inquiry;
    }

    /**
     * @param Inquiry $inquiry
     */
    public function setInquiry(?Inquiry $inquiry): void
    {
        $this->inquiry = $inquiry;
    }

    /**
     * @return Quote
     */
    public function getQuote(): ?Quote
    {
        return $this->quote;
    }

    /**
     * @param Quote $quote
     */
    public function setQuote(?Quote $quote): void
    {
        $this->quote = $quote;
    }

    /**
     * @return SharedContact
     */
    public function getContact(): SharedContact
    {
        return $this->contact;
    }

    /**
     * @param SharedContact $contact
     */
    public function setContact(SharedContact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return Review
     */
    public function getReview(): ?Review
    {
        return $this->review;
    }

    /**
     * @param Review $review
     */
    public function setReview(?Review $review): void
    {
        $this->review = $review;
    }
}
