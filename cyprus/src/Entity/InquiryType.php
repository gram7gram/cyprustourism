<?php

namespace App\Entity;

class InquiryType
{
    const CAR_RENT = 'car_rent';
    const BOAT_RENT = 'boat_rent';
    const ACCOMMODATION_RENT = 'accommodation_rent';
    const TRANSFER = 'transfer';

}