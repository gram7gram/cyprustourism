<?php

namespace App\Entity;

class CategoryGroup
{
    const LOCATION = 'location';
    const CAR_TYPE = 'car_type';
    const EXTRAS = 'extras';
    const BOAT_TYPE = 'boat_type';
    const DESTINATION = 'destination';

}