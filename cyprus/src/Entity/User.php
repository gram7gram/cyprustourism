<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=false)
     *
     * @JMS\Groups({"api_v1_user"})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider", "api_v1_shared_contact"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, unique=true, nullable=true)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider", "api_v1_shared_contact"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $dialCode;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider", "api_v1_shared_contact"})
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider", "api_v1_shared_contact"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider", "api_v1_shared_contact"})
     */
    private $surname;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider"})
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_admin", type="boolean", nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider"})
     */
    private $isAdmin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, unique=true, nullable=true)
     */
    private $passwordToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordTokenExpiresAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=12, unique=true, nullable=true)
     */
    private $emailCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $emailCodeExpiresAt;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider"})
     */
    private $isPhoneVerified;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider"})
     */
    private $isEmailVerified;

    /**
     * @var Provider
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Provider", mappedBy="user")
     *
     * @JMS\Groups("api_v1")
     */
    private $provider;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @JMS\Groups({"api_v1_user", "api_v1_provider"})
     */
    private $isEmailSubscribed;

    public function __construct()
    {
        $this->isActive = false;
        $this->isAdmin = false;
        $this->isPhoneVerified = false;
        $this->isEmailVerified = false;
        $this->isEmailSubscribed = true;
        $this->createdAt = new \DateTime();
        $this->status = UserStatus::CREATED;

        $this->refreshEmailCode();
    }

    public function refreshPasswordToken()
    {
        $this->passwordToken = hash('sha256', uniqid());
        $this->passwordTokenExpiresAt = new \DateTime();
        $this->passwordTokenExpiresAt->modify("+24 hours");
    }

    public function refreshEmailCode()
    {
        $this->emailCode = rand(10000, 99999) . '';
        $this->emailCodeExpiresAt = new \DateTime();
        $this->emailCodeExpiresAt->modify("+24 hours");
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }


    public function getRoles()
    {
        $roles = [Role::USER];

        if ($this->isAdmin) {
            $roles[] = Role::ADMIN;
        }

        if ($this->provider) {
            $roles[] = Role::PROVIDER;
        }

        return $roles;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function getUsername(): ?string
    {
        if ($this->email) return $this->email;
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getDialCode(): ?string
    {
        return $this->dialCode;
    }

    /**
     * @param string $dialCode
     */
    public function setDialCode(?string $dialCode): void
    {
        $this->dialCode = $dialCode;
    }

    /**
     * @return string
     */
    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    /**
     * @param string $passwordToken
     */
    public function setPasswordToken(?string $passwordToken): void
    {
        $this->passwordToken = $passwordToken;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordTokenExpiresAt(): ?\DateTime
    {
        return $this->passwordTokenExpiresAt;
    }

    /**
     * @param \DateTime $passwordTokenExpiresAt
     */
    public function setPasswordTokenExpiresAt(?\DateTime $passwordTokenExpiresAt): void
    {
        $this->passwordTokenExpiresAt = $passwordTokenExpiresAt;
    }

    /**
     * @return Provider
     */
    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    /**
     * @return bool
     */
    public function isPhoneVerified(): bool
    {
        return $this->isPhoneVerified;
    }

    /**
     * @param bool $isPhoneVerified
     */
    public function setPhoneVerified(bool $isPhoneVerified): void
    {
        $this->isPhoneVerified = $isPhoneVerified;
    }

    /**
     * @return bool
     */
    public function isEmailVerified(): bool
    {
        return $this->isEmailVerified;
    }

    /**
     * @param bool $value
     */
    public function setEmailVerified(bool $value): void
    {
        $this->isEmailVerified = $value;
    }

    /**
     * @return \DateTime
     */
    public function getEmailCodeExpiresAt(): ?\DateTime
    {
        return $this->emailCodeExpiresAt;
    }

    /**
     * @param \DateTime $value
     */
    public function setEmailCodeExpiresAt(?\DateTime $value): void
    {
        $this->emailCodeExpiresAt = $value;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->phone,
            $this->email,
            $this->password,
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->phone,
            $this->email,
            $this->password,
            ) = unserialize($serialized);
    }

    public function __toString()
    {
        return $this->getUsername() . '';
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getEmailCode(): ?string
    {
        return $this->emailCode;
    }

    /**
     * @param string $emailCode
     */
    public function setEmailCode(?string $emailCode): void
    {
        $this->emailCode = $emailCode;
    }

    /**
     * @return bool
     */
    public function isEmailSubscribed():? bool
    {
        return $this->isEmailSubscribed;
    }

    /**
     * @param bool $isEmailSubscribed
     */
    public function setIsEmailSubscribed(?bool $isEmailSubscribed): void
    {
        $this->isEmailSubscribed = $isEmailSubscribed;
    }
}
