<?php

namespace App\Entity;

use App\Classes\Guid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="inquiries")
 * @ORM\Entity(repositoryClass="App\Repository\InquiryRepository")
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Inquiry
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $expiresAt;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $extendCount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $type;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $creator;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $receiveEmailOnNewQuote;

    /**
     * @var InquiryParameters
     *
     * @ORM\OneToOne(targetEntity="App\Entity\InquiryParameters")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $parameters;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $text;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\InquiryCategory", mappedBy="inquiry", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups("api_v1")
     */
    private $categories;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\InquiryQuote", mappedBy="inquiry", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups("api_v1")
     */
    private $quotes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\InquirySelectedQuote", mappedBy="inquiry", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups("api_v1")
     */
    private $selectedQuotes;

    /**
     * @var Review
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Review", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $review;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $areProvidersNotified;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->receiveEmailOnNewQuote = true;
        $this->categories = new ArrayCollection();
        $this->selectedQuotes = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->guid = Guid::generate();
        $this->status = InquiryStatus::PENDING;
        $this->areProvidersNotified = false;
        $this->extendCount = 0;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): ?\DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime $expiresAt
     */
    public function setExpiresAt(?\DateTime $expiresAt): void
    {
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return User
     */
    public function getCreator(): ?User
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator(?User $creator): void
    {
        $this->creator = $creator;
    }

    /**
     * @return bool
     */
    public function isReceiveEmailOnNewQuote(): ?bool
    {
        return $this->receiveEmailOnNewQuote;
    }

    /**
     * @param bool $receiveEmailOnNewQuote
     */
    public function setReceiveEmailOnNewQuote(?bool $receiveEmailOnNewQuote): void
    {
        $this->receiveEmailOnNewQuote = $receiveEmailOnNewQuote;
    }

    /**
     * @return InquiryParameters
     */
    public function getParameters(): ?InquiryParameters
    {
        return $this->parameters;
    }

    /**
     * @param InquiryParameters $parameters
     */
    public function setParameters(?InquiryParameters $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories()
    {
        if (is_null($this->categories)) {
            $this->categories = new ArrayCollection();
        }
        return $this->categories;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuotes()
    {
        if (is_null($this->quotes)) {
            $this->quotes = new ArrayCollection();
        }
        return $this->quotes;
    }

    /**
     * @return ArrayCollection
     */
    public function getSelectedQuotes()
    {
        if (is_null($this->selectedQuotes)) {
            $this->selectedQuotes = new ArrayCollection();
        }
        return $this->selectedQuotes;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isAreProvidersNotified(): ?bool
    {
        return $this->areProvidersNotified;
    }

    /**
     * @param bool $areProvidersNotified
     */
    public function setAreProvidersNotified(?bool $areProvidersNotified): void
    {
        $this->areProvidersNotified = $areProvidersNotified;
    }

    /**
     * @return string
     */
    public function getExtendCount(): ?string
    {
        return $this->extendCount;
    }

    /**
     * @param string $extendCount
     */
    public function setExtendCount(?string $extendCount): void
    {
        $this->extendCount = $extendCount;
    }

    /**
     * @return Review
     */
    public function getReview(): ?Review
    {
        return $this->review;
    }

    /**
     * @param Review $review
     */
    public function setReview(?Review $review): void
    {
        $this->review = $review;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("isPhoneVerified")
     *
     * @JMS\Groups({"api_v1"})
     */
    public function serializeIsPhoneVerified()
    {
        return $this->creator
            ? $this->creator->isPhoneVerified()
            : false;
    }
}
