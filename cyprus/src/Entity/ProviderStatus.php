<?php

namespace App\Entity;

class ProviderStatus
{
    const CREATED = 'created';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';

}