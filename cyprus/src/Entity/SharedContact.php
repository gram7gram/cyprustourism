<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="shared_contacts", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unq_shared_contacts", columns={"user_id", "provider_id", "selected_quote_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\SharedContactRepository")
 */
class SharedContact
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $cost;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $user;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $provider;

    /**
     * @var InquirySelectedQuote
     *
     * @ORM\OneToOne(targetEntity="App\Entity\InquirySelectedQuote", inversedBy="contact")
     * @ORM\JoinColumn(nullable=true)
     */
    private $selectedQuote;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = SharedContactStatus::PENDING;
        $this->cost = 0;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Provider
     */
    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider(?Provider $provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCost(): ?int
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     */
    public function setCost(?int $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return InquirySelectedQuote
     */
    public function getSelectedQuote(): ?InquirySelectedQuote
    {
        return $this->selectedQuote;
    }

    /**
     * @param InquirySelectedQuote $selectedQuote
     */
    public function setSelectedQuote(?InquirySelectedQuote $selectedQuote): void
    {
        $this->selectedQuote = $selectedQuote;
    }
}
