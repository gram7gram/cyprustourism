<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="inquiry_quotes", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unq_inquiry_quotes", columns={"inquiry_id", "quote_id"})
 * })
 * @ORM\Entity()
 */
class InquiryQuote
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var Inquiry
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Inquiry", inversedBy="quotes")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $inquiry;

    /**
     * @var Quote
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Quote")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $quote;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return Inquiry
     */
    public function getInquiry(): ?Inquiry
    {
        return $this->inquiry;
    }

    /**
     * @param Inquiry $inquiry
     */
    public function setInquiry(?Inquiry $inquiry): void
    {
        $this->inquiry = $inquiry;
    }

    /**
     * @return Quote
     */
    public function getQuote(): ?Quote
    {
        return $this->quote;
    }

    /**
     * @param Quote $quote
     */
    public function setQuote(?Quote $quote): void
    {
        $this->quote = $quote;
    }
}
