<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="inquiry_parameters_subtypes")
 * @ORM\Entity()
 */
class InquiryParametersSubtype
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $name;

    /**
     * @var InquiryParameters
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\InquiryParameters", inversedBy="subtypes")
     *
     * @JMS\Groups("api_v1")
     */
    private $parameters;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return InquiryParameters
     */
    public function getParameters(): ?InquiryParameters
    {
        return $this->parameters;
    }

    /**
     * @param InquiryParameters $parameters
     */
    public function setParameters(?InquiryParameters $parameters): void
    {
        $this->parameters = $parameters;
    }

}
