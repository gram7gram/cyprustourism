<?php

namespace App\Entity;

class ProviderInvoiceStatus
{
    const UNPAID = 'unpaid';
    const PAID = 'paid';
    const CANCELED = 'canceled';

}