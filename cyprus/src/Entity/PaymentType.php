<?php

namespace App\Entity;

class PaymentType
{
    const CASH = 'cash';
    const CREDIT_CARD = 'credit_card';
    const BANK_TRANSFER = 'bank_transfer';

}