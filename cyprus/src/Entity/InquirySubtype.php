<?php

namespace App\Entity;

class InquirySubtype
{
    const HOTEL = 'hotel';
    const BED_AND_BREAKFAST = 'bnb';
    const APARTMENT = 'apartment';
    const HOUSE = 'house';
    const VILLA = 'villa';

}