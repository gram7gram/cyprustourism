<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181212002907 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiry_parameters ADD origin_id INT DEFAULT NULL, ADD destination_id INT DEFAULT NULL, DROP location_lng, DROP location_lat, DROP destination_lat, DROP destination_lng');
        $this->addSql('ALTER TABLE inquiry_parameters ADD CONSTRAINT FK_4158FD6E56A273CC FOREIGN KEY (origin_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE inquiry_parameters ADD CONSTRAINT FK_4158FD6E816C6140 FOREIGN KEY (destination_id) REFERENCES categories (id)');
        $this->addSql('CREATE INDEX IDX_4158FD6E56A273CC ON inquiry_parameters (origin_id)');
        $this->addSql('CREATE INDEX IDX_4158FD6E816C6140 ON inquiry_parameters (destination_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiry_parameters DROP FOREIGN KEY FK_4158FD6E56A273CC');
        $this->addSql('ALTER TABLE inquiry_parameters DROP FOREIGN KEY FK_4158FD6E816C6140');
        $this->addSql('DROP INDEX IDX_4158FD6E56A273CC ON inquiry_parameters');
        $this->addSql('DROP INDEX IDX_4158FD6E816C6140 ON inquiry_parameters');
        $this->addSql('ALTER TABLE inquiry_parameters ADD location_lng DOUBLE PRECISION DEFAULT NULL, ADD location_lat DOUBLE PRECISION DEFAULT NULL, ADD destination_lat DOUBLE PRECISION DEFAULT NULL, ADD destination_lng DOUBLE PRECISION DEFAULT NULL, DROP origin_id, DROP destination_id');
    }
}
