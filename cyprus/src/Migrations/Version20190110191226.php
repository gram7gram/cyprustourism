<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190110191226 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM provider_invoices');
        $this->addSql('ALTER TABLE provider_invoices ADD billing_plan_id INT NOT NULL, ADD type VARCHAR(16) NOT NULL, CHANGE status status VARCHAR(16) NOT NULL');
        $this->addSql('ALTER TABLE provider_invoices ADD CONSTRAINT FK_226D417C83B7894C FOREIGN KEY (billing_plan_id) REFERENCES billing_plans (id)');
        $this->addSql('CREATE INDEX IDX_226D417C83B7894C ON provider_invoices (billing_plan_id)');
    }

    public function down(Schema $schema) : void
    {
    }
}
