<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181027171518 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $pass = '$2y$12$IOAygCW8d.3r/lzK9Xmzqucb1mdHc7KEctE3DINvdIoYxBVBZbgQa'; //12345

        $this->addSql("INSERT INTO users (created_at, email, phone, password, name, is_active, is_admin, is_phone_verified, is_email_verified) VALUES
(now(), 'admin', '00000000', '$pass', 'Admin', true, true, true, true),
(now(), 'user', '00000001', '$pass', 'User', true, false, true, true),
(now(), 'provider', '00000002', '$pass', 'Provider', true, false, true, true)");

        $this->addSql("INSERT INTO providers (user_id, created_at, website, address, company_id, company_name, type, status, credits, paypal_token, rating) 
VALUES ((SELECT id FROM users WHERE email = 'provider'), now(), 'test.com', 'Test', 'Test', '000001', 'car_rent', 'approved', 100, '12345', 4.5)");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
