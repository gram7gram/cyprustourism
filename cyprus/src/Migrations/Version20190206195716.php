<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190206195716 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE inquiry_parameters_subtypes (id INT AUTO_INCREMENT NOT NULL, parameters_id INT DEFAULT NULL, name VARCHAR(16) NOT NULL, INDEX IDX_29B2063E88BD9C1F (parameters_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inquiry_parameters_subtypes ADD CONSTRAINT FK_29B2063E88BD9C1F FOREIGN KEY (parameters_id) REFERENCES inquiry_parameters (id)');
        $this->addSql('ALTER TABLE quotes DROP subtype');
        $this->addSql('ALTER TABLE inquiries DROP subtype');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE inquiry_parameters_subtypes');
    }
}
