<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181027150423 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE provider_invoices (id INT AUTO_INCREMENT NOT NULL, media_id INT NOT NULL, provider_id INT NOT NULL, INDEX IDX_226D417CEA9FDD75 (media_id), INDEX IDX_226D417CA53A8AA (provider_id), UNIQUE INDEX unq_provider_invoices (media_id, provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE providers (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, website LONGTEXT DEFAULT NULL, address LONGTEXT DEFAULT NULL, company_id LONGTEXT DEFAULT NULL, company_name LONGTEXT DEFAULT NULL, type VARCHAR(32) NOT NULL, status VARCHAR(16) NOT NULL, credits INT NOT NULL, paypal_token LONGTEXT DEFAULT NULL, rating DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_E225D417A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(32) DEFAULT NULL, country LONGTEXT DEFAULT NULL, nationality LONGTEXT DEFAULT NULL, password VARCHAR(64) NOT NULL, name VARCHAR(255) DEFAULT NULL, surname VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) NOT NULL, is_admin TINYINT(1) NOT NULL, password_token VARCHAR(128) DEFAULT NULL, password_token_expires_at DATETIME DEFAULT NULL, email_token VARCHAR(128) DEFAULT NULL, email_token_expires_at DATETIME DEFAULT NULL, sms_code VARCHAR(12) DEFAULT NULL, sms_code_expires_at DATETIME DEFAULT NULL, is_phone_verified TINYINT(1) NOT NULL, is_email_verified TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), UNIQUE INDEX UNIQ_1483A5E9444F97DD (phone), UNIQUE INDEX UNIQ_1483A5E9BEAB6C24 (password_token), UNIQUE INDEX UNIQ_1483A5E9C27AE0B4 (email_token), UNIQUE INDEX UNIQ_1483A5E9CC38ACCE (sms_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, locale VARCHAR(2) NOT NULL, type VARCHAR(32) NOT NULL, group_name VARCHAR(32) NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_plans (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, name VARCHAR(255) NOT NULL, credits INT NOT NULL, price DOUBLE PRECISION NOT NULL, discount DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_3018BDFC5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inquiry_parameters (id INT AUTO_INCREMENT NOT NULL, room_count INT DEFAULT NULL, bedroom_count INT DEFAULT NULL, people_count INT DEFAULT NULL, passenger_count INT DEFAULT NULL, driver_age_from INT DEFAULT NULL, driver_age_to INT DEFAULT NULL, location_lng DOUBLE PRECISION DEFAULT NULL, location_lat DOUBLE PRECISION DEFAULT NULL, destination_lat DOUBLE PRECISION DEFAULT NULL, destination_lng DOUBLE PRECISION DEFAULT NULL, payment_type VARCHAR(16) DEFAULT NULL, is_fixed_date TINYINT(1) DEFAULT NULL, rental_start DATETIME DEFAULT NULL, rental_end DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_media (id INT AUTO_INCREMENT NOT NULL, media_id INT NOT NULL, quote_id INT NOT NULL, INDEX IDX_67D32C6DEA9FDD75 (media_id), INDEX IDX_67D32C6DDB805178 (quote_id), UNIQUE INDEX unq_quote_media (media_id, quote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quotes (id INT AUTO_INCREMENT NOT NULL, inquiry_id INT NOT NULL, provider_id INT NOT NULL, parameters_id INT NOT NULL, created_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, type VARCHAR(32) DEFAULT NULL, subtype VARCHAR(32) DEFAULT NULL, text LONGTEXT DEFAULT NULL, is_creator_notified TINYINT(1) DEFAULT NULL, INDEX IDX_A1B588C5A7AD6D71 (inquiry_id), INDEX IDX_A1B588C5A53A8AA (provider_id), UNIQUE INDEX UNIQ_A1B588C588BD9C1F (parameters_id), UNIQUE INDEX unq_quotes (inquiry_id, provider_id, parameters_id, deleted_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shared_contacts (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, provider_id INT NOT NULL, created_at DATETIME NOT NULL, status VARCHAR(16) NOT NULL, INDEX IDX_6664E450A76ED395 (user_id), INDEX IDX_6664E450A53A8AA (provider_id), UNIQUE INDEX unq_shared_contacts (user_id, provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reviews (id INT AUTO_INCREMENT NOT NULL, updated_by_id INT DEFAULT NULL, user_id INT NOT NULL, provider_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, status VARCHAR(16) NOT NULL, text LONGTEXT DEFAULT NULL, rating INT NOT NULL, INDEX IDX_6970EB0F896DBBDE (updated_by_id), INDEX IDX_6970EB0FA76ED395 (user_id), INDEX IDX_6970EB0FA53A8AA (provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, hash VARCHAR(64) NOT NULL, name LONGTEXT NOT NULL, mime_type VARCHAR(255) NOT NULL, size BIGINT NOT NULL, url LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_6A2CA10CD1B862B8 (hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inquiry_quotes (id INT AUTO_INCREMENT NOT NULL, inquiry_id INT NOT NULL, quote_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B2ED1200A7AD6D71 (inquiry_id), INDEX IDX_B2ED1200DB805178 (quote_id), UNIQUE INDEX unq_inquiry_quotes (inquiry_id, quote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inquiry_categories (id INT AUTO_INCREMENT NOT NULL, inquiry_id INT NOT NULL, category_id INT NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_7D38F3F8A7AD6D71 (inquiry_id), INDEX IDX_7D38F3F812469DE2 (category_id), UNIQUE INDEX unq_inquiry_categories (inquiry_id, category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inquiries (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, parameters_id INT NOT NULL, guid VARCHAR(36) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, expires_at DATETIME NOT NULL, status VARCHAR(16) NOT NULL, type VARCHAR(32) NOT NULL, subtype VARCHAR(32) DEFAULT NULL, receive_email_on_new_quote TINYINT(1) NOT NULL, text LONGTEXT DEFAULT NULL, are_providers_notified TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_1CCE4D52B6FCFB2 (guid), INDEX IDX_1CCE4D561220EA6 (creator_id), UNIQUE INDEX UNIQ_1CCE4D588BD9C1F (parameters_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inquiry_selected_quotes (id INT AUTO_INCREMENT NOT NULL, contact_id INT DEFAULT NULL, inquiry_id INT NOT NULL, quote_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_84771C59E7A1254A (contact_id), INDEX IDX_84771C59A7AD6D71 (inquiry_id), INDEX IDX_84771C59DB805178 (quote_id), UNIQUE INDEX unq_inquiry_selected_quotes (inquiry_id, quote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_categories (id INT AUTO_INCREMENT NOT NULL, quote_id INT NOT NULL, category_id INT NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_8AA43CD3DB805178 (quote_id), INDEX IDX_8AA43CD312469DE2 (category_id), UNIQUE INDEX unq_quote_categories (quote_id, category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE provider_invoices ADD CONSTRAINT FK_226D417CEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE provider_invoices ADD CONSTRAINT FK_226D417CA53A8AA FOREIGN KEY (provider_id) REFERENCES providers (id)');
        $this->addSql('ALTER TABLE providers ADD CONSTRAINT FK_E225D417A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE quote_media ADD CONSTRAINT FK_67D32C6DEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE quote_media ADD CONSTRAINT FK_67D32C6DDB805178 FOREIGN KEY (quote_id) REFERENCES quotes (id)');
        $this->addSql('ALTER TABLE quotes ADD CONSTRAINT FK_A1B588C5A7AD6D71 FOREIGN KEY (inquiry_id) REFERENCES inquiries (id)');
        $this->addSql('ALTER TABLE quotes ADD CONSTRAINT FK_A1B588C5A53A8AA FOREIGN KEY (provider_id) REFERENCES providers (id)');
        $this->addSql('ALTER TABLE quotes ADD CONSTRAINT FK_A1B588C588BD9C1F FOREIGN KEY (parameters_id) REFERENCES inquiry_parameters (id)');
        $this->addSql('ALTER TABLE shared_contacts ADD CONSTRAINT FK_6664E450A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE shared_contacts ADD CONSTRAINT FK_6664E450A53A8AA FOREIGN KEY (provider_id) REFERENCES providers (id)');
        $this->addSql('ALTER TABLE reviews ADD CONSTRAINT FK_6970EB0F896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE reviews ADD CONSTRAINT FK_6970EB0FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE reviews ADD CONSTRAINT FK_6970EB0FA53A8AA FOREIGN KEY (provider_id) REFERENCES providers (id)');
        $this->addSql('ALTER TABLE inquiry_quotes ADD CONSTRAINT FK_B2ED1200A7AD6D71 FOREIGN KEY (inquiry_id) REFERENCES inquiries (id)');
        $this->addSql('ALTER TABLE inquiry_quotes ADD CONSTRAINT FK_B2ED1200DB805178 FOREIGN KEY (quote_id) REFERENCES quotes (id)');
        $this->addSql('ALTER TABLE inquiry_categories ADD CONSTRAINT FK_7D38F3F8A7AD6D71 FOREIGN KEY (inquiry_id) REFERENCES inquiries (id)');
        $this->addSql('ALTER TABLE inquiry_categories ADD CONSTRAINT FK_7D38F3F812469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE inquiries ADD CONSTRAINT FK_1CCE4D561220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE inquiries ADD CONSTRAINT FK_1CCE4D588BD9C1F FOREIGN KEY (parameters_id) REFERENCES inquiry_parameters (id)');
        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD CONSTRAINT FK_84771C59E7A1254A FOREIGN KEY (contact_id) REFERENCES shared_contacts (id)');
        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD CONSTRAINT FK_84771C59A7AD6D71 FOREIGN KEY (inquiry_id) REFERENCES inquiries (id)');
        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD CONSTRAINT FK_84771C59DB805178 FOREIGN KEY (quote_id) REFERENCES quotes (id)');
        $this->addSql('ALTER TABLE quote_categories ADD CONSTRAINT FK_8AA43CD3DB805178 FOREIGN KEY (quote_id) REFERENCES quotes (id)');
        $this->addSql('ALTER TABLE quote_categories ADD CONSTRAINT FK_8AA43CD312469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider_invoices DROP FOREIGN KEY FK_226D417CA53A8AA');
        $this->addSql('ALTER TABLE quotes DROP FOREIGN KEY FK_A1B588C5A53A8AA');
        $this->addSql('ALTER TABLE shared_contacts DROP FOREIGN KEY FK_6664E450A53A8AA');
        $this->addSql('ALTER TABLE reviews DROP FOREIGN KEY FK_6970EB0FA53A8AA');
        $this->addSql('ALTER TABLE providers DROP FOREIGN KEY FK_E225D417A76ED395');
        $this->addSql('ALTER TABLE shared_contacts DROP FOREIGN KEY FK_6664E450A76ED395');
        $this->addSql('ALTER TABLE reviews DROP FOREIGN KEY FK_6970EB0F896DBBDE');
        $this->addSql('ALTER TABLE reviews DROP FOREIGN KEY FK_6970EB0FA76ED395');
        $this->addSql('ALTER TABLE inquiries DROP FOREIGN KEY FK_1CCE4D561220EA6');
        $this->addSql('ALTER TABLE inquiry_categories DROP FOREIGN KEY FK_7D38F3F812469DE2');
        $this->addSql('ALTER TABLE quote_categories DROP FOREIGN KEY FK_8AA43CD312469DE2');
        $this->addSql('ALTER TABLE quotes DROP FOREIGN KEY FK_A1B588C588BD9C1F');
        $this->addSql('ALTER TABLE inquiries DROP FOREIGN KEY FK_1CCE4D588BD9C1F');
        $this->addSql('ALTER TABLE quote_media DROP FOREIGN KEY FK_67D32C6DDB805178');
        $this->addSql('ALTER TABLE inquiry_quotes DROP FOREIGN KEY FK_B2ED1200DB805178');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP FOREIGN KEY FK_84771C59DB805178');
        $this->addSql('ALTER TABLE quote_categories DROP FOREIGN KEY FK_8AA43CD3DB805178');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP FOREIGN KEY FK_84771C59E7A1254A');
        $this->addSql('ALTER TABLE provider_invoices DROP FOREIGN KEY FK_226D417CEA9FDD75');
        $this->addSql('ALTER TABLE quote_media DROP FOREIGN KEY FK_67D32C6DEA9FDD75');
        $this->addSql('ALTER TABLE quotes DROP FOREIGN KEY FK_A1B588C5A7AD6D71');
        $this->addSql('ALTER TABLE inquiry_quotes DROP FOREIGN KEY FK_B2ED1200A7AD6D71');
        $this->addSql('ALTER TABLE inquiry_categories DROP FOREIGN KEY FK_7D38F3F8A7AD6D71');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP FOREIGN KEY FK_84771C59A7AD6D71');
        $this->addSql('DROP TABLE provider_invoices');
        $this->addSql('DROP TABLE providers');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE billing_plans');
        $this->addSql('DROP TABLE inquiry_parameters');
        $this->addSql('DROP TABLE quote_media');
        $this->addSql('DROP TABLE quotes');
        $this->addSql('DROP TABLE shared_contacts');
        $this->addSql('DROP TABLE reviews');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE inquiry_quotes');
        $this->addSql('DROP TABLE inquiry_categories');
        $this->addSql('DROP TABLE inquiries');
        $this->addSql('DROP TABLE inquiry_selected_quotes');
        $this->addSql('DROP TABLE quote_categories');
    }
}
