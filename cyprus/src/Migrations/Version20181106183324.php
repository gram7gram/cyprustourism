<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181106183324 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE inquiries SET are_providers_notified = 0');
        $this->addSql('UPDATE quotes SET is_creator_notified = 0');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiries DROP are_providers_notified');
    }
}
