<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181211235149 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Lefkosia','{\"lat\":35.16,\"lng\":33.38}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Lemesos','{\"lat\":34.68,\"lng\":33.05}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Larnaka','{\"lat\":34.92,\"lng\":33.63}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Lefkosa','{\"lat\":35.18,\"lng\":33.37}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Gazimagusa','{\"lat\":35.11,\"lng\":33.94}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Pafos','{\"lat\":34.77,\"lng\":32.41}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Girne','{\"lat\":35.33,\"lng\":33.33}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Guzelyurt','{\"lat\":35.2,\"lng\":32.98}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Aradippou','{\"lat\":34.95,\"lng\":33.58}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Paralimni','{\"lat\":35.05,\"lng\":33.98}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Lefka','{\"lat\":35.11,\"lng\":32.85}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Geri','{\"lat\":35.1,\"lng\":33.42}')");
        $this->addSql("INSERT INTO categories (created_at, locale, ordering, type, group_name, name, extra) VALUES (now(), 'en', 1, 'transfer', 'destination', 'Ypsonas','{\"lat\":34.69,\"lng\":32.97}')");


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
