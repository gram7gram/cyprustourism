<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190110160712 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiries ADD review_id INT DEFAULT NULL, DROP has_review');
        $this->addSql('ALTER TABLE inquiries ADD CONSTRAINT FK_1CCE4D53E2E969B FOREIGN KEY (review_id) REFERENCES reviews (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1CCE4D53E2E969B ON inquiries (review_id)');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP is_review_allowed');

        $this->addSql('DELETE FROM provider_invoices');
        $this->addSql('ALTER TABLE provider_invoices ADD status VARCHAR(16) NOT NULL');

    }

    public function down(Schema $schema) : void
    {
    }
}
