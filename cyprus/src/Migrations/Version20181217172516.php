<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\InquiryType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217172516 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE inquiry_costs (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type VARCHAR(255) NOT NULL, credits INT NOT NULL, UNIQUE INDEX UNIQ_9B1FC4B08CDE5729 (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');

        $this->addSql("INSERT INTO inquiry_costs (created_at, updated_at, type, credits) VALUES (now(), now(), '" . InquiryType::BOAT_RENT . "', 5)");
        $this->addSql("INSERT INTO inquiry_costs (created_at, updated_at, type, credits) VALUES (now(), now(), '" . InquiryType::CAR_RENT . "', 5)");
        $this->addSql("INSERT INTO inquiry_costs (created_at, updated_at, type, credits) VALUES (now(), now(), '" . InquiryType::ACCOMMODATION_RENT . "', 5)");
        $this->addSql("INSERT INTO inquiry_costs (created_at, updated_at, type, credits) VALUES (now(), now(), '" . InquiryType::TRANSFER . "', 5)");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE inquiry_costs');
    }
}
