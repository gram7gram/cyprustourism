<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107145808 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO billing_plans(created_At, name, credits, price, discount) values
(now(), '1 credit', 1, 3, null),
(now(), '5 credits', 5, 13.50, 10),
(now(), '10 credits', 10, 24, 20),
(now(), '20 credits', 20, 42, 30),
(now(), '50 credits', 50, 50, 50); ");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE billing_plans');
    }
}
