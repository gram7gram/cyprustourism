<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218214555 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_history ADD inquiry_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE billing_history ADD CONSTRAINT FK_4C94FB9CA7AD6D71 FOREIGN KEY (inquiry_id) REFERENCES inquiries (id)');
        $this->addSql('CREATE INDEX IDX_4C94FB9CA7AD6D71 ON billing_history (inquiry_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_history DROP FOREIGN KEY FK_4C94FB9CA7AD6D71');
        $this->addSql('DROP INDEX IDX_4C94FB9CA7AD6D71 ON billing_history');
        $this->addSql('ALTER TABLE billing_history DROP inquiry_id');
    }
}
