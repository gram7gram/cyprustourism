<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181108182559 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories ADD icon_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF3466854B9D732 FOREIGN KEY (icon_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_3AF3466854B9D732 ON categories (icon_id)');
        $this->addSql('ALTER TABLE inquiry_parameters CHANGE driver_age_to day_count INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF3466854B9D732');
        $this->addSql('DROP INDEX IDX_3AF3466854B9D732 ON categories');
        $this->addSql('ALTER TABLE categories DROP icon_id');
        $this->addSql('ALTER TABLE inquiry_parameters CHANGE day_count driver_age_to INT DEFAULT NULL');
    }
}
