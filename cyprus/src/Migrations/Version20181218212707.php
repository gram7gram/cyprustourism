<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218212707 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shared_contacts ADD selected_quote_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shared_contacts ADD CONSTRAINT FK_6664E450CCDFAB45 FOREIGN KEY (selected_quote_id) REFERENCES inquiry_selected_quotes (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6664E450CCDFAB45 ON shared_contacts (selected_quote_id)');

        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP FOREIGN KEY FK_84771C59E7A1254A');
        $this->addSql('DROP INDEX IDX_84771C59E7A1254A ON inquiry_selected_quotes');

        $this->addSql('DELETE FROM inquiry_selected_quotes');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP contact_id');

        $this->addSql("DROP INDEX unq_shared_contacts ON shared_contacts");
        $this->addSql("CREATE UNIQUE INDEX unq_shared_contacts ON shared_contacts (user_id, provider_id, selected_quote_id)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD contact_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD CONSTRAINT FK_84771C59E7A1254A FOREIGN KEY (contact_id) REFERENCES shared_contacts (id)');
        $this->addSql('CREATE INDEX IDX_84771C59E7A1254A ON inquiry_selected_quotes (contact_id)');
        $this->addSql('ALTER TABLE shared_contacts DROP FOREIGN KEY FK_6664E450CCDFAB45');
        $this->addSql('DROP INDEX UNIQ_6664E450CCDFAB45 ON shared_contacts');
        $this->addSql('ALTER TABLE shared_contacts DROP selected_quote_id');
    }
}
