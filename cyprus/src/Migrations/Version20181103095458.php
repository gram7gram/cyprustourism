<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181103095458 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
       $this->addSql("INSERT INTO categories (created_at, locale, type, group_name, name) VALUES
(now(), 'en', 'car_rent', 'extras', 'WiFi'),
(now(), 'en', 'car_rent', 'extras', 'GPS'),
(now(), 'en', 'car_rent', 'extras', 'Radio'),
(now(), 'en', 'car_rent', 'extras', 'Air conditioning'),
(now(), 'en', 'car_rent', 'location', 'Airport'),
(now(), 'en', 'car_rent', 'location', 'Harbor'),
(now(), 'en', 'car_rent', 'location', 'Hotel'),
(now(), 'en', 'car_rent', 'car_type', 'Sedan'),
(now(), 'en', 'car_rent', 'car_type', 'Minivan'),
(now(), 'en', 'car_rent', 'car_type', 'Family car'),

(now(), 'en', 'boat_rent', 'location', 'Airport'),
(now(), 'en', 'boat_rent', 'location', 'Harbor'),
(now(), 'en', 'boat_rent', 'location', 'Hotel'),
(now(), 'en', 'boat_rent', 'extras', 'WiFi'),
(now(), 'en', 'boat_rent', 'extras', 'GPS'),
(now(), 'en', 'boat_rent', 'extras', 'Air conditioning'),
(now(), 'en', 'boat_rent', 'extras', 'Signal flare'),
(now(), 'en', 'boat_rent', 'boat_type', 'Fast boat'),
(now(), 'en', 'boat_rent', 'boat_type', 'Cruise boat'),
(now(), 'en', 'boat_rent', 'boat_type', 'Wooden boat'),

(now(), 'en', 'accommodation_rent', 'location', 'Airport'),
(now(), 'en', 'accommodation_rent', 'location', 'Harbor'),
(now(), 'en', 'accommodation_rent', 'location', 'Hotel'),
(now(), 'en', 'accommodation_rent', 'extras', 'WiFi'),
(now(), 'en', 'accommodation_rent', 'extras', 'Air conditioning'),
(now(), 'en', 'accommodation_rent', 'extras', 'Breakfast'),
(now(), 'en', 'accommodation_rent', 'extras', 'Green zone'),

(now(), 'en', 'transfer', 'extras', 'WiFi'),
(now(), 'en', 'transfer', 'extras', 'Air conditioning')
       ");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
