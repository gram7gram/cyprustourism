<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190206211430 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE daytime_ranges (id INT AUTO_INCREMENT NOT NULL, parameters_id INT DEFAULT NULL, name VARCHAR(16) NOT NULL, INDEX IDX_8A08E3488BD9C1F (parameters_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hourly_ranges (id INT AUTO_INCREMENT NOT NULL, parameters_id INT DEFAULT NULL, name VARCHAR(16) NOT NULL, INDEX IDX_501664F88BD9C1F (parameters_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE daytime_ranges ADD CONSTRAINT FK_8A08E3488BD9C1F FOREIGN KEY (parameters_id) REFERENCES inquiry_parameters (id)');
        $this->addSql('ALTER TABLE hourly_ranges ADD CONSTRAINT FK_501664F88BD9C1F FOREIGN KEY (parameters_id) REFERENCES inquiry_parameters (id)');
        $this->addSql('ALTER TABLE inquiry_parameters ADD is_hourly TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE daytime_ranges');
        $this->addSql('DROP TABLE hourly_ranges');
        $this->addSql('ALTER TABLE inquiries ADD subtype VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE inquiry_parameters DROP is_hourly');
        $this->addSql('ALTER TABLE quotes ADD subtype VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
