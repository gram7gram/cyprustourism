<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181114154645 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD review_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inquiry_selected_quotes ADD CONSTRAINT FK_84771C593E2E969B FOREIGN KEY (review_id) REFERENCES reviews (id)');
        $this->addSql('CREATE INDEX IDX_84771C593E2E969B ON inquiry_selected_quotes (review_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP FOREIGN KEY FK_84771C593E2E969B');
        $this->addSql('DROP INDEX IDX_84771C593E2E969B ON inquiry_selected_quotes');
        $this->addSql('ALTER TABLE inquiry_selected_quotes DROP review_id');
    }
}
