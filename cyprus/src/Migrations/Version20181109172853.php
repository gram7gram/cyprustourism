<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\UserStatus;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109172853 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD status VARCHAR(32) NOT NULL, CHANGE password password VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE phone phone VARCHAR(32) NOT NULL');
        $this->addSql("UPDATE users SET status = '" . UserStatus::CREATED . "'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP status, CHANGE password password VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci');
    }
}
