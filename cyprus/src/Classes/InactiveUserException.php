<?php

namespace App\Classes;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class InactiveUserException extends BadCredentialsException
{

}