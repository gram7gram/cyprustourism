<?php

namespace App\Classes;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class InactiveProviderException extends BadCredentialsException
{

}