import React from 'react';
import {connect} from 'react-redux';
import {LOGIN_CREDENTIALS_CHANGED} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import LoginCheck from '../actions/LoginCheck';

class Login extends React.Component {

    submit = () => {

        const {login, password} = this.props.Login

        this.props.dispatch(LoginCheck(login, password))
    }

    loginAdmin = () => {

        if (AppParameters.env !== 'dev') return

        this.props.dispatch(LoginCheck('admin', '12345'))
    }

    loginProvider = () => {

        if (AppParameters.env !== 'dev') return

        this.props.dispatch(LoginCheck('provider', '12345'))
    }

    loginUser = () => {

        if (AppParameters.env !== 'dev') return

        this.props.dispatch(LoginCheck('user', '12345'))
    }

    submitIfEnter = e => {
        switch (e.keyCode) {
            case 13:
                this.submit()
        }
    }

    onChange = name => e => {
        this.props.dispatch({
            type: LOGIN_CREDENTIALS_CHANGED,
            payload: {
                [name]: e.target.value
            }
        })
    }

    onChangeIgnoreCase = name => e => {
        const value = e.target.value = e.target.value.toLowerCase()
        this.props.dispatch({
            type: LOGIN_CREDENTIALS_CHANGED,
            payload: {
                [name]: value
            }
        })
    }

    render() {

        const {login, password, isValid, errors, isLoading, canFocusOnPassword, isAccountActivated} = this.props.Login

        return <div className="row no-gutters space-2">
            <div className="col-md-8 col-lg-7 col-xl-6 offset-md-2 offset-lg-2 offset-xl-3 space-3 space-lg-0">

                <div className="mb-3">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_login')}</h2>
                    <p>{translator('login_subtitle')}</p>
                </div>

                {isAccountActivated ? <div className="alert alert-success">
                    <div>{translator('login_activated_message')}</div>
                </div> : null}

                <div className="row">

                    {AppParameters.env === 'dev'
                        ? <div className="col-12 mb-4">
                            <div className="row">
                                <div className="col">
                                    <button type="button" onClick={this.loginAdmin}
                                            className="btn btn-outline-primary btn-block btn-sm">
                                        <i className="fa fa-user-circle"/>&nbsp;Admin
                                    </button>
                                </div>
                                <div className="col">
                                    <button type="button" onClick={this.loginProvider}
                                            className="btn btn-outline-primary btn-block btn-sm">
                                        <i className="fa fa-user-circle"/>&nbsp;Provider
                                    </button>
                                </div>
                                <div className="col">
                                    <button type="button" onClick={this.loginUser}
                                            className="btn btn-outline-primary btn-block btn-sm">
                                        <i className="fa fa-user-circle"/>&nbsp;User
                                    </button>
                                </div>
                            </div>
                        </div> : null}

                    <div className="col-12">

                        {errors.length > 0 && <div className="alert alert-danger">
                            <ul className="simple">{errors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                        </div>}

                        <div className="form-group">
                            <label className="form-label">{translator('login_placeholder')}</label>
                            <input type="text"
                                   className="form-control"
                                   name="login"
                                   autoFocus={!canFocusOnPassword}
                                   placeholder={translator('login_placeholder')}
                                   onChange={this.onChangeIgnoreCase('login')}
                                   onKeyDown={this.submitIfEnter}
                                   value={login || ''}/>
                        </div>
                        <div className="form-group">
                            <label className="form-label">{translator('password')}</label>
                            <input type="password"
                                   className="form-control"
                                   name="password"
                                   placeholder='********'
                                   autoFocus={canFocusOnPassword}
                                   onChange={this.onChange('password')}
                                   onKeyDown={this.submitIfEnter}
                                   value={password || ''}/>
                        </div>
                        <div className="form-group text-right">

                            <a className="btn btn-outline-secondary mr-1" href="/reset-password">
                                {translator('login_reset_password')}
                            </a>

                            <button className="btn btn-primary transition-3d-hover"
                                    onClick={this.submit}
                                    disabled={!isValid || isLoading}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-lock"}/>
                                &nbsp;{translator('login_action_alt')}
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    }
}

export default connect(selectors)(Login)
