import {all, throttle} from 'redux-saga/effects'

import * as Actions from '../actions'

function* onLogin({payload}) {
    window.location = payload.next
}

export default function* sagas() {
    yield all([
        throttle(400, Actions.LOGIN_SUCCESS, onLogin),
    ])
}
