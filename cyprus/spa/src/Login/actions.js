const prefix = 'Login'

export const LOGIN_BEFORE = prefix + '/LOGIN_BEFORE';
export const LOGIN_SUCCESS = prefix + '/LOGIN_SUCCESS';
export const LOGIN_FAILURE = prefix + '/LOGIN_FAILURE';

export const LOGIN_CREDENTIALS_CHANGED = prefix + '/CREDENTIALS_CHANGED';
export const LOGIN_VALIDATE_REQUEST = prefix + '/LOGIN_VALIDATE_REQUEST';
export const LOGIN_VALIDATE_SUCCESS = prefix + '/LOGIN_VALIDATE_SUCCESS';
export const LOGIN_VALIDATE_FAILURE = prefix + '/LOGIN_VALIDATE_FAILURE';
