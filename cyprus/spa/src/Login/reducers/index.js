import {combineReducers} from 'redux'
import * as Action from '../actions'
import qs from 'query-string'

const query = qs.parse(window.location.search)
const initialLogin = query.login || null
const initialIsActivated = query.from === 'activation'

const login = (prev = initialLogin, action) => {
    switch (action.type) {
        case Action.LOGIN_CREDENTIALS_CHANGED:
            if (action.payload.login !== undefined) {
                return action.payload.login
            }

            return prev
        default:
            return prev
    }
}

const password = (prev = null, action) => {
    switch (action.type) {
        case Action.LOGIN_CREDENTIALS_CHANGED:
            if (action.payload.password !== undefined) {
                return action.payload.password
            }

            return prev
        default:
            return prev
    }
}

const errors = (prev = [], action) => {
    switch (action.type) {
        case Action.LOGIN_BEFORE:
        case Action.LOGIN_SUCCESS:
        case Action.LOGIN_VALIDATE_SUCCESS:
            return []
        case Action.LOGIN_VALIDATE_FAILURE:
            return action.payload.errors
        case Action.LOGIN_FAILURE:
            const data = action.payload.data
            if (data !== undefined && data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }

            return []
        default:
            return prev
    }
}

const isValid = (prev = false, action) => {
    switch (action.type) {
        case Action.LOGIN_VALIDATE_FAILURE:
        case Action.LOGIN_SUCCESS:
            return false
        case Action.LOGIN_VALIDATE_SUCCESS:
            return true
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.LOGIN_BEFORE:
            return true
        case Action.LOGIN_SUCCESS:
        case Action.LOGIN_FAILURE:
            return false
        default:
            return prev
    }
}

const canFocusOnPassword = (prev = !!initialLogin, action) => prev

const isAccountActivated = (prev = initialIsActivated, action) => prev

export default combineReducers({
    login,
    password,
    errors,
    isValid,
    isLoading,
    canFocusOnPassword,
    isAccountActivated,
})
