import translator from '../../../translations/translator'
import EmailValidator from "email-validator";

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.email) {
        if (!model.email) {
            ++validator.count
            validator.errors.email = translator('validation_required')
        } else if (!EmailValidator.validate(model.email)) {
            ++validator.count
            validator.errors.email = translator('validation_invalid_email')
        }
    } else {
        ++validator.count
    }

    return validator
}