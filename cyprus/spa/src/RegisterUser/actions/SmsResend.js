import request from '../../Common/request'
import {SMS_RESEND_BEFORE, SMS_RESEND_FAILURE, SMS_RESEND_SUCCESS} from '../actions'

export default (id) => dispatch => {

    dispatch({
        type: SMS_RESEND_BEFORE
    })

    request.post(AppRouter.POST.smsResend.replace('_ID_', id))
        .then(({data}) => {
            dispatch({
                type: SMS_RESEND_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: SMS_RESEND_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
