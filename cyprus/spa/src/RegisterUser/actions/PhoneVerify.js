import request from '../../Common/request'
import {PHONE_VERIFY_BEFORE, PHONE_VERIFY_FAILURE, PHONE_VERIFY_SUCCESS} from '../actions'

export default data => dispatch => {

    dispatch({
        type: PHONE_VERIFY_BEFORE
    })

    request.post(AppRouter.POST.phoneVerify, data)
        .then(({data, status}) => {
            dispatch({
                type: PHONE_VERIFY_SUCCESS,
                payload: data,
                status
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: PHONE_VERIFY_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
