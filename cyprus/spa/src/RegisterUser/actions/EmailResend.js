import request from '../../Common/request'
import {EMAIL_RESEND_BEFORE, EMAIL_RESEND_FAILURE, EMAIL_RESEND_SUCCESS} from '../actions'

export default email => dispatch => {

    dispatch({
        type: EMAIL_RESEND_BEFORE
    })

    request.post(AppRouter.POST.emailResendAnon, {email})
        .then(({data, status}) => {
            dispatch({
                type: EMAIL_RESEND_SUCCESS,
                payload: data,
                status
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: EMAIL_RESEND_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
