import React from 'react';
import translator from '../../translations/translator';
import {connect} from 'react-redux';
import selectors from './selectors';

class Success extends React.Component {

    render() {

        const {model} = this.props.RegisterUser

        return <div className="row no-gutters">
            <div className="col-10 col-xl-6 mx-auto">

                <div className="banner">

                    <h3>{translator('register_success_title')}</h3>

                    <p>{translator('register_success_subtitle')}</p>

                    <a href={AppRouter.GET.loginIndex + "?login=" + model.email}
                       className="btn btn-primary">
                        <i className="fa fa-user-circle"/>&nbsp;{translator('login_action')}
                    </a>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Success)
