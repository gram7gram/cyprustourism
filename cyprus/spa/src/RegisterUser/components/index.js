import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import Save from '../actions/Save';
import translator from '../../translations/translator';
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Success from "./Success";

class RegisterUser extends React.Component {

    submit = () => {
        const {model} = this.props.RegisterUser

        this.props.dispatch(Save(model))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.RegisterUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    renderStep() {

        const {step} = this.props.RegisterUser

        switch (step) {
            case 1:
                return <Step1/>
            case 2:
                return <Step2/>
            case 3:
                return <Step3/>
        }
    }

    renderContent() {

        const {serverErrors, isSaveSuccess, isAlreadyRegistered, model, isFromInquiry} = this.props.RegisterUser

        if (isSaveSuccess) {
            return <Success/>
        }

        return <div className="row no-gutters">
            <div className="col-12">

                <div className="row">
                    <div className="col-10 col-lg-8 mx-auto">

                        {!isFromInquiry
                            ? <div className="mb-3">
                                <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_user_register')}</h2>
                                <p>{translator('navigation_user_register_description')}</p>
                            </div>
                            : <div className="mb-3">
                                <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_user_register_from_inquiry')}</h2>
                                <p>{translator('navigation_user_register_description_from_inquiry')}</p>
                            </div>}

                        {isAlreadyRegistered && <div className="alert alert-success text-center">
                            <div>{translator('already_registered_message')}</div>

                            <a href={AppRouter.GET.loginIndex + '?login=' + model.email}
                               className="btn btn-primary">
                                <i className="fa fa-user-circle"/>&nbsp;{translator('login_action')}
                            </a>
                        </div>}

                        {serverErrors.length > 0 && <div className="alert alert-danger">
                            <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                        </div>}
                    </div>
                </div>

                {this.renderStep()}
            </div>
        </div>
    }

    render() {

        return <div className="col-12 col-xl-8 mx-auto">
            <div className="space-3">
                {this.renderContent()}
            </div>
        </div>
    }
}

export default connect(selectors)(RegisterUser)
