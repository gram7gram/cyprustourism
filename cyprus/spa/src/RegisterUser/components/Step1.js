import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import EmailResend from "../actions/EmailResend";

class Step1 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    submit = () => {
        const {model} = this.props.RegisterUser

        this.props.dispatch(EmailResend(model.email))
    }

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.RegisterUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isValid, isLoading} = this.props.RegisterUser

        return <div className="row">
            <div className="col-10 col-lg-6 mx-auto">
                <div className="form-group text-left">
                    <label className="m-0 required">{translator('email')}</label>
                    <input type="email"
                           autoFocus={true}
                           className="form-control"
                           name="email"
                           placeholder={translator('enter_email_placeholder')}
                           onChange={this.changeString('email')}
                           value={model.email || ''}/>
                    {this.getError('email')}
                </div>
            </div>

            <div className="col-12">
                <div className="form-group text-center">
                    <button type="button"
                            className="btn btn-primary transition-3d-hover"
                            onClick={this.submit}
                            disabled={isLoading || !isValid}>
                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                        &nbsp;{translator('verify_phone')}
                    </button>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step1)
