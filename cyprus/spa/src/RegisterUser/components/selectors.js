import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    RegisterUser: store => store.RegisterUser,
})
