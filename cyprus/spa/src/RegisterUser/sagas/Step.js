import {all, put, takeEvery} from 'redux-saga/effects'
import {EMAIL_RESEND_SUCCESS, EMAIL_VERIFY_SUCCESS, SET_STEP} from '../actions'

function* step2({status}) {

    //Status 201 - User is new, status 200 - user already exists
    if (status === 200) return

    yield put({
        type: SET_STEP,
        payload: 2
    })
}

function* step3() {
    yield put({
        type: SET_STEP,
        payload: 3
    })
}

export default function* sagas() {
    yield all([
        takeEvery(EMAIL_RESEND_SUCCESS, step2),

        takeEvery(EMAIL_VERIFY_SUCCESS, step3)
    ])
}
