import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import Step from './Step'

export default function* sagas() {
    yield all([
        fork(Validation),
        fork(Step),
    ])
}
