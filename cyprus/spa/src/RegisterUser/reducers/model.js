import {combineReducers} from 'redux'
import * as Action from '../actions'

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.EMAIL_RESEND_SUCCESS:
        case Action.SAVE_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const email = (prev = null, action) => {
    switch (action.type) {
        case Action.EMAIL_RESEND_SUCCESS:
        case Action.MODEL_CHANGED:
            if (action.payload.email !== undefined) {
                return action.payload.email
            }
            return prev
        default:
            return prev
    }
}

const phone = (prev = null, action) => {
    switch (action.type) {
        case Action.PHONE_VERIFY_SUCCESS:
        case Action.MODEL_CHANGED:
            if (action.payload.phone !== undefined) {
                return action.payload.phone
            }
            return prev
        default:
            return prev
    }
}

const dialCode = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.dialCode !== undefined) {
                return action.payload.dialCode
            }
            return prev
        default:
            return prev
    }
}

const isPhoneValid = (prev = true, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isPhoneValid !== undefined) {
                return action.payload.isPhoneValid
            }
            return prev
        default:
            return prev
    }
}

const name = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.name !== undefined) {
                return action.payload.name
            }
            return prev
        default:
            return prev
    }
}

const surname = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.surname !== undefined) {
                return action.payload.surname
            }
            return prev
        default:
            return prev
    }
}

const password = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.password !== undefined) {
                return action.payload.password
            }
            return prev
        default:
            return prev
    }
}

const password2 = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.password2 !== undefined) {
                return action.payload.password2
            }
            return prev
        default:
            return prev
    }
}

const isAccepted = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isAccepted !== undefined) {
                return action.payload.isAccepted
            }
            return prev
        default:
            return prev
    }
}

const country = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.country !== undefined) {
                return action.payload.country
            }
            return prev
        default:
            return prev
    }
}

const smsCode = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.smsCode !== undefined) {
                return action.payload.smsCode
            }
            return prev
        case Action.SMS_RESEND_SUCCESS:
            return null
        default:
            return prev
    }
}

const emailCode = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.emailCode !== undefined) {
                return action.payload.emailCode
            }
            return prev
        case Action.SMS_RESEND_SUCCESS:
            return null
        default:
            return prev
    }
}

export default combineReducers({
    id,
    email,
    phone,
    isPhoneValid,
    dialCode,
    name,
    surname,
    country,
    password,
    password2,
    isAccepted,
    smsCode,
    emailCode,
})