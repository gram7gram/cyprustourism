const prefix = 'MyInquiriesProvider'

export const FETCH_REQUEST = prefix + '/FETCH_REQUEST';
export const FETCH_BEFORE = prefix + '/FETCH_BEFORE';
export const FETCH_SUCCESS = prefix + '/FETCH_SUCCESS';
export const FETCH_FAILURE = prefix + '/FETCH_FAILURE';

export const FETCH_OFFERED_REQUEST = prefix + '/FETCH_OFFERED_REQUEST';
export const FETCH_OFFERED_BEFORE = prefix + '/FETCH_OFFERED_BEFORE';
export const FETCH_OFFERED_SUCCESS = prefix + '/FETCH_OFFERED_SUCCESS';
export const FETCH_OFFERED_FAILURE = prefix + '/FETCH_OFFERED_FAILURE';

export const FETCH_SELECTED_REQUEST = prefix + '/FETCH_SELECTED_REQUEST';
export const FETCH_SELECTED_BEFORE = prefix + '/FETCH_SELECTED_BEFORE';
export const FETCH_SELECTED_SUCCESS = prefix + '/FETCH_SELECTED_SUCCESS';
export const FETCH_SELECTED_FAILURE = prefix + '/FETCH_SELECTED_FAILURE';

export const FILTER_CHANGED = prefix + '/FILTER_CHANGED';
export const FILTER_OFFERED_CHANGED = prefix + '/FILTER_OFFERED_CHANGED';
export const FILTER_SELECTED_CHANGED = prefix + '/FILTER_SELECTED_CHANGED';
