import {all, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_REQUEST} from '../actions'
import FetchItems, {parseBeforeSubmit} from '../actions/FetchItems'
import {updateCurrentUrlWithFilter} from "../../Common/utils";


function* run({payload}) {
    const {filter, pagination} = yield select(store => store.MyInquiriesProvider.Active)

    const page = payload && payload.page ? payload.page : pagination.page

    yield put(FetchItems(filter, page))

    updateCurrentUrlWithFilter(page, parseBeforeSubmit(filter))
}

export default function* sagas() {
    yield all([
        takeEvery(FETCH_REQUEST, run)
    ])
}
