import {all, fork} from 'redux-saga/effects'
import Fetch from './Fetch'
import FetchOffered from './FetchOffered'
import FetchSelected from './FetchSelected'

export default function* sagas() {
    yield all([
        fork(Fetch),
        fork(FetchOffered),
        fork(FetchSelected),
    ])
}
