import {all, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_OFFERED_REQUEST} from '../actions'
import FetchItems from '../actions/FetchOfferedItems'

function* run({payload}) {
    const {filter, pagination} = yield select(store => store.MyInquiriesProvider.Offered)

    const page = payload && payload.page ? payload.page : pagination.page

    yield put(FetchItems(filter, page))
}

export default function* sagas() {
    yield all([
        takeEvery(FETCH_OFFERED_REQUEST, run)
    ])
}
