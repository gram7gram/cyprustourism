import {combineReducers} from 'redux'

import Active from './Active'
import Selected from './Selected'
import Offered from './Offered'

export default combineReducers({
    Active,
    Selected,
    Offered,
})

