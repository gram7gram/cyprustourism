import {combineReducers} from 'redux'
import * as Action from '../../actions'

const search = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_SELECTED_CHANGED:
            if (action.payload.search !== undefined) {
                return action.payload.search
            }
            return prev
        default:
            return prev
    }
}

const types = (prev = [], action) => {
    switch (action.type) {
        case Action.FILTER_SELECTED_CHANGED:
            if (action.payload.types !== undefined) {
                return action.payload.types
            }
            return prev
        default:
            return prev
    }
}

const isVerified = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_SELECTED_CHANGED:
            if (action.payload.isVerified !== undefined) {

                if (!action.payload.isVerified) return null

                return true
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    isVerified,
    search,
    types,
})

