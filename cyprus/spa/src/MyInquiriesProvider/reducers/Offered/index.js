import {combineReducers} from 'redux'
import * as Action from '../../actions'
import filter from './filter'

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.FETCH_OFFERED_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_OFFERED_SUCCESS:
        case Action.FETCH_OFFERED_FAILURE:
            return false
        case Action.FETCH_OFFERED_BEFORE:
            return true
        default:
            return prev
    }
}

const initialPaginator = {
    page: 1,
    total: 0,
    limit: 0,
}

const pagination = (prev = initialPaginator, action) => {
    switch (action.type) {
        case Action.FETCH_OFFERED_SUCCESS:
            return {
                total: action.payload.total,
                page: action.payload.page,
                limit: action.payload.limit,
            }
        default:
            return prev
    }
}

export default combineReducers({
    items,
    isLoading,
    filter,
    pagination,
})

