import request from '../../Common/request'
import {FETCH_OFFERED_BEFORE, FETCH_OFFERED_FAILURE, FETCH_OFFERED_SUCCESS} from '../actions'

export const parseBeforeSubmit = filter => {
    const data = {...filter}

    data.providerQuotedOnly = true

    if (data.types.length > 0) {
        data.types = data.types.map(item => item.value).join(',')
    } else {
        delete data.types
    }

    if (data.search) {
        data.search = data.search.trim()
    } else {
        delete data.search
    }

    return data
}

export default (filter, page) => dispatch => {

    const data = parseBeforeSubmit(filter)

    dispatch({
        type: FETCH_OFFERED_BEFORE
    })

    const query = [
        'page=' + page
    ]

    Object.keys(data).forEach(key => {
        let value = data[key]
        if (typeof value === "boolean") {
            value = value ? 1 : 0
        }
        if (value !== null) {
            query.push('filter[' + key + ']=' + value)
        }
    })

    request.get(AppRouter.GET.inquiries + '?' + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_OFFERED_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_OFFERED_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
