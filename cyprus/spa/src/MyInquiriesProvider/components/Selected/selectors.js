import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    MyInquiries: store => store.MyInquiriesProvider.Selected,
})
