import React from 'react';
import {FETCH_OFFERED_REQUEST, FILTER_OFFERED_CHANGED} from '../../actions';
import translator from '../../../translations/translator';
import Select from "../../../Common/components/Select";
import {connect} from 'react-redux';
import selectors from './selectors';

const typeOptions = [
    {value: 'car_rent', label: translator('inquiry_type_car_rent')},
    {value: 'boat_rent', label: translator('inquiry_type_boat_rent')},
    {value: 'accommodation_rent', label: translator('inquiry_type_accommodation_rent')},
    {value: 'transfer', label: translator('inquiry_type_transfer')},
]

class Filter extends React.Component {

    submit = () => {
        this.props.dispatch({
            type: FETCH_OFFERED_REQUEST,
            payload: {
                page: 1
            }
        })
    }

    change = (key, value = null) => this.props.dispatch({
        type: FILTER_OFFERED_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, e.target.checked)

    render() {

        const {isLoading, filter} = this.props.MyInquiries

        return <div className="row mb-3">
            <div className="col-12">
                <div className="row no-gutters">
                    <div className="col-12 col-md-6 col-lg-3">

                        <Select
                            isMulti={true}
                            isSearchable={false}
                            placeholder={translator('select_type')}
                            value={filter.types}
                            onChange={this.changeByKey('types')}
                            options={typeOptions}
                            className="mr-1 mb-1  w-95"/>
                    </div>
                    <div className="col-12 col-md-6 col-lg-3">
                        <div className="custom-control custom-checkbox mt-2">
                            <input type="checkbox"
                                   id="inquiries-offered-isVerified"
                                   className="custom-control-input"
                                   onChange={this.changeBool('isVerified')}
                                   checked={!!filter.isVerified}/>

                            <label htmlFor="inquiries-offered-isVerified" className="custom-control-label">
                                {translator('verified_contact_only')}
                            </label>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-2">

                        <button className="btn btn-primary btn-sm transition-3d-hover"
                                type="button"
                                onClick={this.submit}
                                disabled={isLoading}>
                            <i className={"fa " + (isLoading
                                ? "fa-spin fa-circle-notch"
                                : "fa-search")}/>
                            &nbsp;{translator('search')}
                        </button>

                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Filter)
