import React from 'react';

import Active from './Active';
import Selected from './Selected';
import Offered from './Offered';

class MyInquiries extends React.Component {

    render() {

        return <div className="row">
            <div className="col-12">
                <Active/>
            </div>
            <div className="col-12">
                <Offered/>
            </div>
            <div className="col-12">
                <Selected/>
            </div>
        </div>
    }
}

export default MyInquiries
