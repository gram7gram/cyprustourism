import {combineReducers} from 'redux'
import * as Action from '../actions'
import parameters from './parameters'
import keyBy from 'lodash/keyBy'
import {objectValues} from "../../Common/utils";

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const guid = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
            if (action.payload.guid !== undefined) {
                return action.payload.guid
            }
            return null
        default:
            return prev
    }
}

const media = (prev = [], action) => {
    let media
    switch (action.type) {
        case Action.UPLOAD_MEDIA_SUCCESS:
            media = keyBy(prev, 'id')

            media[action.payload.id] = action.payload

            return objectValues(media)
        case Action.REMOVE_MEDIA:
            media = keyBy(prev, 'id')

            delete media[action.payload]

            return objectValues(media)
        case Action.MODEL_CHANGED:
            if (action.payload.media !== undefined) {
                return action.payload.media
            }
            return prev
        default:
            return prev
    }
}

const text = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.text !== undefined) {
                return action.payload.text
            }
            return prev
        default:
            return prev
    }
}

const price = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.price !== undefined) {
                return action.payload.price
            }
            return prev
        default:
            return prev
    }
}

const locations = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.categories !== undefined) {
                return action.payload.categories
                    .filter(item => item.group === 'location')
                    .map(item => {

                        item.id = item.categoryId

                        delete item.categoryId

                        return item
                    })
            }
            return []
        case Action.ADD_LOCATION:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_LOCATION:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const extras = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.categories !== undefined) {
                return action.payload.categories
                    .filter(item => item.group === 'extras')
                    .map(item => {

                        item.id = item.categoryId

                        delete item.categoryId

                        return item
                    })
            }
            return []
        case Action.ADD_EXTRA:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_EXTRA:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const carTypes = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.categories !== undefined) {
                return action.payload.categories
                    .filter(item => item.group === 'car_type')
                    .map(item => {

                        item.id = item.categoryId

                        delete item.categoryId

                        return item
                    })
            }
            return []
        case Action.ADD_CAR_TYPE:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_CAR_TYPE:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const boatTypes = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.categories !== undefined) {
                return action.payload.categories
                    .filter(item => item.group === 'boat_type')
                    .map(item => {

                        item.id = item.categoryId

                        delete item.categoryId

                        return item
                    })
            }
            return []
        case Action.ADD_BOAT_TYPE:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_BOAT_TYPE:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const type = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.type !== undefined) {
                return action.payload.type
            }
            return null
        default:
            return prev
    }
}

export default combineReducers({
    id,
    guid,
    parameters,
    carTypes,
    boatTypes,
    locations,
    extras,
    media,
    type,
    text,
    price,
})
