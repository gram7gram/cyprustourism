import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    BoatRentProvider: store => store.BoatRentProvider,
    locale: store => store.UI.locale,
})
