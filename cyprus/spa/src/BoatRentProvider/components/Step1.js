import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import moment from 'moment';
import selectors from './selectors';
import translator from '../../translations/translator';
import DateTime from "../../Common/components/Date";
import {dateFormat, daysToWeeks} from "../../Common/utils";
import CategoryOption from "../../CarRent/components/CategoryOption";
import {daytimeOptions, hourlyOptions} from "../../BoatRent/components/Step1";

class Step1 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.quote_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    setRentalEnd = date => {

        const {rentalStart, isFixedDate} = this.props.BoatRentProviderProvider.quote.parameters

        let dayCount = 1

        if (rentalStart && isFixedDate) {
            const rentalEnd = moment(date, 'YYYY-MM-DD')

            dayCount = rentalEnd.diff(moment(rentalStart, 'YYYY-MM-DD'), 'days')
        }

        this.props.dispatch({
            type: Actions.MODEL_CHANGED,
            payload: {
                rentalEnd: date,
                dayCount
            }
        })
    }

    setFixedDate = isFixedDate => () => {

        const {rentalStart, rentalEnd} = this.props.BoatRentProvider.quote.parameters

        let dayCount = 1

        if (rentalStart && rentalEnd) {
            dayCount = moment(rentalEnd, 'YYYY-MM-DD').diff(moment(rentalStart, 'YYYY-MM-DD'), 'days')
        }

        this.props.dispatch({
            type: Actions.MODEL_CHANGED,
            payload: {
                isFixedDate,
                dayCount
            }
        })
    }

    getError = key => {
        const {errors} = this.props.BoatRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    setHourly = isHourly => () => {
        this.props.dispatch({
            type: Actions.MODEL_CHANGED,
            payload: {
                isHourly,
            }
        })
    }

    toggleHourlyRange = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_HOURLY_RANGE,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_HOURLY_RANGE,
                payload
            })
        }

    }

    toggleDaytimeRange = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_DAYTIME_RANGE,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_DAYTIME_RANGE,
                payload
            })
        }

    }

    renderHourlyStep2() {

        const {quote} = this.props.BoatRentProvider

        const isHourly = quote.parameters.isHourly
        const isFixedDate = quote.parameters.isFixedDate

        if (isHourly) {
            return <div className="row" id="boat-rent-step-1-daterange">

                <div className="col-12">
                    <h3 className="fg-primary">{translator('provider_boat_rent_step_1_title_4')}</h3>
                    <p dangerouslySetInnerHTML={{__html: translator('provider_boat_rent_step_1_description_4')}}/>
                </div>

                <div className="col-12 col-md-10 col-xl-8 mx-auto">

                    <div className="form-group">
                        <label className="m-0 required">
                            {translator('fixed_date_start')}
                        </label>
                        <DateTime
                            viewMode={"days"}
                            minDate={moment()}
                            onChange={this.changeByKey('rentalStart')}
                            value={dateFormat(quote.parameters.rentalStart)}
                            placeholderText={translator('from')}/>
                    </div>

                    <div className="row">
                        <div className="col-12 col-md-4 mb-4">

                            <div className="option-container">
                                {hourlyOptions.map((option, i) => {

                                    const isSelected = !!quote.parameters.hourlyRanges.find(item => item.id === option.value)

                                    return <CategoryOption
                                        key={i}
                                        isSelected={isSelected}
                                        onChange={this.toggleHourlyRange}
                                        model={{
                                            id: option.value,
                                            name: option.label,
                                        }}/>
                                })}
                            </div>
                        </div>

                        <div className="col-12 col-md-4 mb-4">

                            <div className="option-container">
                                {daytimeOptions.map((option, i) => {

                                    const isSelected = !!quote.parameters.daytimeRanges.find(item => item.id === option.value)

                                    return <CategoryOption
                                        key={i}
                                        isSelected={isSelected}
                                        onChange={this.toggleDaytimeRange}
                                        model={{
                                            id: option.value,
                                            name: option.label,
                                        }}/>
                                })}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        }

        const dayCount = quote.parameters.dayCount

        const dayCountTranslation = daysToWeeks(dayCount)

        let daysDiff = 0;
        const dayOptions = []
        if (quote.parameters.rentalStart && quote.parameters.rentalEnd) {
            const rentalEnd = moment(quote.parameters.rentalEnd, 'YYYY-MM-DD')
            const rentalStart = moment(quote.parameters.rentalStart, 'YYYY-MM-DD')

            daysDiff = rentalEnd.diff(rentalStart, 'days')

            for (var i = 1; i <= daysDiff; i++) {
                dayOptions.push(i)
            }
        }

        return <div className="row mb-4" id="boat-rent-step-1-daterange">

            <div className="col-12">
                <h3 className="fg-primary">{translator('provider_boat_rent_step_1_title_2')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('provider_boat_rent_step_1_description_2')}}/>
            </div>

            <div className="col-12 col-md-10 col-xl-8 mx-auto">
                <div className="row no-gutters mb-3">

                    <div className="col">
                        <div className="mr-1 mb-1">
                            <label className="m-0 required">
                                {!quote.parameters.isFixedDate
                                    ? translator('flexible_date_start')
                                    : translator('fixed_date_start')}
                            </label>
                            <DateTime
                                viewMode={"days"}
                                minDate={moment()}
                                onChange={this.changeByKey('rentalStart')}
                                value={dateFormat(quote.parameters.rentalStart)}
                                placeholderText={translator('from')}/>
                        </div>

                    </div>
                    <div className="col">
                        <div className="mr-1 mb-1">
                            <label className="m-0 required">
                                {!quote.parameters.isFixedDate
                                    ? translator('flexible_date_end')
                                    : translator('fixed_date_end')}
                            </label>
                            <DateTime
                                viewMode={"days"}
                                minDate={quote.parameters.rentalStart
                                    ? moment(quote.parameters.rentalStart, 'YYYY-MM-DD').add(1, 'day')
                                    : moment()}
                                onChange={this.setRentalEnd}
                                value={dateFormat(quote.parameters.rentalEnd)}
                                placeholderText={translator('to')}/>
                        </div>
                    </div>
                </div>

                <div className="row no-gutters">

                    {!isFixedDate
                        ? <div className="col-12">
                            <label className="m-0 required">
                                {translator('flexible_days_count')}
                            </label>
                            <div className="mr-1 mb-1">
                                <select
                                    name="dayCount"
                                    className="form-control"
                                    onChange={this.changeInt('dayCount')}
                                    value={quote.parameters.dayCount}>
                                    {dayOptions.map((day, i) =>
                                        <option key={i}
                                                value={day}>{day}</option>
                                    )}
                                </select>

                            </div>
                        </div>
                        : <div className="col-12 text-center">
                            <div className="font-weight-bold">
                                {translator('boat_rent_day_offer_title')}
                                &nbsp;<span>{translator(dayCountTranslation)}</span>
                            </div>
                        </div>}
                </div>
            </div>
        </div>
    }

    render() {
        const {quote} = this.props.BoatRentProvider

        const isHourly = quote.parameters.isHourly
        const isFixedDate = quote.parameters.isFixedDate

        return <div className="row">

            <div className="col-12">

                <div className="row mb-4" id="boat-rent-step-1-hourly">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('provider_boat_rent_step_1_title_3')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('provider_boat_rent_step_1_description_3')}}/>
                    </div>

                    <div className="col-12 col-md-10 col-xl-8 mx-auto">

                        <div className="row mb-3">
                            <div className="col-12">
                                <div className="btn-group btn-group-toggle d-flex">
                                    <label
                                        className={"btn btn-sm flex-fill " + (isHourly ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               id="rent_is_date_fixed"
                                               checked={isHourly}
                                               onChange={this.setHourly(true)}/>
                                        {isHourly && <i className="fa fa-check"/>}
                                        &nbsp;{translator('boat_rent_hourly_range')}
                                    </label>

                                    <label
                                        className={"btn btn-sm flex-fill " + (!isHourly ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               id="rent_is_date_flexible"
                                               checked={!isHourly}
                                               onChange={this.setHourly(false)}/>
                                        {!isHourly && <i className="fa fa-check"/>}
                                        &nbsp;{translator('boat_rent_daily_range')}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {!isHourly ? <div className="row mb-4" id="boat-rent-step-1-fixed-date">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('provider_boat_rent_step_1_title_1')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('provider_boat_rent_step_1_description_1')}}/>
                    </div>

                    <div className="col-12 col-md-10 col-xl-8 mx-auto">

                        <div className="row mb-3">
                            <div className="col-12">
                                <div className="btn-group btn-group-toggle d-flex">
                                    <label
                                        className={"btn btn-sm flex-fill " + (isFixedDate ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               id="rent_is_date_fixed"
                                               checked={isFixedDate}
                                               onChange={this.setFixedDate(true)}/>
                                        {isFixedDate && <i className="fa fa-check"/>}
                                        &nbsp;{translator('rent_is_date_fixed')}
                                    </label>

                                    <label
                                        className={"btn btn-sm flex-fill " + (!isFixedDate ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               id="rent_is_date_flexible"
                                               checked={!isFixedDate}
                                               onChange={this.setFixedDate(false)}/>
                                        {!isFixedDate && <i className="fa fa-check"/>}
                                        &nbsp;{translator('rent_is_date_flexible')}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null}

                {this.renderHourlyStep2()}

            </div>
        </div>
    }
}

export default connect(selectors)(Step1)
