import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import Media from "../../Common/components/Media";
import UploadMedia from "../actions/UploadMedia";

class Step5 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeFloat = name => e => {

        let value = parseFloat(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    removeMedia = payload => () => {
        this.props.dispatch({
            type: Actions.REMOVE_MEDIA,
            payload
        })
    }

    addFiles = e => {
        const files = e.target.files

        Array.from(files).forEach(file => {
            this.props.dispatch(UploadMedia(file))
        })

        e.target.value = null
    }

    getError = key => {
        const {errors} = this.props.BoatRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.BoatRentProvider

        return <div className="row mb-4" id="boat-rent-step-5-price">

            <div className="col-12">
                <h3 className="fg-primary">{translator('provider_boat_rent_step_5_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('provider_boat_rent_step_5_description_1')}}/>
            </div>

            <div className="col-12">

                <div className="row">
                    <div className="col-12 col-md-6 col-xl-4">
                        <div className="form-group">
                            <label className="m-0">{translator('price')}</label>
                            <div className="input-group">
                                <input type="number"
                                       className="form-control"
                                       min={0}
                                       step={0.01}
                                       autoFocus={true}
                                       value={quote.price || ''}
                                       onChange={this.changeFloat('price')}/>
                                <div className="input-group-append">
                                    <span className="input-group-text"><i className="fas fa-euro-sign"/></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-xl-8">
                        <div className="form-group">
                            <label className="m-0">{translator('comment')}</label>
                            <textarea
                                name="text"
                                className="form-control"
                                onChange={this.changeString('text')}
                                value={quote.text || ''}/>
                            <div className="fg-warning">
                                <i className="fa fa-exclamation-circle"/>&nbsp;{translator('quote_comment_warning')}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row no-gutters">
                    <div className="col-12">
                        <div className="form-group">
                            <label className="m-0">{translator('quote_media')}</label>
                            <input type="file"
                                   name="media"
                                   min={1}
                                   max={10}
                                   multiple={true}
                                   accept={AppParameters.uploadImageMimeTypes}
                                   onChange={this.addFiles}
                                   disabled={quote.media.length === 10}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row no-gutters">
                            {quote.media.map((media, i) => <Media media={media} key={i}/>)}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    }
}

export default connect(selectors)(Step5)
