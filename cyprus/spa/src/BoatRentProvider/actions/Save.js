import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.media) {
        data.media = data.media.map(item => item.id)
    }

    if (data.parameters.daytimeRanges) {
        data.parameters.daytimeRanges = data.parameters.daytimeRanges.map(item => item.id)
    }

    if (data.parameters.hourlyRanges) {
        data.parameters.hourlyRanges = data.parameters.hourlyRanges.map(item => item.id)
    }

    if (data.locations || data.boatTypes || data.extras) {
        data.categories = []

        if (data.locations) {
            data.locations.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.locations
        }

        if (data.boatTypes) {
            data.boatTypes.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.boatTypes
        }

        if (data.extras) {
            data.extras.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.extras
        }
    }

    return data
}

export default (guid, model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: SAVE_BEFORE
    })

    request.post(AppRouter.POST.quotesByGuid.replace('_GUID_', guid), data)
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })

            if (callback) {
                callback()
            }
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
