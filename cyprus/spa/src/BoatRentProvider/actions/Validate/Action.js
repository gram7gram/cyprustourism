import translator from '../../../translations/translator'
import moment from 'moment'

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.price) {
        if (model.price <= 0) {
            ++validator.count;
            validator.errors.price = translator('validation_required')
        }
    } else {
        ++validator.count;
    }

    if (model.locations.length === 0) {
        ++validator.count
        validator.errors.locations = translator('validation_required')
    }

    if (model.boatTypes.length === 0) {
        ++validator.count
        validator.errors.boatTypes = translator('validation_required')
    }

    if (!model.parameters.dayCount) {
        ++validator.count
        validator.errors.dayCount = translator('validation_required')
    }

    if (!model.parameters.driverAgeFrom) {
        ++validator.count
        validator.errors.driverAgeFrom = translator('validation_required')
    } else if (model.parameters.driverAgeFrom < 18) {
        ++validator.count
        validator.errors.driverAgeFrom = translator('validation_invalid_driver_age_minor')
    } else if (model.parameters.driverAgeFrom > 100) {
        ++validator.count
        validator.errors.driverAgeFrom = translator('validation_invalid_driver_age_senile')
    }

    if (!model.parameters.isHourly) {
        if (!model.parameters.rentalStart) {
            ++validator.count
            validator.errors.rentalStart = translator('validation_required')
        }

        if (!model.parameters.rentalEnd) {
            ++validator.count
            validator.errors.rentalEnd = translator('validation_required')
        }

        if (model.parameters.rentalStart && model.parameters.rentalEnd) {
            const date1 = moment(model.parameters.rentalStart, 'YYYY-MM-DD')
            const date2 = moment(model.parameters.rentalEnd, 'YYYY-MM-DD')

            if (date2.isSameOrBefore(date1)) {
                ++validator.count
                validator.errors.rentalEnd = translator('validation_rental_range')

            }
        }
    } else {

        if (!model.parameters.daytimeRanges.length) {
            ++validator.count
            validator.errors.daytimeRanges = translator('validation_required')
        }

        if (!model.parameters.hourlyRanges.length) {
            ++validator.count
            validator.errors.hourlyRanges = translator('validation_required')
        }

    }
    return validator
}
