import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import * as Actions from '../actions';
import CategoryOption from "../../CarRent/components/CategoryOption";

class Step3 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    toggleSubtypes = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_SUBTYPE,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_SUBTYPE,
                payload
            })
        }

    }

    getError = key => {
        const {errors} = this.props.AccommodationRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    renderSubtypeForm() {

        const {quote} = this.props.AccommodationRentProvider

        const subtypes = quote.parameters.subtypes.map(item => item.id)

        const hasRooms = subtypes.indexOf('hotel') !== -1
            || subtypes.indexOf('bnb') !== -1

        const hasBedrooms = subtypes.indexOf('apartment') !== -1
            || subtypes.indexOf('house') !== -1
            || subtypes.indexOf('villa') !== -1

        return <div className="row no-gutters">

            <div className="col-12 col-md-3">

                <label className="m-0 required">
                    {translator('accommodation_rent_adults_count')}
                </label>

                <div className="mr-1 mb-1">
                    <input type="number"
                           min={1}
                           step={1}
                           onChange={this.changeInt('adultsCount')}
                           placeholder={translator('enter_amount_placeholder')}
                           value={quote.parameters.adultsCount || ''}
                           className="form-control"/>
                </div>
            </div>

            <div className="col-12 col-md-3">

                <label className="m-0">
                    {translator('accommodation_rent_children_count')}
                </label>

                <div className="mr-1 mb-1">
                    <input type="number"
                           min={1}
                           step={1}
                           onChange={this.changeInt('childrenCount')}
                           placeholder={translator('enter_amount_placeholder')}
                           value={quote.parameters.childrenCount || ''}
                           className="form-control"/>
                </div>
            </div>

            {hasRooms ? <div className="col-12 col-md-3">

                <label className="m-0 required">
                    {translator('accommodation_rent_room_count')}
                </label>

                <div className="mr-1 mb-1">
                    <input type="number"
                           min={1}
                           step={1}
                           onChange={this.changeInt('roomCount')}
                           placeholder={translator('enter_amount_placeholder')}
                           value={quote.parameters.roomCount || ''}
                           className="form-control"/>
                </div>
            </div> : null}

            {hasBedrooms ? <div className="col-12 col-md-3">

                <label className="m-0 required">
                    {translator('accommodation_rent_bedroom_count')}
                </label>

                <div className="mr-1 mb-1">
                    <input type="number"
                           min={1}
                           step={1}
                           onChange={this.changeInt('bedroomCount')}
                           placeholder={translator('enter_amount_placeholder')}
                           value={quote.parameters.bedroomCount || ''}
                           className="form-control"/>
                </div>
            </div> : null}
        </div>
    }

    render() {

        const {quote} = this.props.AccommodationRentProvider
        const {subtypeOptions} = this.props.AccommodationRentProvider.Categories

        return <div className="row">

            <div className="col-12">

                <div className="row mb-4" id="accommodation-rent-step-3-hotel">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('accommodation_rent_step_3_title_1')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('accommodation_rent_step_3_description_1')}}/>
                    </div>

                    <div className="col-12">

                        <div className="option-container">
                            {subtypeOptions.map((option, i) => {

                                const isSelected = !!quote.parameters.subtypes.find(item => item.id === option.value)

                                return <CategoryOption
                                    key={i}
                                    isSelected={isSelected}
                                    onChange={this.toggleSubtypes}
                                    model={{
                                        id: option.value,
                                        name: option.label,
                                    }}/>
                            })}
                        </div>
                    </div>
                </div>

                <div className="row mb-4" id="accommodation-rent-step-3-rooms">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('accommodation_rent_step_3_title_2')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('accommodation_rent_step_3_description_2')}}/>
                    </div>

                    <div className="col-12">
                        {this.renderSubtypeForm()}
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step3)
