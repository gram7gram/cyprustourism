import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import * as Actions from '../actions';
import CategoryOption from "../../CarRent/components/CategoryOption";

class Step2 extends React.Component {

    toggleLocation = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_LOCATION,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_LOCATION,
                payload
            })
        }

    }

    getError = key => {
        const {errors} = this.props.AccommodationRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.AccommodationRentProvider
        const {locationOptions} = this.props.AccommodationRentProvider.Categories

        const groups = []
        let currentGroup = -1;

        locationOptions.forEach((option, idx) => {

            if (idx % 5 === 0) {
                ++currentGroup
                groups[currentGroup] = []
            }

            groups[currentGroup].push(option)
        })

        return <div className="row mb-4" id="accommodation-rent-step-2-location">

            <div className="col-12">
                <h3 className="fg-primary">{translator('accommodation_rent_step_2_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('accommodation_rent_step_2_description_1')}}/>
            </div>

            <div className="col-12">

                <div className="option-container">
                    <div className="row">
                        {groups.map((options, idx) => {

                            return <div key={idx} className="col-10 col-lg-6 text-truncate">

                                {options.map((option, key) => {

                                    const isSelected = !!quote.locations.find(item => item.id === option.id)

                                    return <CategoryOption
                                        key={key}
                                        isSelected={isSelected}
                                        model={option}
                                        onChange={this.toggleLocation}/>
                                })}
                            </div>
                        })}
                    </div>
                </div>

            </div>
        </div>
    }
}

export default connect(selectors)(Step2)
