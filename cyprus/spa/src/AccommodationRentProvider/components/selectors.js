import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AccommodationRentProvider: store => store.AccommodationRentProvider,
    locale: store => store.UI.locale,
})
