import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import FetchCategories from '../../AdminCategories/actions/FetchItems';
import translator from '../../translations/translator';

import Step1 from "./Step1";
import Step3 from "./Step3";
import Step2 from "./Step2";
import Step4 from "./Step4";
import Step5 from "./Step5";
import Success from "./Success";

import FetchInquiry from "../actions/Fetch";
import Save from "../actions/Save";
import {scrollTo} from "../../Common/utils";

class AccommodationRent extends React.Component {

    componentWillMount() {

        this.props.dispatch(FetchCategories({
            locale: this.props.locale,
            type: 'accommodation_rent'
        }, 1, 0))

        this.props.dispatch(FetchInquiry(AppParameters.currentRoute.params.guid))
    }

    scrollTo = (target) => (e) => {

        e.preventDefault();

        scrollTo(target)
    }

    submit = () => {
        const {quote, inquiry} = this.props.AccommodationRentProvider

        this.props.dispatch(Save(inquiry.guid, quote))
    }

    renderContent = () => {
        const {isSaveSuccess, isValid, isLoading} = this.props.AccommodationRentProvider

        if (isSaveSuccess) {
            return <div className="row space-bottom-2">
                <div className="col-12 col-lg-8 mx-auto">
                    <Success/>
                </div>
            </div>
        }

        return <div className="row">

            <div className="d-none d-xl-block col-xl-3">
                <nav className="card border-0 bg-primary">
                    <ul className="simple py-3 px-5">
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-5-price')}
                               href="#accommodation-rent-step-5-price">{translator('provider_accommodation_rent_step_5_title_1')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-1-fixed-date')}
                               href="#accommodation-rent-step-1-fixed-date">{translator('provider_accommodation_rent_step_1_title_1')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-1-daterange')}
                               href="#accommodation-rent-step-1-daterange">{translator('provider_accommodation_rent_step_1_title_2')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-2-location')}
                               href="#accommodation-rent-step-2-location">{translator('provider_accommodation_rent_step_2_title_1')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-3-hotel')}
                               href="#accommodation-rent-step-3-hotel">{translator('provider_accommodation_rent_step_3_title_1')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-3-rooms')}
                               href="#accommodation-rent-step-3-rooms">{translator('provider_accommodation_rent_step_3_title_2')}</a>
                        </li>
                        <li>
                            <a className="text-white py-3 d-block"
                               onClick={this.scrollTo('accommodation-rent-step-4-extra')}
                               href="#accommodation-rent-step-4-extra">{translator('provider_accommodation_rent_step_4_title_1')}</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div className="col-12 col-md-10 col-xl-9 mx-auto">

                <Step5/>

                <div className="row">
                    <div className="col-12 col-md-6 col-xl-4 mx-auto space-2">

                        <button type="button"
                                className="btn btn-primary btn-block transition-3d-hover"
                                onClick={this.submit}
                                disabled={!isValid || isLoading}>
                            <i className={"fa " + (isLoading
                                ? "fa-spin fa-circle-notch"
                                : "fa-check")}/>
                            &nbsp;{translator('inquiry_submit')}
                        </button>
                    </div>
                </div>

                <Step1/>

                <Step2/>

                <Step3/>

                <Step4/>

                <div className="row">
                    <div className="col-12 col-md-6 col-xl-4 mx-auto space-2">

                        <button type="button"
                                className="btn btn-primary btn-block transition-3d-hover"
                                onClick={this.submit}
                                disabled={!isValid || isLoading}>
                            <i className={"fa " + (isLoading
                                ? "fa-spin fa-circle-notch"
                                : "fa-check")}/>
                            &nbsp;{translator('inquiry_submit')}
                        </button>
                    </div>
                </div>

            </div>
        </div>
    }

    render() {
        const {serverErrors} = this.props.AccommodationRentProvider

        return <div className="row">
            <div className="col-12">

                <div className="space-top-2 space-bottom-1">
                    <h1 className="h2 mb-0">
                        <span>{translator('provider_accommodation_rent_banner_title')}</span>
                        &nbsp;<span
                        className="fg-primary font-weight-bold">{translator('provider_accommodation_rent_banner_title_cyprus')}</span>
                    </h1>
                    <p>{translator('provider_accommodation_rent_banner_subtitle')}</p>
                </div>

                {serverErrors.length > 0 && <div className="alert alert-danger text-center my-3">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                {this.renderContent()}
            </div>
        </div>
    }
}

export default connect(selectors)(AccommodationRent)
