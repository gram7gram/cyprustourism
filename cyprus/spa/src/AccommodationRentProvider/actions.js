const prefix = 'AccommodationRentProvider'

export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const VALIDATE_REQUEST = prefix + '/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = prefix + '/VALIDATE_SUCCESS';
export const VALIDATE_FAILURE = prefix + '/VALIDATE_FAILURE';

export const MODEL_CHANGED = prefix + '/MODEL_CHANGED';

export const REMOVE_SUBTYPE = prefix + '/REMOVE_SUBTYPE';
export const ADD_SUBTYPE = prefix + '/ADD_SUBTYPE';

export const REMOVE_EXTRA = prefix + '/REMOVE_EXTRA';
export const ADD_EXTRA = prefix + '/ADD_EXTRA';

export const REMOVE_LOCATION = prefix + '/REMOVE_LOCATION';
export const ADD_LOCATION = prefix + '/ADD_LOCATION';

export const UPLOAD_MEDIA_BEFORE = prefix + '/UPLOAD_MEDIA_BEFORE';
export const UPLOAD_MEDIA_SUCCESS = prefix + '/UPLOAD_MEDIA_SUCCESS';
export const UPLOAD_MEDIA_FAILURE = prefix + '/UPLOAD_MEDIA_FAILURE';

export const FETCH_BEFORE = prefix + '/FETCH_BEFORE';
export const FETCH_SUCCESS = prefix + '/FETCH_SUCCESS';
export const FETCH_FAILURE = prefix + '/FETCH_FAILURE';

