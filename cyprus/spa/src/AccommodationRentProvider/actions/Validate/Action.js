import translator from '../../../translations/translator'
import moment from 'moment'

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.price) {
        if (model.price <= 0) {
            ++validator.count;
            validator.errors.price = translator('validation_required')
        }
    } else {
        ++validator.count;
    }

    if (model.parameters.subtypes.length === 0) {
        ++validator.count
        validator.errors.subtypes = translator('validation_required')
    }

    if (model.parameters.roomCount <= 0 && model.parameters.bedroomCount <= 0) {
        ++validator.count
        validator.errors.roomCount = translator('validation_required')
    }

    if (model.locations.length === 0) {
        ++validator.count
        validator.errors.locations = translator('validation_required')
    }

    if (model.parameters.adultsCount < 1) {
        ++validator.count
        validator.errors.adultsCount = translator('validation_required')
    }

    if (model.parameters.childrenCount < 0) {
        ++validator.count
        validator.errors.childrenCount = translator('validation_required')
    }

    if (!model.parameters.dayCount) {
        ++validator.count
        validator.errors.dayCount = translator('validation_required')
    }

    if (!model.parameters.rentalStart) {
        ++validator.count
        validator.errors.rentalStart = translator('validation_required')
    }

    if (!model.parameters.rentalEnd) {
        ++validator.count
        validator.errors.rentalEnd = translator('validation_required')
    }

    if (model.parameters.rentalStart && model.parameters.rentalEnd) {
        const date1 = moment(model.parameters.rentalStart, 'YYYY-MM-DD')
        const date2 = moment(model.parameters.rentalEnd, 'YYYY-MM-DD')

        if (date2.isSameOrBefore(date1)) {
            ++validator.count
            validator.errors.rentalEnd = translator('validation_rental_range')

        }
    }

    return validator
}
