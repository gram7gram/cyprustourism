import {combineReducers} from 'redux'
import * as Action from '../actions'
import filter from './filter'

import qs from 'query-string'

const query = qs.parse(window.location.search)

let initialPage = parseInt(query.page);
if (isNaN(initialPage) || initialPage <= 0) {
    initialPage = 1
}

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
        case Action.FETCH_FAILURE:
            return false
        case Action.FETCH_BEFORE:
            return true
        default:
            return prev
    }
}

const initialPaginator = {
    total: 0,
    page: initialPage,
    limit: 0,
}

const pagination = (prev = initialPaginator, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            return {
                total: action.payload.total,
                page: action.payload.page,
                limit: action.payload.limit,
            }
        default:
            return prev
    }
}

export default combineReducers({
    items,
    isLoading,
    filter,
    pagination,
})

