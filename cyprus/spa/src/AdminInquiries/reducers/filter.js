import {combineReducers} from 'redux'
import * as Action from '../actions'
import translator from "../../translations/translator";
import qs from 'query-string'

const query = qs.parse(window.location.search)

const initial = {
    statuses: query['filter[statuses]'] ? query['filter[statuses]'].split(',').map(value => ({
        label: translator('inquiry_status_' + value),
        value
    })) : [],
    types: query['filter[types]'] ? query['filter[types]'].split(',').map(value => ({
        label: translator('inquiry_type_' + value),
        value
    })) : [],
    search: query['filter[search]'] && query['filter[search]'] !== 'null' ? query['filter[search]'] : null,
}

const statuses = (prev = initial.statuses, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.statuses !== undefined) {
                return action.payload.statuses
            }
            return prev
        default:
            return prev
    }
}

const types = (prev = initial.types, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.types !== undefined) {
                return action.payload.types
            }
            return prev
        default:
            return prev
    }
}

const search = (prev = initial.search, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.search !== undefined) {
                return action.payload.search
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    search,
    statuses,
    types,
})

