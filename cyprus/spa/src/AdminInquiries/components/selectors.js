import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminInquiries: store => store.AdminInquiries,
})
