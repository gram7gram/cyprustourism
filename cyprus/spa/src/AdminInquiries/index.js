import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import Root from '../Common/components/Root'
import Layout from './components'
import Store from '../store'

const id = 'admin-inquiries-app';
const app = document.getElementById(id);
if (!app) {
    throw 'No DOM element with id: ' + id
}

render(
    <Provider store={Store}>
        <Root>
            <Layout/>
        </Root>
    </Provider>,
    app
);
