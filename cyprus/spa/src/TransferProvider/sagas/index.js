import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import Notification from './Notification'
import Map from './Map'

export default function* sagas() {
    yield all([
        fork(Map),
        fork(Validation),
        fork(Notification),
    ])
}
