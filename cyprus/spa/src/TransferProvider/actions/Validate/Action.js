import translator from '../../../translations/translator'

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.price) {
        if (model.price <= 0) {
            ++validator.count;
            validator.errors.price = translator('validation_required')
        }
    } else {
        ++validator.count;
    }

    if (!model.parameters.departAt) {
        ++validator.count
        validator.errors.departAt = translator('validation_required')
    }

    if (model.parameters.isReturn) {
        if (!model.parameters.returnAt) {
            ++validator.count
            validator.errors.returnAt = translator('validation_required')
        }
    }

    if (!model.parameters.passengerCount) {
        ++validator.count
        validator.errors.passengerCount = translator('validation_required')
    }

    if (model.parameters.destination === null) {
        ++validator.count
        validator.errors.destination = translator('validation_required')
    }

    if (model.parameters.origin === null) {
        ++validator.count
        validator.errors.origin = translator('validation_required')
    }

    return validator
}
