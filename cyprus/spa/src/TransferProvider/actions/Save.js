import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.media) {
        data.media = data.media.map(item => item.id)
    }

    if (data.extras || data.destinations || data.origins) {

        data.categories = []

        if (data.extras) {

            data.extras.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.extras
        }

        if (data.destinations) {

            data.destinations.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.destinations
        }

        if (data.origins) {

            data.origins.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.origins
        }
    }

    if (data.parameters.origin) {
        data.parameters.origin = data.parameters.origin.id
    }

    if (data.parameters.destination) {
        data.parameters.destination = data.parameters.destination.id
    }

    return data
}

export default (guid, model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: SAVE_BEFORE
    })

    request.post(AppRouter.POST.quotesByGuid.replace('_GUID_', guid), data)
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })

            if (callback) {
                callback()
            }
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
