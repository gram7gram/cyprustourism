const prefix = 'TransferProvider'

export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const VALIDATE_REQUEST = prefix + '/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = prefix + '/VALIDATE_SUCCESS';
export const VALIDATE_FAILURE = prefix + '/VALIDATE_FAILURE';

export const MODEL_CHANGED = prefix + '/MODEL_CHANGED';

export const ADD_DESTINATION = prefix + '/ADD_DESTINATION';
export const REMOVE_DESTINATION = prefix + '/REMOVE_DESTINATION';

export const ADD_ORIGIN = prefix + '/ADD_ORIGIN';
export const REMOVE_ORIGIN = prefix + '/REMOVE_ORIGIN';

export const FETCH_DIRECTION_BEFORE = prefix + '/FETCH_DIRECTION_BEFORE';
export const FETCH_DIRECTION_SUCCESS = prefix + '/FETCH_DIRECTION_SUCCESS';
export const FETCH_DIRECTION_FAILURE = prefix + '/FETCH_DIRECTION_FAILURE';

export const REMOVE_EXTRA = prefix + '/REMOVE_EXTRA';
export const ADD_EXTRA = prefix + '/ADD_EXTRA';

export const UPLOAD_MEDIA_BEFORE = prefix + '/UPLOAD_MEDIA_BEFORE';
export const UPLOAD_MEDIA_SUCCESS = prefix + '/UPLOAD_MEDIA_SUCCESS';
export const UPLOAD_MEDIA_FAILURE = prefix + '/UPLOAD_MEDIA_FAILURE';

export const FETCH_BEFORE = prefix + '/FETCH_BEFORE';
export const FETCH_SUCCESS = prefix + '/FETCH_SUCCESS';
export const FETCH_FAILURE = prefix + '/FETCH_FAILURE';
