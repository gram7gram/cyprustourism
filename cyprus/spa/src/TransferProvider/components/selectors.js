import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    TransferProvider: store => store.TransferProvider,
    locale: store => store.UI.locale,
})
