import React from 'react';
import {connect} from 'react-redux';
import * as Action from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import GoogleMap from "../../Common/components/GoogleMap";
import FetchDirections from "../actions/FetchDirections";

class Step2 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Action.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    getError = key => {
        const {errors} = this.props.TransferProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    getMarkers = () => {
        const {quote} = this.props.TransferProvider

        const markers = [];

        if (quote.parameters.origin) {
            markers.push(quote.parameters.origin.extra)
        }

        if (quote.parameters.destination) {
            markers.push(quote.parameters.destination.extra)
        }

        return markers
    }

    render() {

        const {directions, quote} = this.props.TransferProvider
        const {duration, distance} = quote.parameters

        const markers = this.getMarkers();

        return <div className="row mb-4" id="transfer-step-2-map">

            <div className="col-12">
                <h3 className="fg-primary">{translator('provider_transfer_step_2_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('provider_transfer_step_2_description_1')}}/>
            </div>

            <div className="col-12">

                <GoogleMap
                    markers={markers}
                    directions={directions}/>
            </div>

            {distance && duration ? <div className="col-12 text-center">
                <div className="row">
                    <div className="col">
                        <h5>{translator('transfer_distance')}: {distance}</h5>
                    </div>
                    <div className="col">
                        <h5>{translator('transfer_duration')}: {duration}</h5>
                    </div>
                </div>
            </div> : null}

        </div>
    }
}

export default connect(selectors)(Step2)
