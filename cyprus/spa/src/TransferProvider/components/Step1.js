import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import CategoryOption from "../../CarRent/components/CategoryOption";
import moment from "moment";
import DateTime from "../../Common/components/Date";
import {dateFormat} from "../../Common/utils";

const containerStyle = {maxHeight: '200px'}

class Step1 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    setReturn = isReturn => () => {

        this.props.dispatch({
            type: Actions.MODEL_CHANGED,
            payload: {
                isReturn,
            }
        })
    }

    toggleOrigin = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_ORIGIN,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_ORIGIN,
                payload
            })
        }

    }

    toggleDestination = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_DESTINATION,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_DESTINATION,
                payload
            })
        }

    }

    getError = key => {
        const {errors} = this.props.TransferProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.TransferProvider
        const {destinationOptions} = this.props.TransferProvider.Categories

        const isOneWay = !quote.parameters.isReturn

        return <div className="row">

            <div className="col-12">

                <div className="row mb-4" id="transfer-step-1-date">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('provider_transfer_step_1_title_1')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('provider_transfer_step_1_description_1')}}/>
                    </div>

                    <div className="col-12">

                        <div className="row mb-3">
                            <div className="col-12">
                                <div className="btn-group btn-group-toggle d-flex">
                                    <label
                                        className={"btn btn-sm flex-fill " + (isOneWay ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               checked={isOneWay}
                                               onChange={this.setReturn(false)}/>
                                        {isOneWay && <i className="fa fa-check"/>}
                                        &nbsp;{translator('transfer_one_way')}
                                    </label>

                                    <label
                                        className={"btn btn-sm flex-fill " + (!isOneWay ? "btn-primary" : "btn-outline-primary")}>
                                        <input type="radio"
                                               checked={!isOneWay}
                                               onChange={this.setReturn(true)}/>
                                        {!isOneWay && <i className="fa fa-check"/>}
                                        &nbsp;{translator('transfer_two_way')}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label className="m-0 required">
                                        {translator('transfer_depart_at')}
                                    </label>
                                    <DateTime
                                        viewMode={"days"}
                                        minDate={moment()}
                                        onChange={this.changeByKey('departAt')}
                                        value={dateFormat(quote.parameters.departAt)}
                                        placeholderText={translator('from')}/>
                                </div>
                            </div>

                            {!isOneWay && <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label className="m-0 required">
                                        {translator('transfer_return_at')}
                                    </label>
                                    <DateTime
                                        viewMode={"days"}
                                        minDate={moment()}
                                        onChange={this.changeByKey('returnAt')}
                                        value={dateFormat(quote.parameters.returnAt)}
                                        placeholderText={translator('to')}/>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>

                <div className="row mb-4" id="transfer-step-1-direction">

                    <div className="col-12">
                        <h3 className="fg-primary">{translator('provider_transfer_step_1_title_2')}</h3>
                        <p dangerouslySetInnerHTML={{__html: translator('provider_transfer_step_1_description_2')}}/>
                    </div>

                    <div className="col-12">
                        <div className="option-container" style={containerStyle}>
                            <div className="row">
                                <div className="col-12 col-md-6 text-truncate">
                                    <label className="required">{translator("transfer_origin")}</label>
                                    {destinationOptions.map((option, key) => {

                                        const isSelected = quote.parameters.origin && quote.parameters.origin.id === option.id
                                        const isDestinationSelected = quote.parameters.destination && quote.parameters.destination.id === option.id

                                        return <CategoryOption
                                            id={'origins'}
                                            key={key}
                                            disabled={!!isDestinationSelected}
                                            isSelected={!!isSelected}
                                            model={option}
                                            onChange={this.toggleOrigin}/>
                                    })}
                                </div>

                                <div className="col-12 col-md-6 text-truncate">
                                    <label className="required">{translator("transfer_destination")}</label>
                                    {destinationOptions.map((option, key) => {

                                        const isSelected = quote.parameters.destination && quote.parameters.destination.id === option.id
                                        const isOriginSelected = quote.parameters.origin && quote.parameters.origin.id === option.id

                                        return <CategoryOption
                                            id={'destinations'}
                                            key={key}
                                            disabled={!!isOriginSelected}
                                            isSelected={!!isSelected}
                                            model={option}
                                            onChange={this.toggleDestination}/>
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step1)
