import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import CategoryOption from "../../CarRent/components/CategoryOption";

class Step3 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    toggleExtra = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_EXTRA,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_EXTRA,
                payload
            })
        }

    }

    getError = key => {
        const {errors} = this.props.TransferProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.TransferProvider
        const {extraOptions} = this.props.TransferProvider.Categories

        return <div className="row mb-4" id="transfer-step-3-extra">

            <div className="col-12">
                <h3 className="fg-primary">{translator('provider_transfer_step_3_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('provider_transfer_step_3_description_1')}}/>
            </div>

            <div className="col-12">

                <div className="row">
                    <div className="col-12 col-md-7 text-truncate">
                        <label className="m-0">{translator('car_rent_extras')}</label>
                        <div className="option-container">
                            {extraOptions.map((option, key) => {

                                const isSelected = !!quote.extras.find(item => item.id === option.id)

                                return <CategoryOption
                                    key={key}
                                    isSelected={isSelected}
                                    model={option}
                                    onChange={this.toggleExtra}/>
                            })}
                        </div>
                    </div>

                    <div className="col-12 col-md-5 text-truncate">
                        <label className="m-0 required">
                            {translator('transfer_passenger_count')}
                        </label>
                        <div className="mr-1 mb-1">
                            <input type="number"
                                   min={1}
                                   max={18}
                                   step={1}
                                   onChange={this.changeInt('passengerCount')}
                                   value={quote.parameters.passengerCount || ''}
                                   placeholder={translator('enter_amount_placeholder')}
                                   className="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step3)
