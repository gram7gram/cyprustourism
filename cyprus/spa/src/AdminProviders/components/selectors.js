import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminProviders: store => store.AdminProviders,
})
