import React from 'react';
import {connect} from 'react-redux';
import {FETCH_REQUEST} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import Filter from "./Filter";
import Paginator from "../../Common/components/Paginator";
import Spinner from "../../Common/components/Spinner";
import {renderUsername} from "../../AdminReviews/utils";
import Rating from "../../Common/components/Rating";
import {dateTimeFormat, numberFormat} from "../../Common/utils";
import {renderStatus} from "../utils";
import {renderIsActive} from "../../AdminUsers/utils";

class AdminProviders extends React.Component {

    submit = (page) => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page
            }
        })
    }

    componentWillMount() {
        const {pagination} = this.props.AdminProviders

        this.submit(pagination.page)
    }

    renderContent() {

        const {items, isLoading} = this.props.AdminProviders

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_providers_title')}</h3>
                    <h4>{translator('no_providers_footer')}</h4>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">#</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('provider')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('company')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('account_status')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => {
                    return <tr key={key}>
                        <td className="align-middle text-nowrap">
                            <a href={AppRouter.GET.providerShow.replace('_ID_', item.id)}
                               className="btn btn-xs btn-primary">
                                <i className="fa fa-eye"/>
                            </a>
                        </td>
                        <td className="align-middle text-nowrap">
                            <div>{renderUsername(item.user)}</div>
                            <Rating rating={item.rating}/>&nbsp;{numberFormat(item.rating)}
                        </td>
                        <td className="align-middle text-nowrap">
                            <div>{item.companyName}</div>
                            {item.companyId && <small className="text-secondary">{item.companyId}</small>}
                        </td>
                        <td className="align-middle text-nowrap">{renderStatus(item.status)}</td>
                        <td className="align-middle text-nowrap">{renderIsActive(item.user.isActive)}</td>
                        <td className="align-middle text-nowrap text-secondary">{dateTimeFormat(item.createdAt)}</td>
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    }

    render() {

        const {pagination} = this.props.AdminProviders

        return <div className="card shadow-sm">

            <div className="card-header">
                <Filter/>
            </div>

            <div className="card-body">

                <div className="row">
                    <div className="col-12">
                        {this.renderContent()}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <Paginator
                            onChange={this.submit}
                            page={pagination.page}
                            limit={pagination.limit}
                            total={pagination.total}
                        />
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminProviders)
