import React from 'react'
import translator from '../translations/translator';

export const renderStatus = status => {

    switch (status) {
        case 'created':
            return <span className="badge badge-warning">
                <i className="fa fa-info-circle"/>&nbsp;{translator('provider_status_created')}
            </span>
        case 'approved':
            return <span className="badge badge-success">
                <i className="fa fa-check"/>&nbsp;{translator('provider_status_approved')}
            </span>
        case 'rejected':
            return <span className="badge badge-danger">
                <i className="fa fa-ban"/>&nbsp;{translator('provider_status_rejected')}
            </span>
        default:
            return status
    }
}