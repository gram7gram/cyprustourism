import request from '../../Common/request'
import {FETCH_BEFORE, FETCH_FAILURE, FETCH_SUCCESS} from '../actions'

export const parseBeforeSubmit = filter => {
    const data = {...filter}

    if (data.statuses.length > 0) {
        data.statuses = data.statuses.map(item => item.value).join(',')
    } else {
        delete data.statuses
    }

    if (data.search) {
        data.search = data.search.trim()
    } else {
        delete data.search
    }

    return data
}

export default (filter, page) => dispatch => {

    const data = parseBeforeSubmit(filter)

    dispatch({
        type: FETCH_BEFORE
    })

    const query = [
        'page=' + page
    ]

    Object.keys(data).forEach(key => {
        const value = data[key]
        query.push('filter[' + key + ']=' + value)
    })

    request.get(AppRouter.GET.providers + '?' + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
