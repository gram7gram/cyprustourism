import {combineReducers} from 'redux'
import * as Action from '../actions'
import moment from 'moment'
import {objectValues} from "../../Common/utils";
import keyBy from "lodash/keyBy";

const now = moment().add(1, 'days')
const end = moment().add(4, 'days')
const diff = end.diff(now, 'days')

const dayCount = (prev = diff, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.dayCount !== undefined) {
                return action.payload.dayCount
            }
            return prev
        default:
            return prev
    }
}

const driverAgeFrom = (prev = 18, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.driverAgeFrom !== undefined) {
                return action.payload.driverAgeFrom
            }
            return prev
        default:
            return prev
    }
}

const paymentType = (prev = 'cash', action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.paymentType !== undefined) {
                return action.payload.paymentType
            }
            return prev
        default:
            return prev
    }
}

const isFixedDate = (prev = true, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isFixedDate !== undefined) {
                return action.payload.isFixedDate
            }
            return prev
        default:
            return prev
    }
}

const rentalStart = (prev = now.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalStart !== undefined) {
                return action.payload.rentalStart
            }
            return prev
        default:
            return prev
    }
}

const rentalEnd = (prev = end.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalEnd !== undefined) {
                return action.payload.rentalEnd
            }
            return prev
        default:
            return prev
    }
}

const isHourly = (prev = true, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isHourly !== undefined) {
                return action.payload.isHourly
            }
            return prev
        default:
            return prev
    }
}

const daytimeRanges = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.ADD_DAYTIME_RANGE:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_DAYTIME_RANGE:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const hourlyRanges = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.ADD_HOURLY_RANGE:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_HOURLY_RANGE:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

export default combineReducers({
    dayCount,
    driverAgeFrom,
    paymentType,
    isFixedDate,
    rentalStart,
    rentalEnd,
    isHourly,
    hourlyRanges,
    daytimeRanges,
})
