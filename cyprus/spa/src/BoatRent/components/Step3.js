import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import CategoryOption from "../../CarRent/components/CategoryOption";

const driverAgeOptions = []
for (let i = 18; i <= 100; i++) {
    driverAgeOptions.push(i)
}

class Step3 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }


    toggleBoatType = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_BOAT_TYPE,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_BOAT_TYPE,
                payload
            })
        }

    }


    getError = key => {
        const {errors} = this.props.BoatRent.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model} = this.props.BoatRent
        const {boatTypeOptions} = this.props.BoatRent.Categories

        return <div className="row mb-4" id="boat-rent-step-3-boat">

            <div className="col-12">
                <h3 className="fg-primary">{translator('boat_rent_step_3_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('boat_rent_step_3_description_1')}}/>
            </div>

            <div className="col-12">

                <div className="row">
                    <div className="col-12 col-md-7 text-truncate">
                        <div className="option-container">
                            {boatTypeOptions.map((option, key) => {

                                const isSelected = !!model.boatTypes.find(item => item.id === option.id)

                                return <CategoryOption
                                    key={key}
                                    isSelected={isSelected}
                                    model={option}
                                    onChange={this.toggleBoatType}/>
                            })}
                        </div>
                    </div>
                    <div className="col-12 col-md-5">
                        <label className="m-0 required">{translator('boat_rent_driver_age')}</label>
                        <select
                            name="driverAgeFrom"
                            className="form-control"
                            onChange={this.changeInt('driverAgeFrom')}
                            value={model.parameters.driverAgeFrom}>
                            {driverAgeOptions.map((year, i) =>
                                <option key={i} value={year}>{year} {translator('and_more_suffix')}</option>
                            )}
                        </select>
                    </div>
                </div>

            </div>
        </div>
    }
}

export default connect(selectors)(Step3)
