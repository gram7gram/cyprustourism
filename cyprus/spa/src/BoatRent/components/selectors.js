import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    BoatRent: store => store.BoatRent,
    locale: store => store.UI.locale,
})
