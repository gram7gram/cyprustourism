import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    BoatRent: store => store.BoatRent,
})
