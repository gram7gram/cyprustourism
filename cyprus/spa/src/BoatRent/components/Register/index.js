import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../../translations/translator';
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";

class RegisterUser extends React.Component {

    renderStep() {

        const {creator} = this.props.BoatRent.model
        const {step, isAuthenticated} = this.props.BoatRent.RegisterUser

        if (isAuthenticated) {
            return <div className="row">
                <div className="col-10 col-md-6 mx-auto">
                    <div className="row no-gutters">
                        <div className="col-12">
                            <p>{translator('logged_in_as')}</p>
                        </div>
                        <div className="col-auto">
                            <i className="fa fa-2x fa-user-circle p-3"/>
                        </div>
                        <div className="col">
                            <h3 className="m-0 fg-primary">{creator.surname}&nbsp;{creator.name}</h3>
                            <p>{creator.email}</p>
                        </div>
                    </div>
                </div>
            </div>
        }

        switch (step) {
            case 1:
                return <Step1/>
            case 2:
                return <Step2/>
            case 3:
                return <Step3/>
        }
    }

    renderTitle = () => {

        const {step} = this.props.BoatRent.RegisterUser

        switch (step) {
            case 1:
                return <div className="mb-3">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('boat_rent_register_title')}</h2>
                    <p>{translator('boat_rent_register_subtitle_1')}</p>
                </div>
            case 2:
                return <div className="mb-3">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('boat_rent_register_title')}</h2>
                    <p>{translator('boat_rent_register_subtitle_2')}</p>
                </div>
            case 3:
                return <div className="mb-3">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('boat_rent_register_title')}</h2>
                    <p>{translator('boat_rent_register_subtitle_3')}</p>
                </div>
        }

    }

    render() {
        const serverErrors = this.props.BoatRent.serverErrors
        const registerServerErrors = this.props.BoatRent.RegisterUser.serverErrors

        return <div className="row no-gutters">
            <div className="col-12 col-md-10 col-lg-8 mx-auto">

                {this.renderTitle()}

                {registerServerErrors.length > 0 && <div className="alert alert-danger text-center my-3">
                    <ul className="simple">{registerServerErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                {serverErrors.length > 0 && <div className="alert alert-danger text-center my-3">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                {this.renderStep()}
            </div>
        </div>
    }
}

export default connect(selectors)(RegisterUser)
