import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import FetchCategories from '../../AdminCategories/actions/FetchItems';
import translator from '../../translations/translator';

import Step1 from "./Step1";
import Step3 from "./Step3";
import Step2 from "./Step2";
import Step4 from "./Step4";
import Success from "./Success";
import Register from "./Register";

import {SAVE_REQUEST, SET_INQUIRY_STEP} from "../actions";
import {scrollTo} from "../../Common/utils";

class BoatRent extends React.Component {

    componentWillMount() {
        const {model} = this.props.BoatRent

        this.props.dispatch(FetchCategories({
            locale: this.props.locale,
            type: model.type
        }, 1, 0))
    }

    next = () => {

        this.props.dispatch({
            type: SET_INQUIRY_STEP,
            payload: 2
        })
    }

    scrollTo = (target) => (e) => {

        e.preventDefault();

        scrollTo(target)
    }

    submit = () => {
        this.props.dispatch({
            type: SAVE_REQUEST
        })
    }

    renderContent = () => {
        const {isSaveSuccess, isValid, isLoading, step, model, serverErrors} = this.props.BoatRent

        const {
            isAuthenticated,
            isValid: isUserValid
        } = this.props.BoatRent.RegisterUser

        const hasCreator = model.creator && model.creator.id

        if (isSaveSuccess) {
            return <div className="row space-bottom-2">
                <div className="col-12 col-lg-8 mx-auto">
                    <div className="space-top-2 space-bottom-1 w-100">
                        <Success/>
                    </div>
                </div>
            </div>
        }

        switch (step) {
            case 1:
                return <div className="row" id="boat-rent-step-1">

                    <div className="col-12">
                        <div className="space-top-2 space-bottom-1">
                            <h1 className="h2 mb-0">
                                <span>{translator('boat_banner_title')}</span>
                                &nbsp;<span
                                className="fg-primary font-weight-bold">{translator('boat_banner_title_cyprus')}</span>
                            </h1>
                            <p>{translator('boat_banner_subtitle')}</p>
                        </div>
                    </div>

                    <div className="col-12">
                        {serverErrors.length > 0 && <div className="alert alert-danger text-center my-3">
                            <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                        </div>}
                    </div>

                    <div className="d-none d-xl-block col-xl-3">
                        <nav className="card border-0 bg-primary">
                            <ul className="simple py-3 px-5">
                                <li>
                                    <a className="text-white py-3 d-block"
                                       onClick={this.scrollTo('boat-rent-step-1-hourly')}
                                       href="#boat-rent-step-1-hourly">{translator('boat_rent_step_1_title_3')}</a>
                                </li>

                                {model.parameters.isHourly
                                    ? <li>
                                        <a className="text-white py-3 d-block"
                                           onClick={this.scrollTo('boat-rent-step-1-daterange')}
                                           href="#boat-rent-step-1-daterange">{translator('boat_rent_step_1_title_4')}</a>
                                    </li>
                                    : <li>
                                        <a className="text-white py-3 d-block"
                                           onClick={this.scrollTo('boat-rent-step-1-fixed-date')}
                                           href="#boat-rent-step-1-fixed-date">{translator('boat_rent_step_1_title_1')}</a>
                                    </li> }

                                {!model.parameters.isHourly
                                    ? <li>
                                        <a className="text-white py-3 d-block"
                                           onClick={this.scrollTo('boat-rent-step-1-daterange')}
                                           href="#boat-rent-step-1-daterange">{translator('boat_rent_step_1_title_2')}</a>
                                    </li> : null}

                                <li>
                                    <a className="text-white py-3 d-block"
                                       onClick={this.scrollTo('boat-rent-step-2-location')}
                                       href="#boat-rent-step-2-location">{translator('boat_rent_step_2_title_1')}</a>
                                </li>
                                <li>
                                    <a className="text-white py-3 d-block"
                                       onClick={this.scrollTo('boat-rent-step-3-boat')}
                                       href="#boat-rent-step-3-boat">{translator('boat_rent_step_3_title_1')}</a>
                                </li>
                                <li>
                                    <a className="text-white py-3 d-block"
                                       onClick={this.scrollTo('boat-rent-step-4-extra')}
                                       href="#boat-rent-step-4-extra">{translator('boat_rent_step_4_title_1')}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div className="col-12 col-md-10 mx-auto col-xl-9">
                        <Step1/>

                        <Step2/>

                        <Step3/>

                        <Step4/>
                    </div>

                    <div className="col-12 col-md-6 col-xl-4 mx-auto space-2">

                        {!isAuthenticated
                            ? <button type="button"
                                      className="btn btn-primary btn-block transition-3d-hover"
                                      onClick={this.next}
                                      disabled={!isValid}>
                                {translator('continue')}&nbsp;
                                <i className="fa fa-arrow-right"/>
                            </button>
                            : <button type="button"
                                      className="btn btn-primary btn-block transition-3d-hover"
                                      onClick={this.submit}
                                      disabled={!isValid || isLoading || !hasCreator || !isUserValid}>
                                <i className={"fa " + (isLoading
                                    ? "fa-spin fa-circle-notch"
                                    : "fa-check")}/>
                                &nbsp;{translator('inquiry_submit')}
                            </button>}
                    </div>
                </div>

            case 2:
                return <div className="row" id="boat-rent-step-2">

                    <div className="space-top-2 space-bottom-1 w-100">

                        <div className="col-12">

                            <Register/>

                        </div>

                        <div className="col-12 col-md-6 col-xl-4 mx-auto space-2">

                            <button type="button"
                                    className="btn btn-primary btn-block transition-3d-hover"
                                    onClick={this.submit}
                                    disabled={!isValid || isLoading || !hasCreator || !isUserValid}>
                                <i className={"fa " + (isLoading
                                    ? "fa-spin fa-circle-notch"
                                    : "fa-check")}/>
                                &nbsp;{translator('inquiry_submit')}
                            </button>
                        </div>
                    </div>
                </div>
        }
    }

    render() {
        return <div className="row">
            <div className="col-12">
                {this.renderContent()}
            </div>
        </div>
    }
}

export default connect(selectors)(BoatRent)
