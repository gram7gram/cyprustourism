import {all, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import toast from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* alertError({payload}) {
    toast(translator('save_failure_alert') + ": " + payload.data.message, true)
}

export default function* sagas() {
    yield all([
        takeEvery(Actions.SAVE_FAILURE, alertError),
    ])
}
