import {all, put, select, takeEvery, throttle} from 'redux-saga/effects'
import * as Actions from "../actions";
import Validate from '../actions/Validate'

function* requestValidation() {
    yield put({
        type: Actions.VALIDATE_REQUEST
    })
}

function* runValidation() {
    const {model, changes} = yield select(store => store.BoatRent)

    yield put(Validate(model, changes))
}

export default function* sagas() {
    yield all([
        throttle(400, [
            Actions.MODEL_CHANGED,
            Actions.ADD_HOURLY_RANGE,
            Actions.REMOVE_HOURLY_RANGE,
            Actions.ADD_DAYTIME_RANGE,
            Actions.REMOVE_DAYTIME_RANGE,
            Actions.ADD_EXTRA,
            Actions.REMOVE_EXTRA,
            Actions.ADD_LOCATION,
            Actions.REMOVE_LOCATION,
            Actions.ADD_BOAT_TYPE,
            Actions.REMOVE_BOAT_TYPE,
            Actions.LOGIN_SUCCESS,
            Actions.VALIDATE_USER_STEP_SUCCESS,
        ], requestValidation),

        takeEvery(Actions.VALIDATE_REQUEST, runValidation)
    ])
}
