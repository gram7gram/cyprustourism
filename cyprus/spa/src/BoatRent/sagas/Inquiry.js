import {all, put, select, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import LoginCheck from '../actions/LoginCheck'
import RegisterUserAction from "../actions/RegisterUser";
import Save from "../actions/Save";

function* saveAfterRegister() {
    const {model} = yield select(store => store.BoatRent)

    yield put(Save(model))
}

function* saveIfLoggedIn() {
    const {model, RegisterUser} = yield select(store => store.BoatRent)

    if (!RegisterUser.isAuthenticated) {
        yield put(RegisterUserAction(model.creator))
    } else {
        yield put(Save(model))
    }
}

function* login() {
    const {creator} = yield select(store => store.BoatRent.model)

    yield put(LoginCheck(creator.email, creator.password))
}

export default function* sagas() {
    yield all([

        takeEvery(Actions.SAVE_REQUEST, saveIfLoggedIn),

        takeEvery(Actions.REGISTER_SUCCESS, saveAfterRegister),

        takeEvery(Actions.REGISTER_SUCCESS, login),

    ])
}
