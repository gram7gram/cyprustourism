import request from '../../Common/request'
import {REGISTER_BEFORE, REGISTER_FAILURE, REGISTER_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.country) {
        data.country = data.country.value
    }

    return data
}

export default (model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: REGISTER_BEFORE
    })

    let promise

    if (!data.id) {
        promise = request.post(AppRouter.POST.users, data)
    } else {
        promise = request.put(AppRouter.PUT.user.replace('_ID_', data.id), data)
    }

    promise
        .then(({data}) => {
            dispatch({
                type: REGISTER_SUCCESS,
                payload: data
            })

            if (callback) {
                callback()
            }
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: REGISTER_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
