const prefix = 'BoatRent'

export const SAVE_REQUEST = prefix + '/SAVE_REQUEST';
export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const VALIDATE_REQUEST = prefix + '/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = prefix + '/VALIDATE_SUCCESS';
export const VALIDATE_FAILURE = prefix + '/VALIDATE_FAILURE';

export const VALIDATE_USER_REQUEST = prefix + '/VALIDATE_USER_REQUEST';
export const VALIDATE_USER_SUCCESS = prefix + '/VALIDATE_USER_SUCCESS';
export const VALIDATE_USER_FAILURE = prefix + '/VALIDATE_USER_FAILURE';

export const VALIDATE_USER_STEP_REQUEST = prefix + '/VALIDATE_USER_STEP_REQUEST';
export const VALIDATE_USER_STEP_SUCCESS = prefix + '/VALIDATE_USER_STEP_SUCCESS';
export const VALIDATE_USER_STEP_FAILURE = prefix + '/VALIDATE_USER_STEP_FAILURE';

export const MODEL_CHANGED = prefix + '/MODEL_CHANGED';
export const USER_MODEL_CHANGED = prefix + '/USER_MODEL_CHANGED';

export const REMOVE_HOURLY_RANGE = prefix + '/REMOVE_HOURLY_RANGE';
export const ADD_HOURLY_RANGE = prefix + '/ADD_HOURLY_RANGE';

export const REMOVE_DAYTIME_RANGE = prefix + '/REMOVE_DAYTIME_RANGE';
export const ADD_DAYTIME_RANGE = prefix + '/ADD_DAYTIME_RANGE';

export const REMOVE_EXTRA = prefix + '/REMOVE_EXTRA';
export const ADD_EXTRA = prefix + '/ADD_EXTRA';

export const REMOVE_BOAT_TYPE = prefix + '/REMOVE_BOAT_TYPE';
export const ADD_BOAT_TYPE = prefix + '/ADD_BOAT_TYPE';

export const REMOVE_LOCATION = prefix + '/REMOVE_LOCATION';
export const ADD_LOCATION = prefix + '/ADD_LOCATION';

export const SET_STEP = prefix + '/SET_STEP';
export const SET_INQUIRY_STEP = prefix + '/SET_INQUIRY_STEP';

export const EMAIL_VERIFY_BEFORE = prefix + '/EMAIL_VERIFY_BEFORE';
export const EMAIL_VERIFY_SUCCESS = prefix + '/EMAIL_VERIFY_SUCCESS';
export const EMAIL_VERIFY_FAILURE = prefix + '/EMAIL_VERIFY_FAILURE';

export const EMAIL_RESEND_BEFORE = prefix + '/EMAIL_RESEND_BEFORE';
export const EMAIL_RESEND_SUCCESS = prefix + '/EMAIL_RESEND_SUCCESS';
export const EMAIL_RESEND_FAILURE = prefix + '/EMAIL_RESEND_FAILURE';

export const LOGIN_BEFORE = prefix + '/LOGIN_BEFORE';
export const LOGIN_SUCCESS = prefix + '/LOGIN_SUCCESS';
export const LOGIN_FAILURE = prefix + '/LOGIN_FAILURE';

export const REGISTER_BEFORE = prefix + '/REGISTER_BEFORE';
export const REGISTER_SUCCESS = prefix + '/REGISTER_SUCCESS';
export const REGISTER_FAILURE = prefix + '/REGISTER_FAILURE';
