import translator from '../../../translations/translator'
import moment from 'moment'

export default (model) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (model.locations.length === 0) {
        ++validator.count
        validator.errors.locations = translator('validation_required')
    }

    if (model.carTypes.length === 0) {
        ++validator.count
        validator.errors.carTypes = translator('validation_required')
    }

    if (!model.parameters.dayCount) {
        ++validator.count
        validator.errors.dayCount = translator('validation_required')
    }

    if (!model.parameters.rentalStart) {
        ++validator.count
        validator.errors.rentalStart = translator('validation_required')
    }

    if (!model.parameters.rentalEnd) {
        ++validator.count
        validator.errors.rentalEnd = translator('validation_required')
    }

    if (model.parameters.rentalStart) {
        const now = moment()
        const date1 = moment(model.parameters.rentalStart, 'YYYY-MM-DD')

        if (date1.isSameOrBefore(now)) {
            ++validator.count
            validator.errors.rentalStart = translator('validation_rental_range')
        }
    }

    if (model.parameters.rentalStart && model.parameters.rentalEnd) {
        const date1 = moment(model.parameters.rentalStart, 'YYYY-MM-DD')
        const date2 = moment(model.parameters.rentalEnd, 'YYYY-MM-DD')

        if (date2.isSameOrBefore(date1)) {
            ++validator.count
            validator.errors.rentalEnd = translator('validation_rental_range')

        }
    }

    return validator
}
