import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.creator) {
        data.creator = data.creator.id
    }

    if (data.locations || data.carTypes || data.extras) {
        data.categories = []

        if (data.locations) {
            data.locations.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.locations
        }

        if (data.carTypes) {
            data.carTypes.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.carTypes
        }

        if (data.extras) {
            data.extras.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.extras
        }
    }

    return data
}

export default (model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: SAVE_BEFORE
    })

    let promise
    if (model.guid) {
        promise = request.put(AppRouter.PUT.inquiries.replace('_GUID_', model.guid), data)
    } else {
        promise = request.post(AppRouter.POST.inquiries, data)
    }

    promise
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })

            if (callback) {
                callback()
            }
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
