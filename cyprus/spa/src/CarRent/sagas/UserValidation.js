import {all, put, select, takeEvery, throttle} from 'redux-saga/effects'
import * as Actions from '../actions'
import ValidateUserStep from '../actions/ValidateUserStep'
import ValidateUser from '../actions/ValidateUser'

function* requestStepValidation() {
    yield put({
        type: Actions.VALIDATE_USER_STEP_REQUEST
    })
}

function* requestValidation() {
    yield put({
        type: Actions.VALIDATE_USER_REQUEST
    })
}

function* runStepValidation() {
    const {model, changes, RegisterUser} = yield select(store => store.CarRent)

    yield put(ValidateUserStep(RegisterUser.step, model.creator, changes))
}

function* runValidation() {
    const {model, changes} = yield select(store => store.CarRent)

    yield put(ValidateUser(model.creator, changes))
}

export default function* sagas() {
    yield all([
        throttle(400, [
            Actions.USER_MODEL_CHANGED,
            Actions.SET_STEP,
        ], requestStepValidation),

        throttle(400, [
            Actions.USER_MODEL_CHANGED,
        ], requestValidation),

        takeEvery(Actions.VALIDATE_USER_STEP_REQUEST, runStepValidation),

        takeEvery(Actions.VALIDATE_USER_REQUEST, runValidation)
    ])
}
