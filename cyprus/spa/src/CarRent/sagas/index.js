import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import UserValidation from './UserValidation'
import Notification from './Notification'
import Step from './Step'
import Inquiry from './Inquiry'

export default function* sagas() {
    yield all([
        fork(Inquiry),
        fork(Step),
        fork(Validation),
        fork(UserValidation),
        fork(Notification),
    ])
}
