import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    CarRent: store => store.CarRent,
    locale: store => store.UI.locale,
})
