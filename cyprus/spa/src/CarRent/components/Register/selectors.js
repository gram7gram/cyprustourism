import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    CarRent: store => store.CarRent,
})
