import React from 'react';
import PropTypes from 'prop-types';

const iconStyle = {maxWidth: '30px'}

class CategoryOption extends React.Component {

    render() {

        const {id, model, isSelected, onChange, disabled} = this.props

        const checkboxId = "category-" + model.id + '-' + model.type + "-" + id

        return <div className="row no-gutters">
            <div className="col-auto">

                <div className="custom-control custom-checkbox">
                    <input type="checkbox"
                           id={checkboxId}
                           className="custom-control-input"
                           onChange={onChange(model)}
                           disabled={disabled}
                           checked={isSelected}/>

                    <label htmlFor={checkboxId} className="custom-control-label">
                        {model.icon
                            ? <img src={model.icon.url}
                                   style={iconStyle}
                                   className="img-fluid p-1"/>
                            : null}
                        {model.name}
                    </label>
                </div>
            </div>
        </div>
    }
}

CategoryOption.propTypes = {
    id: PropTypes.string,
    model: PropTypes.any.isRequired,
    isSelected: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
}

CategoryOption.defaultProps = {
    id: '',
    isSelected: false,
    disabled: false,
}

export default CategoryOption
