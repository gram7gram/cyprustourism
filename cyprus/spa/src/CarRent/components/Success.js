import React from 'react';
import translator from '../../translations/translator';

const btnClasses = "btn btn-sm btn-block text-nowrap text-truncate mb-1 transition-3d-hover mx-auto btn-primary"
const btnStyle = {width: '95%'}
const iconStyle = {maxHeight: '150px'}

class InquirySuccess extends React.Component {

    render() {

        return <div className="row">
            <div className="col-12">

                <div className="row mb-4 mt-10 mt-xl-0">
                    <div className="col-12 col-md-3 col-lg-4 text-center">
                        <img src="/img/success-icon.png" className="img-fluid" style={iconStyle}/>
                    </div>
                    <div className="col-12 col-md-9 col-lg-8 text-center text-md-left">
                        <h2 className="h4 text-primary font-weight-normal">{translator('order_success_message')}</h2>

                        <a href={AppRouter.GET.myInquiriesIndex}
                           className="btn btn-sm transition-3d-hover btn-primary">
                            {translator('navigation_my_inquiries')}
                        </a>
                    </div>
                </div>

                <h3 className="text-center my-3">{translator('inquiry_success_alternatives')}</h3>

                <div className="row no-gutters">
                    <div className="col-12 col-sm-6 col-md-4 text-left">

                        <a href={AppRouter.GET.boatRentIndex}
                           title={translator('landing_boat_rent')}
                           style={btnStyle}
                           className={btnClasses}>
                            <i className="fa fa-ship"/>&nbsp;{translator('landing_boat_rent')}
                        </a>
                    </div>

                    <div className="col-12 col-sm-6 col-md-4 text-center">

                        <a href={AppRouter.GET.transferIndex}
                           title={translator('landing_transfer')}
                           style={btnStyle}
                           className={btnClasses}>
                            <i className="fa fa-truck"/>&nbsp;{translator('landing_transfer')}
                        </a>
                    </div>

                    <div className="col-12 col-sm-6 col-md-4 text-right">

                        <a href={AppRouter.GET.accommodationRentIndex}
                           title={translator('landing_accommodation_rent')}
                           style={btnStyle}
                           className={btnClasses}>
                            <i className="fa fa-building"/>&nbsp;{translator('landing_accommodation_rent')}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default InquirySuccess