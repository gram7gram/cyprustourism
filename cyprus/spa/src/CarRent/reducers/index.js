import {combineReducers} from 'redux'
import * as Action from '../actions'
import * as CategoryAction from '../../AdminCategories/actions'
import model from './model'
import RegisterUser from './RegisterUser'
import Categories from './Categories'

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.SAVE_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.SAVE_BEFORE:
            return []
        default:
            return prev
    }
}

const isSaveSuccess = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
        case Action.SAVE_BEFORE:
        case Action.SAVE_FAILURE:
            return false
        case Action.SAVE_SUCCESS:
            return true
        default:
            return prev
    }
}

const isValid = (prev = false, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return true
        case Action.VALIDATE_FAILURE:
            return false
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
        case CategoryAction.FETCH_FAILURE:
        case Action.SAVE_SUCCESS:
        case Action.SAVE_FAILURE:
        case Action.REGISTER_FAILURE:
        case Action.REGISTER_SUCCESS:
            return false
        case CategoryAction.FETCH_BEFORE:
        case Action.SAVE_BEFORE:
        case Action.REGISTER_BEFORE:
            return true
        default:
            return prev
    }
}

const initialValidator = {
    count: 0,
    messages: [],
    errors: {}
}
const validator = (prev = initialValidator, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return initialValidator
        case Action.VALIDATE_FAILURE:
            return action.payload
        default:
            return prev
    }
}

const changes = (prev = {}, action) => {
    switch (action.type) {
        case Action.USER_MODEL_CHANGED:
        case Action.MODEL_CHANGED:

            const changes = {...prev}

            Object.keys(action.payload).forEach(key => {
                changes[key] = true
            })

            return changes
        default:
            return prev
    }
}

const step = (prev = 1, action) => {
    switch (action.type) {
        case Action.SET_INQUIRY_STEP:
            return action.payload
        default:
            return prev
    }
}

export default combineReducers({
    RegisterUser,
    Categories,
    isSaveSuccess,
    isValid,
    isLoading,
    validator,
    changes,
    serverErrors,
    model,
    step,
})

