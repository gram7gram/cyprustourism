import {combineReducers} from 'redux'
import * as CategoryAction from '../../AdminCategories/actions'

const categories = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const extraOptions = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
                .filter(item => item.group === 'extras')
        default:
            return prev
    }
}

const locationOptions = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
                .filter(item => item.group === 'location')
        default:
            return prev
    }
}

const carTypeOptions = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
                .filter(item => item.group === 'car_type')
        default:
            return prev
    }
}

export default combineReducers({
    categories,
    carTypeOptions,
    locationOptions,
    extraOptions,
})

