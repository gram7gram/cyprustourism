import {combineReducers} from 'redux'
import * as Action from '../../actions'

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.EMAIL_VERIFY_FAILURE:
        case Action.EMAIL_RESEND_FAILURE:
        case Action.REGISTER_FAILURE:
        case Action.LOGIN_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.EMAIL_RESEND_BEFORE:
        case Action.EMAIL_VERIFY_BEFORE:
        case Action.REGISTER_BEFORE:
        case Action.LOGIN_BEFORE:
            return []
        default:
            return prev
    }
}

const step = (prev = 1, action) => {
    switch (action.type) {
        case Action.SET_STEP:
            return action.payload
        default:
            return prev
    }
}

const isAlreadyRegistered = (prev = false, action) => {
    switch (action.type) {
        case Action.USER_MODEL_CHANGED:
            if (action.payload.isAlreadyRegistered !== undefined) {
                return action.payload.isAlreadyRegistered
            }
            return prev
        case Action.EMAIL_RESEND_SUCCESS:
            return action.status === 200
        case Action.EMAIL_RESEND_BEFORE:
            return false
        default:
            return prev
    }
}

const isValid = (prev = AppParameters.isAuthenticated, action) => {
    switch (action.type) {
        case Action.LOGIN_SUCCESS:
        case Action.VALIDATE_USER_SUCCESS:
            return true
        case Action.VALIDATE_USER_FAILURE:
            return false
        default:
            return prev
    }
}

const isStepValid = (prev = AppParameters.isAuthenticated, action) => {
    switch (action.type) {
        case Action.LOGIN_SUCCESS:
        case Action.VALIDATE_USER_STEP_SUCCESS:
            return true
        case Action.VALIDATE_USER_STEP_FAILURE:
            return false
        default:
            return prev
    }
}

const initialValidator = {
    count: 0,
    messages: [],
    errors: {}
}
const validator = (prev = initialValidator, action) => {
    switch (action.type) {
        case Action.VALIDATE_USER_STEP_SUCCESS:
            return initialValidator
        case Action.VALIDATE_USER_STEP_FAILURE:
            return action.payload
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.EMAIL_VERIFY_SUCCESS:
        case Action.EMAIL_VERIFY_FAILURE:
        case Action.EMAIL_RESEND_SUCCESS:
        case Action.EMAIL_RESEND_FAILURE:
        case Action.LOGIN_SUCCESS:
        case Action.LOGIN_FAILURE:
            return false
        case Action.EMAIL_RESEND_BEFORE:
        case Action.EMAIL_VERIFY_BEFORE:
        case Action.LOGIN_BEFORE:
            return true
        default:
            return prev
    }
}

const isAuthenticated = (prev = AppParameters.isAuthenticated, action) => {
    switch (action.type) {
        case Action.LOGIN_BEFORE:
        case Action.LOGIN_FAILURE:
            return false
        case Action.LOGIN_SUCCESS:
            return true
        default:
            return prev
    }
}

export default combineReducers({
    isAuthenticated,
    step,
    isAlreadyRegistered,
    isValid,
    isStepValid,
    validator,
    isLoading,
    serverErrors,
})

