import {combineReducers} from 'redux'
import * as Action from '../actions'
import moment from 'moment'

const start = moment().add(1, 'days')
const end = moment().add(4, 'days')
const diff = end.diff(start, 'days')

const dayCount = (prev = diff, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.dayCount !== undefined) {
                return action.payload.dayCount
            }
            return prev
        default:
            return prev
    }
}

const driverAgeFrom = (prev = 18, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.driverAgeFrom !== undefined) {
                return action.payload.driverAgeFrom
            }
            return prev
        default:
            return prev
    }
}

const paymentType = (prev = 'cash', action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.paymentType !== undefined) {
                return action.payload.paymentType
            }
            return prev
        default:
            return prev
    }
}

const isFixedDate = (prev = true, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isFixedDate !== undefined) {
                return action.payload.isFixedDate
            }
            return prev
        default:
            return prev
    }
}

const rentalStart = (prev = start.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalStart !== undefined) {
                return action.payload.rentalStart
            }
            return prev
        default:
            return prev
    }
}

const rentalEnd = (prev = end.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalEnd !== undefined) {
                return action.payload.rentalEnd
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    dayCount,
    driverAgeFrom,
    paymentType,
    isFixedDate,
    rentalStart,
    rentalEnd,
})
