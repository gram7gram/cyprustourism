import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import FetchMe from '../actions/FetchMe';
import Save from '../actions/Save';
import translator from '../../translations/translator';
import Select from '../../Common/components/Select';
import {options as countryOptions} from "../../api/countries";
import Rating from "../../Common/components/Rating";
import {numberFormat} from "../../Common/utils";
import PhoneInput from "../../Common/components/PhoneInput";
import PhoneVerify from "./PhoneVerify";
import EmailVerify from "./EmailVerify";
import EmailResend from "../actions/EmailResend";
import SmsResend from "../actions/SmsResend";

class ProfileProvider extends React.Component {

    componentWillMount() {
        this.props.dispatch(FetchMe())
    }

    requestEmailCode = () => {
        this.props.dispatch(EmailResend())
    }

    requestSmsCode = () => {
        this.props.dispatch(SmsResend())
    }

    submit = () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(Save(model))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changePhone = (phone, dialCode, isValid) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            phone, dialCode, isPhoneValid: isValid
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.ProfileProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {
            model,
            isValid,
            isLoading,
            serverErrors,
            isPhoneVerificationActive,
            isEmailVerificationActive
        } = this.props.ProfileProvider

        return <div className="card shadow-sm">

            <div className="card-header">
                <div className="row">
                    <div className="col-auto">
                        <h5 className="m-0">
                            {translator('your_rating')}:&nbsp;
                            <Rating rating={model.rating}/>&nbsp;{numberFormat(model.rating)}
                        </h5>
                    </div>
                </div>
            </div>
            <div className="card-body">

                {serverErrors.length > 0 && <div className="alert alert-danger">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                <div className="row">
                    <div className="col-12 col-md-7 col-lg-8">

                        <h3>{translator('profile_personal_info')}</h3>

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0">{translator('name')}</label>
                                    <input type="text"
                                           name="name"
                                           autoFocus={true}
                                           className="form-control"
                                           onChange={this.changeString('name')}
                                           value={model.user.name || ''}/>
                                    {this.getError('name')}
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0">{translator('surname')}</label>
                                    <input type="text"
                                           name="surname"
                                           className="form-control"
                                           onChange={this.changeString('surname')}
                                           value={model.user.surname || ''}/>
                                    {this.getError('surname')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('company_id')}</label>
                                    <input type="text"
                                           name="companyId"
                                           className="form-control"
                                           onChange={this.changeString('companyId')}
                                           value={model.companyId || ''}/>
                                    {this.getError('companyId')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('company_name')}</label>
                                    <input type="text"
                                           name="companyName"
                                           className="form-control"
                                           onChange={this.changeString('companyName')}
                                           value={model.companyName || ''}/>
                                    {this.getError('companyName')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('website')}</label>
                                    <input type="text"
                                           name="website"
                                           className="form-control"
                                           onChange={this.changeString('website')}
                                           value={model.website || ''}/>
                                    {this.getError('website')}
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('country_resident')}</label>
                                    <Select
                                        name="country"
                                        className="text-black"
                                        options={countryOptions}
                                        onChange={this.changeByKey('country')}
                                        value={model.user.country || ''}/>
                                    {this.getError('country')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('address')}</label>
                                    <input type="text"
                                           name="address"
                                           className="form-control"
                                           onChange={this.changeString('address')}
                                           value={model.address || ''}/>
                                    {this.getError('address')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0">
                                        {translator('email')}&nbsp;

                                        {model.user.isEmailVerified === true
                                            ? <small className="fg-success">
                                                <i className="fa fa-check"/>&nbsp;{translator('is_verified')}
                                            </small>
                                            : null}

                                        {model.user.isEmailVerified === false
                                            ? <small className="fg-danger">
                                                <i className="fa fa-exclamation-circle"/>&nbsp;{translator('is_not_verified')}
                                            </small> : null}
                                    </label>
                                    <input type="email"
                                           name="email"
                                           className="form-control"
                                           onChange={this.changeString('email')}
                                           value={model.user.email || ''}/>
                                    {this.getError('email')}
                                </div>
                            </div>
                            <div className="col">

                                {model.user.isEmailVerified === false
                                    ? <button className="btn btn-primary transition-3d-hover btn-sm mt-4"
                                              onClick={this.requestEmailCode}
                                              disabled={isLoading}
                                              type="button">
                                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                        &nbsp;{translator('verify_email')}
                                    </button>
                                    : null}

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 col-md-6">

                                <div className="form-group text-left">
                                    <label className="m-0">
                                        {translator('phone')}&nbsp;

                                        {model.user.isPhoneVerified === true
                                            ? <small className="fg-success">
                                                <i className="fa fa-check"/>&nbsp;{translator('is_verified')}
                                            </small>
                                            : null}

                                        {model.user.isPhoneVerified === false
                                            ? <small className="fg-danger">
                                                <i className="fa fa-exclamation-circle"/>&nbsp;{translator('is_not_verified')}
                                            </small> : null}
                                    </label>

                                    <PhoneInput
                                        name="phone"
                                        onChange={this.changePhone}
                                        value={model.user.phone || ''}/>

                                    {this.getError('phone')}
                                </div>
                            </div>
                            <div className="col-12 col-md-6">

                                {model.user.isPhoneVerified === false
                                    ? <button className="btn btn-primary transition-3d-hover btn-sm mt-4"
                                              onClick={this.requestSmsCode}
                                              disabled={isLoading}
                                              type="button">
                                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                        &nbsp;{translator('verify_sms')}
                                    </button>
                                    : null}
                            </div>
                        </div>


                    </div>
                    <div className="col-12 col-md-5 col-lg-4 border-left">

                        <h3>{translator('profile_security')}</h3>

                        <div className="row">
                            <div className="col-12">
                                <div className="form-group text-left">
                                    <label className="m-0">{translator('current_password')}</label>
                                    <input type="password"
                                           name="currentPassword"
                                           className="form-control"
                                           onChange={this.changeString('currentPassword')}
                                           value={model.user.currentPassword || ''}/>
                                    {this.getError('currentPassword')}
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group text-left">
                                    <label className="m-0">{translator('password')}</label>
                                    <input type="password"
                                           name="password"
                                           className="form-control"
                                           onChange={this.changeString('password')}
                                           value={model.user.password || ''}/>
                                    {this.getError('password')}
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group text-left">
                                    <label
                                        className="m-0">{translator('password_repeat')}</label>
                                    <input type="password"
                                           name="password2"
                                           className="form-control"
                                           onChange={this.changeString('password2')}
                                           value={model.user.password2 || ''}/>
                                    {this.getError('password2')}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">


                    <div className="col-12">

                        <div className="form-group text-center">
                            <button className="btn btn-primary transition-3d-hover"
                                    type="button"
                                    onClick={this.submit}
                                    disabled={isLoading || !isValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                &nbsp;{translator('save')}
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            {isEmailVerificationActive
                ? <EmailVerify/>
                : null}

            {isPhoneVerificationActive
                ? <PhoneVerify/>
                : null}
        </div>
    }
}

export default connect(selectors)(ProfileProvider)
