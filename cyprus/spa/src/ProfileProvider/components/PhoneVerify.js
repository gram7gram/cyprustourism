import React from 'react';
import {connect} from 'react-redux';
import {DISABLE_SMS_VERIFY, MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import SmsResend from "../actions/SmsResend";
import SmsVerify from "../actions/SmsVerify";

class PhoneVerify extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    close = () => this.props.dispatch({
        type: DISABLE_SMS_VERIFY
    })

    resend = () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(SmsResend(model.user.id))
    }

    verify = () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(SmsVerify(model.user.id, model.user.smsCode))
    }

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.ProfileProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isLoading, serverErrors} = this.props.ProfileProvider

        const isValid = !!model.user.smsCode

        return <div className="modal d-block fade show" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">

                    <div className="modal-header">
                        <h4 className="m-0">{translator('profile_verify_phone')}</h4>
                    </div>

                    <div className="modal-body">
                        <div className="row">
                            <div className="col-12">

                                {serverErrors.length > 0 && <div className="alert alert-danger">
                                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                                </div>}

                            </div>
                            <div className="col-12 col-md-8 col-lg-6 mx-auto">

                                <div className="row">

                                    <div className="col-12">
                                        <div className="form-group text-left">
                                            <label className="m-0 required">{translator('sms_code')}</label>
                                            <input type="number"
                                                   name="smsCode"
                                                   autoFocus={true}
                                                   className="form-control"
                                                   onChange={this.changeString('smsCode')}
                                                   value={model.user.smsCode || ''}/>
                                            {this.getError('smsCode')}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="modal-footer">
                        <button type="button"
                                className="btn btn-outline-secondary transition-3d-hover btn-sm"
                                onClick={this.resend}
                                disabled={isLoading}>
                            <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-redo"}/>
                            &nbsp;{translator('resend_sms')}
                        </button>

                        <button type="button"
                                className="btn btn-primary transition-3d-hover btn-sm"
                                onClick={this.verify}
                                disabled={isLoading || !isValid}>
                            <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                            &nbsp;{translator('verify')}
                        </button>

                        <button type="button"
                                className="btn btn-default transition-3d-hover btn-sm"
                                onClick={this.close}>
                            <i className="fa fa-times"/>
                            &nbsp;{translator('close_short')}
                        </button>
                    </div>
                </div>
            </div>
        </div>


    }
}

export default connect(selectors)(PhoneVerify)
