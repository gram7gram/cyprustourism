import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    ProfileProvider: store => store.ProfileProvider,
})
