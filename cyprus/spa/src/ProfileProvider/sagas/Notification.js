import {all, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import toast from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* onEmailVerified() {
    toast(translator('email_verified_success'))
}

function* onPhoneVerified() {
    toast(translator('phone_verified_success'))
}

function* alertSuccess() {
    toast(translator('save_success'))
}

export default function* sagas() {
    yield all([
        takeEvery(Actions.EMAIL_VERIFY_SUCCESS, onEmailVerified),

        takeEvery(Actions.SMS_VERIFY_SUCCESS, onPhoneVerified),

        takeEvery(Actions.SAVE_SUCCESS, alertSuccess),
    ])
}
