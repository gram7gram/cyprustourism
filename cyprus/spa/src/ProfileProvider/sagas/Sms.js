import {all, put, takeEvery} from 'redux-saga/effects'
import {SMS_RESEND_SUCCESS, ENABLE_SMS_VERIFY} from '../actions'

function* smsVerification() {
    yield put({
        type: ENABLE_SMS_VERIFY
    })
}

export default function* sagas() {
    yield all([
        takeEvery([
            SMS_RESEND_SUCCESS,
        ], smsVerification)
    ])
}
