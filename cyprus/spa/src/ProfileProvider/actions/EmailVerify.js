import request from '../../Common/request'
import {EMAIL_VERIFY_BEFORE, EMAIL_VERIFY_FAILURE, EMAIL_VERIFY_SUCCESS} from '../actions'

export default (id, code) => dispatch => {

    dispatch({
        type: EMAIL_VERIFY_BEFORE
    })

    request.post(AppRouter.POST.emailVerification.replace('_ID_', id), {code})
        .then(({data}) => {
            dispatch({
                type: EMAIL_VERIFY_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            if (!e.response) {
                console.log(e);
                return
            }

            dispatch({
                type: EMAIL_VERIFY_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
