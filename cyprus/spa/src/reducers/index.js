import {combineReducers} from 'redux'
import User from './User'
import UI from './UI'
import Login from '../Login/reducers'
import CarRent from '../CarRent/reducers'
import CarRentProvider from '../CarRentProvider/reducers'
import BoatRent from '../BoatRent/reducers'
import BoatRentProvider from '../BoatRentProvider/reducers'
import AccommodationRent from '../AccommodationRent/reducers'
import AccommodationRentProvider from '../AccommodationRentProvider/reducers'
import Transfer from '../Transfer/reducers'
import TransferProvider from '../TransferProvider/reducers'
import RegisterUser from '../RegisterUser/reducers'
import RegisterProvider from '../RegisterProvider/reducers'
import PasswordReset from '../PasswordReset/reducers'
import PasswordSet from '../PasswordSet/reducers'
import MyInquiries from '../MyInquiries/reducers'
import MyInquiriesProvider from '../MyInquiriesProvider/reducers'
import ProfileUser from '../ProfileUser/reducers'
import ProfileProvider from '../ProfileProvider/reducers'
import InquiryUser from '../InquiryUser/reducers'
import InquiryProvider from '../InquiryProvider/reducers'
import Review from '../Review/reducers'
import AdminProviders from '../AdminProviders/reducers'
import AdminReviews from '../AdminReviews/reducers'
import AdminUsers from '../AdminUsers/reducers'
import AdminProfileProvider from '../AdminProfileProvider/reducers'
import AdminProfileUser from '../AdminProfileUser/reducers'
import AdminInquiries from '../AdminInquiries/reducers'
import AdminCategories from '../AdminCategories/reducers'
import AdminCategory from '../AdminCategory/reducers'
import ProviderBilling from '../ProviderBilling/reducers'
import Landing from '../Landing/reducers'
import AdminInquiryCost from '../AdminInquiryCost/reducers'
import AdminInvoices from '../AdminInvoices/reducers'

export default combineReducers({
    AdminInquiryCost,
    Landing,
    AdminCategories,
    AdminCategory,
    AdminUsers,
    AdminProviders,
    AdminReviews,
    AdminProfileProvider,
    AdminProfileUser,
    AdminInquiries,
    AdminInvoices,
    UI,
    User,
    Login,
    CarRent,
    CarRentProvider,
    BoatRent,
    BoatRentProvider,
    AccommodationRent,
    AccommodationRentProvider,
    Transfer,
    TransferProvider,
    RegisterUser,
    RegisterProvider,
    PasswordSet,
    PasswordReset,
    MyInquiries,
    MyInquiriesProvider,
    ProfileProvider,
    ProfileUser,
    InquiryUser,
    InquiryProvider,
    Review,
    ProviderBilling,
})
