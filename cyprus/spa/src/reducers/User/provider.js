import {combineReducers} from 'redux'
import {LOGIN_SUCCESS} from '../../Login/actions'

const initial = AppParameters.user.provider

const id = (state = initial.id, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            if (action.payload.user.provider !== undefined) {
                if (action.payload.user.provider.id !== undefined) {
                    return action.payload.user.provider.id
                }
            }
            return null
        default:
            return state
    }
}

export default combineReducers({
    id,
})
