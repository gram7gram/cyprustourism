import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import Save from '../actions/Save';
import translator from '../../translations/translator';

class PasswordSet extends React.Component {

    submit = () => {
        const {model} = this.props.PasswordSet

        this.props.dispatch(Save(model, () => {
            this.setState({
                canRedirect: true
            })
        }))
    }
    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })
    changeString = name => e => this.change(name, e.target.value)
    getError = key => {
        const {errors} = this.props.PasswordSet.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    componentWillUnmount() {
        this.change('token', null)
    }

    render() {

        const {model, isLoading, isValid, serverErrors, isSaveSuccess} = this.props.PasswordSet

        return <div className="row no-gutters space-2">
            <div className="col-md-8 col-lg-7 col-xl-6 offset-md-2 offset-lg-2 offset-xl-3 space-3 space-lg-0">

                <div className="mb-7">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_set_password')}</h2>
                    <p>{translator('set_password_subtitle')}</p>
                </div>

                {isSaveSuccess && <div className="alert alert-success text-center">
                    <div>{translator('set_password_success')}</div>

                    <a className="btn btn-sm btn-info" href={AppRouter.GET.loginIndex}>{translator('signin')}</a>
                </div>}

                {serverErrors.length > 0 && <div className="alert alert-danger text-center">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                <div className="row">
                    <div className="col-12">

                        <div className="form-group">
                            <label className="required">{translator('password')}</label>
                            <input type="password"
                                   name="password"
                                   autoFocus={true}
                                   className="form-control"
                                   onChange={this.changeString('password')}
                                   value={model.user.password || ''}/>
                            {this.getError('password')}
                        </div>

                    </div>
                    <div className="col-12">

                        <div className="form-group">
                            <label className="required">{translator('password_repeat')}</label>
                            <input type="password"
                                   name="password2"
                                   className="form-control"
                                   onChange={this.changeString('password2')}
                                   value={model.user.password2 || ''}/>
                            {this.getError('password2')}
                        </div>

                    </div>

                    <div className="col-12">
                        <div className="form-group text-center">
                            <button type="button"
                                    className="btn btn-primary transition-3d-hover"
                                    onClick={this.submit}
                                    disabled={isLoading || !isValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-lock"}/>
                                &nbsp;{translator('confirm')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(PasswordSet)
