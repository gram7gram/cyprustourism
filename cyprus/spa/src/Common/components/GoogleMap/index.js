import React from 'react'
import PropTypes from 'prop-types'
import mapStyle from './style'

import {DirectionsRenderer, GoogleMap, withGoogleMap, withScriptjs} from "react-google-maps"

import {compose, lifecycle} from "recompose";

const defaultOptions = {
    styles: mapStyle,
    fullscreenControl: false,
    streetViewControl: false,
}

const defaultCenter = {
    lat: 34.9885984,
    lng: 33.1934534
}

const MapWrapper = compose(
    withScriptjs,
    withGoogleMap,
    lifecycle({
        componentDidMount() {

            const {markers, onMapMounted} = this.props

            if (onMapMounted) {
                onMapMounted(markers)
            }

        }
    })
)(props =>
    <GoogleMap
        defaultZoom={9}
        maxZoom={6}
        options={defaultOptions}
        defaultCenter={defaultCenter}
        onClick={props.onMapClick}>
        {/*{props.markers.map((marker, i) => (
            <Marker
                key={i}
                position={marker}
                onClick={props.onMarkerClick.bind(this, marker)}>
            </Marker>
        ))}*/}
        {props.directions && <DirectionsRenderer directions={props.directions}/>}
    </GoogleMap>);

MapWrapper.propTypes = {
    onMapClick: PropTypes.func,
    onMarkerClick: PropTypes.func,
    onMapMounted: PropTypes.func,
    dispatch: PropTypes.func,
    markers: PropTypes.array,
}

MapWrapper.defaultProps = {
    googleMapURL: "https://maps.googleapis.com/maps/api/js?" + ([
        'v=3.exp',
        'libraries=geometry,drawing,places',
        "key=" + AppParameters.googleMapsApiKey,
        "language=" + AppParameters.locale,
    ].join('&')),
    loadingElement: <div/>,
    containerElement: <div className="map-container"/>,
    mapElement: <div className="map"/>,
    markers: [],
    onMapClick: () => {
    },
    onMarkerClick: () => {
    },
    onMapMounted: () => {
    },
}

export default MapWrapper