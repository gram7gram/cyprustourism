import React from 'react';
import Select from 'react-select';

const customStyles = {

    control: props => ({
        ...props,
        height: 40,
    }),
}


export default props => <Select {...props}/>