import React from 'react'
import PropTypes from 'prop-types'
import translator from "../../translations/translator";
import {dateFormat} from "../utils";
import {daysToWeeks} from "../../Common/utils";

const InquiryParametersDate = ({parameters}) => {

    const dayCount = parameters.dayCount
    const dayCountTranslation = dayCount !== null ? daysToWeeks(dayCount) : ''

    return <div>
        <div>{dateFormat(parameters.rentalStart)} - {dateFormat(parameters.rentalEnd)}</div>

        <div>{dayCountTranslation}</div>

        <div>{parameters.isFixedDate
            ? translator('rent_is_date_fixed')
            : translator('rent_is_date_flexible')}</div>
    </div>
}

InquiryParametersDate.propTypes = {
    parameters: PropTypes.any.isRequired
}

export default InquiryParametersDate