import React from 'react'

const style = {maxHeight: '100px'}

export default () => <img src="/img/loading.svg" style={style}/>
