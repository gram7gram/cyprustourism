import 'react-datepicker/dist/react-datepicker.min.css'
import './style.css'

import React from 'react'
import PropType from 'prop-types'

import Date from 'react-datepicker'
import translator from '../../../translations/translator'

class DateWrapper extends React.Component {

    onChange = e => {
        const value = typeof e === 'string' ? null : e.format('YYYY-MM-DD')
        this.props.onChange(value)
    }

    render() {
        return <Date
            closeOnSelect={true}
            viewMode="days"
            placeholderText={translator('select_date')}
            dateFormat={'DD.MM.YYYY'}
            className="form-control"
            {...this.props}
            onChange={this.onChange}/>
    }
}

DateWrapper.propTypes = {
    value: PropType.any,
    onChange: PropType.func.isRequired,
}

export default DateWrapper