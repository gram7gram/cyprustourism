import React from 'react'
import Rating from "./Rating";
import {numberFormat} from "../utils";
import translator from "../../translations/translator";

export default ({provider, text = null}) => <div className="row no-gutters">
    <div className="col-auto text-center">
        <i className="fa fa-user-circle fa-2x p-2"/>
    </div>
    <div className="col">
        <div className="px-1">
            <Rating rating={provider.rating}/>&nbsp;{numberFormat(provider.rating)}
        </div>
        <div className="px-1">
            <small className="text-secondary font-italic">{translator('provider_identity_hidden')}</small>

            <small className="fg-success ml-2">
                <i className="fa fa-check"/>&nbsp;{translator('is_verified')}
            </small>
        </div>

        {text ? <blockquote className="h5 p-2 border-left font-italic font-weight-light bg-light review">
            <i className="fa fa-comment"/>&nbsp;{text}
        </blockquote> : null}
    </div>
</div>
