import React from 'react'
import translator from "../../translations/translator";

const TransferDirection = ({model}) => {

    const parameters = model.parameters

    let {origin, destination, isReturn} = parameters

    return <div>
        <span className="badge badge-outline-dark mr-1 mb-1">{origin ? origin.name : '-'}</span>
        &nbsp;

        {isReturn
            ? <span>
                <i className="fa fa-exchange-alt"/>
                &nbsp;{translator('transfer_two_way')}</span>
            : <span>
                <i className="fa fa-long-arrow-alt-right"/>
                &nbsp;{translator('transfer_one_way')}</span>}

        &nbsp;
        <span className="badge badge-outline-dark mr-1 mb-1">{destination ? destination.name : '-'}</span>
    </div>
}

export default TransferDirection