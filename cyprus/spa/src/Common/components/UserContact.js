import React from 'react';
import PropTypes from 'prop-types';
import translator from '../../translations/translator';
import UserIcon from './UserIcon';

const UserContact = ({contact}) => {

    const location = contact.user.country
    const phone = contact.user.phone
    const email = contact.user.email

    return <div className="card bg-light shadow-sm mb-1 mr-1 transition-3d-hover">
        <div className="card-header">
            <UserIcon
                user={contact.user}/>
        </div>
        <div className="card-body">

            {email && <div className="text-nowrap text-truncate mb-2" title={email}>
                <i className="fa fa-at"/>&nbsp;{email}
            </div>}

            {phone && <div className="text-nowrap text-truncate mb-2" title={phone}>
                <i className="fa fa-phone"/>&nbsp;{phone}
            </div>}

            {location && <div className="text-nowrap text-truncate mb-2" title={location}>
                <i className="fa fa-map-marker-alt"/>&nbsp;{location}
            </div>}
        </div>
    </div>
}

UserContact.propTypes = {
    contact: PropTypes.any.isRequired,
}

export default UserContact
