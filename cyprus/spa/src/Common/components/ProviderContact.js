import React from 'react';
import PropTypes from 'prop-types';
import translator from '../../translations/translator';
import ProviderIcon from './ProviderIcon';

const ProviderContact = ({contact}) => {

    const location = contact.provider.user.country
    const company = contact.provider.companyName
    const phone = contact.provider.user.phone
    const email = contact.provider.user.email

    let website = contact.provider.website
    if (website && website.indexOf('http') !== 0) {
        website = 'https://' + website
    }

    return <div className="card bg-light shadow-sm mb-1 mr-1 transition-3d-hover">
        <div className="card-header">
            <ProviderIcon
                provider={contact.provider}/>
        </div>
        <div className="card-body">

            {company && <div className="text-nowrap text-truncate mb-2" title={company}>
                {company}
            </div>}

            {website && <div className="text-nowrap text-truncate mb-2" title={website}>
                <a href={website} target="_blank" rel="nofollow">
                    {translator('visit_site')}&nbsp;<i className="fa fa-external-link-alt"/>
                </a>
            </div>}

            {email && <div className="text-nowrap text-truncate mb-2" title={email}>
                <i className="fa fa-at"/>&nbsp;{email}
            </div>}

            {phone && <div className="text-nowrap text-truncate mb-2" title={phone}>
                <i className="fa fa-phone"/>&nbsp;{phone}
            </div>}

            {location && <div className="text-nowrap text-truncate mb-2" title={location}>
                <i className="fa fa-map-marker-alt"/>&nbsp;{location}
            </div>}
        </div>
    </div>
}

ProviderContact.propTypes = {
    contact: PropTypes.any.isRequired,
}

export default ProviderContact
