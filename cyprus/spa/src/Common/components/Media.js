import React from 'react'
import PropTypes from 'prop-types'
import translator from "../../translations/translator";
import {dateFormat} from "../utils";
import {daysToWeeks} from "../../Common/utils";

const cardStyle = {maxHeight: '100px', overflow: 'hidden'}

const Media = ({media}) => {

    return <div className="col-6 col-md-4 col-lg-2">
        <a href={media.url} download={true}
           className="card mb-1 mr-1 shadow-sm bg-white text-black text-center">
            <div className="card-body p-1 text-truncate text-nowrap">
                <small>
                    <i className="fa fa-download"/>
                    &nbsp;{translator('download')}
                </small>
            </div>
            <div className="card-img-bottom p-0"
                 style={cardStyle}>
                <img src={media.url} className="img-fluid"/>
            </div>
        </a>
    </div>
}

Media.propTypes = {
    media: PropTypes.any.isRequired
}

export default Media