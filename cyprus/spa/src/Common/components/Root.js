import 'react-toastify/dist/ReactToastify.min.css';

import React from 'react'
import {ToastContainer} from 'react-toastify';

class RootLayout extends React.Component {

    render() {

        return <div className="root-layout w-100">
            {this.props.children}
            <div className="notification-container">
                <ToastContainer/>
            </div>
        </div>
    }
}

export default RootLayout