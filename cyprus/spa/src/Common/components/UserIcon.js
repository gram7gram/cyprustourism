import React from 'react'
import Rating from "./Rating";
import {numberFormat} from "../utils";

export default ({user}) => <div className="row no-gutters">
    <div className="col-auto text-center">
        <i className="fa fa-user-circle fa-2x p-1"/>
    </div>
    <div className="col">
        <div className="px-1">
            {user.surname}&nbsp;{user.name}
        </div>
    </div>
</div>
