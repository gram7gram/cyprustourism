import React from 'react'
import PropType from 'prop-types'

class Modal extends React.Component {

    render() {
        const {isOpen, footer, header, body} = this.props

        if (!isOpen) return null;

        return <div className="modal fade in" tabIndex="-1" role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        {header}
                    </div>
                    <div className="modal-body">
                        {body}
                    </div>
                    <div className="modal-footer">
                        {footer}
                    </div>
                </div>
            </div>
        </div>
    }
}

Modal.propTypes = {
    isOpen: PropType.bool.isRequired,
    header: PropType.any.isRequired,
    body: PropType.any.isRequired,
    footer: PropType.any,
}

export default Modal