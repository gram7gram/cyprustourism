import React from 'react'
import {connect} from 'react-redux'
import {Link, withRouter} from 'react-router-dom'
import {TOGGLE_SIDEBAR} from "../../actions";
import selectors from "./selectors";
import translator from "../../../translations/translator";

const logoStyle = {width: '70px', height: '65px', overflow: 'hidden'}

class Sidebar extends React.Component {

    toggleSidebar = () => {
        this.props.dispatch({
            type: TOGGLE_SIDEBAR,
            payload: {
                isSidebarVisible: !this.props.isSidebarVisible
            }
        })
    }

    render() {

        return <div className="sidebar">
            <div className="sidebar-inner">
                <div className="sidebar-logo">
                    <div className="peers ai-c fxw-nw">
                        <div className="peer peer-greed">
                            <div className="sidebar-link td-n">
                                <div className="peers ai-c fxw-nw">
                                    <div className="peer">
                                        <div className="logo" style={logoStyle} onClick={this.toggleSidebar}>
                                            <img src="/img/spinner.png" className="img-fluid"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul className="sidebar-menu scrollable pos-r ps">

                    <li className="nav-item">
                        <Link className="sidebar-link my-2" to="/categories">
                            <span className="icon-holder"><i className="c-purple-500 fa fa-code-branch"/></span>
                            <span className="title">{translator('navigation_categories')}</span>
                        </Link>
                    </li>

                    <li className="nav-item my-2">
                        <Link className="sidebar-link" to="/profile">
                            <span className="icon-holder"><i className="fa fa-user-circle"/></span>
                            <span className="title">{translator('navigation_profile')}</span>
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    }
}

export default withRouter(connect(selectors)(Sidebar))