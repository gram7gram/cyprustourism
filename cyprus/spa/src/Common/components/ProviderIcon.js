import React from 'react'
import Rating from "./Rating";
import {numberFormat} from "../utils";
import translator from "../../translations/translator";

export default ({provider}) => <div className="row no-gutters">
    <div className="col-auto text-center">
        <i className="fa fa-user-circle fa-2x p-1"/>
    </div>
    <div className="col">
        <div className="px-1">
            {provider.user.surname}&nbsp;{provider.user.name}
        </div>
        <div className="px-1">
            <Rating rating={provider.rating}/>&nbsp;{numberFormat(provider.rating)}
        </div>
        <div className="px-1">
            <small className="fg-success">
                <i className="fa fa-check"/>&nbsp;{translator('is_verified')}
            </small>
        </div>
    </div>
</div>
