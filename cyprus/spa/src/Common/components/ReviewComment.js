import React from 'react'
import Rating from "./Rating";
import {dateFormat, numberFormat} from "../utils";
import {renderStatus} from "../../Review/utils";

export default ({review}) => <div className="row no-gutters">
    <div className="col-auto text-center">
        <i className="fa fa-user-circle fa-2x p-1"/>
    </div>
    <div className="col">
        <div className="px-1">
            <div>
                <span className="mr-2">
                    <Rating rating={review.rating}/>&nbsp;{numberFormat(review.rating)}
                </span>

                <span className="mr-2">
                    {renderStatus(review.status)}
                </span>
            </div>

            <div>
                <small className="text-muted">{dateFormat(review.createdAt)}</small>
            </div>

            {review.text ? <blockquote className="h5 p-2 border-left font-italic font-weight-light bg-light review">
                <i className="fa fa-comment"/>&nbsp;{review.text}
            </blockquote> : null}
        </div>
    </div>
</div>
