import React from 'react'

const Rating = (props) => <span className="text-center">
    <i className={"rating-star "
    + (props.rating >= 1 ? 'fas' : 'far') + ' '
    + (1 < props.rating && props.rating < 2 ? 'fa-star-half-alt' : 'fa-star')}/>

    <i className={"rating-star "
    + (props.rating >= 2 ? 'fas' : 'far') + ' '
    + (2 < props.rating && props.rating < 3 ? 'fa-star-half-alt' : 'fa-star')}/>

    <i className={"rating-star "
    + (props.rating >= 3 ? 'fas' : 'far') + ' '
    + (3 < props.rating && props.rating < 4 ? 'fa-star-half-alt' : 'fa-star')}/>

    <i className={"rating-star "
    + (props.rating >= 4 ? 'fas' : 'far') + ' '
    + (4 < props.rating && props.rating < 5 ? 'fa-star-half-alt' : 'fa-star')}/>

    <i className={"rating-star fa-star " + (props.rating === 5 ? 'fas' : 'far')}/>
</span>

export default Rating