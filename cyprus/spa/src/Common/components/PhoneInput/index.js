import 'react-intl-tel-input/dist/libphonenumber.js'
import 'react-intl-tel-input/dist/main.css'
import './style.css'

import React from 'react'
import PropType from 'prop-types'

import Input from 'react-intl-tel-input'

class PhoneInput extends React.Component {

    state = {
        isPlusEnabled: true
    }

    onChange = (isValid, shortNumber, event, fullNumber) => {
        this.props.onChange(fullNumber, event.dialCode, isValid)
    }

    disablePlusPrefix = () => {
        this.setState({
            isPlusEnabled: false
        })
    }

    render() {

        let value = this.props.value

        if (this.state.isPlusEnabled) {
            if (value && value[0] !== '+') {
                value = '+' + value
            }
        }

        return <Input
            formatOnInit={false}
            preferredCountries={['cy']}
            utilsScript={'libphonenumber.js'}
            css={['intl-tel-input', 'form-control']}
            {...this.props}
            value={value}
            onSelectFlag={this.disablePlusPrefix}
            onPhoneNumberChange={this.onChange}/>
    }
}

PhoneInput.propTypes = {
    value: PropType.any,
    onChange: PropType.func.isRequired,
}

export default PhoneInput