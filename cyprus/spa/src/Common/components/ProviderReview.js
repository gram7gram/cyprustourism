import React from 'react'
import Rating from "./Rating";
import {numberFormat} from "../utils";

export default ({provider, text = null}) => <div className="row no-gutters">
    <div className="col-auto text-center">
        <i className="fa fa-user-circle fa-2x p-1"/>
    </div>
    <div className="col">
        <div className="px-1">
            <a href={AppRouter.GET.providerShow.replace('_ID_', provider.id)} target="_blank">
                {provider.user.name}
            </a>
        </div>
        <div className="px-1">
            <Rating rating={provider.rating}/>&nbsp;{numberFormat(provider.rating)}
        </div>

        <blockquote className="h5 p-2 border-left font-italic font-weight-light bg-light review">
            <i className="fa fa-comment"/>&nbsp;{text || '-'}
        </blockquote>
    </div>
</div>
