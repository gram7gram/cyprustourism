import moment from 'moment-timezone'
import translator from "../translations/translator";

export const scrollTo = target => {

    try {

        const element = document.getElementById(target);

        if (!element) return

        const header = document.getElementById('header');
        const offset = header.offsetTop;

        const bodyRect = document.body.getBoundingClientRect().top;
        const elementRect = element.getBoundingClientRect().top;
        const elementPosition = elementRect - bodyRect;
        const offsetPosition = elementPosition - offset;

        window.scrollTo({
            top: offsetPosition,
            behavior: "smooth"
        });

    } catch (e) {
        console.log(e);
    }
}

export const updateCurrentUrlWithFilter = (page, filter) => {
    if (window.history && window.history.pushState !== undefined) {
        const query = [
            'page=' + page
        ]

        Object.keys(filter).forEach(key => {
            let value = filter[key]
            if (typeof value === "boolean") {
                value = value ? 1 : 0
            }

            if (value !== null) {
                query.push('filter[' + key + ']=' + value)
            }
        })


        window.history.pushState({}, null, window.location.pathname + '?' + query.join('&'))
    }
}

export const daysToWeeks = days => {
    if (days === null) return ''

    const weekCount = parseInt(days / 7)
    const daysLeftInWeek = days % 7

    let dayCountTranslation = ''

    if (weekCount >= 1) {
        dayCountTranslation += weekCount + ' '
            + (weekCount === 1 ? translator('week_single') : translator('week_plural'))

        if (daysLeftInWeek >= 1) {
            dayCountTranslation += ' ' + translator('and') + ' '
                + daysLeftInWeek + ' '
                + (daysLeftInWeek === 1 ? translator('day_single') : translator('day_plural'))
        }
    } else {
        dayCountTranslation += days + ' '
            + (days === 1 ? translator('day_single') : translator('day_plural'))
    }

    return dayCountTranslation
}

export const objectValues = (obj) => obj ? Object.keys(obj).map(i => obj[i]) : []

export const setTitle = (value = '') => document.title = value.trim()

export const numberFormat = number => (number || 0).toFixed(2)

export const dateFormat = (value, format = 'DD.MM.YYYY') => {
    if (!value) return null

    return moment(value, 'YYYY-MM-DD')
    //.tz(AppParameters.timezone)
        .format(format)
}

export const dateTimeFormat = (value, format = 'DD.MM.YYYY HH:mm:ss') => {
    if (!value) return null

    return moment(value, 'YYYY-MM-DD HH:mm:ss')
    //.tz(AppParameters.timezone)
        .format(format)
}

export const cid = (length = 5) => Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, length);
