import {toast} from 'react-toastify';

export default (message, isError = false) => {
    if (isError) {
        toast.error(message, {
            position: toast.POSITION.TOP_RIGHT,
        });
    } else {
        toast.success(message, {
            position: toast.POSITION.TOP_RIGHT,
        });
    }
}