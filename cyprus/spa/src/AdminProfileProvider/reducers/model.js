import {combineReducers} from 'redux'
import * as Action from '../actions'
import user from './user'
import translator from '../../translations/translator'

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const createdAt = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.createdAt !== undefined) {
                return action.payload.createdAt
            }
            return null
        default:
            return prev
    }
}

const status = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.status !== undefined) {
                return action.payload.status
            }
            return null
        default:
            return prev
    }
}

const rating = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.rating !== undefined) {
                return action.payload.rating
            }
            return null
        default:
            return prev
    }
}

const credits = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.credits !== undefined) {
                return action.payload.credits
            }
            return null
        default:
            return prev
    }
}

const tmpCredits = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.credits !== undefined) {
                return action.payload.credits
            }
            return prev
        default:
            return prev
    }
}

const companyName = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.companyName !== undefined) {
                return action.payload.companyName
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.companyName !== undefined) {
                return action.payload.companyName
            }
            return prev
        default:
            return prev
    }
}

const companyId = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.companyId !== undefined) {
                return action.payload.companyId
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.companyId !== undefined) {
                return action.payload.companyId
            }
            return prev
        default:
            return prev
    }
}

const website = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.website !== undefined) {
                return action.payload.website
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.website !== undefined) {
                return action.payload.website
            }
            return prev
        default:
            return prev
    }
}

const address = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.address !== undefined) {
                return action.payload.address
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.address !== undefined) {
                return action.payload.address
            }
            return prev
        default:
            return prev
    }
}

const type = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.type !== undefined) {
                return {
                    value: action.payload.type,
                    label: translator('inquiry_type_' + action.payload.type)
                }
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.type !== undefined) {
                return action.payload.type
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    id,
    createdAt,
    tmpCredits,
    credits,
    status,
    rating,
    type,
    companyName,
    companyId,
    address,
    website,
    user,
})