import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import FetchItem from '../actions/FetchItem';
import Save from '../actions/Save';
import translator from '../../translations/translator';
import Select from '../../Common/components/Select';
import {options as countryOptions} from "../../api/countries";
import Rating from "../../Common/components/Rating";
import PhoneInput from "../../Common/components/PhoneInput";
import {dateTimeFormat, numberFormat, setTitle} from "../../Common/utils";
import {renderStatus} from "../../AdminProviders/utils";

class ProfileProvider extends React.Component {


    componentWillMount() {
        const id = AppParameters.currentRoute.params.id
        this.props.dispatch(FetchItem(id))
    }

    submit = () => {
        const {model} = this.props.ProfileProvider

        const data = {...model}

        delete data.credits
        delete data.tmpCredits

        this.props.dispatch(Save(model))
    }

    submitCredits = () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(Save({
            id: model.id,
            credits: model.credits + model.tmpCredits
        }))
    }

    submitActive = isActive => () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(Save({
            id: model.id,
            user: {
                isActive
            }
        }))
    }

    submitStatus = status => () => {
        const {model} = this.props.ProfileProvider

        this.props.dispatch(Save({
            id: model.id,
            status
        }))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changePhone = (phone, dialCode, isValid) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            phone, dialCode, isPhoneValid: isValid
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    changeInt = name => e => {
        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0
        this.change(name, value)
    }

    getError = key => {
        const {errors} = this.props.ProfileProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isValid, isLoading, serverErrors, changes} = this.props.ProfileProvider

        if (model.id) {
            setTitle(model.user.surname + ' ' + model.user.name + " - " + translator('navigation_profile'))
        }

        const newCredits = model.credits + model.tmpCredits

        return <div className="card shadow-sm">

            <div className="card-header">
                <div className="row">
                    <div className="col">
                        <div className="text-muted">
                            {translator('created_at')}: {dateTimeFormat(model.createdAt)}
                        </div>
                        <div className="text-muted">
                            {translator('status')}: {renderStatus(model.status)}
                        </div>
                    </div>
                    <div className="col-12 col-lg-auto text-center">
                        {model.status === 'created'
                            ?
                            <button className="btn btn-sm btn-outline-success mr-1 text-nowrap transition-3d-hover"
                                    type="button"
                                    onClick={this.submitStatus('approved')}>
                                <i className="fa fa-thumbs-up"/>&nbsp;{translator('approve_provider')}
                            </button>
                            : null}

                        {model.status === 'created'
                            ? <button className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                      type="button"
                                      onClick={this.submitStatus('rejected')}>
                                <i className="fa fa-thumbs-down"/>&nbsp;{translator('reject_provider')}
                            </button>
                            : null}

                        {model.status === 'approved' && !model.user.isActive
                            ?
                            <button className="btn btn-sm btn-outline-success mr-1 text-nowrap transition-3d-hover"
                                    type="button"
                                    onClick={this.submitActive(true)}>
                                <i className="fa fa-thumbs-up"/>&nbsp;{translator('activate')}
                            </button>
                            : null}

                        {model.status === 'approved' && model.user.isActive
                            ? <button className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                      type="button"
                                      onClick={this.submitActive(false)}>
                                <i className="fa fa-thumbs-down"/>&nbsp;{translator('deactivate')}
                            </button>
                            : null}
                    </div>
                </div>
            </div>

            <div className="card-body">

                {serverErrors.length > 0 && <div className="alert alert-danger">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                <div className="row">
                    <div className="col-12 col-md-4 col-lg-3 text-center">
                        <div className="mb-4">
                            <h5 className="text-left">{translator('rating')}</h5>

                            <h5>{numberFormat(model.rating)}</h5>
                            <Rating rating={model.rating}/>
                        </div>

                        <div className="mb-4">
                            <h5 className="text-left">{translator('credits')}</h5>
                            <h5>{model.credits}</h5>
                        </div>

                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('add_credits')}</label>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                    <i className="fa fa-plus"/>
                                    </span>
                                </div>
                                <input type="number"
                                       min={0}
                                       name="credits"
                                       className="form-control"
                                       onChange={this.changeInt('credits')}
                                       value={model.tmpCredits || ''}/>
                                {this.getError('credits')}
                            </div>
                        </div>

                        {changes.credits && <div className="text-secondary text-center my-2">
                            <p>{translator('new_provider_credits')}</p>
                            {newCredits}

                            &nbsp;

                            {newCredits === 1
                                ? translator('credits_short_singular')
                                : translator('credits_short_plural')}

                        </div>}

                        <button
                            type="button"
                            className={"btn btn-sm btn-block mr-1 text-nowrap transition-3d-hover "
                            + (changes.credits ? "btn-danger" : "btn-outline-danger")}
                            disabled={!changes.credits}
                            onClick={this.submitCredits}>
                            <i className="fa fa-plus"/>&nbsp;{translator('save_credits')}
                        </button>
                    </div>
                    <div className="col-12 col-md-8 col-lg-9">

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('name')}</label>
                                    <input type="text"
                                           name="name"
                                           autoFocus={true}
                                           className="form-control"
                                           onChange={this.changeString('name')}
                                           value={model.user.name || ''}/>
                                    {this.getError('name')}
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('surname')}</label>
                                    <input type="text"
                                           name="surname"
                                           className="form-control"
                                           onChange={this.changeString('surname')}
                                           value={model.user.surname || ''}/>
                                    {this.getError('surname')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('email')}</label>
                                    <input type="email"
                                           name="email"
                                           className="form-control"
                                           onChange={this.changeString('email')}
                                           value={model.user.email || ''}/>
                                    {this.getError('email')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('phone')}</label>
                                    <PhoneInput
                                        name="phone"
                                        onChange={this.changePhone}
                                        value={model.user.phone || ''}/>
                                    {this.getError('phone')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('company_id')}</label>
                                    <input type="text"
                                           name="companyId"
                                           className="form-control"
                                           onChange={this.changeString('companyId')}
                                           value={model.companyId || ''}/>
                                    {this.getError('companyId')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('company_name')}</label>
                                    <input type="text"
                                           name="companyName"
                                           className="form-control"
                                           onChange={this.changeString('companyName')}
                                           value={model.companyName || ''}/>
                                    {this.getError('companyName')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0">{translator('website')}</label>
                                    <input type="text"
                                           name="website"
                                           className="form-control"
                                           onChange={this.changeString('website')}
                                           value={model.website || ''}/>
                                    {this.getError('website')}
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('country_resident')}</label>
                                    <Select
                                        name="country"
                                        className="text-black"
                                        options={countryOptions}
                                        onChange={this.changeByKey('country')}
                                        value={model.user.country || ''}/>
                                    {this.getError('country')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('address')}</label>
                                    <input type="text"
                                           name="address"
                                           className="form-control"
                                           onChange={this.changeString('address')}
                                           value={model.address || ''}/>
                                    {this.getError('address')}
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col-12 col-md-6">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('password')}</label>
                                    <input type="password"
                                           name="password"
                                           className="form-control"
                                           onChange={this.changeString('password')}
                                           value={model.user.password || ''}/>
                                    {this.getError('password')}
                                </div>
                            </div>

                            <div className="col-12 col-md-6">
                                <div className="form-group text-left">
                                    <label
                                        className="m-0 required">{translator('password_repeat')}</label>
                                    <input type="password"
                                           name="password2"
                                           className="form-control"
                                           onChange={this.changeString('password2')}
                                           value={model.user.password2 || ''}/>
                                    {this.getError('password2')}
                                </div>

                            </div>
                        </div>

                        <div className="row">


                            <div className="col-12">

                                <div className="form-group text-center">
                                    <button className="btn btn-primary transition-3d-hover"
                                            type="button"
                                            onClick={this.submit}
                                            disabled={isLoading || !isValid}>
                                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                        &nbsp;{translator('save')}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(ProfileProvider)
