import request from '../../Common/request'
import {FETCH_BEFORE, FETCH_FAILURE, FETCH_SUCCESS} from '../actions'

const parseBeforeSubmit = filter => {
    const data = {...filter}

    if (data.isActive) {
        data.isActive = data.isActive.value === 'active' ? 1 : 0
    } else {
        delete data.isActive
    }

    if (data.search) {
        data.search = data.search.trim()
    } else {
        delete data.search
    }

    return data
}

export default (filter, page) => dispatch => {

    const data = parseBeforeSubmit(filter)

    dispatch({
        type: FETCH_BEFORE
    })

    const query = [
        'page=' + page
    ]

    Object.keys(data).forEach(key => {
        const value = data[key]
        query.push('filter[' + key + ']=' + value)
    })

    request.get(AppRouter.GET.users + '?' + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
