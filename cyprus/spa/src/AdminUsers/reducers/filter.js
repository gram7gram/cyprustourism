import {combineReducers} from 'redux'
import * as Action from '../actions'

const isActive = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.isActive !== undefined) {
                return action.payload.isActive
            }
            return prev
        default:
            return prev
    }
}

const search = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.search !== undefined) {
                return action.payload.search
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    search,
    isActive,
})

