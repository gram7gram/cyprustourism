import React from 'react';
import {FETCH_REQUEST, FILTER_CHANGED} from '../actions';
import translator from '../../translations/translator';
import Select from "../../Common/components/Select";
import {connect} from 'react-redux';
import selectors from './selectors';

const statusOptions = [
    {value: 'active', label: translator('users_status_active')},
    {value: 'inactive', label: translator('users_status_inactive')},
]

class Filter extends React.Component {

    submit = () => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page: 1
            }
        })
    }

    change = (key, value = null) => this.props.dispatch({
        type: FILTER_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    render() {

        const {isLoading, filter} = this.props.AdminUsers

        return <div className="row">
            <div className="col-12">
                <div className="row">
                    <div className="col-12 col-md-6 col-lg-3">

                        <input type="text" className="form-control mr-1 mb-1"
                               value={filter.search || ''}
                               onChange={this.changeString('search')}
                               placeholder={translator('search_placeholder')}/>
                    </div>
                    <div className="col-12 col-md-6 col-lg-3">

                        <Select
                            isSearchable={false}
                            isClearable={true}
                            placeholder={translator('select_status')}
                            value={filter.isActive}
                            onChange={this.changeByKey('isActive')}
                            options={statusOptions}
                            className="mr-1 mb-1 w-100"/>
                    </div>
                    <div className="col-12 col-md-6 col-lg-3">

                        <button className="btn btn-primary btn-sm transition-3d-hover"
                                type="button"
                                onClick={this.submit}
                                disabled={isLoading}>
                            <i className={"fa " + (isLoading
                                ? "fa-spin fa-circle-notch"
                                : "fa-search")}/>
                            &nbsp;{translator('search')}
                        </button>

                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Filter)
