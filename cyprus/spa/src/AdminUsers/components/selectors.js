import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminUsers: store => store.AdminUsers,
})
