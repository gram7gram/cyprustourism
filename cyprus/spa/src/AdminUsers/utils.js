import React from 'react'
import translator from '../translations/translator';

export const renderIsActive = isActive => {

    if (isActive) {
        return <span className="badge badge-success">
                <i className="fa fa-check"/>&nbsp;{translator('users_status_active')}
            </span>
    } else {
        return <span className="badge badge-danger">
                <i className="fa fa-ban"/>&nbsp;{translator('users_status_inactive')}
            </span>
    }
}