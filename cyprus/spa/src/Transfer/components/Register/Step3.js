import React from 'react';
import {connect} from 'react-redux';
import {USER_MODEL_CHANGED} from '../../actions';
import selectors from './selectors';
import translator from '../../../translations/translator';
import Select from '../../../Common/components/Select';
import {options as countryOptions} from "../../../api/countries";
import PhoneInput from "../../../Common/components/PhoneInput";

class Step3 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: USER_MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changePhone = (phone, dialCode, isValid) => this.props.dispatch({
        type: USER_MODEL_CHANGED,
        payload: {
            phone, dialCode, isPhoneValid: isValid
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.Transfer.RegisterUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {creator} = this.props.Transfer.model

        return <div className="row">
            <div className="col-12">

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('name')}</label>
                            <input type="text"
                                   name="name"
                                   autoFocus={true}
                                   className="form-control"
                                   onChange={this.changeString('name')}
                                   value={creator.name || ''}/>
                            {this.getError('name')}
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('surname')}</label>
                            <input type="text"
                                   name="surname"
                                   className="form-control"
                                   onChange={this.changeString('surname')}
                                   value={creator.surname || ''}/>
                            {this.getError('surname')}
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-12 col-md-6">

                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('country')}</label>
                            <Select
                                name="country"
                                className="text-black"
                                options={countryOptions}
                                onChange={this.changeByKey('country')}
                                value={creator.country || ''}/>
                            {this.getError('country')}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('email')}</label>
                            <input type="email"
                                   name="email"
                                   disabled={true}
                                   className="form-control"
                                   value={creator.email || ''}/>
                            {this.getError('email')}
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('phone')}</label>
                            <PhoneInput
                                placeholder={translator('enter_phone_placeholder')}
                                onChange={this.changePhone}
                                value={creator.phone || ''}/>
                            {this.getError('phone')}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('password')}</label>
                            <input type="password"
                                   name="password"
                                   className="form-control"
                                   onChange={this.changeString('password')}
                                   value={creator.password || ''}/>
                            {this.getError('password')}
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label
                                className="m-0 required">{translator('password_repeat')}</label>
                            <input type="password"
                                   name="password2"
                                   className="form-control"
                                   onChange={this.changeString('password2')}
                                   value={creator.password2 || ''}/>
                            {this.getError('password2')}
                        </div>

                    </div>
                </div>
            </div>

            <div className="col-12">
                <div className="form-group text-center">
                    <div className="custom-control custom-checkbox text-muted">
                        <input type="checkbox"
                               id="register-isAccepted"
                               className="custom-control-input"
                               name="isAccepted"
                               onChange={this.changeBool('isAccepted')}
                               checked={creator.isAccepted}/>

                        <label htmlFor="register-isAccepted" className="custom-control-label">
                            {translator('register_user_terms_prefix')}
                            &nbsp;<a href={AppRouter.GET.termsIndex} target="_blank">{translator('navigation_terms')}</a>
                            ,&nbsp;<a href={AppRouter.GET.privacyIndex} target="_blank">{translator('navigation_privacy')}</a>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step3)
