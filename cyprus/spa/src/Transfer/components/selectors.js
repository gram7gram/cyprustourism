import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    Transfer: store => store.Transfer,
    locale: store => store.UI.locale,
})
