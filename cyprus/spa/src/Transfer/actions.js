const prefix = 'Transfer'

export const SAVE_REQUEST = prefix + '/SAVE_REQUEST';
export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const VALIDATE_REQUEST = prefix + '/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = prefix + '/VALIDATE_SUCCESS';
export const VALIDATE_FAILURE = prefix + '/VALIDATE_FAILURE';

export const VALIDATE_USER_REQUEST = prefix + '/VALIDATE_USER_REQUEST';
export const VALIDATE_USER_SUCCESS = prefix + '/VALIDATE_USER_SUCCESS';
export const VALIDATE_USER_FAILURE = prefix + '/VALIDATE_USER_FAILURE';

export const VALIDATE_USER_STEP_REQUEST = prefix + '/VALIDATE_USER_STEP_REQUEST';
export const VALIDATE_USER_STEP_SUCCESS = prefix + '/VALIDATE_USER_STEP_SUCCESS';
export const VALIDATE_USER_STEP_FAILURE = prefix + '/VALIDATE_USER_STEP_FAILURE';

export const MODEL_CHANGED = prefix + '/MODEL_CHANGED';
export const USER_MODEL_CHANGED = prefix + '/USER_MODEL_CHANGED';

export const ADD_DESTINATION = prefix + '/ADD_DESTINATION';
export const REMOVE_DESTINATION = prefix + '/REMOVE_DESTINATION';

export const ADD_ORIGIN = prefix + '/ADD_ORIGIN';
export const REMOVE_ORIGIN = prefix + '/REMOVE_ORIGIN';

export const FETCH_DIRECTION_BEFORE = prefix + '/FETCH_DIRECTION_BEFORE';
export const FETCH_DIRECTION_SUCCESS = prefix + '/FETCH_DIRECTION_SUCCESS';
export const FETCH_DIRECTION_FAILURE = prefix + '/FETCH_DIRECTION_FAILURE';

export const REMOVE_EXTRA = prefix + '/REMOVE_EXTRA';
export const ADD_EXTRA = prefix + '/ADD_EXTRA';

export const SET_STEP = prefix + '/SET_STEP';
export const SET_INQUIRY_STEP = prefix + '/SET_INQUIRY_STEP';

export const EMAIL_VERIFY_BEFORE = prefix + '/EMAIL_VERIFY_BEFORE';
export const EMAIL_VERIFY_SUCCESS = prefix + '/EMAIL_VERIFY_SUCCESS';
export const EMAIL_VERIFY_FAILURE = prefix + '/EMAIL_VERIFY_FAILURE';

export const EMAIL_RESEND_BEFORE = prefix + '/EMAIL_RESEND_BEFORE';
export const EMAIL_RESEND_SUCCESS = prefix + '/EMAIL_RESEND_SUCCESS';
export const EMAIL_RESEND_FAILURE = prefix + '/EMAIL_RESEND_FAILURE';

export const LOGIN_BEFORE = prefix + '/LOGIN_BEFORE';
export const LOGIN_SUCCESS = prefix + '/LOGIN_SUCCESS';
export const LOGIN_FAILURE = prefix + '/LOGIN_FAILURE';

export const REGISTER_BEFORE = prefix + '/REGISTER_BEFORE';
export const REGISTER_SUCCESS = prefix + '/REGISTER_SUCCESS';
