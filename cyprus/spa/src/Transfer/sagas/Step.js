import {all, put, takeEvery} from 'redux-saga/effects'
import {EMAIL_RESEND_SUCCESS, EMAIL_VERIFY_SUCCESS, SAVE_SUCCESS, SET_INQUIRY_STEP, SET_STEP} from '../actions'

function* scrollToTop() {
    scrollTo(0, 0)
}

function* step2({status}) {

    //Status 201 - User is new, status 200 - user already exists
    if (status === 200) return

    yield put({
        type: SET_STEP,
        payload: 2
    })
}

function* step3() {
    yield put({
        type: SET_STEP,
        payload: 3
    })
}

export default function* sagas() {
    yield all([
        takeEvery([
            SET_INQUIRY_STEP,
            SAVE_SUCCESS,
        ], scrollToTop),

        takeEvery(EMAIL_RESEND_SUCCESS, step2),

        takeEvery(EMAIL_VERIFY_SUCCESS, step3)
    ])
}
