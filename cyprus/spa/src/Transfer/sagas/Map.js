import {all, put, select, throttle} from 'redux-saga/effects'
import * as Actions from '../actions'
import FetchDirections from "../actions/FetchDirections";

function* requestDirections() {
    const {origin, destination} = yield select(store => store.Transfer.model.parameters)

    if (origin && destination) {
        yield put(FetchDirections(origin.extra, destination.extra))
    }
}

export default function* sagas() {
    yield all([
        throttle(400, [
            Actions.ADD_ORIGIN,
            Actions.ADD_DESTINATION,
        ], requestDirections),
    ])
}
