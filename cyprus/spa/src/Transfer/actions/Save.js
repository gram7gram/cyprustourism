import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.creator) {
        data.creator = data.creator.id
    }

    if (data.extras || data.destinations || data.origins) {

        data.categories = []

        if (data.extras) {

            data.extras.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.extras
        }

        if (data.destinations) {

            data.destinations.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.destinations
        }

        if (data.origins) {

            data.origins.forEach(item => {
                data.categories.push(item.id)
            })

            delete data.origins
        }
    }

    if (data.parameters.origin) {
        data.parameters.origin = data.parameters.origin.id
    }

    if (data.parameters.destination) {
        data.parameters.destination = data.parameters.destination.id
    }

    return data
}

export default (model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: SAVE_BEFORE
    })

    let promise
    if (model.guid) {
        promise = request.put(AppRouter.PUT.inquiries.replace('_GUID_', model.guid), data)
    } else {
        promise = request.post(AppRouter.POST.inquiries, data)
    }

    promise
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })

            if (callback) {
                callback()
            }
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
