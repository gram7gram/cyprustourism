import {VALIDATE_USER_STEP_FAILURE, VALIDATE_USER_STEP_SUCCESS} from '../../actions'

import Step1 from "../../../RegisterUser/actions/Validate/Step1";
import Step2 from "../../../RegisterUser/actions/Validate/Step2";
import Step3 from "../../../RegisterUser/actions/Validate/Step3";

export default (step, model, changes) => dispatch => {
    let validator = {}

    switch (step) {
        case 1:
            validator = Step1(model, changes)
            break;
        case 2:
            validator = Step2(model, changes)
            break;
        case 3:
            validator = Step3(model, changes)
            break;
    }

    if (validator.count === 0) {
        dispatch({
            type: VALIDATE_USER_STEP_SUCCESS
        })
    } else {
        dispatch({
            type: VALIDATE_USER_STEP_FAILURE,
            payload: validator
        })
    }

    return validator
}