import {combineReducers} from 'redux'
import * as Action from '../actions'
import parameters from './parameters'
import {objectValues} from "../../Common/utils";
import keyBy from "lodash/keyBy";
import creator from "./RegisterUser/model";

const type = () => 'transfer'

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const guid = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
            if (action.payload.guid !== undefined) {
                return action.payload.guid
            }
            return null
        default:
            return prev
    }
}

const extras = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.ADD_EXTRA:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_EXTRA:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

const destinations = (prev = [], action) => {
    switch (action.type) {
        case Action.ADD_DESTINATION:
            return [action.payload]
        case Action.REMOVE_DESTINATION:
            return []
        default:
            return prev
    }
}

const origins = (prev = [], action) => {
    switch (action.type) {
        case Action.ADD_ORIGIN:
            return [action.payload]
        case Action.REMOVE_ORIGIN:
            return []
        default:
            return prev
    }
}

export default combineReducers({
    id,
    guid,
    type,
    creator,
    parameters,
    extras,
    destinations,
    origins,
})
