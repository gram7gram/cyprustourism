import {combineReducers} from 'redux'
import * as Action from '../actions'
import moment from 'moment'

const now = moment().add(1, 'days')

const duration = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_DIRECTION_SUCCESS:

            const directions = action.payload

            if (directions.routes.length > 0) {
                const route = directions.routes[0]
                if (route.legs.length > 0) {
                    const leg = route.legs[0]

                    if (leg.duration) {
                        return leg.duration.text
                    }
                }
            }

            return null
        default:
            return prev
    }
}

const distance = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_DIRECTION_SUCCESS:

            const directions = action.payload

            if (directions.routes.length > 0) {
                const route = directions.routes[0]
                if (route.legs.length > 0) {
                    const leg = route.legs[0]

                    if (leg.distance) {
                        return leg.distance.text
                    }
                }
            }

            return null
        default:
            return prev
    }
}

const isReturn = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isReturn !== undefined) {
                return action.payload.isReturn
            }
            return prev
        default:
            return prev
    }
}

const returnAt = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:

            if (action.payload.returnAt !== undefined) {
                return action.payload.returnAt
            }

            if (action.payload.isReturn === false) {
                return null
            }

            return prev
        default:
            return prev
    }
}

const departAt = (prev = now.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:

            if (action.payload.departAt !== undefined) {
                return action.payload.departAt
            }

            return prev
        default:
            return prev
    }
}

const passengerCount = (prev = 3, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.passengerCount !== undefined) {
                return action.payload.passengerCount
            }
            return prev
        default:
            return prev
    }
}

const origin = (prev = null, action) => {
    switch (action.type) {
        case Action.ADD_ORIGIN:
            return action.payload
        case Action.REMOVE_ORIGIN:
            return null
        default:
            return prev
    }
}

const destination = (prev = null, action) => {
    switch (action.type) {
        case Action.ADD_DESTINATION:
            return action.payload
        case Action.REMOVE_DESTINATION:
            return null
        default:
            return prev
    }
}

export default combineReducers({
    passengerCount,
    destination,
    origin,
    isReturn,
    returnAt,
    departAt,
    distance,
    duration,
})
