import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminCategories: store => store.AdminCategories,
})
