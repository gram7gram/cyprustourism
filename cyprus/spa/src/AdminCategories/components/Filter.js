import React from 'react';
import {FETCH_REQUEST, FILTER_CHANGED} from '../actions';
import translator from '../../translations/translator';
import Select from "../../Common/components/Select";
import {connect} from 'react-redux';
import selectors from './selectors';

const typeOptions = [
    {value: 'car_rent', label: translator('inquiry_type_car_rent')},
    {value: 'boat_rent', label: translator('inquiry_type_boat_rent')},
    {value: 'accommodation_rent', label: translator('inquiry_type_accommodation_rent')},
    {value: 'transfer', label: translator('inquiry_type_transfer')},
]

const groupOptions = [
    {value: 'location', label: translator('category_group_location')},
    {value: 'car_type', label: translator('category_group_car_type')},
    {value: 'boat_type', label: translator('category_group_boat_type')},
    {value: 'extras', label: translator('category_group_extras')},
    {value: 'destination', label: translator('category_group_destination')},
]

class Filter extends React.Component {

    submit = () => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page: 1
            }
        })
    }

    change = (key, value = null) => this.props.dispatch({
        type: FILTER_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    render() {

        const {isLoading, filter} = this.props.AdminCategories

        return <div className="row">
            <div className="col-12 col-md-6 col-lg-3">

                <input type="text" className="form-control mr-1 mb-1"
                       value={filter.search || ''}
                       onChange={this.changeString('search')}
                       placeholder={translator('search_placeholder')}/>
            </div>
            <div className="col-12 col-md-6 col-lg-3">

                <Select
                    isMulti={true}
                    isSearchable={false}
                    placeholder={translator('select_group')}
                    value={filter.statuses}
                    onChange={this.changeByKey('groups')}
                    options={groupOptions}
                    className="mr-1 mb-1 w-100"/>
            </div>
            <div className="col-12 col-md-6 col-lg-3">

                <Select
                    isMulti={true}
                    isSearchable={false}
                    placeholder={translator('select_type')}
                    value={filter.types}
                    onChange={this.changeByKey('types')}
                    options={typeOptions}
                    className="mr-1 mb-1 w-100"/>
            </div>
            <div className="col-12 col-md-6 col-lg-3">

                <button className="btn btn-primary btn-sm transition-3d-hover mr-1"
                        type="button"
                        onClick={this.submit}
                        disabled={isLoading}>
                    <i className={"fa " + (isLoading
                        ? "fa-spin fa-circle-notch"
                        : "fa-search")}/>
                    &nbsp;{translator('search')}
                </button>

            </div>
        </div>
    }
}

export default connect(selectors)(Filter)
