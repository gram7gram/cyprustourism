import React from 'react';
import {connect} from 'react-redux';
import {FETCH_REQUEST} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import Filter from "./Filter";
import Paginator from "../../Common/components/Paginator";
import Spinner from "../../Common/components/Spinner";
import {renderType} from "../../MyInquiriesProvider/utils";
import {dateTimeFormat} from "../../Common/utils";

const iconStyle = {width: '30px'}

class AdminCategories extends React.Component {

    submit = (page) => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page
            }
        })
    }

    componentWillMount() {
        const {pagination} = this.props.AdminCategories

        this.submit(pagination.page)
    }

    renderContent() {

        const {items, isLoading} = this.props.AdminCategories

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_categories_title')}</h3>
                    <h4>{translator('no_categories_footer')}</h4>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">#</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('icon')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('name')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('group')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('ordering')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('locale')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => {
                    return <tr key={key}>
                        <td className="align-middle text-nowrap">
                            <a href={AppRouter.GET.categoryShow.replace('_ID_', item.id)}
                               className="btn btn-xs btn-primary">
                                <i className="fa fa-eye"/>
                            </a>
                        </td>
                        <td className="align-middle text-nowrap">
                            {item.icon
                                ? <img src={item.icon.url} style={iconStyle} className="img-fluid p-1"/>
                                : null}
                        </td>
                        <td className="align-middle text-nowrap">{item.name}</td>
                        <td className="align-middle text-nowrap">{translator('category_group_' + item.group)}</td>
                        <td className="align-middle text-nowrap">{renderType(item.type)}</td>
                        <td className="align-middle text-nowrap">{item.ordering || '-'}</td>
                        <td className="align-middle text-nowrap">{item.locale}</td>
                        <td className="align-middle text-nowrap text-secondary">{dateTimeFormat(item.createdAt)}</td>
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    }

    render() {

        const {pagination} = this.props.AdminCategories

        return <div className="card shadow-sm">
            <div className="row">
                <div className="col-12">

                    <div className="card-header">

                        <div className="row no-gutters">
                            <div className="col">
                                <Filter/>
                            </div>
                            <div className="col-12 col-lg-auto">
                                <a href={AppRouter.GET.categoryNew}
                                   className="btn btn-success btn-sm transition-3d-hover">
                                    <i className="fa fa-plus"/>&nbsp;{translator('add')}
                                </a>
                            </div>
                        </div>
                    </div>

                    <div className="card-body">

                        <div className="row">
                            <div className="col-12">
                                {this.renderContent()}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12">
                                <Paginator
                                    onChange={this.submit}
                                    page={pagination.page}
                                    limit={pagination.limit}
                                    total={pagination.total}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminCategories)
