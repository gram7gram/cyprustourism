import {all, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_REQUEST} from '../actions'
import FetchItems from '../actions/FetchItems'


function* run({payload}) {
    const {filter, pagination} = yield select(store => store.AdminCategories)

    const page = payload && payload.page ? payload.page : pagination.page
    const limit = payload && payload.limit ? payload.limit : pagination.limit

    yield put(FetchItems(filter, page, limit))
}

export default function* sagas() {
    yield all([
        takeEvery(FETCH_REQUEST, run)
    ])
}
