import request from '../../Common/request'
import {FETCH_BEFORE, FETCH_FAILURE, FETCH_SUCCESS} from '../actions'

const parseBeforeSubmit = filter => {
    const data = {...filter}

    if (data.groups && data.groups.length > 0) {
        data.groups = data.groups.map(item => item.value).join(',')
    } else {
        delete data.groups
    }

    if (data.types && data.types.length > 0) {
        data.types = data.types.map(item => item.value).join(',')
    } else {
        delete data.types
    }

    if (data.search) {
        data.search = data.search.trim()
    } else {
        delete data.search
    }

    return data
}

export default (filter, page = 1, limit = 10) => dispatch => {

    const data = parseBeforeSubmit(filter)

    dispatch({
        type: FETCH_BEFORE
    })

    const query = [
        'page=' + page,
        'limit=' + limit,
    ]

    Object.keys(data).forEach(key => {
        const value = data[key]
        query.push('filter[' + key + ']=' + value)
    })

    request.get(AppRouter.GET.categories + "?" + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
