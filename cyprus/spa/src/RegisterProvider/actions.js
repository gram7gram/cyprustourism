const prefix = 'RegisterProvider'

export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const PHONE_VERIFY_BEFORE = prefix + '/PHONE_VERIFY_BEFORE';
export const PHONE_VERIFY_SUCCESS = prefix + '/PHONE_VERIFY_SUCCESS';
export const PHONE_VERIFY_FAILURE = prefix + '/PHONE_VERIFY_FAILURE';

export const SMS_VERIFY_BEFORE = prefix + '/SMS_VERIFY_BEFORE';
export const SMS_VERIFY_SUCCESS = prefix + '/SMS_VERIFY_SUCCESS';
export const SMS_VERIFY_FAILURE = prefix + '/SMS_VERIFY_FAILURE';

export const SMS_RESEND_BEFORE = prefix + '/SMS_RESEND_BEFORE';
export const SMS_RESEND_SUCCESS = prefix + '/SMS_RESEND_SUCCESS';
export const SMS_RESEND_FAILURE = prefix + '/SMS_RESEND_FAILURE';

export const EMAIL_VERIFY_BEFORE = prefix + '/EMAIL_VERIFY_BEFORE';
export const EMAIL_VERIFY_SUCCESS = prefix + '/EMAIL_VERIFY_SUCCESS';
export const EMAIL_VERIFY_FAILURE = prefix + '/EMAIL_VERIFY_FAILURE';

export const EMAIL_RESEND_BEFORE = prefix + '/EMAIL_RESEND_BEFORE';
export const EMAIL_RESEND_SUCCESS = prefix + '/EMAIL_RESEND_SUCCESS';
export const EMAIL_RESEND_FAILURE = prefix + '/EMAIL_RESEND_FAILURE';

export const VALIDATE_REQUEST = prefix + '/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = prefix + '/VALIDATE_SUCCESS';
export const VALIDATE_FAILURE = prefix + '/VALIDATE_FAILURE';

export const MODEL_CHANGED = prefix + '/MODEL_CHANGED';
export const SET_STEP = prefix + '/SET_STEP';
