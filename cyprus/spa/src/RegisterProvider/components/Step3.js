import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import Save from '../actions/Save';
import translator from '../../translations/translator';
import Select from '../../Common/components/Select';
import {options as countryOptions} from "../../api/countries";
import PhoneInput from "../../Common/components/PhoneInput";

class Step3 extends React.Component {

    submit = () => {
        const {model} = this.props.RegisterProvider

        this.props.dispatch(Save(model))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changePhone = (phone, dialCode, isValid) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            phone, dialCode, isPhoneValid: isValid
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.RegisterProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isValid, isLoading} = this.props.RegisterProvider

        return <div className="row">
            <div className="col-12">

                <div className="row">
                    <div className="col">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('name')}</label>
                            <input type="text"
                                   name="name"
                                   autoFocus={true}
                                   className="form-control"
                                   onChange={this.changeString('name')}
                                   value={model.user.name || ''}/>
                            {this.getError('name')}
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('surname')}</label>
                            <input type="text"
                                   name="surname"
                                   className="form-control"
                                   onChange={this.changeString('surname')}
                                   value={model.user.surname || ''}/>
                            {this.getError('surname')}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('email')}</label>
                            <input type="email"
                                   name="email"
                                   disabled={true}
                                   className="form-control"
                                   value={model.user.email || ''}/>
                            {this.getError('email')}
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('phone')}</label>
                            <PhoneInput
                                onChange={this.changePhone}
                                value={model.user.phone || ''}/>
                            {this.getError('phone')}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">

                        <div className="form-group text-left">
                            <label className="m-0">{translator('website')}</label>
                            <input type="text"
                                   name="website"
                                   className="form-control"
                                   onChange={this.changeString('website')}
                                   value={model.website || ''}/>
                            {this.getError('website')}
                        </div>
                    </div>

                    <div className="col">

                        <div className="form-group text-left">
                            <label className="m-0">{translator('company_id')}</label>
                            <input type="text"
                                   name="companyId"
                                   className="form-control"
                                   onChange={this.changeString('companyId')}
                                   value={model.companyId || ''}/>
                            {this.getError('companyId')}
                        </div>
                    </div>
                    <div className="col">

                        <div className="form-group text-left">
                            <label className="m-0">{translator('company_name')}</label>
                            <input type="text"
                                   name="companyName"
                                   className="form-control"
                                   onChange={this.changeString('companyName')}
                                   value={model.companyName || ''}/>
                            {this.getError('companyName')}
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col">

                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('country_resident')}</label>
                            <Select
                                name="country"
                                className="text-black"
                                options={countryOptions}
                                onChange={this.changeByKey('country')}
                                value={model.user.country || ''}/>
                            {this.getError('country')}
                        </div>
                    </div>
                    <div className="col">

                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('address')}</label>
                            <input type="text"
                                   name="address"
                                   className="form-control"
                                   onChange={this.changeString('address')}
                                   value={model.address || ''}/>
                            {this.getError('address')}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('password')}</label>
                            <input type="password"
                                   name="password"
                                   className="form-control"
                                   onChange={this.changeString('password')}
                                   value={model.user.password || ''}/>
                            {this.getError('password')}
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group text-left">
                            <label
                                className="m-0 required">{translator('password_repeat')}</label>
                            <input type="password"
                                   name="password2"
                                   className="form-control"
                                   onChange={this.changeString('password2')}
                                   value={model.user.password2 || ''}/>
                            {this.getError('password2')}
                        </div>

                    </div>
                </div>
            </div>

            <div className="col-12">
                <div className="form-group text-center">
                    <div className="custom-control custom-checkbox text-muted">
                        <input type="checkbox"
                               id="register-isAccepted"
                               className="custom-control-input"
                               name="isAccepted"
                               onChange={this.changeBool('isAccepted')}
                               checked={model.isAccepted}/>

                        <label htmlFor="register-isAccepted" className="custom-control-label">
                            {translator('register_user_terms_prefix')}
                            &nbsp;<a href={AppRouter.GET.termsIndex} target="_blank">{translator('navigation_terms')}</a>
                            ,&nbsp;<a href={AppRouter.GET.privacyIndex} target="_blank">{translator('navigation_privacy')}</a>
                        </label>
                    </div>
                </div>
                <div className="form-group text-center">
                    <button type="button"
                            className="btn btn-primary transition-3d-hover"
                            onClick={this.submit}
                            disabled={isLoading || !isValid}>
                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                        &nbsp;{translator('signup')}
                    </button>
                </div>
            </div>
        </div>

    }
}

export default connect(selectors)(Step3)
