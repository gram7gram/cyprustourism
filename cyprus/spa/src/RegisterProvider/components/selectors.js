import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    RegisterProvider: store => store.RegisterProvider,
})
