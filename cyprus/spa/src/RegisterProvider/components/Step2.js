import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import EmailResend from "../actions/EmailResend";
import EmailVerify from "../actions/EmailVerify";

class Step2 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    resend = () => {
        const {user} = this.props.RegisterProvider.model

        this.props.dispatch(EmailResend(user.email))
    }

    verify = () => {
        const {user} = this.props.RegisterProvider.model

        this.props.dispatch(EmailVerify(user.id, user.emailCode))
    }

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.RegisterProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isValid, isLoading} = this.props.RegisterProvider

        return <div className="row">
            <div className="col-10 col-lg-6 mx-auto">

                <div className="row">
                    <div className="col-12">
                        <div className="form-group text-left">
                            <label className="m-0 required">{translator('email_code')}</label>
                            <input type="number"
                                   name="emailCode"
                                   autoFocus={true}
                                   className="form-control"
                                   onChange={this.changeString('emailCode')}
                                   value={model.user.emailCode || ''}/>
                            {this.getError('emailCode')}
                        </div>
                    </div>


                    <div className="col-12">
                        <div className="form-group text-center">
                            <button type="button"
                                    className="btn btn-outline-secondary transition-3d-hover mr-1"
                                    onClick={this.resend}
                                    disabled={isLoading}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-redo"}/>
                                &nbsp;{translator('resend_sms')}
                            </button>

                            <button type="button"
                                    className="btn btn-primary transition-3d-hover"
                                    onClick={this.verify}
                                    disabled={isLoading || !isValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                &nbsp;{translator('verify')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    }
}

export default connect(selectors)(Step2)
