import {combineReducers} from 'redux'
import * as Action from '../actions'
import model from './model'

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.EMAIL_VERIFY_FAILURE:
        case Action.EMAIL_RESEND_FAILURE:
        case Action.PHONE_VERIFY_FAILURE:
        case Action.SMS_VERIFY_FAILURE:
        case Action.SAVE_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.EMAIL_RESEND_BEFORE:
        case Action.EMAIL_RESEND_SUCCESS:
        case Action.EMAIL_VERIFY_BEFORE:
        case Action.EMAIL_VERIFY_SUCCESS:
        case Action.PHONE_VERIFY_SUCCESS:
        case Action.PHONE_VERIFY_BEFORE:
        case Action.SMS_VERIFY_BEFORE:
        case Action.SMS_VERIFY_SUCCESS:
        case Action.SAVE_SUCCESS:
        case Action.SAVE_BEFORE:
            return []
        default:
            return prev
    }
}

const step = (prev = 1, action) => {
    switch (action.type) {
        case Action.SET_STEP:
            return action.payload
        default:
            return prev
    }
}

const isAlreadyRegistered = (prev = false, action) => {
    switch (action.type) {
        case Action.EMAIL_RESEND_SUCCESS:
            return action.status === 200
        case Action.EMAIL_RESEND_BEFORE:
            return false
        default:
            return prev
    }
}

const isSaveSuccess = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
        case Action.SAVE_BEFORE:
        case Action.SAVE_FAILURE:
            return false
        case Action.SAVE_SUCCESS:
            return true
        default:
            return prev
    }
}

const isValid = (prev = false, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return true
        case Action.VALIDATE_FAILURE:
        case Action.SMS_VERIFY_SUCCESS:
        case Action.SMS_VERIFY_FAILURE:
        case Action.SAVE_FAILURE:
        case Action.SET_STEP:
            return false
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.PHONE_VERIFY_SUCCESS:
        case Action.PHONE_VERIFY_FAILURE:
        case Action.SMS_RESEND_SUCCESS:
        case Action.SMS_RESEND_FAILURE:
        case Action.SMS_VERIFY_SUCCESS:
        case Action.SMS_VERIFY_FAILURE:
        case Action.EMAIL_VERIFY_SUCCESS:
        case Action.EMAIL_VERIFY_FAILURE:
        case Action.EMAIL_RESEND_SUCCESS:
        case Action.EMAIL_RESEND_FAILURE:
        case Action.SAVE_SUCCESS:
        case Action.SAVE_FAILURE:
            return false
        case Action.EMAIL_RESEND_BEFORE:
        case Action.EMAIL_VERIFY_BEFORE:
        case Action.SMS_RESEND_BEFORE:
        case Action.SMS_VERIFY_BEFORE:
        case Action.SAVE_BEFORE:
            return true
        default:
            return prev
    }
}

const initialValidator = {
    count: 0,
    messages: [],
    errors: {}
}
const validator = (prev = initialValidator, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return initialValidator
        case Action.VALIDATE_FAILURE:
            return action.payload
        default:
            return prev
    }
}

const changes = (prev = {}, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:

            const changes = {...prev}

            Object.keys(action.payload).forEach(key => {
                changes[key] = true
            })

            return changes
        default:
            return prev
    }
}

export default combineReducers({
    step,
    isAlreadyRegistered,
    isSaveSuccess,
    isValid,
    isLoading,
    validator,
    changes,
    model,
    serverErrors,
})

