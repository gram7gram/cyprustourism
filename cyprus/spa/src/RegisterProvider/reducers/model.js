import {combineReducers} from 'redux'
import * as Action from '../actions'
import user from './user'

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const companyName = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.companyName !== undefined) {
                return action.payload.companyName
            }
            return prev
        default:
            return prev
    }
}

const companyId = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.companyId !== undefined) {
                return action.payload.companyId
            }
            return prev
        default:
            return prev
    }
}

const website = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.website !== undefined) {
                return action.payload.website
            }
            return prev
        default:
            return prev
    }
}

const address = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.address !== undefined) {
                return action.payload.address
            }
            return prev
        default:
            return prev
    }
}

const type = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.type !== undefined) {
                return action.payload.type
            }
            return prev
        default:
            return prev
    }
}

const isAccepted = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isAccepted !== undefined) {
                return action.payload.isAccepted
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    id,
    type,
    companyName,
    companyId,
    address,
    website,
    user,
    isAccepted,
})