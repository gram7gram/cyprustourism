import translator from '../../../translations/translator'

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.emailCode) {
        if (!model.user.emailCode) {
            ++validator.count
            validator.errors.emailCode = translator('validation_required')
        }
    }

    return validator
}