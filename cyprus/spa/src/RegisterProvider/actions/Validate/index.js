import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import {VALIDATE_FAILURE, VALIDATE_SUCCESS} from '../../actions'

export default (step, model, changes) => dispatch => {
    let validator = {}

    switch (step) {
        case 1:
            validator = Step1(model, changes)
            break;
        case 2:
            validator = Step2(model, changes)
            break;
        case 3:
            validator = Step3(model, changes)
            break;
    }

    if (validator.count === 0) {
        dispatch({
            type: VALIDATE_SUCCESS
        })
    } else {
        dispatch({
            type: VALIDATE_FAILURE,
            payload: validator
        })
    }

    return validator
}