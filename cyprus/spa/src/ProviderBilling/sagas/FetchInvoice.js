import {all, put, takeEvery} from 'redux-saga/effects'
import {PURCHASE_BANK_SUCCESS} from '../actions'
import FetchInvoices from '../actions/FetchInvoices'

function* run() {
    yield put(FetchInvoices())
}

export default function* sagas() {
    yield all([
        takeEvery(PURCHASE_BANK_SUCCESS, run),
    ])
}
