import {all, takeEvery} from 'redux-saga/effects'
import {PURCHASE_PAYPAL_SUCCESS} from '../actions'

function* run({payload}) {
    window.location = payload.next
}

export default function* sagas() {
    yield all([
        takeEvery(PURCHASE_PAYPAL_SUCCESS, run)
    ])
}
