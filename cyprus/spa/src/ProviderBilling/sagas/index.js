import {all, fork} from 'redux-saga/effects'
import FetchInvoice from './FetchInvoice'
import Paypal from './Paypal'

export default function* sagas() {
    yield all([
        fork(FetchInvoice),
        fork(Paypal),
    ])
}
