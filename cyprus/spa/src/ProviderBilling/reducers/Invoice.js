import {combineReducers} from 'redux'
import * as Action from '../actions'

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.FETCH_INVOICES_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_INVOICES_FAILURE:
        case Action.FETCH_INVOICES_SUCCESS:
            return false
        case Action.FETCH_INVOICES_BEFORE:
            return true
        default:
            return prev
    }
}

const currentInvoice = (prev = null, action) => {
    switch (action.type) {
        case Action.PURCHASE_BANK_SUCCESS:
            return action.payload
        default:
            return prev
    }
}


export default combineReducers({
    items,
    isLoading,
    currentInvoice,
})

