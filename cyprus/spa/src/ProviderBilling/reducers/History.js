import {combineReducers} from 'redux'
import * as Action from '../actions'

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.FETCH_HISTORY_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_HISTORY_FAILURE:
        case Action.FETCH_HISTORY_SUCCESS:
            return false
        case Action.FETCH_HISTORY_BEFORE:
            return true
        default:
            return prev
    }
}
const initialPaginator = {
    total: 0,
    page: 1,
    limit: 10,
}

const pagination = (prev = initialPaginator, action) => {
    switch (action.type) {
        case Action.FETCH_HISTORY_SUCCESS:
            return {
                total: action.payload.total,
                page: action.payload.page,
                limit: action.payload.limit,
            }
        default:
            return prev
    }
}

export default combineReducers({
    items,
    pagination,
    isLoading,
})

