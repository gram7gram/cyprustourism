import {combineReducers} from 'redux'
import * as Action from '../actions'
import Invoice from './Invoice'
import History from './History'

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.PURCHASE_BANK_FAILURE:
        case Action.PURCHASE_PAYPAL_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.PURCHASE_BANK_BEFORE:
        case Action.PURCHASE_PAYPAL_BEFORE:
            return []
        default:
            return prev
    }
}

const isBankPurchaseSuccess = (prev = false, action) => {
    switch (action.type) {
        case Action.PURCHASE_BANK_SUCCESS:
            return true
        case Action.PURCHASE_BANK_BEFORE:
        case Action.PURCHASE_BANK_FAILURE:
        case Action.PURCHASE_PAYPAL_BEFORE:
            return false
        default:
            return prev
    }
}

const isPaypalPurchaseSuccess = (prev = false, action) => {
    switch (action.type) {
        case Action.PURCHASE_PAYPAL_SUCCESS:
            return true
        case Action.PURCHASE_PAYPAL_BEFORE:
        case Action.PURCHASE_PAYPAL_FAILURE:
        case Action.PURCHASE_BANK_BEFORE:
            return false
        default:
            return prev
    }
}

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const provider = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_ME_SUCCESS:
            return action.payload
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
        case Action.FETCH_FAILURE:
            return false
        case Action.FETCH_BEFORE:
            return true
        default:
            return prev
    }
}

const isPurchasingPaypal = (prev = false, action) => {
    switch (action.type) {
        case Action.PURCHASE_BANK_BEFORE:
        case Action.PURCHASE_PAYPAL_SUCCESS:
        case Action.PURCHASE_PAYPAL_FAILURE:
            return false
        case Action.PURCHASE_PAYPAL_BEFORE:
            return true
        default:
            return prev
    }
}

const isPurchasingBank = (prev = false, action) => {
    switch (action.type) {
        case Action.PURCHASE_PAYPAL_BEFORE:
        case Action.PURCHASE_BANK_SUCCESS:
        case Action.PURCHASE_BANK_FAILURE:
            return false
        case Action.PURCHASE_BANK_BEFORE:
            return true
        default:
            return prev
    }
}


export default combineReducers({
    Invoice,
    History,
    provider,
    items,
    isLoading,
    isPurchasingBank,
    isPurchasingPaypal,
    isBankPurchaseSuccess,
    isPaypalPurchaseSuccess,
    serverErrors,
})

