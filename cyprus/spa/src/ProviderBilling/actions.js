const prefix = 'Billing'

export const FETCH_BEFORE = prefix + '/FETCH_BEFORE';
export const FETCH_SUCCESS = prefix + '/FETCH_SUCCESS';
export const FETCH_FAILURE = prefix + '/FETCH_FAILURE';

export const FETCH_INVOICES_BEFORE = prefix + '/FETCH_INVOICES_BEFORE';
export const FETCH_INVOICES_SUCCESS = prefix + '/FETCH_INVOICES_SUCCESS';
export const FETCH_INVOICES_FAILURE = prefix + '/FETCH_INVOICES_FAILURE';

export const FETCH_HISTORY_BEFORE = prefix + '/FETCH_HISTORY_BEFORE';
export const FETCH_HISTORY_SUCCESS = prefix + '/FETCH_HISTORY_SUCCESS';
export const FETCH_HISTORY_FAILURE = prefix + '/FETCH_HISTORY_FAILURE';

export const FETCH_ME_BEFORE = prefix + '/FETCH_ME_BEFORE';
export const FETCH_ME_SUCCESS = prefix + '/FETCH_ME_SUCCESS';
export const FETCH_ME_FAILURE = prefix + '/FETCH_ME_FAILURE';

export const PURCHASE_BANK_BEFORE = prefix + '/PURCHASE_BANK_BEFORE';
export const PURCHASE_BANK_SUCCESS = prefix + '/PURCHASE_BANK_SUCCESS';
export const PURCHASE_BANK_FAILURE = prefix + '/PURCHASE_BANK_FAILURE';

export const PURCHASE_PAYPAL_BEFORE = prefix + '/PURCHASE_PAYPAL_BEFORE';
export const PURCHASE_PAYPAL_SUCCESS = prefix + '/PURCHASE_PAYPAL_SUCCESS';
export const PURCHASE_PAYPAL_FAILURE = prefix + '/PURCHASE_PAYPAL_FAILURE';
