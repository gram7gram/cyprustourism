import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import FetchInvoices from "../actions/FetchInvoices";
import Spinner from "../../Common/components/Spinner";
import {dateFormat} from "../../Common/utils";
import {renderStatus, renderType} from "../../AdminInvoices/utils";

class ProviderInvoice extends React.Component {

    componentWillMount() {
        this.props.dispatch(FetchInvoices())
    }

    renderPlans() {

        const {items, isLoading} = this.props.ProviderBilling.Invoice

        if (isLoading) {
            return <div className="banner">
                <Spinner/>
            </div>
        }

        if (items.length === 0) {
            return <div className="banner">
                <h4>{translator('no_invoices_title')}</h4>
            </div>
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('name')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('billing_plan')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => <tr key={key}>
                    <td className="align-middle">
                        <div>{item.number}</div>
                        <small className="text-secondary">{dateFormat(item.createdAt)}</small>
                    </td>
                    <td className="align-middle">{item.billingPlan.name}</td>
                    <td className="align-middle">{renderStatus(item.status)}</td>
                    <td className="align-middle">{renderType(item.type)}</td>
                    <td className="align-middle">
                        <a href={item.media.url}
                           download={true}
                           className="btn btn-xs btn-outline-primary transition-3d-hover">
                            <i className="fa fa-download"/>&nbsp;{translator('download')}
                        </a>
                    </td>
                </tr>)}
                </tbody>
            </table>
        </div>
    }

    render() {

        return <div className="card shadow-sm mb-4">
            <div className="card-header">
                <h4 className="m-0">{translator('invoices')}</h4>
            </div>
            <div className="card-body">
                {this.renderPlans()}
            </div>
        </div>
    }
}

export default connect(selectors)(ProviderInvoice)
