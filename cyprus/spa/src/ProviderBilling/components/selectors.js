import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    ProviderBilling: store => store.ProviderBilling,
})
