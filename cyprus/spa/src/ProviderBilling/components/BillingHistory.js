import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import FetchHistory from "../actions/FetchHistory";
import Spinner from "../../Common/components/Spinner";
import {dateFormat} from "../../Common/utils";
import Paginator from "../../Common/components/Paginator";
import {renderType} from "../../MyInquiries/utils";

class BillingHistory extends React.Component {

    componentWillMount() {
        this.fetch(1)
    }

    fetch = (page) => {
        this.props.dispatch(FetchHistory(page))
    }

    renderContent() {

        const {items, pagination, isLoading} = this.props.ProviderBilling.History

        if (isLoading) {
            return <div className="banner">
                <Spinner/>
            </div>
        }

        if (items.length === 0) {
            return <div className="banner">
                <h4>{translator('no_provider_history_title')}</h4>
            </div>
        }

        return <div className="row">
            <div className="col-12">
                <div className="table-responsive mb-4">
                    <table className="table table-sm table-borderless table-hover table-striped">
                        <colgroup>
                            <col width="30%"/>
                            <col width="20%"/>
                            <col width="20%"/>
                            <col width="30%"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('date')}</th>
                            <th colSpan={2} className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('inquiry')}</th>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('amount')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {items.map((item, key) => {

                            const isAdded = item.type === 'plus'

                            return <tr key={key}>
                                <td className="align-middle">{dateFormat(item.createdAt)}</td>
                                <td className="align-middle">
                                    {item.inquiry
                                        ? renderType(item.inquiry.type)
                                        : '-'}
                                </td>
                                <td className="align-middle">
                                    {item.inquiry
                                        ? <a href={AppRouter.GET.myInquiryByGuid.replace('_GUID_', item.inquiry.guid)}
                                           target="_blank">
                                            <small>{translator('open')}&nbsp;<i
                                                className="fa fa-external-link-alt"/></small>
                                        </a>
                                        : null}
                                </td>
                                <td className="align-middle">
                                    <div className={isAdded ? "fg-success" : "fg-danger"}>
                                        <i className={"fa " + (isAdded ? "fa-caret-up" : "fa-caret-down")}></i>
                                        &nbsp;{item.value}
                                    </div>

                                </td>
                            </tr>
                        })}
                        </tbody>
                    </table>
                </div>
            </div>

            <div className="col-12">
                <Paginator
                    onChange={this.fetch}
                    page={pagination.page}
                    limit={pagination.limit}
                    total={pagination.total}
                />
            </div>
        </div>
    }

    render() {

        return <div className="card shadow-sm mb-4">
            <div className="card-header">
                <h4 className="m-0">{translator('billing_history')}</h4>
            </div>
            <div className="card-body">
                {this.renderContent()}
            </div>
        </div>
    }
}

export default connect(selectors)(BillingHistory)
