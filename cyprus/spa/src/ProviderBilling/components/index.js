import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import FetchItems from "../actions/FetchItems";
import PurchasePaypal from "../actions/PurchasePaypal";
import PurchaseBank from "../actions/PurchaseBank";
import FetchMe from "../actions/FetchMe";
import Spinner from "../../Common/components/Spinner";
import {numberFormat} from "../../Common/utils";

import ProviderInvoice from "./ProviderInvoice";
import BillingHistory from "./BillingHistory";

class ProviderBilling extends React.Component {

    componentWillMount() {
        this.props.dispatch(FetchItems())
        this.props.dispatch(FetchMe())
    }

    purchasePayPal = plan => () => {

        if (!confirm(translator('billing_confirmation').replace('_NAME_', plan.name))) return

        this.props.dispatch(PurchasePaypal(plan.id))
    }

    purchase = plan => () => {

        if (!confirm(translator('billing_confirmation').replace('_NAME_', plan.name))) return

        this.props.dispatch(PurchaseBank(plan.id))
    }

    renderPlans() {

        const {items, isLoading, isPurchasingBank, isPurchasingPaypal} = this.props.ProviderBilling

        if (isLoading) {
            return <div className="banner">
                <Spinner/>
            </div>
        }

        return <div className="row mb-4">
            {items.map((item, key) => <div className="col-6 col-md-4 col-lg-3 mx-auto text-center" key={key}>
                <div className="card bg-light shadow-sm m-2 transition-3d-hover">
                    <div className="card-body">
                        <h4 className="card-title">{item.name}</h4>
                        <p>
                            <span className="badge">{numberFormat(item.price)}&euro;</span>
                            &nbsp;
                            {item.discount > 0
                                ? <span className="badge badge-danger text-uppercase">
                                        -{numberFormat(item.discount)}%&nbsp;{translator('discount_off')}
                                        </span>
                                : null}
                        </p>
                        <div className="mb-1">
                            <button className="btn btn-sm btn-primary btn-block"
                                    type="button"
                                    disabled={isPurchasingPaypal}
                                    onClick={this.purchasePayPal(item)}>
                                {isPurchasingPaypal
                                    ? <i className="fa fa-spin fa-circle-notch"/>
                                    : <i className="fab fa-paypal"/>}
                                &nbsp;{translator('pay_pal')}
                            </button>
                        </div>
                        <div className="mb-1">
                            <button className="btn btn-sm btn-outline-success btn-block"
                                    type="button"
                                    disabled={isPurchasingBank}
                                    onClick={this.purchase(item)}>
                                {isPurchasingBank && <i className="fa fa-spin fa-circle-notch"/>}
                                &nbsp;{translator('bank_transfer')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>)}
        </div>
    }

    render() {

        const {provider, serverErrors, isBankPurchaseSuccess, isPaypalPurchaseSuccess} = this.props.ProviderBilling
        const {currentInvoice} = this.props.ProviderBilling.Invoice

        return <div className="row">
            <div className="col-12">
                <div className="card shadow-sm mb-4">

                    <div className="card-header">

                        {provider
                            ? <h4 className="font-weight-semi-bold m-0">{translator('balance')}:
                                &nbsp;{provider.credits}&nbsp;{translator('credits_short')}</h4>
                            : null}

                    </div>
                    <div className="card-body">

                        <p dangerouslySetInnerHTML={{__html: translator('provider_billing_info')}}/>

                        {isBankPurchaseSuccess && currentInvoice
                            ? <div className="alert alert-success text-center">
                                <div>{translator('bank_purchase_success_message')}</div>
                                <a href={currentInvoice.media.url} className="btn btn-sm btn-primary" download={true}>
                                    <i className="fa fa-download"/>&nbsp;{translator('download')}
                                </a>
                            </div>
                            : null}

                        {isPaypalPurchaseSuccess
                            ? <div className="alert alert-success text-center">
                                <div>{translator('paypal_purchase_success_message')}</div>
                            </div>
                            : null}

                        {serverErrors.length > 0 && <div className="alert alert-danger text-center">
                            <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                        </div>}

                        <div className="row mb-4">
                            <div className="col-12">
                                <h4>{translator('billing_plans')}</h4>
                                {this.renderPlans()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col-12 col-md-7">
                <ProviderInvoice/>
            </div>

            <div className="col-12 col-md-5">
                <BillingHistory/>
            </div>
        </div>
    }
}

export default connect(selectors)(ProviderBilling)
