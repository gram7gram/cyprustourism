import request from '../../Common/request'
import {FETCH_HISTORY_BEFORE, FETCH_HISTORY_FAILURE, FETCH_HISTORY_SUCCESS} from '../actions'

export default (page) => dispatch => {

    const query = [
        'page=' + page,
        'limit=10'
    ]

    dispatch({
        type: FETCH_HISTORY_BEFORE
    })

    request.get(AppRouter.GET.billingHistory + '?' + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_HISTORY_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_HISTORY_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
