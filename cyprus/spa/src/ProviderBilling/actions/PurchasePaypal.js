import request from '../../Common/request'
import {PURCHASE_PAYPAL_BEFORE, PURCHASE_PAYPAL_FAILURE, PURCHASE_PAYPAL_SUCCESS} from '../actions'

export default id => dispatch => {

    dispatch({
        type: PURCHASE_PAYPAL_BEFORE
    })

    request.get(AppRouter.GET.billingPlanPurchasePayPal.replace('_ID_', id))
        .then(({data}) => {
            dispatch({
                type: PURCHASE_PAYPAL_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: PURCHASE_PAYPAL_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
