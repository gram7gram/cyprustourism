import request from '../../Common/request'
import {FETCH_INVOICES_BEFORE, FETCH_INVOICES_FAILURE, FETCH_INVOICES_SUCCESS} from '../actions'

export default () => dispatch => {

    dispatch({
        type: FETCH_INVOICES_BEFORE
    })

    request.get(AppRouter.GET.providerInvoices)
        .then(({data}) => {
            dispatch({
                type: FETCH_INVOICES_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_INVOICES_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
