import request from '../../Common/request'
import {PURCHASE_BANK_BEFORE, PURCHASE_BANK_FAILURE, PURCHASE_BANK_SUCCESS} from '../actions'

export default id => dispatch => {

    dispatch({
        type: PURCHASE_BANK_BEFORE
    })

    request.get(AppRouter.GET.billingPlanPurchaseBank.replace('_ID_', id))
        .then(({data}) => {
            dispatch({
                type: PURCHASE_BANK_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: PURCHASE_BANK_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
