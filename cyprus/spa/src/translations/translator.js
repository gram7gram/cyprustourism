let messages = {};
import messagesEn from './messages.en.js'

const trans = (key, locale = AppParameters.locale) => {

    switch (locale) {
        case 'en':
            messages = messagesEn
            break;
        default:
            throw 'Unknown locale ' + locale
    }

    return messages[key] !== undefined ? messages[key] : key
}

export default trans
