import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    CarRentProvider: store => store.CarRentProvider,
    locale: store => store.UI.locale,
})
