import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import CategoryOption from "../../CarRent/components/CategoryOption";

const driverAgeOptions = []
for (let i = 18; i <= 100; i++) {
    driverAgeOptions.push(i)
}

class Step3 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    toggleCarType = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_CAR_TYPE,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_CAR_TYPE,
                payload
            })
        }
    }

    getError = key => {
        const {errors} = this.props.CarRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.CarRentProvider
        const {carTypeOptions} = this.props.CarRentProvider.Categories

        const groups = []
        let currentGroup = -1;

        carTypeOptions.forEach((option, idx) => {

            if (idx % 5 === 0) {
                ++currentGroup
                groups[currentGroup] = []
            }

            groups[currentGroup].push(option)
        })

        return <div className="row mb-4" id="car-rent-step-3-car">

            <div className="col-12">
                <h3 className="fg-primary">{translator('car_rent_step_3_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('car_rent_step_3_description_1')}}/>
            </div>

            <div className="col-12">


                <div className="option-container">
                    <div className="row">
                        {groups.map((options, idx) => {

                            return <div key={idx} className="col-10 col-lg-6 text-truncate">
                                {options.map((option, key) => {

                                    const isSelected = !!quote.carTypes.find(item => item.id === option.id)

                                    return <CategoryOption
                                        key={key}
                                        isSelected={isSelected}
                                        model={option}
                                        onChange={this.toggleCarType}/>
                                })}
                            </div>
                        })}

                    </div>
                </div>

            </div>
        </div>
    }
}

export default connect(selectors)(Step3)
