import React from 'react';
import translator from '../../translations/translator';

const iconStyle = {maxHeight: '150px'}

class InquirySuccess extends React.Component {

    render() {

        return <div className="row">
            <div className="col-12">

                <div className="row mb-4 mt-10 mt-xl-0">
                    <div className="col-12 col-md-3 col-lg-4 text-center">
                        <img src="/img/success-icon.png" className="img-fluid" style={iconStyle}/>
                    </div>
                    <div className="col-12 col-md-9 col-lg-8 text-center text-md-left">
                        <h2 className="h4 text-primary font-weight-normal">{translator('quote_success_message')}</h2>

                        <a href={AppRouter.GET.myInquiryByGuid.replace('_GUID_', AppParameters.currentRoute.params.guid)}
                           className="btn btn-sm transition-3d-hover btn-primary">
                            {translator('back')}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default InquirySuccess