import React from 'react';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import selectors from './selectors';
import translator from '../../translations/translator';
import CategoryOption from "../../CarRent/components/CategoryOption";

class Step4 extends React.Component {


    change = (key, value = null) => this.props.dispatch({
        type: Actions.MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeString = name => e => this.change(name, e.target.value)

    changeBool = name => e => this.change(name, !e.target.checked)

    changeInt = name => e => {

        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    toggleExtra = payload => e => {

        if (e.target.checked) {
            this.props.dispatch({
                type: Actions.ADD_EXTRA,
                payload
            })
        } else {
            this.props.dispatch({
                type: Actions.REMOVE_EXTRA,
                payload
            })
        }

    }

    getError = key => {
        const {errors} = this.props.CarRentProvider.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {quote} = this.props.CarRentProvider
        const {extraOptions} = this.props.CarRentProvider.Categories

        return <div className="row mb-4" id="car-rent-step-4-extra">

            <div className="col-12">
                <h3 className="fg-primary">{translator('car_rent_step_4_title_1')}</h3>
                <p dangerouslySetInnerHTML={{__html: translator('car_rent_step_4_description_1')}}/>
            </div>

            <div className="col-12">

                <div className="row">
                    <div className="col-12 col-md-7 text-truncate">
                        <div className="option-container">
                            {extraOptions.map((option, key) => {

                                const isSelected = !!quote.extras.find(item => item.id === option.id)

                                return <CategoryOption
                                    key={key}
                                    isSelected={isSelected}
                                    model={option}
                                    onChange={this.toggleExtra}/>
                            })}
                        </div>
                    </div>

                    <div className="col-12 col-md-5 text-truncate">

                        <div className="row">

                            <div className="col-12 text-left">
                                <label className="m-0">
                                    {translator('payment_type')}
                                </label>
                            </div>

                            <div className="col-12">
                                <div className="custom-control custom-radio">
                                    <input type="radio"
                                           id="payment_type_card"
                                           className="custom-control-input"
                                           onChange={() => this.change('paymentType', 'card')}
                                           checked={quote.parameters.paymentType === 'card'}/>

                                    <label htmlFor="payment_type_card" className="custom-control-label">
                                        {translator('payment_type_card')}
                                    </label>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="custom-control custom-radio">
                                    <input type="radio"
                                           id="payment_type_cash"
                                           className="custom-control-input"
                                           onChange={() => this.change('paymentType', 'cash')}
                                           checked={quote.parameters.paymentType === 'cash'}/>

                                    <label htmlFor="payment_type_cash" className="custom-control-label">
                                        {translator('payment_type_cash')}
                                    </label>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="custom-control custom-radio">
                                    <input type="radio"
                                           id="payment_type_any"
                                           className="custom-control-input"
                                           onChange={() => this.change('paymentType', 'any')}
                                           checked={quote.parameters.paymentType === 'any'}/>

                                    <label htmlFor="payment_type_any" className="custom-control-label">
                                        {translator('payment_type_any')}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step4)
