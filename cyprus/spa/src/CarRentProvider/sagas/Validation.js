import {all, put, select, takeEvery, throttle} from 'redux-saga/effects'
import * as Actions from '../actions'
import Validate from '../actions/Validate'

function* requestValidation() {
    yield put({
        type: Actions.VALIDATE_REQUEST
    })
}

function* runValidation() {
    const {quote, changes} = yield select(store => store.CarRentProvider)

    yield put(Validate(quote, changes))
}

export default function* sagas() {
    yield all([
        throttle(400, [
            Actions.MODEL_CHANGED,
            Actions.ADD_EXTRA,
            Actions.REMOVE_EXTRA,
            Actions.ADD_LOCATION,
            Actions.REMOVE_LOCATION,
            Actions.ADD_CAR_TYPE,
            Actions.REMOVE_CAR_TYPE,
        ], requestValidation),

        takeEvery(Actions.VALIDATE_REQUEST, runValidation)
    ])
}
