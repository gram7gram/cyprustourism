import {combineReducers} from 'redux'
import * as Action from '../actions'

const isReturn = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.isReturn !== undefined) {
                return action.payload.parameters.isReturn
            }
            return false
        case Action.MODEL_CHANGED:
            if (action.payload.isReturn !== undefined) {
                return action.payload.isReturn
            }
            return prev
        default:
            return prev
    }
}

const returnAt = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.returnAt !== undefined) {
                return action.payload.parameters.returnAt
            }
            return null
        case Action.MODEL_CHANGED:

            if (action.payload.returnAt !== undefined) {
                return action.payload.returnAt
            }

            if (action.payload.isReturn === false) {
                return null
            }

            return prev
        default:
            return prev
    }
}

const departAt = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.departAt !== undefined) {
                return action.payload.parameters.departAt
            }
            return null
        case Action.MODEL_CHANGED:

            if (action.payload.departAt !== undefined) {
                return action.payload.departAt
            }

            return prev
        default:
            return prev
    }
}

const distance = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_DIRECTION_SUCCESS:

            const directions = action.payload

            if (directions.routes.length > 0) {
                const route = directions.routes[0]
                if (route.legs.length > 0) {
                    const leg = route.legs[0]

                    if (leg.distance) {
                        return leg.distance.text
                    }
                }
            }

            return null
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.distance !== undefined) {
                return action.payload.parameters.distance
            }
            return null
        default:
            return prev
    }
}

const duration = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_DIRECTION_SUCCESS:

            const directions = action.payload

            if (directions.routes.length > 0) {
                const route = directions.routes[0]
                if (route.legs.length > 0) {
                    const leg = route.legs[0]

                    if (leg.duration) {
                        return leg.duration.text
                    }
                }
            }

            return null
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.duration !== undefined) {
                return action.payload.parameters.duration
            }
            return null
        default:
            return prev
    }
}

const dayCount = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.dayCount !== undefined) {
                return action.payload.parameters.dayCount
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.dayCount !== undefined) {
                return action.payload.dayCount
            }
            return prev
        default:
            return prev
    }
}

const roomCount = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.roomCount !== undefined) {
                return action.payload.parameters.roomCount
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.roomCount !== undefined) {
                return action.payload.roomCount
            }
            return prev
        default:
            return prev
    }
}

const bedroomCount = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.bedroomCount !== undefined) {
                return action.payload.parameters.bedroomCount
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.bedroomCount !== undefined) {
                return action.payload.bedroomCount
            }
            return prev
        default:
            return prev
    }
}

const passengerCount = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.passengerCount !== undefined) {
                return action.payload.parameters.passengerCount
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.passengerCount !== undefined) {
                return action.payload.passengerCount
            }
            return prev
        default:
            return prev
    }
}

const paymentType = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.paymentType !== undefined) {
                return action.payload.parameters.paymentType
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.paymentType !== undefined) {
                return action.payload.paymentType
            }
            return prev
        default:
            return prev
    }
}

const isFixedDate = (prev = true, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.isFixedDate !== undefined) {
                return action.payload.parameters.isFixedDate
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.isFixedDate !== undefined) {
                return action.payload.isFixedDate
            }
            return prev
        default:
            return prev
    }
}

const rentalStart = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.rentalStart !== undefined) {
                return action.payload.parameters.rentalStart
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.rentalStart !== undefined) {
                return action.payload.rentalStart
            }
            return prev
        default:
            return prev
    }
}

const rentalEnd = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.rentalEnd !== undefined) {
                return action.payload.parameters.rentalEnd
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.rentalEnd !== undefined) {
                return action.payload.rentalEnd
            }
            return prev
        default:
            return prev
    }
}

const driverAgeFrom = (prev = 18, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.driverAgeFrom !== undefined) {
                return action.payload.parameters.driverAgeFrom
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.driverAgeFrom !== undefined) {
                return action.payload.driverAgeFrom
            }
            return prev
        default:
            return prev
    }
}

const origin = (prev = null, action) => {
    switch (action.type) {
        case Action.ADD_ORIGIN:
            return action.payload
        case Action.REMOVE_ORIGIN:
            return null
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.origin !== undefined) {
                return action.payload.parameters.origin
            }
            return null
        default:
            return prev
    }
}

const destination = (prev = null, action) => {
    switch (action.type) {
        case Action.ADD_DESTINATION:
            return action.payload
        case Action.REMOVE_DESTINATION:
            return null
        case Action.FETCH_SUCCESS:
            if (action.payload.parameters !== undefined && action.payload.parameters.destination !== undefined) {
                return action.payload.parameters.destination
            }
            return null
        default:
            return prev
    }
}

export default combineReducers({
    dayCount,
    roomCount,
    bedroomCount,
    passengerCount,
    driverAgeFrom,
    paymentType,
    isFixedDate,
    rentalStart,
    rentalEnd,
    destination,
    origin,
    isReturn,
    departAt,
    returnAt,
    distance,
    duration,
})
