import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import {renderStatus, renderType} from "../../MyInquiries/utils";
import {dateFormat, dateTimeFormat} from "../../Common/utils";
import GoogleMap from "../../Common/components/GoogleMap";
import FetchDirections from "../actions/FetchDirections";
import TransferDirection from "../../Common/components/TransferDirection";

class Transfer extends React.Component {

    onMapMounted = (markers) => {
        if (markers.length === 2) {
            this.props.dispatch(FetchDirections(markers[0], markers[1]))
        }
    }

    render() {

        const {model, directions} = this.props.InquiryProvider

        if (!model) return null

        const {duration, distance} = model.parameters

        const markers = [];

        if (model.parameters.origin) {
            markers.push(model.parameters.origin.extra)
        }

        if (model.parameters.destination) {
            markers.push(model.parameters.destination.extra)
        }

        return <div className="row">
            <div className="col-12">
                <div className="table-responsive mb-4">
                    <table className="table table-sm table-borderless table-hover table-striped">
                        <colgroup>
                            <col width="20%"/>
                            <col width="80%"/>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                            <td>{dateTimeFormat(model.createdAt, 'DD.MM.YYYY HH:mm')}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                            <td>{dateTimeFormat(model.updatedAt, 'DD.MM.YYYY HH:mm')}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('expires_at')}</th>
                            <td>{dateFormat(model.expiresAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                            <td>{renderStatus(model.status)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                            <td>{renderType(model.type)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('direction')}</th>
                            <td>
                                <TransferDirection model={model}/>
                            </td>
                        </tr>
                        <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_depart_at')}</th>
                            <td className="align-middle text-nowrap">
                                {dateFormat(model.parameters.departAt)}
                            </td>
                        </tr>
                        {model.parameters.returnAt ? <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_return_at')}</th>
                            <td className="align-middle text-nowrap">
                                {dateFormat(model.parameters.returnAt)}
                            </td>
                        </tr> : null}

                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_passenger_count')}</th>
                            <td>
                                <div>{model.parameters.passengerCount}</div>
                            </td>
                        </tr>

                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_extras')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'extras')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_distance')}</th>
                            <td>
                                <div>{distance || '-'}</div>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_duration')}</th>
                            <td>
                                <div>{duration || '-'}</div>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('contact')}</th>
                            <td>
                                {model.isPhoneVerified
                                    ? <small className="fg-success">
                                        <i className={"fa fa-check"}/>&nbsp;{translator('is_verified')}
                                    </small>
                                    : <small className="text-muted">
                                        <i className={"fa fa-times"}/>&nbsp;{translator('not_verified')}
                                    </small>}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div className="col-12">

                <GoogleMap
                    markers={markers}
                    directions={directions}
                    onMapMounted={this.onMapMounted}/>
            </div>
        </div>
    }
}

export default connect(selectors)(Transfer)
