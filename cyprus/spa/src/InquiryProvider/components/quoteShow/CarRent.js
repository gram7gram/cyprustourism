import React from 'react';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import InquiryParametersDate from "../../../Common/components/InquiryParametersDate";
import QuoteLayout from "./Layout";

class CarRent extends React.Component {

    render() {

        const {quote} = this.props

        return <QuoteLayout quote={quote}>
            <div className="table-responsive">
                <table className="table table-sm table-borderless table-hover table-striped">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%"/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                        <td className="align-middle">
                            <InquiryParametersDate parameters={quote.parameters}/>
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_driver_age')}</th>
                        <td className="align-middle text-nowrap">{quote.parameters.driverAgeFrom}</td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_locations')}</th>
                        <td className="align-middle">
                            {quote.categories
                                .filter(item => item.group === 'location')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_car_types')}</th>
                        <td className="align-middle">
                            {quote.categories
                                .filter(item => item.group === 'car_type')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_extras')}</th>
                        <td className="align-middle">
                            {quote.categories
                                .filter(item => item.group === 'extras')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </QuoteLayout>
    }
}

export default connect(selectors)(CarRent)
