import React from 'react';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import QuoteLayout from "./Layout";
import GoogleMap from "../../../Common/components/GoogleMap";
import FetchQuoteDirections from "../../actions/FetchQuoteDirections";
import TransferDirection from "../../../Common/components/TransferDirection";
import {dateFormat} from "../../../Common/utils";

class Transfer extends React.Component {

    onMapMounted = (markers) => {
        const {quote} = this.props

        if (markers.length === 2) {
            this.props.dispatch(FetchQuoteDirections(quote.id, markers[0], markers[1]))
        }
    }

    render() {

        const {quoteDirections} = this.props.InquiryProvider
        const {quote} = this.props

        const directions = quoteDirections[quote.id]

        const markers = [];

        if (quote.parameters.origin) {
            markers.push(quote.parameters.origin.extra)
        }

        if (quote.parameters.destination) {
            markers.push(quote.parameters.destination.extra)
        }

        return <QuoteLayout quote={quote}>
            <div className="row">
                <div className="col-12 col-lg-6">
                    <div className="table-responsive">
                        <table className="table table-sm table-borderless table-hover table-striped">
                            <colgroup>
                                <col width="20%"/>
                                <col width="80%"/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('direction')}</th>
                                <td>
                                    <TransferDirection model={quote}/>
                                </td>
                            </tr>
                            <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_depart_at')}</th>
                                <td className="align-middle text-nowrap">
                                    {dateFormat(quote.parameters.departAt)}
                                </td>
                            </tr>
                            {quote.parameters.returnAt ? <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_return_at')}</th>
                                <td className="align-middle text-nowrap">
                                    {dateFormat(quote.parameters.returnAt)}
                                </td>
                            </tr> : null}
                            <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_passenger_count')}</th>
                                <td className="align-middle text-nowrap">
                                    {quote.parameters.passengerCount}
                                </td>
                            </tr>
                            <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_distance')}</th>
                                <td className="align-middle text-nowrap">
                                    {quote.parameters.distance || '-'}
                                </td>
                            </tr>
                            <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_duration')}</th>
                                <td className="align-middle text-nowrap">
                                    {quote.parameters.duration || '-'}
                                </td>
                            </tr>
                            <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('transfer_extras')}</th>
                                <td className="align-middle">
                                    {quote.categories
                                        .filter(item => item.group === 'extras')
                                        .map((item, i) =>
                                            <span key={i}
                                                  className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                        )}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="col-12 col-lg-6">

                    <GoogleMap
                        markers={markers}
                        directions={directions}
                        onMapMounted={this.onMapMounted}/>
                </div>
            </div>

        </QuoteLayout>
    }
}

export default connect(selectors)(Transfer)
