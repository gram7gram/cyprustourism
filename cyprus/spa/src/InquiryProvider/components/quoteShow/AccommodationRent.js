import React from 'react';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import InquiryParametersDate from "../../../Common/components/InquiryParametersDate";
import QuoteLayout from "./Layout";

class AccommodationRent extends React.Component {

    render() {

        const {quote} = this.props

        return <QuoteLayout quote={quote}>
            <div className="table-responsive">
                <table className="table table-sm table-borderless table-hover table-striped">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%"/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                        <td className="align-middle text-nowrap">
                            <InquiryParametersDate parameters={quote.parameters}/>
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_subtype')}</th>
                        <td>{quote.parameters.subtypes.map((item, i) =>
                            <span key={i}
                                  className="badge badge-outline-dark mr-1 mb-1">{translator('inquiry_subtype_' + item)}</span>
                        )}
                        </td>
                    </tr>
                    <tr>
                        <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_adults_count')}</th>
                        <td className="align-middle text-nowrap">
                            <div>{quote.parameters.adultsCount}</div>
                        </td>
                    </tr>

                    {quote.parameters.childrenCount
                        ? <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_children_count')}</th>
                            <td className="align-middle text-nowrap">
                                <div>{quote.parameters.childrenCount}</div>
                            </td>
                        </tr> : null}

                    {quote.parameters.roomCount
                        ? <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_room_count')}</th>
                            <td className="align-middle text-nowrap">
                                <div>{quote.parameters.roomCount}</div>
                            </td>
                        </tr> : null}

                    {quote.parameters.bedroomCount
                        ? <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_bedroom_count')}</th>
                            <td className="align-middle text-nowrap">
                                <div>{quote.parameters.bedroomCount}</div>
                            </td>
                        </tr> : null}

                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_locations')}</th>
                        <td className="align-middle">
                            {quote.categories
                                .filter(item => item.group === 'location')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_extras')}</th>
                        <td className="align-middle">
                            {quote.categories
                                .filter(item => item.group === 'extras')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </QuoteLayout>
    }
}

export default connect(selectors)(AccommodationRent)
