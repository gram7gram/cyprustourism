import React from 'react';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import {dateTimeFormat, numberFormat} from "../../../Common/utils";

import Media from "../../../Common/components/Media";
import ProviderComment from "../../../Common/components/ProviderComment";

import FetchContact from "../../actions/FetchContact";
import DeleteQuote from "../../actions/DeleteQuote";
import UserContact from "../../../Common/components/UserContact";

class QuoteLayout extends React.Component {

    findSelectedQuote = () => {
        const {id} = this.props.quote
        const {model} = this.props.InquiryProvider

        if (!model) return null;

        return model.selectedQuotes.find(selected => selected.quote.id === id)
    }

    fetchContact = () => {

        const selected = this.findSelectedQuote()
        if (!selected) return

        const {id} = selected.contact

        this.props.dispatch(FetchContact(id))
    }

    remove = () => {
        const {model} = this.props.InquiryProvider
        const {quote} = this.props

        if (!confirm(translator('confirm_remove'))) return

        this.props.dispatch(DeleteQuote(model.id, quote.id))
    }

    render() {

        const {sharedContacts, isLoading} = this.props.InquiryProvider.Contact
        const {isQuoteLoading} = this.props.InquiryProvider
        const {quote} = this.props

        const selectedQuote = this.findSelectedQuote()

        let isCurrentContactLoading = false;
        let canFetchContact = false;
        let currentContact = null;

        if (selectedQuote) {
            isCurrentContactLoading = selectedQuote.contact && isLoading[selectedQuote.contact.id] === true

            currentContact = selectedQuote.contact && sharedContacts[selectedQuote.contact.id] !== undefined
                ? sharedContacts[selectedQuote.contact.id]
                : null

            canFetchContact = selectedQuote.contact && selectedQuote.contact.status === 'created'
        }

        return <div className={"card shadow-sm mb-4" + (selectedQuote ? " selected-quote" : "")}>

            {selectedQuote ? <i className="fa fa-2x fa-check selected-mark"/> : null}

            <div className="card-header">
                <div className="row">
                    <div className="col-12 col-md-6 text-center text-md-left">
                        <a className="h4 m-0" href={"#quote-" + quote.id}>
                            {translator('quote')} #{quote.id}
                        </a>
                    </div>
                    <div className="col-12 col-md-6 text-center text-md-right">
                        {quote.price
                            ? <h4 className="m-0" id="selected-quote-">
                                {numberFormat(quote.price.price)}&nbsp;{quote.price.currency}
                            </h4>
                            : null}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-6 text-center text-md-left">
                        <div>
                            <small className="text-muted">{dateTimeFormat(quote.createdAt)}</small>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 text-center text-md-right">
                        <div className="text-center text-lg-right">

                            {canFetchContact
                                ? <button type="button"
                                          onClick={this.fetchContact}
                                          className="btn btn-primary btn-sm transition-3d-hover mr-1">
                                    {isCurrentContactLoading ? <i className="fa fa-spin fa-circle-notch"/> : null}
                                    &nbsp;{translator('show_contact')}
                                </button>
                                : null}

                            {selectedQuote && !canFetchContact
                                ? <span className="px-2 text-muted">
                                    <i className="fa fa-lock"/>&nbsp;{translator('contact_is_pending_notice')}
                                </span>
                                : null}

                            {!selectedQuote
                                ? <button type="button"
                                          onClick={this.remove}
                                          className="btn btn-outline-danger btn-sm">
                                    <i className={"fa " + (isQuoteLoading
                                        ? "fa-spin fa-circle-notch"
                                        : "fa-times")}/>
                                    &nbsp;{translator('remove')}
                                </button>
                                : null}

                        </div>
                    </div>
                </div>
            </div>

            <div className="card-body">

                <div className="row">

                    <div className={currentContact ? "col-12 col-lg-8" : "col-12"}>

                        <ProviderComment
                            provider={quote.provider}
                            text={quote.text}/>

                        <div className="row my-2">
                            <div className="col-12">
                                {this.props.children}
                            </div>
                        </div>

                        <div className="col-12">
                            <div className="row">
                                {quote.media.map((media, i) =>
                                    <Media media={media} key={i}/>
                                )}
                            </div>
                        </div>
                    </div>

                    {currentContact ? <div className="col-12 col-lg-4">
                        <UserContact contact={currentContact}/>
                    </div> : null}
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(QuoteLayout)
