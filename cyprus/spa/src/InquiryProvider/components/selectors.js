import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    InquiryProvider: store => store.InquiryProvider,
    locale: store => store.UI.locale,
})
