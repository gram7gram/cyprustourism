import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import {renderStatus, renderType} from "../../MyInquiries/utils";
import {dateFormat, dateTimeFormat} from "../../Common/utils";
import InquiryParametersDate from "../../Common/components/InquiryParametersDate";

class CarRent extends React.Component {

    render() {

        const {model} = this.props.InquiryProvider

        if (!model) return null

        return <div className="row">
            <div className="col-12">
                <div className="table-responsive">
                    <table className="table table-sm table-borderless table-hover table-striped">
                        <colgroup>
                            <col width="20%"/>
                            <col width="80%"/>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                            <td>{dateTimeFormat(model.createdAt, 'DD.MM.YYYY HH:mm')}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                            <td>{dateTimeFormat(model.updatedAt, 'DD.MM.YYYY HH:mm')}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('expires_at')}</th>
                            <td>{dateFormat(model.expiresAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                            <td>{renderStatus(model.status)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                            <td>{renderType(model.type)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                            <td>
                                <InquiryParametersDate parameters={model.parameters}/>
                            </td>
                        </tr>
                        <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_driver_age')}</th>
                            <td className="align-middle text-nowrap">{model.parameters.driverAgeFrom}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_locations')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'location')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_car_types')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'car_type')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('car_rent_extras')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'extras')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('contact')}</th>
                            <td>
                                {model.isPhoneVerified
                                    ? <small className="fg-success">
                                        <i className={"fa fa-check"}/>&nbsp;{translator('is_verified')}
                                    </small>
                                    : <small className="text-muted">
                                        <i className={"fa fa-times"}/>&nbsp;{translator('not_verified')}
                                    </small>}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(CarRent)
