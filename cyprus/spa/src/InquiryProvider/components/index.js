import React from 'react';
import {connect} from 'react-redux';
import FetchItem from '../actions/FetchItem';
import selectors from './selectors';
import translator from '../../translations/translator';
import Spinner from "../../Common/components/Spinner";

import BoatRent from "./BoatRent";
import AccommodationRent from "./AccommodationRent";
import Transfer from "./Transfer";
import CarRent from "./CarRent";

import BoatRentQuoteShow from "./quoteShow/BoatRent";
import AccommodationRentQuoteShow from "./quoteShow/AccommodationRent";
import TransferQuoteShow from "./quoteShow/Transfer";
import CarRentQuoteShow from "./quoteShow/CarRent";

import {setTitle} from "../../Common/utils";

class InquiryProvider extends React.Component {

    componentWillMount() {

        setTitle(translator('loading'));

        const guid = AppParameters.currentRoute.params.guid

        this.props.dispatch(FetchItem(guid))
    }

    renderActions() {

        const {model} = this.props.InquiryProvider

        if (!model) return null
        if (model.status !== 'open') return null

        return <div className="row">
            <div className="col-12 col-sm-8 col-lg-12 mx-auto">
                <div className="form-group">
                    <a className="btn btn-success btn-block transition-3d-hover"
                       href={AppRouter.GET.quoteCreate.replace('_GUID_', model.guid)}>
                        <i className="fa fa-check-circle"/>&nbsp;{translator('make_offer')}
                    </a>
                </div>
            </div>
        </div>
    }

    renderQuotes() {

        const {model, isLoading} = this.props.InquiryProvider

        if (!model || isLoading) {
            return <div className="banner">
                <Spinner/>
            </div>
        }

        if (!model.quotes || model.quotes.length === 0) {
            return <div className="card shadow-sm mb-4">
                <div className="card-header">
                    <h4 className="m-0">{translator('quotes')}</h4>
                </div>
                <div className="card-body">
                    <div className="banner">
                        <h3>{translator('no_quotes_provider_title')}</h3>
                        <h4>{translator('no_quotes_provider_footer')}</h4>
                    </div>
                </div>
            </div>
        }

        return model.quotes.map((item, i) => {

            switch (model.type) {
                case 'car_rent':
                    return <CarRentQuoteShow
                        key={i}
                        quote={item}/>
                case 'boat_rent':
                    return <BoatRentQuoteShow
                        key={i}
                        quote={item}/>
                case 'accommodation_rent':
                    return <AccommodationRentQuoteShow
                        key={i}
                        quote={item}/>
                case 'transfer':
                    return <TransferQuoteShow
                        key={i}
                        quote={item}/>
            }
        })

    }

    renderContent() {

        const {model} = this.props.InquiryProvider

        if (!model) return null

        switch (model.type) {
            case 'car_rent':
                return <CarRent/>
            case 'boat_rent':
                return <BoatRent/>
            case 'accommodation_rent':
                return <AccommodationRent/>
            case 'transfer':
                return <Transfer/>
        }

        return null
    }

    render() {

        const {model} = this.props.InquiryProvider

        if (model && model.id) {
            setTitle('#' + model.id + ' ' + translator('inquiry_type_' + model.type) + ' - ' + AppParameters.currentRoute.host)
        }

        return <div className="row">
            <div className="col-12">

                <div className="card shadow-sm mb-4">
                    <div className="card-body">

                        <div className="row no-gutters">
                            <div className="col-12 col-lg-9 mb-2">
                                {this.renderContent()}
                            </div>
                            <div className="col-12 col-lg-3 mb-2">
                                {this.renderActions()}
                            </div>
                        </div>
                    </div>
                </div>

                {this.renderQuotes()}

            </div>
        </div>
    }
}

export default connect(selectors)(InquiryProvider)
