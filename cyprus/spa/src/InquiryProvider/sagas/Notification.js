import {all, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import toast from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* notifyDeleteSuccess() {
    toast(translator('quote_remove_success'))
}

function* notifyDeleteFailure({payload}) {
    toast(translator('quote_remove_failure') + ": " + payload.data.message, true)
}

export default function* sagas() {
    yield all([
        takeEvery(Actions.DELETE_QUOTE_SUCCESS, notifyDeleteSuccess),

        takeEvery(Actions.DELETE_QUOTE_FAILURE, notifyDeleteFailure),
    ])
}
