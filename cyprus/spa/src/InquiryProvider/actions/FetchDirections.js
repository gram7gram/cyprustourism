import {FETCH_DIRECTION_BEFORE, FETCH_DIRECTION_SUCCESS, FETCH_DIRECTION_FAILURE} from '../actions'

export default (origin, destination) => dispatch => {

    dispatch({
        type: FETCH_DIRECTION_BEFORE
    })

    const DirectionsService = new google.maps.DirectionsService();

    const originCoordinate = new google.maps.LatLng(origin.lat, origin.lng)
    const destinationCoordinate = new google.maps.LatLng(destination.lat, destination.lng)

    DirectionsService.route({
        origin: originCoordinate,
        destination: destinationCoordinate,
        travelMode: google.maps.TravelMode.DRIVING,
    }, (result, status) => {
        // console.log(`fetching directions`, status, result);

        if (status === google.maps.DirectionsStatus.OK) {
            dispatch({
                type: FETCH_DIRECTION_SUCCESS,
                payload: result
            })
        } else {
            dispatch({
                type: FETCH_DIRECTION_FAILURE,
                payload: {
                    status,
                    data: result
                }
            })
        }
    });
}