import request from '../../Common/request'
import {DELETE_QUOTE_BEFORE, DELETE_QUOTE_FAILURE, DELETE_QUOTE_SUCCESS} from '../actions'

export default (inquiry, quote) => dispatch => {

    dispatch({
        type: DELETE_QUOTE_BEFORE
    })

    request.delete(AppRouter.DELETE.quote.replace('_INQUIRY_ID_', inquiry).replace('_QUOTE_ID_', quote))
        .then(({data}) => {
            dispatch({
                type: DELETE_QUOTE_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: DELETE_QUOTE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
