import {combineReducers} from 'redux'
import * as Action from '../actions'
import Categories from './Categories'
import Contact from './Contact'

const model = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_SUCCESS:
            return action.payload
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.FETCH_FAILURE:
        case Action.FETCH_SUCCESS:
            return false
        case Action.FETCH_BEFORE:
            return true
        default:
            return prev
    }
}

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.SAVE_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.SAVE_BEFORE:
            return []
        default:
            return prev
    }
}

const directions = (prev = null, action) => {
    switch (action.type) {
        case Action.FETCH_DIRECTION_FAILURE:
            return null
        case Action.FETCH_DIRECTION_SUCCESS:
            return action.payload
        default:
            return prev
    }
}

const quoteDirections = (prev = {}, action) => {
    switch (action.type) {
        case Action.FETCH_QUOTE_DIRECTION_FAILURE:
            let state = {...prev}

            delete state[action.payload.quote]

            return state
        case Action.FETCH_QUOTE_DIRECTION_SUCCESS:
            return {
                ...prev,
                [action.payload.quote]: action.payload.data
            }
        default:
            return prev
    }
}


export default combineReducers({
    Contact,
    Categories,
    isLoading,
    serverErrors,
    model,
    directions,
    quoteDirections,
})

