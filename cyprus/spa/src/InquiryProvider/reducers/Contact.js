import {combineReducers} from 'redux'
import * as Action from '../actions'

const sharedContacts = (prev = {}, action) => {
    switch (action.type) {
        case Action.FETCH_CONTACT_SUCCESS:
            return {
                ...prev,
                [action.payload.id]: action.payload
            }
        default:
            return prev
    }
}

const isLoading = (prev = {}, action) => {
    switch (action.type) {
        case Action.FETCH_CONTACT_BEFORE:
            return {
                ...prev,
                [action.payload.id]: true
            }
        case Action.FETCH_CONTACT_SUCCESS:
            return {
                ...prev,
                [action.payload.id]: false
            }
        case Action.FETCH_CONTACT_FAILURE:
            return {}
        default:
            return prev
    }
}

export default combineReducers({
    sharedContacts,
    isLoading,
})

