import React from 'react'
import translator from '../translations/translator';

export const renderStatus = status => {

    switch (status) {
        case 'open':
            return <span className="badge badge-success">
                <i className="fa fa-check"/>&nbsp;{translator('inquiry_status_open')}
            </span>
        case 'closed':
            return <span className="badge badge-secondary">
                <i className="fa fa-ban"/>&nbsp;{translator('inquiry_status_closed')}
            </span>
        case 'pending':
            return <span className="badge badge-warning">
                <i className="fa fa-clock"/>&nbsp;{translator('inquiry_status_pending')}
            </span>
        default:
            return status
    }
}

export const renderType = type => {

    switch (type) {
        case 'car_rent':
            return <span className="badge badge-warning">
                <i className="fa fa-car"/>&nbsp;{translator('inquiry_type_car_rent')}
            </span>
        case 'boat_rent':
            return <span className="badge badge-primary">
                <i className="fa fa-ship"/>&nbsp;{translator('inquiry_type_boat_rent')}
            </span>
        case 'accommodation_rent':
            return <span className="badge badge-success">
                <i className="fa fa-building"/>&nbsp;{translator('inquiry_type_accommodation_rent')}
            </span>
        case 'transfer':
            return <span className="badge badge-info">
                <i className="fa fa-truck"/>&nbsp;{translator('inquiry_type_transfer')}
            </span>
        default:
            return status
    }
}