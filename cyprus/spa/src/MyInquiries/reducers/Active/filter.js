import {combineReducers} from 'redux'
import * as Action from '../../actions'
import translator from '../../../translations/translator'
import qs from "query-string";

const query = qs.parse(window.location.search)

const initial = {
    types: query['filter[types]'] ? query['filter[types]'].split(',').map(value => ({
        label: translator('inquiry_type_' + value),
        value
    })) : [],
}

const types = (prev = initial.types, action) => {
    switch (action.type) {
        case Action.FILTER_ACTIVE_CHANGED:
            if (action.payload.types !== undefined) {
                return action.payload.types
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    types,
})

