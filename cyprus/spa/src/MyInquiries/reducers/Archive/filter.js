import {combineReducers} from 'redux'
import * as Action from '../../actions'

const types = (prev = [], action) => {
    switch (action.type) {
        case Action.FILTER_ARCHIVE_CHANGED:
            if (action.payload.types !== undefined) {
                return action.payload.types
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    types,
})

