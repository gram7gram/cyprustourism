import {combineReducers} from 'redux'

import Active from './Active'
import Archive from './Archive'

export default combineReducers({
    Active,
    Archive,
})

