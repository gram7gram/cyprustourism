import {all, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_ACTIVE_REQUEST} from '../actions'
import FetchActiveItems, {parseBeforeSubmit} from '../actions/FetchActiveItems'
import {updateCurrentUrlWithFilter} from "../../Common/utils";

function* runActive({payload}) {
    const {filter, pagination} = yield select(store => store.MyInquiries.Active)

    const page = payload && payload.page ? payload.page : pagination.page

    yield put(FetchActiveItems(filter, page))

    updateCurrentUrlWithFilter(page, parseBeforeSubmit(filter))
}

export default function* sagas() {
    yield all([
        takeEvery(FETCH_ACTIVE_REQUEST, runActive),
    ])
}
