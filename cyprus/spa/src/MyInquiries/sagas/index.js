import {all, fork} from 'redux-saga/effects'
import FetchActive from './FetchActive'
import FetchArchive from './FetchArchive'

export default function* sagas() {
    yield all([
        fork(FetchActive),
        fork(FetchArchive),
    ])
}
