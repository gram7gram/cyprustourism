import {all, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_ARCHIVE_REQUEST} from '../actions'
import FetchArchiveItems from '../actions/FetchArchiveItems'

function* run({payload}) {
    const {filter, pagination} = yield select(store => store.MyInquiries.Archive)

    const page = payload && payload.page ? payload.page : pagination.page

    yield put(FetchArchiveItems(filter, page))
}

export default function* sagas() {
    yield all([
        takeEvery(FETCH_ARCHIVE_REQUEST, run),
    ])
}
