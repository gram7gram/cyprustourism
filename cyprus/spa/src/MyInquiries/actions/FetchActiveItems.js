import request from '../../Common/request'
import {FETCH_ACTIVE_BEFORE, FETCH_ACTIVE_FAILURE, FETCH_ACTIVE_SUCCESS} from '../actions'

export const parseBeforeSubmit = filter => {
    const data = {...filter}

    data.statuses = 'pending,open'

    if (data.types.length > 0) {
        data.types = data.types.map(item => item.value).join(',')
    } else {
        delete data.types
    }

    return data
}

export default (filter, page) => dispatch => {

    const data = parseBeforeSubmit(filter)

    dispatch({
        type: FETCH_ACTIVE_BEFORE
    })

    const query = [
        'page=' + page
    ]

    Object.keys(data).forEach(key => {
        const value = data[key]
        query.push('filter[' + key + ']=' + value)
    })

    request.get(AppRouter.GET.inquiries + '?' + query.join('&'))
        .then(({data}) => {
            dispatch({
                type: FETCH_ACTIVE_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_ACTIVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
