import React from 'react';
import {connect} from 'react-redux';
import {FETCH_ARCHIVE_REQUEST} from '../../actions';
import selectors from './selectors';
import translator from '../../../translations/translator';
import Filter from "./Filter";
import Paginator from "../../../Common/components/Paginator";
import Spinner from "../../../Common/components/Spinner";
import {renderStatus, renderType} from "../../utils";
import {dateFormat} from "../../../Common/utils";
import QuoteLimit from "../../../InquiryUser/components/QuoteLimit";

class Archive extends React.Component {

    submit = (page) => {
        this.props.dispatch({
            type: FETCH_ARCHIVE_REQUEST,
            payload: {
                page
            }
        })
    }

    componentWillMount() {
        const {pagination} = this.props.MyInquiries

        this.submit(pagination.page)
    }

    renderContent() {

        const {items, isLoading} = this.props.MyInquiries

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_inquiries_title')}</h3>
                    <h4>{translator('no_inquiries_archived_footer')}</h4>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">#</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1 text-right">{translator('quote_count')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1 text-center">{translator('selected_quote_count')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('expires_at')}</th>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => {
                    return <tr key={key}>
                        <td className="align-middle text-nowrap">
                            <a href={AppRouter.GET.myInquiryByGuid.replace('_GUID_', item.guid)}
                               className="btn btn-xs btn-primary">
                                <i className="fa fa-eye"/>
                            </a>
                        </td>
                        <td className="align-middle text-nowrap">{renderStatus(item.status)}</td>
                        <td className="align-middle text-nowrap">{renderType(item.type)}</td>
                        <td className="align-middle text-nowrap text-right">
                            {item.quoteCount}
                        </td>
                        <td className="align-middle text-nowrap">
                            <QuoteLimit count={item.selectedQuoteCount}/>
                        </td>
                        <td className="align-middle text-nowrap text-secondary">{dateFormat(item.updatedAt)}</td>
                        <td className="align-middle text-nowrap text-secondary">{dateFormat(item.createdAt)}</td>
                        <td className="align-middle text-nowrap">{dateFormat(item.expiresAt)}</td>
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    }

    render() {

        const {pagination} = this.props.MyInquiries

        return <div className="card shadow-sm mb-4">

            <div className="card-header">
                <h4 className="m-0">{translator('inquiries_archive')}</h4>
            </div>

            <div className="card-body">

                <Filter/>

                <div className="row">
                    <div className="col-12">
                        {this.renderContent()}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <Paginator
                            onChange={this.submit}
                            page={pagination.page}
                            limit={pagination.limit}
                            total={pagination.total}
                        />
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Archive)
