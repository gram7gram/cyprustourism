import React from 'react';

import Active from './Active';
import Archive from './Archive';

class MyInquiries extends React.Component {

    render() {

        return <div className="row">
            <div className="col-12">
                <Active/>
            </div>
            <div className="col-12">
                <Archive/>
            </div>
        </div>
    }
}

export default MyInquiries
