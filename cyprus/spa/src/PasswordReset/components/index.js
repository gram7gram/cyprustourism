import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import Save from '../actions/Save';
import translator from '../../translations/translator';

class PasswordReset extends React.Component {

    submit = () => {
        const {model} = this.props.PasswordReset

        this.props.dispatch(Save(model))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.PasswordReset.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {model, isLoading, isValid, isSaveSuccess, serverErrors} = this.props.PasswordReset

        return <div className="row no-gutters space-2">
            <div className="col-md-8 col-lg-7 col-xl-6 offset-md-2 offset-lg-2 offset-xl-3 space-3 space-lg-0">

                <div className="mb-7">

                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_reset_password')}</h2>
                    <p>{translator('reset_password_subtitle')}</p>
                </div>

                <p>{translator('reset_password_remember')}
                    &nbsp;<a href="/login">{translator('signin')}</a></p>

                {serverErrors.length > 0 && <div className="alert alert-danger text-center">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                {isSaveSuccess && <div className="alert alert-success text-center">
                    <div>{translator('password_reset_success_notice')}</div>
                </div>}

                <div className="row">
                    <div className="col-12">
                        <div className="form-group">
                            <label className="form-label">{translator('email_placeholder')}</label>
                            <input type="text"
                                   name="login"
                                   autoFocus={true}
                                   className="form-control"
                                   placeholder={translator('email_placeholder')}
                                   onChange={this.changeString('login')}
                                   value={model.login || ''}/>
                            {this.getError('login')}
                        </div>
                    </div>

                    <div className="col-12">
                        <div className="form-group text-right">
                            <button type="button"
                                    className="btn btn-primary transition-3d-hover"
                                    onClick={this.submit}
                                    disabled={isLoading || !isValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-lock"}/>
                                &nbsp;{translator('reset_password_confirm')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(PasswordReset)
