import {all, put, takeEvery} from 'redux-saga/effects'
import {EMAIL_RESEND_SUCCESS, ENABLE_EMAIL_VERIFY} from '../actions'

function* emailVerification() {
    yield put({
        type: ENABLE_EMAIL_VERIFY
    })
}

export default function* sagas() {
    yield all([
        takeEvery([
            EMAIL_RESEND_SUCCESS,
        ], emailVerification)
    ])
}
