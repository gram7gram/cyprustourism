import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import Email from './Email'
import Sms from './Sms'
import Notification from './Notification'

export default function* sagas() {
    yield all([
        fork(Validation),
        fork(Email),
        fork(Sms),
        fork(Notification),
    ])
}
