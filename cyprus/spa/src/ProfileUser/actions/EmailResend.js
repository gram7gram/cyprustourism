import request from '../../Common/request'
import {EMAIL_RESEND_BEFORE, EMAIL_RESEND_FAILURE, EMAIL_RESEND_SUCCESS} from '../actions'

export default () => dispatch => {

    dispatch({
        type: EMAIL_RESEND_BEFORE
    })

    request.post(AppRouter.POST.emailResend)
        .then(({data}) => {
            dispatch({
                type: EMAIL_RESEND_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: EMAIL_RESEND_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
