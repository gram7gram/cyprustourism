import request from '../../Common/request'
import {SMS_VERIFY_BEFORE, SMS_VERIFY_FAILURE, SMS_VERIFY_SUCCESS} from '../actions'

export default (id, code) => dispatch => {

    dispatch({
        type: SMS_VERIFY_BEFORE
    })

    request.post(AppRouter.POST.smsVerification.replace('_ID_', id), {code})
        .then(({data}) => {
            dispatch({
                type: SMS_VERIFY_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: SMS_VERIFY_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
