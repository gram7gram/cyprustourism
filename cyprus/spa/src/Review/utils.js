import React from 'react'
import translator from '../translations/translator';

export const renderStatus = status => {

    switch (status) {
        case 'created':
            return <span className="badge badge-warning">
                <i className="fa fa-clock"/>&nbsp;{translator('review_status_created')}
            </span>
        case 'rejected':
            return <span className="badge badge-secondary">
                <i className="fa fa-ban"/>&nbsp;{translator('review_status_rejected')}
            </span>
        case 'approved':
            return <span className="badge badge-success">
                <i className="fa fa-times"/>&nbsp;{translator('review_status_approved')}
            </span>
        default:
            return status
    }
}