import {combineReducers} from 'redux'
import * as Action from '../actions'

const rating = (prev = 0, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rating !== undefined) {
                return action.payload.rating
            }
            return prev
        default:
            return prev
    }
}

const isPublishAllowed = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isPublishAllowed !== undefined) {
                return action.payload.isPublishAllowed
            }
            return prev
        default:
            return prev
    }
}

const provider = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.provider !== undefined) {
                return action.payload.provider
            }
            return prev
        default:
            return prev
    }
}

const text = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.text !== undefined) {
                return action.payload.text
            }
            return prev
        default:
            return prev
    }
}


export default combineReducers({
    isPublishAllowed,
    provider,
    rating,
    text,
})