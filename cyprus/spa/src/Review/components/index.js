import React from 'react';
import {connect} from 'react-redux';
import Save from '../actions/Save';
import selectors from './selectors';
import translator from '../../translations/translator';
import {MODEL_CHANGED} from "../actions";

class Review extends React.Component {

    componentWillMount() {
        const id = AppParameters.currentRoute.params.id

        this.change('provider', id)
    }

    submit = () => {
        const {model} = this.props.Review

        this.props.dispatch(Save(model.provider, model))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    setRating = value => () => this.change('rating', value)

    renderContent = () => {
        const {model, isLoading, isValid} = this.props.Review

        return <div className="row">
            <div className="col-12">
                <div className="row no-gutters">
                    <div className="col-auto pt-3">{translator('review_bad')}</div>
                    <div className="col">
                        <div className="p-3 text-center">
                            <i className={"fa-star fa-2x " + (model.rating >= 1 ? 'fas' : 'far')}
                               onClick={this.setRating(1)}/>
                            <i className={"fa-star fa-2x " + (model.rating >= 2 ? 'fas' : 'far')}
                               onClick={this.setRating(2)}/>
                            <i className={"fa-star fa-2x " + (model.rating >= 3 ? 'fas' : 'far')}
                               onClick={this.setRating(3)}/>
                            <i className={"fa-star fa-2x " + (model.rating >= 4 ? 'fas' : 'far')}
                               onClick={this.setRating(4)}/>
                            <i className={"fa-star fa-2x " + (model.rating === 5 ? 'fas' : 'far')}
                               onClick={this.setRating(5)}/>
                        </div>
                    </div>
                    <div className="col-auto pt-3">{translator('review_good')}</div>
                </div>

            </div>
            <div className="col-12">
                <div className="form-group">
                    <label className="form-label">{translator('comment')}</label>
                    <textarea
                        name="text"
                        className="form-control"
                        value={model.text || ''}
                        onChange={this.changeString('text')}/>
                </div>
            </div>

            <div className="col-12">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox"
                           id="review-isPublishAllowed"
                           className="custom-control-input"
                           onChange={this.changeBool('isPublishAllowed')}
                           checked={model.isPublishAllowed}/>

                    <label htmlFor="review-isPublishAllowed" className="custom-control-label">
                        {translator('review_is_publish_allowed')}
                    </label>
                </div>
            </div>

            <div className="col-12 mt-3 text-center">
                <button className="btn btn-primary transition-3d-hover"
                        disabled={!isValid || isLoading}
                        onClick={this.submit}>
                    <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                    &nbsp;{translator('save')}
                </button>
            </div>
        </div>
    }

    render() {

        const {serverErrors, isSaveSuccess} = this.props.Review

        return <div className="row no-gutters space-2">
            <div className="col-md-8 col-lg-7 col-xl-6 offset-md-2 offset-lg-2 offset-xl-3 space-3 space-lg-0">

                <div className="mb-4">
                    <h2 className="h3 text-primary font-weight-normal mb-0">{translator('navigation_review_new')}</h2>
                    <p>{translator('review_subtitle')}</p>
                </div>

                {isSaveSuccess && <div className="alert alert-success text-center">
                    <div>{translator('review_create_success')}</div>

                    <a href={AppParameters.currentRoute.referer}
                       className="btn btn-outline-primary">
                        {translator('back')}
                    </a>
                </div>}

                {serverErrors.length > 0 && <div className="alert alert-danger">
                    <ul className="simple">{errors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                {!isSaveSuccess ? this.renderContent() : null}
            </div>
        </div>
    }
}

export default connect(selectors)(Review)
