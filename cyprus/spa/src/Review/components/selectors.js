import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    Review: store => store.Review,
})
