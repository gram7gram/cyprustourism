import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import Notification from './Notification'
import Step from './Step'
import UserValidation from './UserValidation'
import Inquiry from './Inquiry'

export default function* sagas() {
    yield all([
        fork(Inquiry),
        fork(UserValidation),
        fork(Step),
        fork(Notification),
        fork(Validation),
    ])
}
