import {VALIDATE_USER_FAILURE, VALIDATE_USER_SUCCESS} from '../../actions'

import Step1 from "../../../RegisterUser/actions/Validate/Step1";
import Step2 from "../../../RegisterUser/actions/Validate/Step2";
import Step3 from "../../../RegisterUser/actions/Validate/Step3";

export default (model, changes) => dispatch => {

    const validator1 = Step1(model, changes)
    const validator2 = Step2(model, changes)
    const validator3 = Step3(model, changes)

    let validator = {
        count: validator1.count + validator2.count + validator3.count,
        errors: {
            ...validator1.errors,
            ...validator2.errors,
            ...validator3.errors,
        }
    }

    if (validator.count === 0) {
        dispatch({
            type: VALIDATE_USER_SUCCESS
        })
    } else {
        dispatch({
            type: VALIDATE_USER_FAILURE,
            payload: validator
        })
    }

    return validator
}