import {combineReducers} from 'redux'
import translator from '../../translations/translator'
import * as CategoryAction from '../../AdminCategories/actions'

const categories = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const locationOptions = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
                .filter(item => item.group === 'location')
        default:
            return prev
    }
}

const extraOptions = (prev = [], action) => {
    switch (action.type) {
        case CategoryAction.FETCH_SUCCESS:
            return action.payload.items
                .filter(item => item.group === 'extras')
        default:
            return prev
    }
}

const initialSubTypes = [
    {value: 'hotel', label: translator('inquiry_subtype_hotel')},
    {value: 'bnb', label: translator('inquiry_subtype_bnb')},
    {value: 'apartment', label: translator('inquiry_subtype_apartment')},
    {value: 'house', label: translator('inquiry_subtype_house')},
    {value: 'villa', label: translator('inquiry_subtype_villa')},
]

const subtypeOptions = (prev = initialSubTypes) => prev

export default combineReducers({
    categories,
    subtypeOptions,
    locationOptions,
    extraOptions,
})

