import {combineReducers} from 'redux'
import * as Action from '../actions'
import moment from 'moment'
import {objectValues} from "../../Common/utils";
import keyBy from "lodash/keyBy";

const start = moment().add(1, 'days')
const end = moment().add(4, 'days')
const diff = end.diff(start, 'days')

const dayCount = (prev = diff, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.dayCount !== undefined) {
                return action.payload.dayCount
            }
            return prev
        default:
            return prev
    }
}

const roomCount = (prev = 1, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.roomCount !== undefined) {
                return action.payload.roomCount
            }
            return prev
        default:
            return prev
    }
}

const bedroomCount = (prev = 1, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.bedroomCount !== undefined) {
                return action.payload.bedroomCount
            }
            return prev
        default:
            return prev
    }
}

const adultsCount = (prev = 2, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.adultsCount !== undefined) {
                return action.payload.adultsCount
            }
            return prev
        default:
            return prev
    }
}

const childrenCount = (prev = null, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.childrenCount !== undefined) {
                return action.payload.childrenCount
            }
            return prev
        default:
            return prev
    }
}

const paymentType = (prev = 'cash', action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.paymentType !== undefined) {
                return action.payload.paymentType
            }
            return prev
        default:
            return prev
    }
}

const isFixedDate = (prev = true, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.isFixedDate !== undefined) {
                return action.payload.isFixedDate
            }
            return prev
        default:
            return prev
    }
}

const rentalStart = (prev = start.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalStart !== undefined) {
                return action.payload.rentalStart
            }
            return prev
        default:
            return prev
    }
}

const rentalEnd = (prev = end.format('YYYY-MM-DD'), action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
            if (action.payload.rentalEnd !== undefined) {
                return action.payload.rentalEnd
            }
            return prev
        default:
            return prev
    }
}

const subtypes = (prev = [], action) => {
    let state
    switch (action.type) {
        case Action.ADD_SUBTYPE:

            state = [...prev]

            state.push(action.payload)

            return objectValues(keyBy(state, 'id'))
        case Action.REMOVE_SUBTYPE:

            state = keyBy(prev, 'id')

            delete state[action.payload.id]

            return objectValues(state)
        default:
            return prev
    }
}

export default combineReducers({
    dayCount,
    roomCount,
    bedroomCount,
    adultsCount,
    childrenCount,
    paymentType,
    isFixedDate,
    rentalStart,
    rentalEnd,
    subtypes,
})
