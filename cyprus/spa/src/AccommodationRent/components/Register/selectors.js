import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AccommodationRent: store => store.AccommodationRent,
})
