import React from 'react';
import {connect} from 'react-redux';
import {USER_MODEL_CHANGED} from '../../actions';
import selectors from './selectors';
import translator from '../../../translations/translator';
import EmailResend from "../../actions/EmailResend";
import LoginCheck from "../../actions/LoginCheck";

class Step1 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: USER_MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    cancelLogin = () => {
        this.change('isAlreadyRegistered', false)
    }

    login = () => {
        const {creator} = this.props.AccommodationRent.model

        this.props.dispatch(LoginCheck(creator.email, creator.password))
    }

    submit = () => {
        const {creator} = this.props.AccommodationRent.model

        this.props.dispatch(EmailResend(creator.email))
    }

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.AccommodationRent.RegisterUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {creator} = this.props.AccommodationRent.model
        const {isStepValid, isLoading, isAlreadyRegistered} = this.props.AccommodationRent.RegisterUser

        if (isAlreadyRegistered) {
            return <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label className="m-0">{translator('already_registered_message')}</label>
                        <input type="email"
                               autoFocus={true}
                               className="form-control"
                               name="email"
                               placeholder={translator('enter_email_placeholder')}
                               onChange={this.changeString('email')}
                               value={creator.email || ''}/>
                        {this.getError('email')}
                    </div>

                    <div className="form-group">
                        <label className="m-0 required">{translator('password')}</label>
                        <input type="password"
                               className="form-control"
                               name="password"
                               onChange={this.changeString('password')}
                               value={creator.password || ''}/>
                        {this.getError('password')}
                    </div>

                    <div className="form-group text-center">
                        <button type="button"
                                className="btn btn-primary transition-3d-hover mr-1"
                                onClick={this.login}
                                disabled={isLoading || !isStepValid || !creator.password}>
                            <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                            &nbsp;{translator('login')}
                        </button>
                        <button type="button"
                                className="btn btn-outline-primary transition-3d-hover"
                                onClick={this.cancelLogin}>
                            <i className="fa fa-ban"/>
                            &nbsp;{translator('cancel')}
                        </button>
                    </div>
                </div>
            </div>
        }

        return <div className="row">
            <div className="col-12">
                <div className="form-group text-left">
                    <div className="input-group">
                        <input type="email"
                               autoFocus={true}
                               className="form-control"
                               name="email"
                               placeholder={translator('enter_email_placeholder')}
                               onChange={this.changeString('email')}
                               value={creator.email || ''}/>
                        <div className="input-group-append">
                            <button type="button"
                                    className="btn btn-primary transition-3d-hover"
                                    onClick={this.submit}
                                    disabled={isLoading || !isStepValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                &nbsp;{translator('verify_phone')}
                            </button>
                        </div>
                    </div>
                    {this.getError('email')}
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Step1)
