import React from 'react';
import {connect} from 'react-redux';
import {USER_MODEL_CHANGED} from '../../actions';
import selectors from './selectors';
import translator from '../../../translations/translator';
import EmailResend from "../../actions/EmailResend";
import EmailVerify from "../../actions/EmailVerify";

class Step2 extends React.Component {

    change = (key, value = null) => this.props.dispatch({
        type: USER_MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    resend = () => {
        const {creator} = this.props.AccommodationRent.model

        this.props.dispatch(EmailResend(creator.email))
    }

    verify = () => {
        const {creator} = this.props.AccommodationRent.model

        this.props.dispatch(EmailVerify(creator.id, creator.emailCode))
    }

    changeString = name => e => this.change(name, e.target.value)

    getError = key => {
        const {errors} = this.props.AccommodationRent.RegisterUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    render() {

        const {creator} = this.props.AccommodationRent.model
        const {isStepValid, isLoading} = this.props.AccommodationRent.RegisterUser

        return <div className="row">
            <div className="col-12">

                <div className="form-group text-left">
                    <label className="m-0 required">{translator('email_code')}</label>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button type="button"
                                    className="btn btn-outline-secondary transition-3d-hover"
                                    onClick={this.resend}
                                    disabled={isLoading}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-redo"}/>
                                &nbsp;{translator('resend_sms')}
                            </button>
                        </div>

                        <input type="number"
                               name="emailCode"
                               autoFocus={true}
                               className="form-control"
                               onChange={this.changeString('emailCode')}
                               value={creator.emailCode || ''}/>

                        <div className="input-group-append">
                            <button type="button"
                                    className="btn btn-primary transition-3d-hover"
                                    onClick={this.verify}
                                    disabled={isLoading || !isStepValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                &nbsp;{translator('verify')}
                            </button>
                        </div>
                    </div>
                    {this.getError('emailCode')}
                </div>
            </div>

        </div>

    }
}

export default connect(selectors)(Step2)
