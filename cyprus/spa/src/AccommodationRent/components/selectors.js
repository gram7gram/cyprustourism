import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AccommodationRent: store => store.AccommodationRent,
    locale: store => store.UI.locale,
})
