import {combineReducers} from 'redux'
import * as Action from '../actions'

const tab = (prev = 'car_rent', action) => {
    switch (action.type) {
        case Action.SET_TAB:
            return action.payload
        default:
            return prev
    }
}

export default combineReducers({
    tab
})

