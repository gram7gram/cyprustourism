import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import * as Actions from '../actions';

const btnClasses = "btn btn-sm btn-block text-nowrap text-truncate mb-1 transition-3d-hover mx-auto px-1 "
const btnStyle = {width: '99%'}

class Landing extends React.Component {

    setTab = payload => () => {
        this.props.dispatch({
            type: Actions.SET_TAB,
            payload
        })
    }

    renderTab() {

        const {tab} = this.props.Landing

        switch (tab) {
            case 'car_rent':
                return <div className="banner">
                    <h3>{translator('landing_car_rent_title')}</h3>

                    <a href={AppRouter.GET.carRentIndex} className="btn btn-outline-primary">
                        {translator('continue')}
                        &nbsp;<i className="fa fa-arrow-right"/>
                    </a>
                </div>
            case 'boat_rent':
                return <div className="banner">
                    <h3>{translator('landing_boat_rent_title')}</h3>

                    <a href={AppRouter.GET.boatRentIndex} className="btn btn-outline-primary">
                        {translator('continue')}
                        &nbsp;<i className="fa fa-arrow-right"/>
                    </a>
                </div>
            case 'accommodation_rent':
                return <div className="banner">
                    <h3>{translator('landing_accommodation_rent_title')}</h3>

                    <a href={AppRouter.GET.accommodationRentIndex} className="btn btn-outline-primary">
                        {translator('continue')}
                        &nbsp;<i className="fa fa-arrow-right"/>
                    </a>
                </div>
            case 'transfer':
                return <div className="banner">
                    <h3>{translator('landing_transfer_title')}</h3>

                    <a href={AppRouter.GET.transferIndex} className="btn btn-outline-primary">
                        {translator('continue')}
                        &nbsp;<i className="fa fa-arrow-right"/>
                    </a>
                </div>
        }
    }

    render() {

        const {tab} = this.props.Landing

        return <div className="row no-gutters">
            <div className="col-12">

                <div className="row no-gutters">
                    <div className="col-12 col-sm-6 text-center">
                        <button type="button" onClick={this.setTab('car_rent')}
                                title={translator('landing_car_rent')}
                                style={btnStyle}
                                className={btnClasses + (tab === 'car_rent' ? 'btn-warning' : 'btn-primary')}>
                            <i className="fa fa-car"/>&nbsp;{translator('landing_car_rent')}
                        </button>
                    </div>
                    <div className="col-12 col-sm-6 text-center">

                        <button type="button" onClick={this.setTab('boat_rent')}
                                title={translator('landing_boat_rent')}
                                style={btnStyle}
                                className={btnClasses + (tab === 'boat_rent' ? 'btn-warning' : 'btn-primary')}>
                            <i className="fa fa-ship"/>&nbsp;{translator('landing_boat_rent')}
                        </button>
                    </div>

                    <div className="col-12 col-sm-6 text-center">
                        <button type="button" onClick={this.setTab('accommodation_rent')}
                                title={translator('landing_accommodation_rent')}
                                style={btnStyle}
                                className={btnClasses + (tab === 'accommodation_rent' ? 'btn-warning' : 'btn-primary')}>
                            <i className="fa fa-building"/>&nbsp;{translator('landing_accommodation_rent')}
                        </button>
                    </div>
                    <div className="col-12 col-sm-6 text-center">
                        <button type="button" onClick={this.setTab('transfer')}
                                title={translator('landing_transfer')}
                                style={btnStyle}
                                className={btnClasses + (tab === 'transfer' ? 'btn-warning' : 'btn-primary')}>
                            <i className="fa fa-truck"/>&nbsp;{translator('landing_transfer')}
                        </button>
                    </div>
                </div>
            </div>
            <div className="col-12">
                <div className="p-2 bg-light">
                    {this.renderTab()}
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(Landing)
