import React from 'react';
import {connect} from 'react-redux';
import Save from '../actions/Save';
import FetchItems from '../actions/FetchItems';
import selectors from './selectors';
import translator from '../../translations/translator';
import {MODEL_CHANGED} from "../actions";
import Spinner from "../../Common/components/Spinner";
import {renderType} from "../../MyInquiriesProvider/utils";
import {dateTimeFormat} from "../../Common/utils";

class AdminInquiryCost extends React.Component {

    componentWillMount() {
        this.fetch()
    }

    fetch = () => {
        this.props.dispatch(FetchItems())
    }

    submit = id => () => {
        const {items} = this.props.AdminInquiryCost

        const item = items.find(item => item.id === id)

        this.props.dispatch(Save(item))
    }

    changeInt = (id, key) => e => {
        let value = parseInt(e.target.value)
        if (isNaN(value) || value <= 0) value = 0;

        this.props.dispatch({
            type: MODEL_CHANGED,
            payload: {
                id,
                [key]: value
            }
        })
    }

    renderContent() {

        const {items, isLoading, isItemLoading} = this.props.AdminInquiryCost

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_items_title')}</h3>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <colgroup>
                    <col/>
                    <col width="25%"/>
                    <col width="15%"/>
                </colgroup>
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('credits')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => {

                    const isLoading = isItemLoading[item.id] === true

                    return <tr key={key}>
                        <td className="align-middle text-nowrap">{renderType(item.type)}</td>
                        <td className="align-middle text-nowrap">
                            <div className="input-group input-group-sm">

                                <input type="number"
                                       className="form-control"
                                       value={item.credits}
                                       onChange={this.changeInt(item.id, 'credits')}
                                       min={0} step={1}/>

                                <div className="input-group-append">
                                    <button className="btn btn-primary"
                                            onClick={this.submit(item.id)}
                                            disabled={isLoading || item.credits <= 0}>
                                        {isLoading
                                            ? <i className="fa fa-spin fa-circle-notch"/>
                                            : <i className="fa fa-check"/>}
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td className="align-middle text-nowrap text-secondary">{dateTimeFormat(item.updatedAt)}</td>
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    }

    render() {

        return <div className="card shadow-sm">

            <div className="card-header">
                <h4 className="m-0">{translator('navigation_inquiry_costs')}</h4>
            </div>

            <div className="card-body">
                <div className="row">
                    <div className="col-12">
                        {this.renderContent()}
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminInquiryCost)
