import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminInquiryCost: store => store.AdminInquiryCost,
})
