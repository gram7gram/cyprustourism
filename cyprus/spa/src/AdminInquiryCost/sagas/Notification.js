import {all, takeEvery} from 'redux-saga/effects'
import {SAVE_SUCCESS} from '../actions'
import notification from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* alertSuccess() {
    notification(translator('save_success'))
}

export default function* sagas() {
    yield all([
        takeEvery(SAVE_SUCCESS, alertSuccess)
    ])
}
