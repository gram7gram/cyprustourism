import {combineReducers} from 'redux'
import * as Action from '../actions'

const items = (prev = [], action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.MODEL_CHANGED:
            return prev.map(item => {
                if (item.id === action.payload.id) {
                    return {
                        ...item,
                        ...action.payload
                    }
                }

                return item
            })
        case Action.FETCH_SUCCESS:
            return action.payload.items
        default:
            return prev
    }
}

const serverErrors = (prev = [], action) => {
    switch (action.type) {
        case Action.SAVE_FAILURE:
            if (action.payload.data.message !== undefined) {
                return [
                    action.payload.data.message
                ]
            }
            return []
        case Action.SAVE_SUCCESS:
        case Action.SAVE_BEFORE:
            return []
        default:
            return prev
    }
}

const isSaveSuccess = (prev = false, action) => {
    switch (action.type) {
        case Action.MODEL_CHANGED:
        case Action.SAVE_BEFORE:
        case Action.SAVE_FAILURE:
            return false
        case Action.SAVE_SUCCESS:
            return true
        default:
            return prev
    }
}

const isValid = (prev = false, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return true
        case Action.VALIDATE_FAILURE:
        case Action.SAVE_FAILURE:
            return false
        default:
            return prev
    }
}

const isLoading = (prev = false, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.SAVE_FAILURE:
            return false
        case Action.SAVE_BEFORE:
            return true
        default:
            return prev
    }
}

const isItemLoading = (prev = {}, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.SAVE_FAILURE:
            return {
                ...prev,
                [action.payload.id]: false
            }
        case Action.SAVE_BEFORE:
            return {
                ...prev,
                [action.payload.id]: true
            }
        default:
            return prev
    }
}

const initialValidator = {
    count: 0,
    messages: [],
    errors: {}
}
const validator = (prev = initialValidator, action) => {
    switch (action.type) {
        case Action.VALIDATE_SUCCESS:
            return initialValidator
        case Action.VALIDATE_FAILURE:
            return action.payload
        default:
            return prev
    }
}

export default combineReducers({
    isSaveSuccess,
    isValid,
    isLoading,
    validator,
    serverErrors,
    items,
    isItemLoading,
})

