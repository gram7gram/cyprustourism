import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

export default (data) => dispatch => {

    dispatch({
        type: SAVE_BEFORE,
        payload: {id: data.id}

    })

    request.put(AppRouter.PUT.inquiryCost.replace('_ID_', data.id), data)
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    id: data.id,
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
