import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import {renderStatus, renderType} from "../../MyInquiries/utils";
import {dateFormat, dateTimeFormat} from "../../Common/utils";
import InquiryParametersDate from "../../Common/components/InquiryParametersDate";

class AccommodationRent extends React.Component {

    render() {

        const {model} = this.props.InquiryUser

        if (!model) return null

        return <div className="row">
            <div className="col-12">
                <div className="table-responsive">
                    <table className="table table-sm table-borderless table-hover table-striped">
                        <colgroup>
                            <col width="20%"/>
                            <col width="80%"/>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                            <td>{dateTimeFormat(model.createdAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                            <td>{dateTimeFormat(model.updatedAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('expires_at')}</th>
                            <td>{dateFormat(model.expiresAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                            <td>{renderStatus(model.status)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                            <td>{renderType(model.type)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_subtype')}</th>
                            <td>{model.parameters.subtypes.map((item, i) =>
                                <span key={i}
                                      className="badge badge-outline-dark mr-1 mb-1">{translator('inquiry_subtype_' + item)}</span>
                            )}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                            <td>
                                <InquiryParametersDate parameters={model.parameters}/>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_adults_count')}</th>
                            <td>
                                <div>{model.parameters.adultsCount}</div>
                            </td>
                        </tr>
                        {model.parameters.childrenCount
                            ? <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_children_count')}</th>
                            <td>
                                <div>{model.parameters.childrenCount}</div>
                            </td>
                            </tr> : null}

                        {model.parameters.roomCount
                            ? <tr>
                                <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_room_count')}</th>
                                <td>
                                    <div>{model.parameters.roomCount}</div>
                                </td>
                            </tr> : null}

                        {model.parameters.bedroomCount
                            ? <tr>
                                <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_bedroom_count')}</th>
                                <td>
                                    <div>{model.parameters.bedroomCount}</div>
                                </td>
                            </tr> : null}

                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('payment_type')}</th>
                            <td>
                                <span className="badge badge-outline-dark mr-1 mb-1">
                                    {translator(model.parameters.paymentType
                                        ? 'payment_type_' + model.parameters.paymentType
                                        : 'payment_type_any')}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_locations')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'location')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('accommodation_rent_extras')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'extras')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AccommodationRent)
