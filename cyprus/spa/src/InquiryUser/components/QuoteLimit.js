import React from 'react'

const inquiryReserveBadgeStyle = {
    minWidth: '50px',
    maxWidth: '50px',
}

const QuoteLimit = ({count}) => <div className="text-nowrap text-right">
    <div
        className={"mr-1 w-30 badge " + (count >= 1 ? 'badge-warning' : 'badge-light')}
        style={inquiryReserveBadgeStyle}>&nbsp;</div>
    <div
        className={"mr-1 w-30 badge " + (count >= 2 ? 'badge-warning' : 'badge-light')}
        style={inquiryReserveBadgeStyle}>&nbsp;</div>
    <div
        className={"mr-1 w-30 badge " + (count >= 3 ? 'badge-warning' : 'badge-light')}
        style={inquiryReserveBadgeStyle}>&nbsp;</div>
</div>

export default QuoteLimit