import React from 'react';
import {connect} from 'react-redux';
import selectors from './selectors';
import translator from '../../translations/translator';
import {renderStatus, renderType} from "../../MyInquiries/utils";
import {dateFormat, dateTimeFormat} from "../../Common/utils";
import InquiryParametersDate from "../../Common/components/InquiryParametersDate";

class BoatRent extends React.Component {

    render() {

        const {model} = this.props.InquiryUser

        if (!model) return null

        return <div className="row">
            <div className="col-12">
                <div className="table-responsive">
                    <table className="table table-sm table-borderless table-hover table-striped">
                        <colgroup>
                            <col width="20%"/>
                            <col width="80%"/>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('created_at')}</th>
                            <td>{dateTimeFormat(model.createdAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('updated_at')}</th>
                            <td>{dateTimeFormat(model.updatedAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('expires_at')}</th>
                            <td>{dateFormat(model.expiresAt)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('status')}</th>
                            <td>{renderStatus(model.status)}</td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('type')}</th>
                            <td>{renderType(model.type)}</td>
                        </tr>

                        {model.parameters.isHourly
                            ? <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_date')}</th>
                                <td className="align-middle text-nowrap">
                                    {dateFormat(model.parameters.rentalStart)}
                                </td>
                            </tr> : null}

                        {!model.parameters.isHourly
                            ? <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                                <td className="align-middle text-nowrap">
                                    <InquiryParametersDate parameters={model.parameters}/>
                                </td>
                            </tr> : null}

                        {model.parameters.isHourly
                            ? <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('duration')}</th>
                                <td>
                                    {model.parameters.hourlyRanges.map((item, i) =>
                                        <span key={i}
                                              className="badge badge-outline-dark mr-1 mb-1">{translator('boat_rent_hourly_options_' + item)}</span>
                                    )}
                                </td>
                            </tr> : null}

                        {model.parameters.isHourly
                            ? <tr>
                                <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('time')}</th>
                                <td>
                                    {model.parameters.daytimeRanges.map((item, i) =>
                                        <span key={i}
                                              className="badge badge-outline-dark mr-1 mb-1">{translator('boat_rent_daytime_options_' + item)}</span>
                                    )}
                                </td>
                            </tr> : null}

                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_driver_age')}</th>
                            <td>
                                <div>{model.parameters.driverAgeFrom}</div>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('payment_type')}</th>
                            <td>
                                <span className="badge badge-outline-dark mr-1 mb-1">
                                    {translator(model.parameters.paymentType
                                        ? 'payment_type_' + model.parameters.paymentType
                                        : 'payment_type_any')}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_locations')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'location')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_boat_types')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'boat_type')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        <tr>
                            <th className="text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_extras')}</th>
                            <td>
                                {model.categories
                                    .filter(item => item.group === 'extras')
                                    .map((item, i) =>
                                        <span key={i} className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                    )}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(BoatRent)
