import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import {dateTimeFormat, numberFormat} from "../../../Common/utils";
import Media from "../../../Common/components/Media";
import ReviewComment from "../../../Common/components/ReviewComment";
import ProviderContact from "../../../Common/components/ProviderContact";
import FetchContact from "../../actions/FetchContact";
import Rating from "../../../Common/components/Rating";

class SelectedQuoteLayout extends React.Component {

    fetchContact = () => {

        const {id} = this.props.selectedQuote.contact

        this.props.dispatch(FetchContact(id))
    }

    renderReview() {

        const {selectedQuote} = this.props

        if (!selectedQuote.review) return null;

        return <div className="card-footer" id={"review-" + selectedQuote.review.id}>
            <div className="row">
                <div className="col-12 col-md-10 col-lg-8 mx-auto">
                    <h5>{translator('your_review')}</h5>

                    <ReviewComment review={selectedQuote.review}/>
                </div>
            </div>
        </div>
    }

    render() {

        const {sharedContacts, isLoading} = this.props.InquiryUser.Contact
        const {selectedQuote, isProviderDisplayed} = this.props
        const inquiry = this.props.InquiryUser.model

        const isCurrentContactLoading = selectedQuote.contact && isLoading[selectedQuote.contact.id] === true

        const currentContact = selectedQuote.contact && sharedContacts[selectedQuote.contact.id] !== undefined
            ? sharedContacts[selectedQuote.contact.id]
            : null

        const canFetchContact = selectedQuote.contact && selectedQuote.contact.status === 'created'

        const quote = selectedQuote.quote

        return <div className="mb-5">

            {isProviderDisplayed && <div className="row no-gutters">
                <div className="col-auto text-center">
                    <i className="fa fa-user-circle fa-2x p-2"/>
                </div>
                <div className="col">
                    <div className="px-1">
                        <Rating rating={quote.provider.rating}/>
                        &nbsp;{numberFormat(quote.provider.rating)}
                    </div>
                    <div className="px-1">
                        <small className="text-secondary font-italic">{translator('provider_identity_hidden')}</small>

                        <small className="fg-success ml-2">
                            <i className={"fa fa-check"}/>&nbsp;{translator('is_verified')}
                        </small>
                    </div>
                </div>
            </div>}
            <div className="row no-gutters">
                <div className="col-11 offset-1">

                    <div className="card selected-quote mt-4">

                        <i className="fa fa-2x fa-check selected-mark"/>

                        <div className="card-header">
                            <div className="row">
                                <div className="col-12 col-md-6 text-center text-md-left">
                                    <a className="h4 m-0" href={"#selected-quote-" + selectedQuote.id}>
                                        {translator('quote')} #{quote.id}
                                    </a>
                                </div>
                                <div className="col-12 col-md-6 text-center text-md-right">
                                    {quote.price
                                        ? <h4 className="m-0" id="selected-quote-">
                                            {numberFormat(quote.price.price)}&nbsp;{quote.price.currency}
                                        </h4>
                                        : null}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-6 text-center text-md-left">
                                    <div>
                                        <small className="text-muted">{dateTimeFormat(selectedQuote.createdAt)}</small>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="text-center text-md-right">

                                        {canFetchContact
                                            ? <button type="button"
                                                      onClick={this.fetchContact}
                                                      className="btn btn-primary btn-sm transition-3d-hover mr-1">
                                                {isCurrentContactLoading ?
                                                    <i className="fa fa-spin fa-circle-notch"/> : null}
                                                &nbsp;{translator('show_contact')}
                                            </button>
                                            : <span className="px-2 text-muted">
                                                <i className="fa fa-lock"/>&nbsp;{translator('contact_is_pending_notice')}
                                            </span>}

                                        {!selectedQuote.review && !inquiry.review
                                            ? <a href={AppRouter.GET.reviewCreate.replace('_ID_', selectedQuote.id)}
                                                 className="btn btn-warning btn-sm mr-1 transition-3d-hover">
                                                <i className="fa fa-star"/>&nbsp;{translator('create_review')}
                                            </a> : null}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">

                            <div className="row">

                                <div className={currentContact ? "col-12 col-lg-8" : "col-12"}>

                                    {quote.text ? <blockquote
                                        className="h5 p-2 border-left font-italic font-weight-light bg-light review">
                                        <i className="fa fa-comment"/>&nbsp;{quote.text}
                                    </blockquote> : null}

                                    <div className="row my-2">
                                        <div className="col-12">
                                            {this.props.children}
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <div className="row">
                                            {quote.media.map((media, i) =>
                                                <Media media={media} key={i}/>
                                            )}
                                        </div>
                                    </div>
                                </div>

                                {currentContact ? <div className="col-12 col-lg-4">
                                    <ProviderContact contact={currentContact}/>
                                </div> : null}
                            </div>
                        </div>
                        {this.renderReview()}
                    </div>
                </div>
            </div>
        </div>
    }
}

SelectedQuoteLayout.propTypes = {
    isProviderDisplayed: PropTypes.bool.isRequired,
    selectedQuote: PropTypes.object.isRequired,
}

export default connect(selectors)(SelectedQuoteLayout)
