import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    InquiryUser: store => store.InquiryUser,
})
