import React from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import Save from '../actions/Save';
import FetchItem from '../actions/FetchItem';
import selectors from './selectors';
import translator from '../../translations/translator';
import Spinner from "../../Common/components/Spinner";
import {scrollTo, setTitle} from "../../Common/utils";

import BoatRent from "./BoatRent";
import AccommodationRent from "./AccommodationRent";
import Transfer from "./Transfer";
import CarRent from "./CarRent";

import CarRentQuoteShow from "./quoteShow/CarRent";
import BoatRentQuoteShow from "./quoteShow/BoatRent";
import AccommodationRentQuoteShow from "./quoteShow/AccommodationRent";
import TransferQuoteShow from "./quoteShow/Transfer";

import CarRentSelectedQuoteShow from "./selectedQuoteShow/CarRent";
import BoatRentSelectedQuoteShow from "./selectedQuoteShow/BoatRent";
import AccommodationRentSelectedQuoteShow from "./selectedQuoteShow/AccommodationRent";
import TransferSelectedQuoteShow from "./selectedQuoteShow/Transfer";
import QuoteLimit from "./QuoteLimit";

class InquiryUser extends React.Component {

    componentWillMount() {
        setTitle(translator('loading'))

        const guid = AppParameters.currentRoute.params.guid

        this.props.dispatch(FetchItem(guid))
    }

    scrollToReview = () => {
        const {model} = this.props.InquiryUser

        scrollTo("review-" + model.review.id)
    }

    submitSubscribed = receiveEmailOnNewQuote => () => {
        const {model} = this.props.InquiryUser

        this.props.dispatch(Save({
            guid: model.guid,
            receiveEmailOnNewQuote
        }))
    }

    submitStatus = status => () => {
        const {model} = this.props.InquiryUser

        this.props.dispatch(Save({
            guid: model.guid,
            status
        }))
    }

    submitExtend = () => {
        const {model} = this.props.InquiryUser

        if (!confirm(translator('inquiry_extend_notice'))) return

        const expiresAt = moment(model.expiresAt, 'YYYY-MM-DD HH:mm:ss')
            .add(1, 'month')
            .format('YYYY-MM-DD HH:mm:ss')

        this.props.dispatch(Save({
            guid: model.guid,
            expiresAt
        }))
    }

    renderActions() {

        const {isLoading, model} = this.props.InquiryUser

        if (!model) return null

        return <div>

            {model.status !== 'closed'
                ? <div className="form-group">
                    <button className="btn btn-outline-danger btn-block transition-3d-hover"
                            type="button"
                            onClick={this.submitStatus('closed')}
                            disabled={isLoading}>
                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-ban"}/>
                        &nbsp;{translator('close')}
                    </button>
                </div>
                : null}

            {model.status === 'closed' && model.selectedQuotes.length < 3
                ? <div className="form-group">
                    <button className="btn btn-outline-success btn-block transition-3d-hover"
                            type="button"
                            onClick={this.submitStatus('open')}
                            disabled={isLoading}>
                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                        &nbsp;{translator('reopen')}
                    </button>
                </div>
                : null}

            {model.status !== 'closed' && model.extendCount === 0 ? <div className="form-group">
                <button className="btn btn-outline-primary btn-block transition-3d-hover"
                        type="button"
                        onClick={this.submitExtend}
                        disabled={isLoading}>
                    <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-angle-double-right"}/>
                    &nbsp;{translator('inquiry_extend')}
                </button>
            </div> : null}

            <div className="form-group">
                <button className="btn btn-outline-info btn-block transition-3d-hover"
                        type="button"
                        onClick={this.submitSubscribed(!model.receiveEmailOnNewQuote)}
                        disabled={isLoading}>
                    <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-rss"}/>
                    &nbsp;{translator(model.receiveEmailOnNewQuote ? 'unsubscribe' : 'subscribe')}
                </button>
            </div>

            {model.review ? <div className="form-group">
                <button type="button"
                        className="btn btn-outline-warning btn-block transition-3d-hover"
                        onClick={this.scrollToReview}>
                    <i className="fa fa-star"/>
                    &nbsp;{translator('my_review')}
                </button>
            </div> : null}
        </div>
    }

    renderSelectedQuotes() {

        const {model} = this.props.InquiryUser

        if (!model) return null

        if (!model.selectedQuotes || model.selectedQuotes.length === 0) {
            return <div className="banner mb-10">
                <h3>{translator('no_selected_quotes_title')}</h3>
                <h4>{translator('no_selected_quotes_footer')}</h4>
            </div>
        }

        let currentProvider = null
        return model.selectedQuotes.map((selectedQuote, i) => {

            const isProviderDisplayed = selectedQuote.quote.provider.id !== currentProvider

            currentProvider = selectedQuote.quote.provider.id

            switch (model.type) {
                case 'car_rent':
                    return <CarRentSelectedQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        selectedQuote={selectedQuote}/>
                case 'boat_rent':
                    return <BoatRentSelectedQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        selectedQuote={selectedQuote}/>
                case 'accommodation_rent':
                    return <AccommodationRentSelectedQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        selectedQuote={selectedQuote}/>
                case 'transfer':
                    return <TransferSelectedQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        selectedQuote={selectedQuote}/>
            }
        })
    }

    renderQuotes() {

        const {model} = this.props.InquiryUser

        if (!model) return null

        if (!model.quotes || model.quotes.length === 0) {
            return <div className="card shadow-sm mb-4">
                <div className="card-header">
                    <h4 className="m-0">{translator('quotes')}</h4>
                </div>
                <div className="card-body">
                    <div className="banner">
                        <h3>{translator('no_quotes_title')}</h3>
                        <h4>{translator('no_quotes_footer')}</h4>
                    </div>
                </div>
            </div>
        }

        let currentProvider = null
        return model.quotes.map((quote, i) => {

            const isProviderDisplayed = quote.provider.id !== currentProvider

            currentProvider = quote.provider.id

            switch (model.type) {
                case 'car_rent':
                    return <CarRentQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        quote={quote}/>
                case 'boat_rent':
                    return <BoatRentQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        quote={quote}/>
                case 'accommodation_rent':
                    return <AccommodationRentQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        quote={quote}/>
                case 'transfer':
                    return <TransferQuoteShow
                        key={i}
                        isProviderDisplayed={isProviderDisplayed}
                        quote={quote}/>
            }
        })
    }

    renderContent() {

        const {model, isLoaded} = this.props.InquiryUser

        if (!model || !isLoaded) {
            return <div className="banner">
                <Spinner/>
            </div>
        }

        switch (model.type) {
            case 'car_rent':
                return <CarRent/>
            case 'boat_rent':
                return <BoatRent/>
            case 'accommodation_rent':
                return <AccommodationRent/>
            case 'transfer':
                return <Transfer/>
        }

        return null
    }

    render() {

        const {model, serverErrors} = this.props.InquiryUser

        if (model && model.id) {
            setTitle('#' + model.id + ' ' + translator('inquiry_type_' + model.type) + ' - ' + AppParameters.currentRoute.host)
        }

        const selectedQuotesCount = model && model.selectedQuotes ? model.selectedQuotes.length : 0

        return <div className="row">
            <div className="col-12">

                <div className="card shadow-sm mb-4">
                    <div className="card-body">

                        {serverErrors.length > 0 && <div className="alert alert-danger text-center my-3">
                            <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                        </div>}

                        <div className="row no-gutters">
                            <div className="col-12 col-lg-9 mb-2">
                                {this.renderContent()}
                            </div>
                            <div className="col-12 col-lg-3 mb-2">
                                {this.renderActions()}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card shadow-sm mb-4">
                    <div className="card-header">
                        <div className="row">
                            <div className="col">
                                <h4 className="m-0">{translator('selected_quotes')}</h4>
                            </div>
                            <div className="col-auto">
                                <div className="mt-1">
                                    <QuoteLimit count={selectedQuotesCount}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        {this.renderSelectedQuotes()}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        {this.renderQuotes()}
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(InquiryUser)
