import React from 'react';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import InquiryParametersDate from "../../../Common/components/InquiryParametersDate";
import QuoteLayout from "./Layout";
import PropTypes from "prop-types";
import {dateFormat} from "../../../Common/utils";

class BoatRent extends React.Component {

    render() {

        const {quote, isProviderDisplayed} = this.props

        return <QuoteLayout quote={quote} isProviderDisplayed={isProviderDisplayed}>
            <div className="table-responsive">
                <table className="table table-sm table-borderless table-hover table-striped">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%"/>
                    </colgroup>
                    <tbody>

                    {quote.parameters.isHourly
                        ? <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_date')}</th>
                            <td className="align-middle text-nowrap">
                                {dateFormat(quote.parameters.rentalStart)}
                            </td>
                        </tr> : null}

                    {!quote.parameters.isHourly
                        ? <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('rental_range')}</th>
                        <td className="align-middle text-nowrap">
                            <InquiryParametersDate parameters={quote.parameters}/>
                        </td>
                        </tr> : null}

                    {quote.parameters.isHourly
                        ? <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('duration')}</th>
                            <td>
                                {quote.parameters.hourlyRanges.map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{translator('boat_rent_hourly_options_' + item)}</span>
                                )}
                            </td>
                        </tr> : null}

                    {quote.parameters.isHourly
                        ? <tr>
                            <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('time')}</th>
                            <td>
                                {quote.parameters.daytimeRanges.map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{translator('boat_rent_daytime_options_' + item)}</span>
                                )}
                            </td>
                        </tr> : null}

                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('payment_type')}</th>
                        <td className="align-middle text-nowrap">
                            <div className="badge badge-outline-dark mr-1 mb-1">
                                {translator(quote.parameters.paymentType
                                    ? 'payment_type_' + quote.parameters.paymentType
                                    : 'payment_type_any')}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_driver_age')}</th>
                        <td className="align-middle text-nowrap">{quote.parameters.driverAgeFrom}</td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_locations')}</th>
                        <td>
                            {quote.categories
                                .filter(item => item.group === 'location')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_boat_types')}</th>
                        <td>
                            {quote.categories
                                .filter(item => item.group === 'boat_type')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    <tr>
                        <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">{translator('boat_rent_extras')}</th>
                        <td>
                            {quote.categories
                                .filter(item => item.group === 'extras')
                                .map((item, i) =>
                                    <span key={i}
                                          className="badge badge-outline-dark mr-1 mb-1">{item.name}</span>
                                )}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </QuoteLayout>
    }
}


BoatRent.propTypes = {
    quote: PropTypes.object.isRequired,
    isProviderDisplayed: PropTypes.bool.isRequired,
}

export default connect(selectors)(BoatRent)
