import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import selectors from '../selectors';
import translator from '../../../translations/translator';
import {dateTimeFormat, numberFormat} from "../../../Common/utils";
import Media from "../../../Common/components/Media";
import SelectQuote from "../../actions/SelectQuote";
import Rating from "../../../Common/components/Rating";

class QuoteLayout extends React.Component {

    makeSelected = () => {

        if (!confirm(translator('select_quote_confirm'))) return

        const {quote} = this.props
        const {model} = this.props.InquiryUser

        this.props.dispatch(SelectQuote(model.guid, quote.id))
    }

    render() {

        const {quote, isProviderDisplayed} = this.props
        const {model, isLoading} = this.props.InquiryUser

        const isLimitReached = model && model.selectedQuotes.length === 3

        return <div className="mb-4">

            {isProviderDisplayed && <div className="row no-gutters">
                <div className="col-auto text-center">
                    <i className="fa fa-user-circle fa-2x p-2"/>
                </div>
                <div className="col">
                    <div className="px-1">
                        <Rating rating={quote.provider.rating}/>
                        &nbsp;{numberFormat(quote.provider.rating)}
                    </div>
                    <div className="px-1">
                        <small className="text-secondary font-italic">{translator('provider_identity_hidden')}</small>

                        <small className="fg-success ml-2">
                            <i className={"fa fa-check"}/>&nbsp;{translator('is_verified')}
                        </small>
                    </div>
                </div>
            </div>}

            <div className="row no-gutters">
                <div className="col-11 offset-1">

                    <div className="card mt-2">
                        <div className="card-header">
                            <div className="row">
                                <div className="col-12 col-md-6 text-center text-md-left">
                                    <a className="h4 m-0" href={"#quote-" + quote.id}>
                                        {translator('quote')} #{quote.id}
                                    </a>
                                </div>
                                <div className="col-12 col-md-6 text-center text-md-right">
                                    {quote.price
                                        ? <h4 className="m-0" id="selected-quote-">
                                            {numberFormat(quote.price.price)}&nbsp;{quote.price.currency}
                                        </h4>
                                        : null}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-6 text-center text-md-left">
                                    <div>
                                        <small className="text-muted">{dateTimeFormat(quote.createdAt)}</small>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6 text-center text-md-right">

                                    {model.status === 'open' &&
                                    <button className="btn btn-success btn-sm transition-3d-hover"
                                            type="button"
                                            disabled={isLoading || isLimitReached}
                                            onClick={this.makeSelected}>
                                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-info-circle"}/>
                                        &nbsp;{translator('quote_select')}
                                    </button>}

                                </div>
                            </div>
                        </div>
                        <div className="card-body">

                            <div className="row">

                                <div className="col-12">

                                    {quote.text ? <blockquote
                                        className="h5 p-2 border-left font-italic font-weight-light bg-light review">
                                        <i className="fa fa-comment"/>&nbsp;{quote.text}
                                    </blockquote> : null}

                                    <div className="row my-2">
                                        <div className="col-12">
                                            {this.props.children}
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <div className="row">
                                            {quote.media.map((media, i) =>
                                                <Media media={media} key={i}/>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

QuoteLayout.propTypes = {
    quote: PropTypes.object.isRequired,
    isProviderDisplayed: PropTypes.bool.isRequired,
}

export default connect(selectors)(QuoteLayout)
