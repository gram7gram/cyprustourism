import {all, throttle} from 'redux-saga/effects'
import * as Actions from '../actions'
import {scrollTo} from "../../Common/utils";
import {SELECT_QUOTE_SUCCESS} from "../actions";

function* scrollToQuote({payload}) {
    scrollTo('selected-quote-' + payload.id)
}

export default function* sagas() {
    yield all([
        throttle(100, Actions.SELECT_QUOTE_SUCCESS, scrollToQuote),
    ])
}
