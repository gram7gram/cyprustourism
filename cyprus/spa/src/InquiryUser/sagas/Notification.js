import {all, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import toast from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* notifySelectedSuccess() {
    toast(translator('quote_select_success'))
}
function* notifySelectedFailure({payload}) {
    toast(translator('quote_select_failure') + ": " + payload.data.message, true)
}

export default function* sagas() {
    yield all([
        takeEvery(Actions.SELECT_QUOTE_SUCCESS, notifySelectedSuccess),

        takeEvery(Actions.SELECT_QUOTE_FAILURE, notifySelectedFailure),
    ])
}
