import {all, fork} from 'redux-saga/effects'
import Fetch from './Fetch'
import Notification from './Notification'
import Scroll from './Scroll'

export default function* sagas() {
    yield all([
        fork(Fetch),
        fork(Notification),
        fork(Scroll),
    ])
}
