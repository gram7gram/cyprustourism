import {all, put, select, takeEvery} from 'redux-saga/effects'

import * as Actions from '../actions'
import FetchItem from '../actions/FetchItem'


function* fetch() {
    const model = yield select(store => store.InquiryUser.model)

    yield put(FetchItem(model.guid))
}

export default function* sagas() {
    yield all([
        takeEvery([
            Actions.SELECT_QUOTE_SUCCESS
        ], fetch),
    ])
}
