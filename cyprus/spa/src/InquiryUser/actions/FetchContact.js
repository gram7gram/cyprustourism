import request from '../../Common/request'
import {FETCH_CONTACT_BEFORE, FETCH_CONTACT_FAILURE, FETCH_CONTACT_SUCCESS} from '../actions'

export default (id) => dispatch => {

    dispatch({
        type: FETCH_CONTACT_BEFORE,
        payload: {id}
    })

    request.get(AppRouter.GET.contactById.replace('_ID_', id))
        .then(({data}) => {
            dispatch({
                type: FETCH_CONTACT_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: FETCH_CONTACT_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
