import request from '../../Common/request'
import {SELECT_QUOTE_BEFORE, SELECT_QUOTE_FAILURE, SELECT_QUOTE_SUCCESS} from '../actions'

export default (inquiryGuid, quoteId) => dispatch => {

    dispatch({
        type: SELECT_QUOTE_BEFORE
    })

    request.post(AppRouter.POST.selectQuoteByGuid
        .replace('_INQUIRY_GUID_', inquiryGuid)
        .replace('_QUOTE_ID_', quoteId)
    )
        .then(({data}) => {
            dispatch({
                type: SELECT_QUOTE_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            console.log(e);
            if (!e.response) return

            dispatch({
                type: SELECT_QUOTE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
