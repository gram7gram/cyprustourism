const prefix = 'InquiryUser'

export const FETCH_BEFORE = prefix + '/FETCH_BEFORE';
export const FETCH_SUCCESS = prefix + '/FETCH_SUCCESS';
export const FETCH_FAILURE = prefix + '/FETCH_FAILURE';

export const FETCH_CONTACT_BEFORE = prefix + '/FETCH_CONTACT_BEFORE';
export const FETCH_CONTACT_SUCCESS = prefix + '/FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_FAILURE = prefix + '/FETCH_CONTACT_FAILURE';

export const SAVE_BEFORE = prefix + '/SAVE_BEFORE';
export const SAVE_SUCCESS = prefix + '/SAVE_SUCCESS';
export const SAVE_FAILURE = prefix + '/SAVE_FAILURE';

export const SELECT_QUOTE_BEFORE = prefix + '/SELECT_QUOTE_BEFORE';
export const SELECT_QUOTE_SUCCESS = prefix + '/SELECT_QUOTE_SUCCESS';
export const SELECT_QUOTE_FAILURE = prefix + '/SELECT_QUOTE_FAILURE';

export const FILTER_CHANGED = prefix + '/FILTER_CHANGED';

export const FETCH_DIRECTION_BEFORE = prefix + '/FETCH_DIRECTION_BEFORE';
export const FETCH_DIRECTION_SUCCESS = prefix + '/FETCH_DIRECTION_SUCCESS';
export const FETCH_DIRECTION_FAILURE = prefix + '/FETCH_DIRECTION_FAILURE';

export const FETCH_QUOTE_DIRECTION_BEFORE = prefix + '/FETCH_QUOTE_DIRECTION_BEFORE';
export const FETCH_QUOTE_DIRECTION_SUCCESS = prefix + '/FETCH_QUOTE_DIRECTION_SUCCESS';
export const FETCH_QUOTE_DIRECTION_FAILURE = prefix + '/FETCH_QUOTE_DIRECTION_FAILURE';