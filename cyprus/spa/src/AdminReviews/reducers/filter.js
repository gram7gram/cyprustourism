import {combineReducers} from 'redux'
import * as Action from '../actions'

const statuses = (prev = [], action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.statuses !== undefined) {
                return action.payload.statuses
            }
            return prev
        default:
            return prev
    }
}

const search = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.search !== undefined) {
                return action.payload.search
            }
            return prev
        default:
            return prev
    }
}

const rating = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.rating !== undefined) {
                return action.payload.rating
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    rating,
    search,
    statuses,
})

