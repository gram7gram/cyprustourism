import {all, fork} from 'redux-saga/effects'
import Fetch from './Fetch'

export default function* sagas() {
    yield all([
        fork(Fetch),
    ])
}
