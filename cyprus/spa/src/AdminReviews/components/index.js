import React from 'react';
import {connect} from 'react-redux';
import {FETCH_REQUEST} from '../actions';
import Save from '../actions/Save';
import selectors from './selectors';
import translator from '../../translations/translator';
import Filter from "./Filter";
import Paginator from "../../Common/components/Paginator";
import Spinner from "../../Common/components/Spinner";
import {renderStatus, renderUsername} from "../utils";
import Rating from "../../Common/components/Rating";
import {dateTimeFormat, numberFormat} from "../../Common/utils";
import ProviderReview from "../../Common/components/ProviderReview";

class AdminReviews extends React.Component {

    componentWillMount() {
        const {pagination} = this.props.AdminReviews

        this.submit(pagination.page)
    }

    submit = (page) => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page
            }
        })
    }

    submitStatus = (id, status) => () => {
        this.props.dispatch(Save(id, {
            status
        }))
    }

    submitPublic = (id, isPublished) => () => {
        this.props.dispatch(Save(id, {
            isPublished
        }))
    }

    renderContent() {

        const {items, isLoading} = this.props.AdminReviews

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_reviews_title')}</h3>
                    <h4>{translator('no_reviews_footer')}</h4>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="row">
            {items.map((item, key) => {
                return <div key={key} className="col-12 col-md-6">
                    <div className="card mb-2">
                        <div className="card-header">
                            <div className="row">
                                <div className="col-auto">
                                    <Rating rating={item.rating}/>&nbsp;{numberFormat(item.rating)}
                                </div>
                                <div className="col text-right">
                                    {renderStatus(item.status)}
                                </div>
                                <div className="col-12">
                                    <small className="text-secondary">{dateTimeFormat(item.createdAt)}</small>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">

                            {renderUsername(item.user)}

                            <ProviderReview provider={item.provider} text={item.text}/>

                        </div>
                        <div className="card-footer">
                            <div className="row">
                                <div className="col-auto">
                                    {item.status !== 'approved'
                                        ? <button className="btn btn-sm btn-outline-success mr-1 text-nowrap transition-3d-hover"
                                                  onClick={this.submitStatus(item.id, 'approved')}>
                                            <i className="fa fa-thumbs-up"/>&nbsp;{translator('approve')}
                                        </button>
                                        : null}

                                    {item.status !== 'rejected'
                                        ? <button className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                                  onClick={this.submitStatus(item.id, 'rejected')}>
                                            <i className="fa fa-thumbs-down"/>&nbsp;{translator('reject')}
                                        </button>
                                        : null}
                                </div>
                                <div className="col text-right">
                                    {!item.isPublished
                                        ? <button className="btn btn-sm btn-outline-primary text-nowrap transition-3d-hover"
                                                  disabled={item.status !== 'approved'}
                                                  onClick={this.submitPublic(item.id, true)}>
                                            <i className="fa fa-eye"/>&nbsp;{translator('publish')}
                                        </button>
                                        : <button className="btn btn-sm btn-outline-primary text-nowrap transition-3d-hover"
                                                  onClick={this.submitPublic(item.id, false)}>
                                            <i className="fa fa-eye-slash"/>&nbsp;{translator('unpublish')}
                                        </button>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            })}
        </div>
    }

    render() {

        const {pagination} = this.props.AdminReviews

        return <div className="card shadow-sm">

            <div className="card-header">
                <Filter/>
            </div>

            <div className="card-body">

                <div className="row">
                    <div className="col-12">
                        {this.renderContent()}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <Paginator
                            onChange={this.submit}
                            page={pagination.page}
                            limit={pagination.limit}
                            total={pagination.total}
                        />
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminReviews)
