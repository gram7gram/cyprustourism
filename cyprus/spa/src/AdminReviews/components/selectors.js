import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminReviews: store => store.AdminReviews,
})
