import React from 'react'
import translator from '../translations/translator';

export const renderUsername = user => {
    let name = user.name

    if (user.surname) {
        name = user.surname + ' ' + name
    }

    return name
}

export const renderStatus = status => {

    switch (status) {
        case 'created':
            return <span className="badge badge-warning">
                <i className="fa fa-info-circle"/>&nbsp;{translator('review_status_created')}
            </span>
        case 'approved':
            return <span className="badge badge-success">
                <i className="fa fa-check"/>&nbsp;{translator('review_status_approved')}
            </span>
        case 'rejected':
            return <span className="badge badge-danger">
                <i className="fa fa-ban"/>&nbsp;{translator('review_status_rejected')}
            </span>
        default:
            return status
    }
}