import {combineReducers} from 'redux'
import * as Action from '../actions'

const statuses = (prev = [], action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.statuses !== undefined) {
                return action.payload.statuses
            }
            return prev
        default:
            return prev
    }
}

const types = (prev = [], action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.types !== undefined) {
                return action.payload.types
            }
            return prev
        default:
            return prev
    }
}

const search = (prev = null, action) => {
    switch (action.type) {
        case Action.FILTER_CHANGED:
            if (action.payload.search !== undefined) {
                return action.payload.search
            }
            return prev
        default:
            return prev
    }
}

export default combineReducers({
    search,
    statuses,
    types,
})

