import React from 'react';
import {connect} from 'react-redux';
import {FETCH_REQUEST} from '../actions';
import Save from '../actions/Save';
import selectors from './selectors';
import translator from '../../translations/translator';
import Filter from "./Filter";
import Paginator from "../../Common/components/Paginator";
import Spinner from "../../Common/components/Spinner";
import {renderStatus, renderType} from "../utils";
import {dateFormat} from "../../Common/utils";

class AdminInvoices extends React.Component {

    componentWillMount() {
        const {pagination} = this.props.AdminInvoices

        this.submit(pagination.page)
    }

    submit = (page) => {
        this.props.dispatch({
            type: FETCH_REQUEST,
            payload: {
                page
            }
        })
    }

    submitStatus = (id, status) => () => {
        this.props.dispatch(Save(id, {
            status
        }))
    }

    renderContent() {

        const {items, isLoading, isSaving} = this.props.AdminInvoices

        if (!isLoading) {
            if (items.length === 0) {
                return <div className="banner">
                    <h3>{translator('no_invoices_title')}</h3>
                    <h4>{translator('no_invoices_footer')}</h4>
                </div>
            }
        } else {
            if (items.length === 0) {
                return <div className="banner">
                    <Spinner/>
                </div>
            }
        }

        return <div className="table-responsive mb-4">
            <table className="table table-sm table-borderless table-hover table-striped">
                <thead>
                <tr>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">
                        {translator('status')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">
                        {translator('name')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">
                        {translator('billing_plan')}</th>
                    <th className="align-middle text-nowrap font-weight-medium text-uppercase font-size-1">
                        {translator('type')}</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {items.map((item, key) => {

                    return <tr key={key}>
                        <td className="align-middle">
                            {item.status === 'unpaid'
                                ?
                                <button className="btn btn-sm btn-outline-success mr-1 text-nowrap transition-3d-hover"
                                        onClick={this.submitStatus(item.id, 'paid')}
                                        disabled={isSaving}>
                                    <i className="fa fa-thumbs-up"/>&nbsp;{translator('confirm')}
                                </button>
                                : null}

                            {item.status === 'unpaid'
                                ? <button className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                          onClick={this.submitStatus(item.id, 'canceled')}
                                          disabled={isSaving}>
                                    <i className="fa fa-thumbs-down"/>&nbsp;{translator('reject')}
                                </button>
                                : null}

                            {item.status !== 'unpaid'
                                ? renderStatus(item.status)
                                : null}
                        </td>
                        <td className="align-middle">
                            <div>{item.number}</div>
                            <small className="text-secondary">{dateFormat(item.createdAt)}</small>
                        </td>
                        <td className="align-middle">{item.billingPlan.name}</td>
                        <td className="align-middle">{renderType(item.type)}</td>
                        <td className="align-middle">
                            <a href={item.media.url}
                               download={true}
                               className="btn btn-xs btn-outline-primary transition-3d-hover">
                                <i className="fa fa-download"/>&nbsp;{translator('download')}
                            </a>
                        </td>
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    }

    render() {

        const {pagination} = this.props.AdminInvoices

        return <div className="card shadow-sm">

            <div className="card-header">
                <Filter/>
            </div>

            <div className="card-body">

                <div className="row">
                    <div className="col-12">
                        {this.renderContent()}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <Paginator
                            onChange={this.submit}
                            page={pagination.page}
                            limit={pagination.limit}
                            total={pagination.total}
                        />
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminInvoices)
