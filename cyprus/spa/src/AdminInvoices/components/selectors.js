import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminInvoices: store => store.AdminInvoices,
})
