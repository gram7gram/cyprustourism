import React from 'react'
import translator from '../translations/translator';

export const renderStatus = status => {

    switch (status) {
        case 'paid':
            return <span className="badge badge-success">
                <i className="fa fa-info-circle"/>&nbsp;{translator('invoice_status_paid')}
            </span>
        case 'unpaid':
            return <span className="badge badge-warning">
                <i className="fa fa-clock"/>&nbsp;{translator('invoice_status_unpaid')}
            </span>
        case 'canceled':
            return <span className="badge badge-danger">
                <i className="fa fa-ban"/>&nbsp;{translator('invoice_status_canceled')}
            </span>
        default:
            return status
    }
}
export const renderType = status => {

    switch (status) {
        case 'paypal':
            return <span className="badge badge-outline-dark">
                <i className="fab fa-paypal"/>&nbsp;{translator('invoice_type_paypal')}
            </span>
        case 'bank':
            return <span className="badge badge-outline-dark">
                <i className="fas fa-receipt"/>&nbsp;{translator('invoice_type_bank')}
            </span>
        default:
            return status
    }
}