import {all, fork} from 'redux-saga/effects'
import Fetch from './Fetch'
import Notification from './Notification'

export default function* sagas() {
    yield all([
        fork(Fetch),
        fork(Notification),
    ])
}
