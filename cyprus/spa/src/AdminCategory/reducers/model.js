import {combineReducers} from 'redux'
import * as Action from '../actions'
import translator from '../../translations/translator'

const id = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.id !== undefined) {
                return action.payload.id
            }
            return null
        default:
            return prev
    }
}

const createdAt = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.createdAt !== undefined) {
                return action.payload.createdAt
            }
            return null
        default:
            return prev
    }
}

const locale = (prev = 'en', action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.locale !== undefined) {
                return action.payload.locale
            }
            return null
        default:
            return prev
    }
}

const ordering = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.ordering !== undefined) {
                return action.payload.ordering
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.ordering !== undefined) {
                return action.payload.ordering
            }
            return prev
        default:
            return prev
    }
}

const name = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.name !== undefined) {
                return action.payload.name
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.name !== undefined) {
                return action.payload.name
            }
            return prev
        default:
            return prev
    }
}

const extra = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.extra !== undefined) {
                return action.payload.extra
            }
            return null
        case Action.MODEL_CHANGED:

            if (action.payload.lng !== undefined) {
                return {
                    ...prev,
                    lng: action.payload.lng
                }
            }

            if (action.payload.lat !== undefined) {
                return {
                    ...prev,
                    lat: action.payload.lat
                }
            }

            if (action.payload.group !== undefined) {

                switch (action.payload.group.value) {
                    case 'destination':
                        return {
                            lat: 34.9885984,
                            lng: 33.1934534
                        }
                    default:
                        return null
                }
            }

            return prev
        default:
            return prev
    }
}

const group = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.group !== undefined) {
                return {
                    value: action.payload.group,
                    label: translator('category_group_' + action.payload.group)
                }
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.group !== undefined) {
                return action.payload.group
            }
            return prev
        default:
            return prev
    }
}

const type = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.type !== undefined) {
                return {
                    value: action.payload.type,
                    label: translator('inquiry_type_' + action.payload.type)
                }
            }
            return null
        case Action.MODEL_CHANGED:
            if (action.payload.type !== undefined) {
                return action.payload.type
            }
            return prev
        default:
            return prev
    }
}

const icon = (prev = null, action) => {
    switch (action.type) {
        case Action.SAVE_SUCCESS:
        case Action.FETCH_SUCCESS:
            if (action.payload.icon !== undefined) {
                return action.payload.icon
            }
            return null
        case Action.UPLOAD_MEDIA_SUCCESS:
            return action.payload
        default:
            return prev
    }
}

export default combineReducers({
    id,
    createdAt,
    locale,
    ordering,
    name,
    group,
    type,
    icon,
    extra,
})