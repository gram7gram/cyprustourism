import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import FetchItem from '../actions/FetchItem';
import Save from '../actions/Save';
import Delete from '../actions/Delete';
import translator from '../../translations/translator';
import Select from '../../Common/components/Select';
import {dateTimeFormat, setTitle} from "../../Common/utils";
import UploadMedia from "../actions/UploadMedia";

const iconStyle = {width: '40px'}

const typeOptions = [
    {value: 'car_rent', label: translator('inquiry_type_car_rent')},
    {value: 'boat_rent', label: translator('inquiry_type_boat_rent')},
    {value: 'accommodation_rent', label: translator('inquiry_type_accommodation_rent')},
    {value: 'transfer', label: translator('inquiry_type_transfer')},
]

const groupOptions = [
    {value: 'location', label: translator('category_group_location')},
    {value: 'car_type', label: translator('category_group_car_type')},
    {value: 'boat_type', label: translator('category_group_boat_type')},
    {value: 'extras', label: translator('category_group_extras')},
    {value: 'destination', label: translator('category_group_destination')},
]

class AdminCategory extends React.Component {

    submit = () => {
        const {model} = this.props.AdminCategory

        this.props.dispatch(Save(model, data => {
            if (!model.id) {
                window.location = AppRouter.GET.categoryShow.replace('_ID_', data.id)
            }
        }))
    }

    remove = () => {

        if (!confirm(translator('confirm_remove'))) return

        const {model} = this.props.AdminCategory

        this.props.dispatch(Delete(model.id))
    }

    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })

    changeByKey = key => value => this.change(key, value)

    changeBool = name => e => this.change(name, e.target.checked)

    changeString = name => e => this.change(name, e.target.value)

    changeInt = name => e => {
        let value = parseInt(e.target.value)
        if (isNaN(value) || value < 0) value = 0;

        this.change(name, value)
    }

    changeFloat = name => e => {
        let value = parseFloat(e.target.value)
        if (isNaN(value)) value = 0;

        this.change(name, value)
    }

    addFile = e => {
        const files = e.target.files

        Array.from(files).forEach(file => {
            this.props.dispatch(UploadMedia(file))
        })

        e.target.value = null
    }

    getError = key => {
        const {errors} = this.props.AdminCategory.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    componentWillMount() {
        const id = AppParameters.currentRoute.params.id
        if (id) {

            setTitle(translator('loading'))

            this.props.dispatch(FetchItem(id))
        } else {
            setTitle(translator('navigation_new_category') + ' - ' + translator('navigation_categories'))
        }
    }

    renderExtra = () => {
        const {model} = this.props.AdminCategory

        if (!model.group || model.group.value !== 'destination') return null;

        return <div className="col-12">
            <div className="row">
                <div className="col">
                    <div className="form-group">
                        <label className="m-0 required">{translator('lng')}</label>
                        <input type="number"
                               name="lng"
                               className="form-control"
                               onChange={this.changeFloat('lng')}
                               value={model.extra.lng || ''}/>
                        {this.getError('lng')}
                    </div>
                </div>
                <div className="col">
                    <div className="form-group">
                        <label className="m-0 required">{translator('lat')}</label>
                        <input type="number"
                               name="lat"
                               className="form-control"
                               onChange={this.changeFloat('lat')}
                               value={model.extra.lat || ''}/>
                        {this.getError('lat')}
                    </div>
                </div>
            </div>
        </div>
    }


    render() {

        const {model, isValid, isLoading, serverErrors, isSaveSuccess} = this.props.AdminCategory

        if (model.id) {
            setTitle(model.name + ' - ' + translator('navigation_categories'))
        }

        return <div className="card shadow-sm">

            <div className="card-header">
                <div className="row">
                    <div className="col">
                        <h4 className="font-weight-semi-bold m-0">
                            {model.id
                                ? model.name
                                : translator('navigation_new_category')}
                        </h4>
                    </div>
                    <div className="col-auto">
                        {model.id
                            ? <button
                                className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                type="button"
                                onClick={this.remove}>
                                <i className="fa fa-times"/>&nbsp;{translator('remove')}
                            </button>
                            : null}
                    </div>
                    <div className="col-12">
                        {model.id ? <div className="text-muted">
                            {translator('created_at')}: {dateTimeFormat(model.createdAt)}
                        </div> : null}
                    </div>
                </div>


            </div>

            <div className="card-body">

                {isSaveSuccess && <div className="alert alert-success">
                    <div>{translator('save_success_alert')}</div>
                </div>}

                {serverErrors.length > 0 && <div className="alert alert-danger">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                <div className="row">
                    <div className="col-12 col-md-6 offset-md-3">

                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label className="m-0 required">{translator('type')}</label>
                                    <Select
                                        name="type"
                                        isClearable={false}
                                        onChange={this.changeByKey('type')}
                                        options={typeOptions}
                                        value={model.type}/>
                                    {this.getError('type')}
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <label className="m-0 required">{translator('group')}</label>
                                    <Select
                                        name="group"
                                        isClearable={false}
                                        onChange={this.changeByKey('group')}
                                        options={groupOptions}
                                        value={model.group}/>
                                    {this.getError('group')}
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <label className="m-0 required">{translator('name')}</label>
                                    <input type="text"
                                           name="name"
                                           className="form-control"
                                           onChange={this.changeString('name')}
                                           value={model.name || ''}/>
                                    {this.getError('name')}
                                </div>
                            </div>

                            {this.renderExtra()}

                            <div className="col-12">
                                <div className="form-group">
                                    <label className="m-0 required">{translator('ordering')}</label>
                                    <input type="number"
                                           name="ordering"
                                           min={0}
                                           step={1}
                                           className="form-control"
                                           onChange={this.changeInt('ordering')}
                                           value={model.ordering || ''}/>
                                    {this.getError('ordering')}
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="row no-gutters">
                                    {model.icon ? <div className="col-auto text-center">
                                        <img src={model.icon.url} className="img-thumbnail img-fluid mt-4 mr-1"
                                             style={iconStyle}/>
                                    </div> : null}
                                    <div className="col">
                                        <div className="form-group">
                                            <label className="m-0 required">{translator('icon')}</label>
                                            <input type="file"
                                                   name="icon"
                                                   min={1}
                                                   accept={AppParameters.uploadImageMimeTypes}
                                                   onChange={this.addFile}
                                                   className="form-control"/>
                                            {this.getError('icon')}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <label className="m-0 required">{translator('locale')}</label>
                                    <input type="text"
                                           name="locale"
                                           className="form-control"
                                           readOnly={true}
                                           value={model.locale || ''}/>
                                    {this.getError('locale')}
                                </div>
                            </div>

                            <div className="col-12">

                                <div className="form-group text-center">
                                    <button className="btn btn-primary transition-3d-hover"
                                            type="button"
                                            onClick={this.submit}
                                            disabled={isLoading || !isValid}>
                                        <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                        &nbsp;{translator('save')}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(AdminCategory)
