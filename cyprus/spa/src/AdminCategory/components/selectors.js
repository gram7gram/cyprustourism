import {createStructuredSelector} from 'reselect'

export default createStructuredSelector({
    AdminCategory: store => store.AdminCategory,
})
