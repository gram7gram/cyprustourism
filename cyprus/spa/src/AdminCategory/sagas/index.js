import {all, fork} from 'redux-saga/effects'
import Validation from './Validation'
import Delete from './Delete'

export default function* sagas() {
    yield all([
        fork(Delete),
        fork(Validation),
    ])
}
