import {all, takeEvery} from 'redux-saga/effects'
import {DELETE_SUCCESS} from '../actions'

function* redirect() {
    window.location = AppRouter.GET.categoriesIndex
}

export default function* sagas() {
    yield all([
        takeEvery([
            DELETE_SUCCESS,
        ], redirect)
    ])
}
