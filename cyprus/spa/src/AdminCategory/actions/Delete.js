import request from '../../Common/request'
import {DELETE_BEFORE, DELETE_FAILURE, DELETE_SUCCESS} from '../actions'

export default id => dispatch => {

    dispatch({
        type: DELETE_BEFORE
    })

    request.delete(AppRouter.DELETE.category.replace('_ID_', id))
        .then(({data}) => {
            dispatch({
                type: DELETE_SUCCESS,
                payload: data
            })
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: DELETE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
