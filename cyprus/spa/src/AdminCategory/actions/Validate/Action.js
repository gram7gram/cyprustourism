import translator from '../../../translations/translator'

export default (model, changes) => {
    const validator = {
        count: 0,
        messages: [],
        errors: {}
    }

    if (changes.ordering) {
        if (model.ordering <= 0) {
            ++validator.count
            validator.errors.ordering = translator('validation_positive_number')
        }
    }

    if (changes.name) {
        if (!model.name) {
            ++validator.count
            validator.errors.name = translator('validation_required')
        }
    }

    if (changes.type) {
        if (!model.type) {
            ++validator.count
            validator.errors.type = translator('validation_required')
        }
    }

    if (changes.group) {
        if (!model.group) {
            ++validator.count
            validator.errors.group = translator('validation_required')
        }
    }

    return validator
}