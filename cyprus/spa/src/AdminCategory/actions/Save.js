import request from '../../Common/request'
import {SAVE_BEFORE, SAVE_FAILURE, SAVE_SUCCESS} from '../actions'

const parseBeforeSubmit = model => {
    const data = {...model}

    if (data.type) {
        data.type = data.type.value
    }

    if (data.group) {
        data.group = data.group.value
    }

    if (data.icon) {
        data.icon = data.icon.id
    }

    if (data.extra) {
        data.extra = JSON.stringify(data.extra)
    }

    return data
}

export default (model, callback) => dispatch => {

    const data = parseBeforeSubmit(model)

    dispatch({
        type: SAVE_BEFORE
    })

    let promise

    if (data.id) {
        promise = request.put(AppRouter.PUT.category.replace('_ID_', data.id), data)
    } else {
        promise = request.post(AppRouter.POST.category, data)
    }

    promise
        .then(({data}) => {
            dispatch({
                type: SAVE_SUCCESS,
                payload: data
            })

            if (callback) {
                callback(data)
            }
        })
        .catch(e => {
            if (!e.response) return

            dispatch({
                type: SAVE_FAILURE,
                payload: {
                    status: e.response.status,
                    data: e.response.data
                }
            })
        })
}
