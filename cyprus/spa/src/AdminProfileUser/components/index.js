import React from 'react';
import {connect} from 'react-redux';
import {MODEL_CHANGED} from '../actions';
import selectors from './selectors';
import FetchItem from '../actions/FetchItem';
import Save from '../actions/Save';
import translator from '../../translations/translator';
import Select from '../../Common/components/Select';
import {options as countryOptions} from "../../api/countries";
import {dateTimeFormat, setTitle} from "../../Common/utils";
import PhoneInput from "../../Common/components/PhoneInput";

class ProfileUser extends React.Component {

    submitActive = isActive => () => {
        const {model} = this.props.ProfileUser

        this.props.dispatch(Save({
            id: model.id,
            isActive
        }))
    }
    submit = () => {
        const {model} = this.props.ProfileUser

        this.props.dispatch(Save(model))
    }
    change = (key, value = null) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            [key]: value
        }
    })
    changePhone = (phone, dialCode, isValid) => this.props.dispatch({
        type: MODEL_CHANGED,
        payload: {
            phone, dialCode, isPhoneValid: isValid
        }
    })
    changeByKey = key => value => this.change(key, value)
    changeBool = name => e => this.change(name, e.target.checked)
    changeString = name => e => this.change(name, e.target.value)
    getError = key => {
        const {errors} = this.props.ProfileUser.validator

        if (errors[key] === undefined) return null

        return <small className="feedback invalid-feedback d-block">{errors[key]}</small>
    }

    componentWillMount() {
        const id = AppParameters.currentRoute.params.id

        this.props.dispatch(FetchItem(id))
    }

    render() {

        const {model, isValid, isLoading, serverErrors} = this.props.ProfileUser

        if (model.id) {
            setTitle(model.surname + ' ' + model.name + " - " + translator('navigation_profile'))
        }

        return <div className="card shadow-sm">

            <div className="card-header">

                <div className="row">
                    <div className="col">
                        <div className="text-muted">
                            {translator('created_at')}: {dateTimeFormat(model.createdAt)}
                        </div>
                    </div>
                    <div className="col-12 col-lg-auto text-center">
                        {!model.isActive
                            ? <button className="btn btn-sm btn-outline-success mr-1 text-nowrap transition-3d-hover"
                                      type="button"
                                      onClick={this.submitActive(true)}>
                                <i className="fa fa-thumbs-up"/>&nbsp;{translator('activate')}
                            </button>
                            : null}

                        {model.isActive
                            ? <button className="btn btn-sm btn-outline-danger mr-1 text-nowrap transition-3d-hover"
                                      type="button"
                                      onClick={this.submitActive(false)}>
                                <i className="fa fa-thumbs-down"/>&nbsp;{translator('deactivate')}
                            </button>
                            : null}
                    </div>
                </div>
            </div>

            <div className="card-body">

                {serverErrors.length > 0 && <div className="alert alert-danger">
                    <ul className="simple">{serverErrors.map((e, i) => <li key={i}>{e}</li>)}</ul>
                </div>}

                <div className="row">
                    <div className="col-12">

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('name')}</label>
                                    <input type="text"
                                           name="name"
                                           autoFocus={true}
                                           className="form-control"
                                           onChange={this.changeString('name')}
                                           value={model.name || ''}/>
                                    {this.getError('name')}
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('surname')}</label>
                                    <input type="text"
                                           name="surname"
                                           className="form-control"
                                           onChange={this.changeString('surname')}
                                           value={model.surname || ''}/>
                                    {this.getError('surname')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('email')}</label>
                                    <input type="email"
                                           name="email"
                                           className="form-control"
                                           onChange={this.changeString('email')}
                                           value={model.email || ''}/>
                                    {this.getError('email')}
                                </div>
                            </div>
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('phone')}</label>
                                    <PhoneInput
                                        name="phone"
                                        onChange={this.changePhone}
                                        value={model.phone || ''}/>
                                    {this.getError('phone')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">

                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('country')}</label>
                                    <Select
                                        name="country"
                                        className="text-black"
                                        options={countryOptions}
                                        onChange={this.changeByKey('country')}
                                        value={model.country || ''}/>
                                    {this.getError('country')}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="form-group text-left">
                                    <label className="m-0 required">{translator('password')}</label>
                                    <input type="password"
                                           name="password"
                                           className="form-control"
                                           onChange={this.changeString('password')}
                                           value={model.password || ''}/>
                                    {this.getError('password')}
                                </div>
                            </div>

                            <div className="col-12 col-md-6">
                                <div className="form-group text-left">
                                    <label
                                        className="m-0 required">{translator('password_repeat')}</label>
                                    <input type="password"
                                           name="password2"
                                           className="form-control"
                                           onChange={this.changeString('password2')}
                                           value={model.password2 || ''}/>
                                    {this.getError('password2')}
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="col-12">

                        <div className="form-group text-center">
                            <button className="btn btn-primary transition-3d-hover"
                                    type="button"
                                    onClick={this.submit}
                                    disabled={isLoading || !isValid}>
                                <i className={isLoading ? "fa fa-spin fa-circle-notch" : "fa fa-check"}/>
                                &nbsp;{translator('save')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default connect(selectors)(ProfileUser)
