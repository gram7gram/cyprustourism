import {all, takeEvery} from 'redux-saga/effects'
import * as Actions from '../actions'
import toast from '../../Common/utils/Notification'
import translator from '../../translations/translator'

function* alertFailure({payload}) {
    toast(translator('save_failure_alert') + ": " + payload.data.message, true)
}

function* alertSuccess() {
    toast(translator('save_success_alert'))
}

export default function* sagas() {
    yield all([
        takeEvery(Actions.SAVE_FAILURE, alertFailure),

        takeEvery(Actions.SAVE_SUCCESS, alertSuccess),
    ])
}
