import {all, throttle, takeEvery} from 'redux-saga/effects'
import request from "../Common/request";
import * as CarRentActions from '../CarRent/actions'
import * as BoatRentActions from '../BoatRent/actions'
import * as AccommodationRentActions from '../AccommodationRent/actions'
import * as TransferActions from '../Transfer/actions'
import * as RegisterActions from "../RegisterUser/actions";
import * as LoginActions from "../Login/actions";

const setCreator = userId => {
    let inquiries = JSON.parse(localStorage.getItem('inquiry_without_user') || '[]')
    if (!Array.isArray(inquiries)) {
        inquiries = []
    }

    inquiries.forEach(inquiry => {

        const model = {
            guid: inquiry.guid,
            creator: userId
        }

        request.put(AppRouter.PUT.inquiryByGuid.replace('_GUID_', model.guid), model)
    })

    localStorage.removeItem('inquiry_without_user')
}

function* runOrderUpdate({payload}) {
    const userId = payload.id

    if (!userId) return;

    setCreator(userId)

}

function* runOrderUpdateAfterLogin({payload}) {
    const userId = payload.user.id

    if (!userId) return;

    setCreator(userId)

}

function* redirectIfNotLoggedIn({payload}) {
    if (!payload.creator) {
        let inquiries = JSON.parse(localStorage.getItem('inquiry_without_user') || '[]')
        if (!Array.isArray(inquiries)) {
            inquiries = []
        }

        inquiries.push({
            guid: payload.guid
        })

        localStorage.setItem('inquiry_without_user', JSON.stringify(inquiries))

        // window.location = AppRouter.GET.registerUserIndex + '?from=inquiry-creation'
    }
}

export default function* sagas() {
    yield all([
        takeEvery(RegisterActions.SAVE_SUCCESS, runOrderUpdate),

        takeEvery(LoginActions.LOGIN_SUCCESS, runOrderUpdateAfterLogin),

        throttle(1500, [
            CarRentActions.SAVE_SUCCESS,
            BoatRentActions.SAVE_SUCCESS,
            AccommodationRentActions.SAVE_SUCCESS,
            TransferActions.SAVE_SUCCESS,
        ], redirectIfNotLoggedIn)
    ])
}
