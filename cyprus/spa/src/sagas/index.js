import {all, fork} from 'redux-saga/effects'
import Login from '../Login/sagas'
import CarRent from '../CarRent/sagas'
import CarRentProvider from '../CarRentProvider/sagas'
import BoatRent from '../BoatRent/sagas'
import BoatRentProvider from '../BoatRentProvider/sagas'
import AccommodationRent from '../AccommodationRent/sagas'
import AccommodationRentProvider from '../AccommodationRentProvider/sagas'
import Transfer from '../Transfer/sagas'
import TransferProvider from '../TransferProvider/sagas'
import RegisterUser from '../RegisterUser/sagas'
import RegisterProvider from '../RegisterProvider/sagas'
import PasswordReset from '../PasswordReset/sagas'
import PasswordSet from '../PasswordSet/sagas'
import MyInquiries from '../MyInquiries/sagas'
import MyInquiriesProvider from '../MyInquiriesProvider/sagas'
import ProfileProvider from '../ProfileProvider/sagas'
import ProfileUser from '../ProfileUser/sagas'
import InquiryUser from '../InquiryUser/sagas'
import InquiryProvider from '../InquiryProvider/sagas'
import Review from '../Review/sagas'
import AdminProviders from '../AdminProviders/sagas'
import AdminUsers from '../AdminUsers/sagas'
import AdminReviews from '../AdminReviews/sagas'
import AdminProfileProvider from '../AdminProfileProvider/sagas'
import AdminProfileUser from '../AdminProfileUser/sagas'
import AdminInquiries from '../AdminInquiries/sagas'
import AdminCategories from '../AdminCategories/sagas'
import AdminCategory from '../AdminCategory/sagas'
import ProviderBilling from '../ProviderBilling/sagas'
import AdminInquiryCost from '../AdminInquiryCost/sagas'
import AdminInvoices from '../AdminInvoices/sagas'

import Inquiry from './Inquiry'

export default function* sagas() {
    yield all([
        fork(AdminInvoices),
        fork(AdminInquiryCost),
        fork(Inquiry),
        fork(AdminCategory),
        fork(AdminCategories),
        fork(AdminInquiries),
        fork(AdminProviders),
        fork(AdminUsers),
        fork(AdminReviews),
        fork(AdminProfileUser),
        fork(AdminProfileProvider),
        fork(Login),
        fork(CarRent),
        fork(CarRentProvider),
        fork(BoatRent),
        fork(BoatRentProvider),
        fork(AccommodationRent),
        fork(AccommodationRentProvider),
        fork(Transfer),
        fork(TransferProvider),
        fork(RegisterUser),
        fork(RegisterProvider),
        fork(PasswordSet),
        fork(PasswordReset),
        fork(MyInquiries),
        fork(MyInquiriesProvider),
        fork(ProfileProvider),
        fork(ProfileUser),
        fork(InquiryUser),
        fork(InquiryProvider),
        fork(Review),
        fork(ProviderBilling),
    ])
}
