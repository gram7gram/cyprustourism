const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
    entry: {
        LandingApp: ['babel-polyfill', './src/Landing'],
        CarRentApp: ['babel-polyfill', './src/CarRent'],
        BoatRentApp: ['babel-polyfill', './src/BoatRent'],
        AccommodationRentApp: ['babel-polyfill', './src/AccommodationRent'],
        TransferApp: ['babel-polyfill', './src/Transfer'],
        CarRentProviderApp: ['babel-polyfill', './src/CarRentProvider'],
        BoatRentProviderApp: ['babel-polyfill', './src/BoatRentProvider'],
        AccommodationRentProviderApp: ['babel-polyfill', './src/AccommodationRentProvider'],
        TransferProviderApp: ['babel-polyfill', './src/TransferProvider'],
        LoginApp: ['babel-polyfill', './src/Login'],
        RegisterUserApp: ['babel-polyfill', './src/RegisterUser'],
        RegisterProviderApp: ['babel-polyfill', './src/RegisterProvider'],
        PasswordSetApp: ['babel-polyfill', './src/PasswordSet'],
        PasswordResetApp: ['babel-polyfill', './src/PasswordReset'],
        MyInquiriesApp: ['babel-polyfill', './src/MyInquiries'],
        MyInquiriesProviderApp: ['babel-polyfill', './src/MyInquiriesProvider'],
        InquiryUserApp: ['babel-polyfill', './src/InquiryUser'],
        InquiryProviderApp: ['babel-polyfill', './src/InquiryProvider'],
        ProfileProviderApp: ['babel-polyfill', './src/ProfileProvider'],
        ProfileUserApp: ['babel-polyfill', './src/ProfileUser'],
        ReviewApp: ['babel-polyfill', './src/Review'],
        AdminReviewsApp: ['babel-polyfill', './src/AdminReviews'],
        AdminUsersApp: ['babel-polyfill', './src/AdminUsers'],
        AdminProvidersApp: ['babel-polyfill', './src/AdminProviders'],
        AdminProfileProviderApp: ['babel-polyfill', './src/AdminProfileProvider'],
        AdminProfileUserApp: ['babel-polyfill', './src/AdminProfileUser'],
        AdminInquiriesApp: ['babel-polyfill', './src/AdminInquiries'],
        AdminCategoriesApp: ['babel-polyfill', './src/AdminCategories'],
        AdminCategoryApp: ['babel-polyfill', './src/AdminCategory'],
        ProviderBillingApp: ['babel-polyfill', './src/ProviderBilling'],
        AdminInquiryCostApp: ['babel-polyfill', './src/AdminInquiryCost'],
        AdminInvoicesApp: ['babel-polyfill', './src/AdminInvoices'],
    },
    output: {
        path: path.resolve(__dirname + '/../public/js/dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['babel-preset-react-app'],
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            },
            {
                test: /.png$/,
                loader: 'url-loader?limit=10000&mimetype=image/png'
            }
        ]
    },
    devtool: 'source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        })
    ]
};
